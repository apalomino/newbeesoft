--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2016-11-08 08:46:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2317 (class 0 OID 16600)
-- Dependencies: 212
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO departamento VALUES (33, 'Otro departamentos', 2);
INSERT INTO departamento VALUES (1, 'AMAZONAS', 1);
INSERT INTO departamento VALUES (2, 'ANTIOQUIA', 1);
INSERT INTO departamento VALUES (3, 'ARAUCA', 1);
INSERT INTO departamento VALUES (4, 'ATLANTICO', 1);
INSERT INTO departamento VALUES (5, 'BOLIVAR', 1);
INSERT INTO departamento VALUES (6, 'BOYACÁ', 1);
INSERT INTO departamento VALUES (7, 'CALDAS', 1);
INSERT INTO departamento VALUES (8, 'CAQUETA', 1);
INSERT INTO departamento VALUES (9, 'CASANARE', 1);
INSERT INTO departamento VALUES (10, 'CAUCA', 1);
INSERT INTO departamento VALUES (11, 'CESAR', 1);
INSERT INTO departamento VALUES (12, 'CHOCO', 1);
INSERT INTO departamento VALUES (14, 'CUNDINAMARCA', 1);
INSERT INTO departamento VALUES (15, 'GUAINÍA', 1);
INSERT INTO departamento VALUES (18, 'GUAJIRA', 1);
INSERT INTO departamento VALUES (16, 'GUAVIARE', 1);
INSERT INTO departamento VALUES (13, 'CORDOBA', 1);
INSERT INTO departamento VALUES (17, 'HUILA', 1);
INSERT INTO departamento VALUES (19, 'MAGDALENA', 1);
INSERT INTO departamento VALUES (20, 'META', 1);
INSERT INTO departamento VALUES (21, 'NARIÑO', 1);
INSERT INTO departamento VALUES (22, 'NORTE DE SANTANDER', 1);
INSERT INTO departamento VALUES (23, 'PUTUMAYO', 1);
INSERT INTO departamento VALUES (24, 'QUINDÍO', 1);
INSERT INTO departamento VALUES (25, 'RISARALDA', 1);
INSERT INTO departamento VALUES (26, 'SAN ANDRES', 1);
INSERT INTO departamento VALUES (27, 'SANTANDER', 1);
INSERT INTO departamento VALUES (28, 'SUCRE', 1);
INSERT INTO departamento VALUES (29, 'TOLIMA', 1);
INSERT INTO departamento VALUES (30, 'VALLE', 1);
INSERT INTO departamento VALUES (31, 'VAUPES', 1);
INSERT INTO departamento VALUES (32, 'VICHADA', 1);
INSERT INTO departamento VALUES (34, 'CAPITAL', 3);
INSERT INTO departamento VALUES (35, 'CAPITAL', 4);
INSERT INTO departamento VALUES (36, 'CAPITAL', 5);
INSERT INTO departamento VALUES (37, 'CAPITAL', 6);
INSERT INTO departamento VALUES (38, 'CAPITAL', 7);
INSERT INTO departamento VALUES (39, 'CAPITAL', 8);
INSERT INTO departamento VALUES (40, 'CAPITAL', 9);
INSERT INTO departamento VALUES (41, 'CAPITAL', 10);
INSERT INTO departamento VALUES (42, 'CAPITAL', 11);
INSERT INTO departamento VALUES (43, 'CAPITAL', 12);
INSERT INTO departamento VALUES (44, 'CAPITAL', 13);
INSERT INTO departamento VALUES (45, 'CAPITAL', 14);
INSERT INTO departamento VALUES (46, 'CAPITAL', 15);
INSERT INTO departamento VALUES (47, 'CAPITAL', 16);
INSERT INTO departamento VALUES (48, 'CAPITAL', 17);
INSERT INTO departamento VALUES (49, 'CAPITAL', 18);
INSERT INTO departamento VALUES (50, 'CAPITAL', 19);
INSERT INTO departamento VALUES (51, 'CAPITAL', 20);
INSERT INTO departamento VALUES (52, 'CAPITAL', 21);
INSERT INTO departamento VALUES (53, 'CAPITAL', 22);
INSERT INTO departamento VALUES (54, 'CAPITAL', 23);
INSERT INTO departamento VALUES (55, 'CAPITAL', 24);
INSERT INTO departamento VALUES (56, 'CAPITAL', 25);
INSERT INTO departamento VALUES (57, 'CAPITAL', 26);
INSERT INTO departamento VALUES (58, 'CAPITAL', 27);
INSERT INTO departamento VALUES (59, 'CAPITAL', 28);
INSERT INTO departamento VALUES (60, 'CAPITAL', 29);
INSERT INTO departamento VALUES (61, 'CAPITAL', 30);
INSERT INTO departamento VALUES (62, 'CAPITAL', 31);
INSERT INTO departamento VALUES (63, 'CAPITAL', 32);
INSERT INTO departamento VALUES (64, 'CAPITAL', 33);
INSERT INTO departamento VALUES (65, 'CAPITAL', 34);
INSERT INTO departamento VALUES (66, 'CAPITAL', 35);
INSERT INTO departamento VALUES (67, 'CAPITAL', 36);
INSERT INTO departamento VALUES (68, 'CAPITAL', 37);
INSERT INTO departamento VALUES (69, 'CAPITAL', 38);
INSERT INTO departamento VALUES (70, 'CAPITAL', 39);
INSERT INTO departamento VALUES (71, 'CAPITAL', 40);
INSERT INTO departamento VALUES (72, 'CAPITAL', 41);
INSERT INTO departamento VALUES (73, 'CAPITAL', 42);
INSERT INTO departamento VALUES (74, 'CAPITAL', 43);
INSERT INTO departamento VALUES (75, 'CAPITAL', 44);
INSERT INTO departamento VALUES (76, 'CAPITAL', 45);
INSERT INTO departamento VALUES (77, 'CAPITAL', 46);
INSERT INTO departamento VALUES (78, 'CAPITAL', 47);
INSERT INTO departamento VALUES (79, 'CAPITAL', 48);
INSERT INTO departamento VALUES (80, 'CAPITAL', 49);
INSERT INTO departamento VALUES (81, 'CAPITAL', 50);
INSERT INTO departamento VALUES (82, 'CAPITAL', 51);
INSERT INTO departamento VALUES (83, 'CAPITAL', 52);
INSERT INTO departamento VALUES (84, 'CAPITAL', 53);
INSERT INTO departamento VALUES (85, 'CAPITAL', 54);
INSERT INTO departamento VALUES (86, 'CAPITAL', 55);
INSERT INTO departamento VALUES (87, 'CAPITAL', 56);
INSERT INTO departamento VALUES (88, 'CAPITAL', 57);
INSERT INTO departamento VALUES (89, 'CAPITAL', 58);
INSERT INTO departamento VALUES (90, 'CAPITAL', 59);
INSERT INTO departamento VALUES (91, 'CAPITAL', 60);
INSERT INTO departamento VALUES (92, 'CAPITAL', 61);
INSERT INTO departamento VALUES (93, 'CAPITAL', 62);
INSERT INTO departamento VALUES (94, 'CAPITAL', 63);
INSERT INTO departamento VALUES (95, 'CAPITAL', 64);
INSERT INTO departamento VALUES (96, 'CAPITAL', 65);
INSERT INTO departamento VALUES (97, 'CAPITAL', 66);
INSERT INTO departamento VALUES (98, 'CAPITAL', 67);
INSERT INTO departamento VALUES (99, 'CAPITAL', 68);
INSERT INTO departamento VALUES (100, 'CAPITAL', 69);
INSERT INTO departamento VALUES (101, 'CAPITAL', 70);
INSERT INTO departamento VALUES (102, 'CAPITAL', 71);
INSERT INTO departamento VALUES (103, 'CAPITAL', 72);
INSERT INTO departamento VALUES (104, 'CAPITAL', 73);
INSERT INTO departamento VALUES (105, 'CAPITAL', 74);
INSERT INTO departamento VALUES (106, 'CAPITAL', 75);
INSERT INTO departamento VALUES (107, 'CAPITAL', 76);
INSERT INTO departamento VALUES (108, 'CAPITAL', 77);
INSERT INTO departamento VALUES (109, 'CAPITAL', 78);
INSERT INTO departamento VALUES (110, 'CAPITAL', 79);
INSERT INTO departamento VALUES (111, 'CAPITAL', 80);
INSERT INTO departamento VALUES (112, 'CAPITAL', 81);
INSERT INTO departamento VALUES (113, 'CAPITAL', 82);
INSERT INTO departamento VALUES (114, 'CAPITAL', 83);
INSERT INTO departamento VALUES (115, 'CAPITAL', 84);
INSERT INTO departamento VALUES (116, 'CAPITAL', 85);
INSERT INTO departamento VALUES (117, 'CAPITAL', 86);
INSERT INTO departamento VALUES (118, 'CAPITAL', 87);
INSERT INTO departamento VALUES (119, 'CAPITAL', 88);
INSERT INTO departamento VALUES (120, 'CAPITAL', 89);
INSERT INTO departamento VALUES (121, 'CAPITAL', 90);
INSERT INTO departamento VALUES (122, 'CAPITAL', 91);
INSERT INTO departamento VALUES (123, 'CAPITAL', 92);
INSERT INTO departamento VALUES (124, 'CAPITAL', 93);
INSERT INTO departamento VALUES (125, 'CAPITAL', 94);
INSERT INTO departamento VALUES (126, 'CAPITAL', 95);
INSERT INTO departamento VALUES (127, 'CAPITAL', 96);
INSERT INTO departamento VALUES (128, 'CAPITAL', 97);
INSERT INTO departamento VALUES (129, 'CAPITAL', 98);
INSERT INTO departamento VALUES (130, 'CAPITAL', 99);
INSERT INTO departamento VALUES (131, 'CAPITAL', 100);
INSERT INTO departamento VALUES (132, 'CAPITAL', 101);
INSERT INTO departamento VALUES (133, 'CAPITAL', 102);
INSERT INTO departamento VALUES (134, 'CAPITAL', 103);
INSERT INTO departamento VALUES (135, 'CAPITAL', 104);
INSERT INTO departamento VALUES (136, 'CAPITAL', 105);
INSERT INTO departamento VALUES (137, 'CAPITAL', 106);
INSERT INTO departamento VALUES (138, 'CAPITAL', 107);
INSERT INTO departamento VALUES (139, 'CAPITAL', 108);
INSERT INTO departamento VALUES (140, 'CAPITAL', 109);
INSERT INTO departamento VALUES (141, 'CAPITAL', 110);
INSERT INTO departamento VALUES (142, 'CAPITAL', 111);
INSERT INTO departamento VALUES (143, 'CAPITAL', 112);
INSERT INTO departamento VALUES (144, 'FLORIDA', 112);
INSERT INTO departamento VALUES (145, 'CHUNG TEO TAINAN', 94);
INSERT INTO departamento VALUES (146, 'CALIFORNIA', 112);


-- Completed on 2016-11-08 08:46:44

--
-- PostgreSQL database dump complete
--

