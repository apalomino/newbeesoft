--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2016-11-09 16:27:47

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2316 (class 0 OID 16734)
-- Dependencies: 245
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil VALUES (2, 'RADICADOR', true);
INSERT INTO perfil VALUES (3, 'GESTOR', true);
INSERT INTO perfil VALUES (18, 'JEFE', true);
INSERT INTO perfil VALUES (19, 'ACTOR', true);
INSERT INTO perfil VALUES (516, 'EDITOR', true);
INSERT INTO perfil VALUES (1, 'ADMINISTRADOR', true);
INSERT INTO perfil VALUES (518, 'JEFE HL', true);
INSERT INTO perfil VALUES (717551, 'JEFE ENCARGADO', false);
INSERT INTO perfil VALUES (717555, 'GESTOR VB', true);
INSERT INTO perfil VALUES (5, 'INTERVENTOR', true);


-- Completed on 2016-11-09 16:27:47

--
-- PostgreSQL database dump complete
--

