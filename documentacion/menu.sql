--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2016-11-09 15:58:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2317 (class 0 OID 16704)
-- Dependencies: 239
-- Data for Name: menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO menu VALUES (25, '/html/correspondencia/Procesar.xhtml', 'Procesar', NULL, NULL, true, 24, 44);
INSERT INTO menu VALUES (50, '/html/correspondencia/BuscarUsuarioEdeq.xhtml', 'Buscar Usuarios Edeq', NULL, NULL, true, 24, 56);
INSERT INTO menu VALUES (42, '/html/externo/BuscarExterno.xhtml', 'Buscar', NULL, NULL, true, 16, 27);
INSERT INTO menu VALUES (44, NULL, 'Prestamo Documentos', true, NULL, NULL, NULL, 57);
INSERT INTO menu VALUES (21, '/html/reportes/DocumentoEliminado.xhtml', 'Documentos Eliminados', NULL, NULL, true, 19, 35);
INSERT INTO menu VALUES (17, '/html/mail/Buscar.xhtml', 'Buscar', NULL, NULL, true, 14, 23);
INSERT INTO menu VALUES (15, '/html/permiso/Permiso.xhtml', 'Asignar Permisos', NULL, NULL, true, 7, 10);
INSERT INTO menu VALUES (43, '/html/reportes/BuscarDocumentoMercurio.xhtml', 'Reporte Facturas', NULL, NULL, true, 19, 36);
INSERT INTO menu VALUES (57, '/html/reportes/ReporteCodigoNiu.xhtml', 'Reporte codigo Niu', NULL, NULL, true, 19, 37);
INSERT INTO menu VALUES (36, '/html/reportes/ReporteEmpresaCorreo.xhtml', 'Empresas Correo', NULL, NULL, true, 19, 38);
INSERT INTO menu VALUES (26, '/html/correspondencia/BuscarActas.xhtml', 'Actas', NULL, NULL, true, 24, 45);
INSERT INTO menu VALUES (37, '/html/reportes/ReporteContratacion.xhtml', 'Contratación', NULL, NULL, true, 19, 39);
INSERT INTO menu VALUES (33, '/html/correspondencia/BuscarOficio.xhtml', 'Buscar circulares-memorandos-manuales', NULL, NULL, true, 24, 52);
INSERT INTO menu VALUES (29, '/html/correspondencia/BuscarOdc.xhtml', 'Ordenes de Compra', NULL, NULL, true, 24, 48);
INSERT INTO menu VALUES (10, NULL, 'Documento', NULL, true, NULL, 9, 16);
INSERT INTO menu VALUES (46, '/html/prestamo/Prestamo.xhtml', 'Crear', NULL, NULL, true, 45, 59);
INSERT INTO menu VALUES (47, '/html/prestamo/BuscarPrestamo.xhtml', 'Buscar', NULL, NULL, true, 45, 60);
INSERT INTO menu VALUES (11, '/html/documento/Documento.xhtml', 'Crear', NULL, NULL, true, 10, 17);
INSERT INTO menu VALUES (12, '/html/documento/BuscarDocumento.xhtml', 'Buscar', NULL, NULL, true, 10, 18);
INSERT INTO menu VALUES (22, '/html/reportes/AuditoriaDocumento.xhtml', 'Modificaciones Mensuales', NULL, NULL, true, 19, 40);
INSERT INTO menu VALUES (31, '/html/correspondencia/BuscarRadicadores.xhtml', 'Radicadores', NULL, NULL, true, 24, 50);
INSERT INTO menu VALUES (28, '/html/correspondencia/BuscarInstructivos.xhtml', 'Instructivos', NULL, NULL, true, 24, 47);
INSERT INTO menu VALUES (27, '/html/correspondencia/BuscarEscrituras.xhtml', 'Escrituras', NULL, NULL, true, 24, 46);
INSERT INTO menu VALUES (45, NULL, 'Gestion', NULL, true, NULL, 44, 58);
INSERT INTO menu VALUES (18, NULL, 'Reportes', true, NULL, NULL, NULL, 32);
INSERT INTO menu VALUES (13, NULL, 'Comunicados', true, NULL, NULL, NULL, 20);
INSERT INTO menu VALUES (4, NULL, 'Personal', NULL, true, NULL, 1, 5);
INSERT INTO menu VALUES (5, '/html/actor/Actor.xhtml', 'Crear', NULL, NULL, true, 4, 6);
INSERT INTO menu VALUES (6, '/html/actor/BuscarActor.xhtml', 'Gestionar', NULL, NULL, true, 4, 7);
INSERT INTO menu VALUES (7, NULL, 'Perfil', NULL, true, NULL, 1, 8);
INSERT INTO menu VALUES (8, '/html/perfil/Perfil.xhtml', 'Crear', NULL, NULL, true, 7, 9);
INSERT INTO menu VALUES (19, NULL, 'Generar', NULL, true, NULL, 18, 33);
INSERT INTO menu VALUES (35, '/html/externo/InboxComunicados.xhtml', 'Bandeja', NULL, NULL, true, 16, 26);
INSERT INTO menu VALUES (58, '/html/externo/ReporteComunicadosExternos.xhtml', 'Reporte', NULL, NULL, true, 16, 28);
INSERT INTO menu VALUES (54, NULL, 'Comunicados PQR', false, true, NULL, 13, 29);
INSERT INTO menu VALUES (51, '/html/mail/ReporteComunicadosInternos.xhtml', 'Reporte', NULL, NULL, true, 14, 24);
INSERT INTO menu VALUES (38, '/html/usuario/BuscarUsuario.xhtml', 'Gestionar Usuarios', NULL, NULL, true, 2, 4);
INSERT INTO menu VALUES (39, NULL, 'Oficina', NULL, true, NULL, 1, 11);
INSERT INTO menu VALUES (55, '/html/pqr/InboxComunicados.xhtml', 'Inbox', NULL, NULL, true, 54, 30);
INSERT INTO menu VALUES (40, '/html/oficina/Oficina.xhtml', 'Gestionar', NULL, NULL, true, 39, 12);
INSERT INTO menu VALUES (56, '/html/pqr/Buscar.xhtml', 'Buscar', NULL, NULL, true, 54, 31);
INSERT INTO menu VALUES (9, NULL, 'Ventanilla de Correspondencia', true, NULL, NULL, NULL, 15);
INSERT INTO menu VALUES (49, '/html/mail/InboxComunicados.xhtml', 'Bandeja', NULL, NULL, true, 14, 22);
INSERT INTO menu VALUES (13785995, '/html/reportes/Radicados.xhtml', 'Radicados', NULL, NULL, true, 19, 41);
INSERT INTO menu VALUES (30, '/html/correspondencia/BuscarArchivoUsuario.xhtml', 'Codigo Niu', NULL, NULL, true, 24, 49);
INSERT INTO menu VALUES (32, '/html/correspondencia/Oficio.xhtml', 'Crear circulares-memorandos-manuales', NULL, NULL, true, 24, 51);
INSERT INTO menu VALUES (20, '/html/reportes/ReporteRecorrido.xhtml', 'Recorridos Documentos', NULL, NULL, true, 19, 34);
INSERT INTO menu VALUES (34, '/html/correspondencia/BuscarContratos.xhtml', 'Contratos', NULL, NULL, true, 24, 53);
INSERT INTO menu VALUES (1, NULL, 'Administracion', true, NULL, NULL, NULL, 1);
INSERT INTO menu VALUES (2, NULL, 'Usuario', false, true, NULL, 1, 2);
INSERT INTO menu VALUES (3, '/html/usuario/Usuario.xhtml', 'Crear', NULL, NULL, true, 2, 3);
INSERT INTO menu VALUES (52, NULL, 'Tipo Documento', false, true, NULL, 1, 13);
INSERT INTO menu VALUES (53, '/html/mail/CrearTipoComunicado.xhtml', 'Gestionar', NULL, NULL, true, 52, 14);
INSERT INTO menu VALUES (41, '/html/correspondencia/BuscarHistoriasLaborales.xhtml', 'Historias Laborales', NULL, NULL, true, 24, 54);
INSERT INTO menu VALUES (48, '/html/correspondencia/BuscarAEF.xhtml', 'Buscar Aceptacion de Oferta', NULL, NULL, true, 24, 55);
INSERT INTO menu VALUES (11128174, '/html/repositorio/Repositorio.xhtml', 'Repositorio', NULL, NULL, true, 10, 19);
INSERT INTO menu VALUES (23, NULL, 'Gestión de la Información', true, NULL, NULL, NULL, 42);
INSERT INTO menu VALUES (16, NULL, 'Comunicados Externos', NULL, true, NULL, 13, 25);
INSERT INTO menu VALUES (24, NULL, 'Correspondencia', NULL, true, NULL, 23, 43);
INSERT INTO menu VALUES (14, NULL, 'Comunicados Internos', NULL, true, false, 13, 21);


-- Completed on 2016-11-09 15:58:33

--
-- PostgreSQL database dump complete
--

