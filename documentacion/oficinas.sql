--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2016-11-09 16:11:07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2320 (class 0 OID 16728)
-- Dependencies: 243
-- Data for Name: oficina; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO oficina VALUES (484, true, 'SIN AREA1', 1);
INSERT INTO oficina VALUES (486, false, 'CON AREA1', 1);
INSERT INTO oficina VALUES (483, false, 'GERENCIA GENERAL23', 1);
INSERT INTO oficina VALUES (487, false, 'OFICINA TALES DE PASCUALES', 1);
INSERT INTO oficina VALUES (496, true, 'NUEVA OFICINA', 1);
INSERT INTO oficina VALUES (1, true, 'GERENCIA SEGUNDARIA', 1);


--
-- TOC entry 2321 (class 0 OID 16815)
-- Dependencies: 265
-- Data for Name: tiporecorrido; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tiporecorrido VALUES (1, 'RECORRIDO 1');
INSERT INTO tiporecorrido VALUES (2, 'RECORRIDO 2');


-- Completed on 2016-11-09 16:11:07

--
-- PostgreSQL database dump complete
--

