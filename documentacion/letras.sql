--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2016-11-09 16:02:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2318 (class 0 OID 16698)
-- Dependencies: 237
-- Data for Name: letraradicacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO letraradicacion VALUES (8, 'FA', NULL, NULL);
INSERT INTO letraradicacion VALUES (7, 'MA', 3, NULL);
INSERT INTO letraradicacion VALUES (5, 'M', 15, NULL);
INSERT INTO letraradicacion VALUES (717567, 'X', 11, NULL);
INSERT INTO letraradicacion VALUES (6, 'C', 178, NULL);
INSERT INTO letraradicacion VALUES (4, 'P', 26, NULL);
INSERT INTO letraradicacion VALUES (7456145, 'CE', 3, NULL);
INSERT INTO letraradicacion VALUES (7456202, 'CEP', 7, NULL);
INSERT INTO letraradicacion VALUES (9356709, 'AC', 4, NULL);
INSERT INTO letraradicacion VALUES (9374377, 'R', 81, NULL);
INSERT INTO letraradicacion VALUES (9374418, 'E', 231, NULL);


-- Completed on 2016-11-09 16:02:20

--
-- PostgreSQL database dump complete
--

