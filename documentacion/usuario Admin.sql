--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2017-04-20 15:23:51

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2314 (class 0 OID 131297)
-- Dependencies: 277
-- Data for Name: tipopersona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipopersona (id, descripcion) VALUES (1, 'JEFE');
INSERT INTO tipopersona (id, descripcion) VALUES (2, 'EMPLEADO');
INSERT INTO tipopersona (id, descripcion) VALUES (3, 'PERSONA');
INSERT INTO tipopersona (id, descripcion) VALUES (4, 'EMPRESA');


INSERT INTO actor (id, cargo, cedula, direccion1, email, estado, fechanacimiento, nombreyapellido, ciudad_id, jefe_id, oficina_id, tipopersona_id, telefono, direccion2, direccion, firmadigital, empresa_id, telefonoext, firma) VALUES (1, '', '', '', '', true, NULL, 'BEESOFT', 1, NULL, 1, 2, '', '', '', NULL, NULL, '', NULL);

INSERT INTO perfil (id, descripcion, estado) VALUES (1, 'ADMINISTRADOR', true);
INSERT INTO perfil (id, descripcion, estado) VALUES (2, 'RADICADOR', true);
INSERT INTO perfil (id, descripcion, estado) VALUES (3, 'GESTOR', true);
INSERT INTO perfil (id, descripcion, estado) VALUES (4, 'JEFE', true);

INSERT INTO usuario (id, estado, password, username, actor_id, perfil_id) VALUES (1, true, 'Maf0fx1woF9vSi69koO6QZHoML8Zyve0', 'beesoft', 1, 1);

-- Completed on 2017-04-20 15:23:52

--
-- PostgreSQL database dump complete
--

