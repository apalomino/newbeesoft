/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.servlets;

import com.beesoft.beesoft.gestiondocumental.Util.ArchivoDocumentoDTO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

/**
 *
 * @author Cristhian
 */
public class ArchivoOldServidor extends HttpServlet {

    private static final Log LOG = LogFactory.getLog(ArchivoOldServidor.class);
    
    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idArchivo = request.getParameter("idArchivo");
        Connection conn = null;
        try {
            conn = getConnection();
        } catch (Exception ex) {
            LOG.info(ex);
        }
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArchivoDocumentoDTO archivo = null;
        try {
            conn.setAutoCommit(false);

            pstmt = conn.prepareStatement("SELECT archivo,nombrearchivo FROM archivodocumento WHERE id_archivo=?");
            pstmt.setString(1, idArchivo);
            rs = pstmt.executeQuery();
            LargeObjectManager lobj = ((org.postgresql.PGConnection) rs.getStatement().getConnection()).getLargeObjectAPI();
            if (rs != null) {
                while (rs.next()) {
                    long oid = rs.getLong(1);
                    String nombre = rs.getString(2);
                    LargeObject obj = lobj.open(oid, LargeObjectManager.READ);
                    byte buf[] = new byte[obj.size()];
                    obj.read(buf, 0, obj.size());
                    obj.close();
                    archivo = new ArchivoDocumentoDTO(idArchivo, nombre, buf);
                }
                rs.close();
            } else {
                System.out.println("El archivo no pudo ser descargado del servidor...");
            }
            pstmt.close();
            conn.setAutoCommit(true);
        } catch (SQLException sqle) {
            LOG.info(sqle);
        } catch (Exception e) {
            LOG.info(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn.getAutoCommit() == false) {
                    conn.setAutoCommit(true);
                }
            } catch (SQLException se) {
                LOG.info(se);
            }
        }

        ServletOutputStream out = response.getOutputStream();
        try {
            System.out.print("revisando extensiones");
            if (archivo != null) {
                int dotPosition = (archivo.getNombreArchivo()).lastIndexOf('.');
                String extension[] = {"tif", "tiff", "txt", "htm", "jpg", "gif", "pdf", "doc", "ppt", "xls", "docx", "pptx", "xlsx"};
                String mimeType[] = {"image/tiff", "image/tiff", "text/plain", "text/html", "image/jpg", "image/gif", "application/pdf", "application/msword", "application/vnd.ms-powerpoint", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"};
                String fileExtension = (archivo.getNombreArchivo()).substring(dotPosition + 1);
                System.out.print("extension" + fileExtension);

                boolean encontro = false;
                for (int index = 0; index < mimeType.length; index++) {

                    if (fileExtension.equalsIgnoreCase(extension[index])) {
                        response.setContentType(mimeType[index]);
                        String disposition = "attachment; filename=\"" + archivo.getNombreArchivo() + "\"";
                        response.setHeader("Content-disposition", disposition);
                        index = mimeType.length;
                        encontro = true;
                    }
                }
                if (encontro == false) {
                    response.setContentType("text/plain");
                }
                System.out.print(response.toString());

                out.write((archivo.getArchivo()));
            }
        } finally {
            out.close();
        }
    }

    public Connection getConnection() throws Exception {
        String driver = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/crq";
        String username = "postgres";
        String password = "12345";
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
