/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.servlets;

import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristhian
 */
public class ArchivoReferenciaServidor extends HttpServlet {

    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    @EJB
    private DocumentoLocal documentoBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idArchivo = request.getParameter("idArchivo");

        List<ArchivoDocumento> listaArchivos = documentoBean.consultarArchivoDocumento(Long.valueOf(idArchivo));
        ArchivoDocumento archivoDocumento = null;

        for (ArchivoDocumento archivo : listaArchivos) {
            archivoDocumento = archivo;
        }

        ServletOutputStream out = response.getOutputStream();
        try {
            System.out.print("revisando extensiones");

            int dotPosition = (archivoDocumento.getNombreArchivo()).lastIndexOf('.');
            String extension[] = {"tif", "tiff", "txt", "htm", "jpg", "gif", "pdf", "doc", "ppt", "xls", "docx", "pptx", "xlsx"};
            String mimeType[] = {"image/tiff", "image/tiff", "text/plain", "text/html", "image/jpg", "image/gif", "application/pdf", "application/msword", "application/vnd.ms-powerpoint", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"};
            String fileExtension = (archivoDocumento.getNombreArchivo()).substring(dotPosition + 1);
            System.out.print("extension" + fileExtension);

            boolean encontro = false;
            for (int index = 0; index < mimeType.length; index++) {

                if (fileExtension.equalsIgnoreCase(extension[index])) {
                    response.setContentType(mimeType[index]);
                    String disposition = "attachment; filename=\"" + archivoDocumento.getNombreArchivo() + "\"";
                    response.setHeader("Content-disposition", disposition);
                    index = mimeType.length;
                    encontro = true;
                }
            }
            if (encontro == false) {
                response.setContentType("text/plain");
            }
            System.out.print(response.toString());
            out.write((archivoDocumento.getArchivo()));
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
