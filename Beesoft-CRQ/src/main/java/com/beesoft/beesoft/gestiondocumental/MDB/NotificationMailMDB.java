/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.MDB;

import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Util.NotificationMailDTO;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 *
 * @author Cristhian
 */
@MessageDriven(
        mappedName = "jms/NotificationQueue",
        activationConfig = {
            @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/NotificationQueue"),
            @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
            @ActivationConfigProperty(propertyName = "reconnectAttempts", propertyValue = "-1"),
            @ActivationConfigProperty(propertyName = "setupAttempts", propertyValue = "-1")})
public class NotificationMailMDB implements MessageListener {

    public NotificationMailMDB() {
		super();
	}

	@Resource
    private MessageDrivenContext mdctx;

    @EJB
    private NotificationMailLocal notification;

    @Override
    public void onMessage(javax.jms.Message message) {
        ObjectMessage objectMessage = null;
        objectMessage = (ObjectMessage) message;
        try {
            NotificationMailDTO mail = (NotificationMailDTO) objectMessage.getObject();
            notification.enviarCorreo(mail);
        } catch (JMSException ex) {
            Logger.getLogger(NotificationMailMDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
