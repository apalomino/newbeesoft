/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.Util.DestinoDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import com.beesoft.beesoft.gestiondocumental.entidades.ComunicadosInternos;
import com.beesoft.beesoft.gestiondocumental.entidades.DestinosComunicados;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface ComunicadosInternosLocal {

    /**
     * Metodo para encolar los correos.
     *
     * @param radicacion
     * @param responsable
     * @param destinatarios
     * @param revisor, persona que envia correo.
     * @param destinations, lista de destinatarios.
     * @param destinatariosCopia
     * @param asunto, asunto del correo.
     * @param mensaje, mensaje del correo.
     * @param adjuntosGuardar
     * @param aprobacion
     * @param estado
     * @return
     */
    public String guardarComunicadoInterno(Actor origen, Oficina origenOficina, Actor redactor, List<DestinoDTO> destinatarios,
            List<DestinoDTO> destinatariosCopia, String asunto, String mensaje, List<ArchivosAdjuntos> adjuntosGuardar,
            Boolean aprobacion, String estado);

    /**
     *
     * @param radicacion
     * @param emisor
     * @param destinatarios
     * @param asunto
     * @param mensaje
     * @param comentario
     * @param adjuntosGuardar
     * @param tipoComunicado
     */
    public void reenviarComunicadoInterno(Long idComunicado, Actor redactor, List<DestinoDTO> destinatarios, String comentario);

    /**
     * Metodo para buscar comunicados pendientes de aprobacion.
     *
     * @param jefe
     * @return lista de pendientes.
     */
    public List<ComunicadosInternos> buscarComunicadosPendientesAprobacion(Actor actor, String inActor, String estado);

    /**
     * Metodo para cargar la lista de posibles de destinatarios.
     *
     * @return lista de destinatarios.
     */
    public List<Actor> cargarListaDestinatarios();

    /**
     * Metodo para buscar correos segun destinos y el asunto.
     *
     * @param radicacion
     * @param actores, lista de destinos.
     * @param asunto, asunto del correo.
     * @param fechaIni, rango de fecha inicial.
     * @param fechaFin, rango de fecha final.
     * @return lista de correos relacionada.
     */
    public List<ComunicadosInternos> buscarCorreo(String radicacion, List<DestinoDTO> destinatarios, String asunto, Date fechaIni, Date fechaFin);

    /**
     * Método para buscar correos segun destinos y el asunto para repositorio
     *
     * @param radicacion
     * @param procedencia
     * @param destinatarios
     * @param asunto
     * @param fechaIni
     * @param fechaFin
     * @return lista de correos externos que cumplen el criterio
     */
    public List<ComunicadosInternos> buscarCorreoRepositorio(String radicacion, Actor procedencia, Actor destinatarios, String asunto, Date fechaIni, Date fechaFin);

    /**
     *
     * @param fechaIni
     * @param fechaFin
     * @return
     */
    public List<ComunicadosInternos> buscarTodosComunicadosInternos(Date fechaIni, Date fechaFin, String estado);

    /**
     * Metodo para buscar un correo por identificador.
     *
     * @param id, identificador del correo.
     * @return correo.
     */
    public ComunicadosInternos buscarCorreoId(Long id);

    /**
     * Metodo para buscar los concecutivos de radicacion para correo.
     *
     * @return lista de concecutivos.
     * @throws Exception si hay algun error.
     */
    public List<String> consecutivoRadicacionCorreo() throws Exception;

    public List<ComunicadosInternos> buscarRenviados(String radicacion) throws Exception;

    public String buscarConsecutivoCorreo(String radicacion);

    public String consecutivoRadicacionCorreoXLetra(String letra) throws Exception;

    public void generarRadicado(String cadena);

    public List<DestinosComunicados> consultarDestinosComunicados(Long idCI);

    public List<ArchivosAdjuntos> consultarArchivosComunicados(Long idCI);

    public List<ComunicadosInternos> buscarComunicadosInOut(Actor actor, String mensaje, Boolean inOut) throws Exception;

    public List<Actor> cargarListaJefesDestinatarios();

    public List<DestinosComunicados> consultarDestinosCopiaComunicados(Long idCI);

    public void enviarComunicadoAprobado(Long idComunicado, List<DestinoDTO> destinatarios, List<DestinoDTO> destinatariosCopia,
            String asunto, String mensaje, List<ArchivosAdjuntos> adjuntosGuardar, List<ArchivosAdjuntos> adjuntosEliminados);

    public void modificarComunicadoInternoPorAprobar(Long idCi, String comentarios);

    public void anularComunicadoInternoPorAprobar(Long idCi, String comentarios);

    public void modificarEnviarComunicadoAprobado(Long idComunicado, List<DestinoDTO> destinatarios,
            List<DestinoDTO> destinatariosCopia, String asunto, String mensaje,
            List<ArchivosAdjuntos> adjuntosGuardar, List<ArchivosAdjuntos> adjuntosEliminados);

    public List<ComunicadosInternos> buscarComunicadosPendientesAprobacionAdmin(String estado);

    public void extraccionImagenes(Date fechaInicial, Date fechaFinal, String ruta);

    public void generarInfoComunicados(Boolean interno);

    public Oficina mostrarOficina(Actor jefe);

    public void marcarComoLeido(ComunicadosInternos comunicado, Actor usuarioLogin);
}
