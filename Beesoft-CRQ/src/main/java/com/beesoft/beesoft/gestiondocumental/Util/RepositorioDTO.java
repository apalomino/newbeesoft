/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import java.util.Date;
import java.util.List;
import javax.persistence.Temporal;

/**
 *
 * @author luis
 */
public class RepositorioDTO {

    private Long id;
    private String radicacion;
    private Date fechaEmision;
    private Date fechaRecepcion;
    private String asunto;
    private int tipo;
    private List<Actor> emisor;
    private List<Actor> destinatarios;
    private Date fechaSistema;

    public RepositorioDTO() {
    }

    public RepositorioDTO(Long id, String radicacion, Date fechaEmision, Date fechaRecepcion, String asunto, Byte tipo, List<Actor> emisor, List<Actor> destinatarios) {
        this.id = id;
        this.radicacion = radicacion;
        this.fechaEmision = fechaEmision;
        this.fechaRecepcion = fechaRecepcion;
        this.asunto = asunto;
        this.tipo = tipo;
        this.emisor = emisor;
        this.destinatarios = destinatarios;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public List<Actor> getEmisor() {
        return emisor;
    }

    public void setEmisor(List<Actor> emisor) {
        this.emisor = emisor;
    }

    public List<Actor> getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(List<Actor> destinatarios) {
        this.destinatarios = destinatarios;
    }

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaSistema() {
        return this.fechaSistema;
    }

    public void setFechaSistema(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

}
