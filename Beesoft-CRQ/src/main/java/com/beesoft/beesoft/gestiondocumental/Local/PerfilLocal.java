/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface PerfilLocal {

    /**
     * Metodo encargado de crear un perfil.
     *
     * @param descripcion, descripcion del perfil.
     * @throws Exception si hay algun error.
     */
    public void crearPerfil(String descripcion) throws Exception;

    /**
     * Metodo encargado de traer una lista de todos los perfiles.
     *
     * @return lista de perfiles.
     */
    public Map<Long, String> cargarPerfiles() throws Exception;

    /**
     * Metodo encargado de buscar un perfil por su id.
     *
     * @param id numero de identificacion del perfil.
     * @return un perfil.
     */
    public Perfil buscarPerfil(Long id) throws Exception;

    public List<Perfil> listaPerfiles() throws Exception;

    public void cambiarEstado(Long perfil_id, Boolean estado);
}
