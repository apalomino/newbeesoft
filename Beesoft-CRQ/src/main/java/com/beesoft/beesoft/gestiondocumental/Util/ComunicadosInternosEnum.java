/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author ISSLUNO
 */
public class ComunicadosInternosEnum {

    public enum ComunicadosEnum {

        OFICIOS, CIRCULAR, MANUAL, MEMORANDO
    }

    ComunicadosEnum name;

    public ComunicadosInternosEnum(ComunicadosEnum Name) {
        this.name = Name;
    }

}
