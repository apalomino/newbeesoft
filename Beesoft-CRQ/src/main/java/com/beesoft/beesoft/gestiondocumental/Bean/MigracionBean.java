/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.MigracionLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.OficinaOld;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import com.beesoft.beesoft.gestiondocumental.entidades.RevisionDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.SerieOld;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerie;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerieOld;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumentoOld;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.TrdOld;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.util.password.BasicPasswordEncryptor;

/**
 *
 * @author cfernandez@sumset.com
 */
@Stateless
public class MigracionBean implements MigracionLocal {

    @PersistenceContext(name = "BeesoftPU")
    private EntityManager em;

    @EJB
    private UtilidadesLocal utilBean;

    public Connection getConnection() throws Exception {
        String driver = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/crq";
        String username = "postgres";
        String password = "12345";
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }

    public Connection getConnection2() throws Exception {
        String driver = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/beesoftcrq";
        String username = "postgres";
        String password = "12345";
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }

    @Override
    public void migrarActores() {
        ResultSet rs = null;
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {

            List<Actor> actor = new ArrayList<>();
            Boolean isWhere = false;

            conn = getConnection();

            pstmt = conn.prepareStatement("select id,tipopersona,cedula,nombre,apellido,email,direccion,idciudad,estado from datosbasicos where id not in (SELECT id FROM empleado GROUP BY id HAVING COUNT(*) > 1);");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                String id = rs.getString(1);
                String tipopersona = rs.getString(2);
                String cedula = rs.getString(3);
                String nombre = rs.getString(4);
                String apellido = rs.getString(5);
                String email = rs.getString(6);
                String direccion = rs.getString(7);
                String idciudad = rs.getString(8);
                String estado = rs.getString(9);

                if (StringUtils.isNumeric(id)) {
                    System.out.println("el id: " + id);
                    Boolean state = estado.equals("A") ? true : false;
                    Ciudad ciudad = null;
                    if (StringUtils.isNotBlank(idciudad)) {
                        try {
                            ciudad = utilBean.buscarCiudad(Long.valueOf(idciudad));
                        } catch (Exception e) {
                            ciudad = utilBean.buscarCiudad(1L);
                        }
                    } else {
                        ciudad = utilBean.buscarCiudad(1L);
                    }

                    Actor nodefinido = em.find(Actor.class, Long.valueOf(id));

                    if (nodefinido == null) {

                        if (tipopersona.equals("emr")) {
                            Actor empresa = new Actor(Long.valueOf(id), cedula, direccion, email, nombre + " " + apellido, ciudad, utilBean.buscarTipo(4L), state);
                            em.persist(empresa);
                        } else if (tipopersona.equals("emp")) {
                            Actor empleado = new Actor(Long.valueOf(id), cedula, direccion, email, nombre + " " + apellido, ciudad, utilBean.buscarTipo(2L), state);
                            em.persist(empleado);
                        } else if (tipopersona.equals("per")) {
                            Actor persona = new Actor(Long.valueOf(id), cedula, direccion, email, nombre + " " + apellido, ciudad, utilBean.buscarTipo(3L), state);
                            em.persist(persona);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    pstmt.close();
                    conn.close();
                }
            } catch (SQLException e) {

            }
        }
    }

    @Override
    public void migrarTablasRetencion() {
        ResultSet rs = null;
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {

            List<Actor> actor = new ArrayList<>();
            Boolean isWhere = false;

            conn = getConnection();

            pstmt = conn.prepareStatement("select id,codigo,nombre from area;");
            rs = pstmt.executeQuery();

            List<OficinaOld> oficinas = new ArrayList<>();
            List<SerieOld> series = new ArrayList<>();
            List<SubSerieOld> subSeries = new ArrayList<>();
            List<TipoDocumentoOld> tipos = new ArrayList<>();
            while (rs.next()) {
                String id = rs.getString(1);
                String codigo = rs.getString(2);
                String nombre = rs.getString(3);
                OficinaOld oficina = new OficinaOld(Long.valueOf(id), false, nombre, codigo);
                em.persist(oficina);
                oficinas.add(oficina);
            }

            if (oficinas != null && !oficinas.isEmpty()) {
                for (OficinaOld oficina : oficinas) {
                    pstmt = conn.prepareStatement("select id,codigo,nombre from serie where idoficina=?;");
                    pstmt.setString(1, oficina.getId().toString());
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        String id = rs.getString(1);
                        String codigo = rs.getString(2);
                        String nombre = rs.getString(3);
                        SerieOld serie = new SerieOld(Long.valueOf(id), false, nombre, codigo, oficina);
                        em.persist(serie);
                        series.add(serie);
                    }
                }
            }

            if (series != null && !series.isEmpty()) {
                for (SerieOld serie : series) {
                    pstmt = conn.prepareStatement("select id,codigo,nombre from subserie where idserie=?;");
                    pstmt.setString(1, serie.getId().toString());
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        String id = rs.getString(1);
                        String codigo = rs.getString(2);
                        String nombre = rs.getString(3);
                        SubSerieOld subserie = new SubSerieOld(Long.valueOf(id), false, nombre, codigo, serie);
                        em.persist(subserie);
                        subSeries.add(subserie);
                    }
                }
            }

            if (subSeries != null && !subSeries.isEmpty()) {
                for (SubSerieOld subSerie : subSeries) {
                    pstmt = conn.prepareStatement("select id,nombre from tipodocumental where idsubserie=?;");
                    pstmt.setString(1, subSerie.getId().toString());
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        String id = rs.getString(1);
                        String nombre = rs.getString(2);
                        TipoDocumentoOld tipo = new TipoDocumentoOld(Long.valueOf(id), false, nombre, subSerie);
                        em.persist(tipo);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    pstmt.close();
                    conn.close();
                }
            } catch (SQLException e) {

            }
        }
    }

    @Override
    public List<String> consultarIds() {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;
            List<Documento> listaDocumentos = new ArrayList<>();
            Boolean isWhere = false;

            conn = getConnection();

            StringBuilder queryId = new StringBuilder("SELECT DISTINCT (documento.id) from documento where fechasistema between '01/05/2017' and '30/06/2017' ");
            pstmt = conn.prepareStatement(queryId.toString()); // create a statement
            rs = pstmt.executeQuery();
            List<String> ids = new ArrayList<>();
            while (rs.next()) {
                ids.add(rs.getString(1));
            }
            pstmt = null;
            rs = null;
            return ids;
        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void migrarDocumento(List<String> ids
    ) {
        ResultSet rs = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        List<Documento> listaDocumentos = new ArrayList<>();
        Boolean isWhere = false;

        try {
            conn = getConnection();

            for (String id : ids) {
                Documento doc = em.find(Documento.class, Long.valueOf(id));
                if (doc == null) {
                    StringBuilder query = new StringBuilder("SELECT DISTINCT (documento.id),");
                    query.append("documento.radicacion,");
                    query.append("documento.fechaemision,");
                    query.append("documento.fechasistema,");
                    query.append("documento.fecharecepcion,");
                    query.append("documento.comentario,");
                    query.append("documento.recibido,");
                    query.append("documento.guia,");
                    query.append("documento.enviado,");
                    query.append("documento.funcionario,");
                    query.append("documento.prueba,");
                    query.append("documento.notificacion,");
                    query.append("documento.observacion,");
                    query.append("documento.clase,");
                    query.append("documento.aprobado,");
                    query.append("documento.reviso,");
                    query.append("documento.pendiente,");
                    query.append("documento.fechacontrol,");
                    query.append("documento.responsable,");
                    query.append("documento.estado,");
                    query.append("documento.usuario,");
                    query.append("documento.iddocumento,");
                    query.append("documento.estadofisico ");
                    query.append("documento.ciudad ");
                    query.append("FROM documento ");
                    query.append("WHERE documento.id=? ");
                    query.append("ORDER BY documento.fechasistema ");

                    pstmt = conn.prepareStatement(query.toString()); // create a statement
                    pstmt.setString(1, id);

                    rs = pstmt.executeQuery();
                    // extract data from the ResultSet
                    while (rs.next()) {
                        String Id = rs.getString(1);
                        String rad = rs.getString(2);
                        Date fechaEmision = rs.getDate(3);
                        Date fechaRecepcion = rs.getDate(4);
                        Date fechaSistema = rs.getDate(5);
                        String asu = rs.getString(6);
                        String recibido = rs.getString(7);
                        String guia = rs.getString(8);
                        String enviado = rs.getString(9);
                        String funcionario = rs.getString(10);
                        String prueba = rs.getString(11);
                        String notifi = rs.getString(12);
                        String observ = rs.getString(13);
                        String clase = rs.getString(14);
                        Boolean aprob = rs.getBoolean(15);
                        Boolean rev = rs.getBoolean(16);
                        Boolean pendient = rs.getBoolean(17);
                        Date fechaControl = rs.getDate(18);
                        String responsable = rs.getString(19);
                        String estado = rs.getString(20);
                        String radicador = rs.getString(21);
                        String referenciacruzada = rs.getString(22);
                        String anexos = rs.getString(23);
                        String ciudadT = rs.getString(24);
                        Boolean activo = false;
                        if (StringUtils.isNotBlank(estado)) {
                            activo = estado.equals("A") ? true : false;
                        }

                        Documento referencia = null;
                        if (StringUtils.isNotBlank(referenciacruzada) && StringUtils.isNumeric(referenciacruzada)) {
                            referencia = em.find(Documento.class, Long.valueOf(referenciacruzada));
                        }
                        Actor responsible = null;
                        if (StringUtils.isNotBlank(responsable) && StringUtils.isNumeric(responsable)) {
                            responsible = em.find(Actor.class, Long.valueOf(responsable));
                        }

                        Actor radicadorA = null;
                        if (StringUtils.isNotBlank(radicador)) {
                            List<Actor> radicadores = em.createQuery("select a from Actor a where a.email=:email").setParameter("email", radicador).getResultList();
                            if (radicadores != null && !radicadores.isEmpty()) {
                                radicadorA = radicadores.get(0);
                            }
                        }
                        
                        Ciudad ciudad = null;
                        if(StringUtils.isNoneBlank(ciudadT) && StringUtils.isNumeric(ciudadT)) {
                        	ciudad = em.find(Ciudad.class, Long.valueOf(ciudadT));
                        }

                        Documento documento = new Documento(Long.valueOf(Id), rad, activo, anexos, aprob, asu, fechaControl, fechaEmision, fechaRecepcion, fechaSistema, pendient, rev, clase, enviado, responsible, radicadorA, true, ciudad);
                        em.persist(documento);
//                        listaDocumentos.add(documento);
                    }
                }
            }
            if (rs != null) {
                rs.close();
                pstmt.close();
                conn.close();
            }
        } catch (Exception e) {
            System.out.println("errrooorrrr: " + e.getMessage());
        }
    }

    @Override
    public void migrarRevisores(List<String> ids
    ) {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection();

            for (String id : ids) {
                List<Actor> procedencia = new ArrayList<>();
                List<Actor> destinos = new ArrayList<>();
                List<Actor> revisores = new ArrayList<>();

                StringBuilder queryRevisor = new StringBuilder("SELECT idrevisor from revisordocumento where iddocumento=?");
                pstmt = conn.prepareStatement(queryRevisor.toString()); // create a statement
                pstmt.setString(1, id);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    String idRevisor = rs.getString(1);
                    if (StringUtils.isNotBlank(idRevisor) && StringUtils.isNumeric(idRevisor)) {
                        Actor revisor = em.find(Actor.class, Long.valueOf(idRevisor));
                        revisores.add(revisor);
                    }
                }
                pstmt = null;
                rs = null;

                if (!procedencia.isEmpty() || !destinos.isEmpty() || !revisores.isEmpty()) {
                    Documento documento = em.find(Documento.class, Long.valueOf(id));
                    documento.setRevisores(revisores);
                    em.merge(documento);
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void migrarProcedencia(List<String> ids
    ) {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection();

            for (String id : ids) {
                Documento doc = em.find(Documento.class, Long.valueOf(id));
                if (doc.getProcedencia().isEmpty()) {
                    List<Actor> procedencia = new ArrayList<>();
                    List<Actor> destinos = new ArrayList<>();
                    List<Actor> revisores = new ArrayList<>();
                    StringBuilder queryOrigen = new StringBuilder("SELECT iddatosbasicos from origendocumento where iddocumento=?");
                    pstmt = conn.prepareStatement(queryOrigen.toString()); // create a statement
                    pstmt.setString(1, id);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        String idOrigen = rs.getString(1);
                        if (StringUtils.isNotBlank(idOrigen) && StringUtils.isNumeric(idOrigen)) {
                            Actor origen = em.find(Actor.class, Long.valueOf(idOrigen));
                            procedencia.add(origen);
                        }
                    }
                    pstmt = null;
                    rs = null;

                    if (!procedencia.isEmpty()) {
                        doc.setProcedencia(procedencia);
                        em.merge(doc);
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> consultarDocumentosFaltantes(String id) {
        try {
            List<String> lista = new ArrayList<>();
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection();
            StringBuilder queryDestino = new StringBuilder("SELECT iddocumento from origendocumento where iddatosbasicos=?");
            pstmt = conn.prepareStatement(queryDestino.toString()); // create a statement
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String idDocumento = rs.getString(1);
                lista.add(idDocumento);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void migrarDestinosFaltantes(List<String> ids, String idDestino) {
        try {
            List<Actor> destinos = new ArrayList<>();
            if (StringUtils.isNotBlank(idDestino) && StringUtils.isNumeric(idDestino)) {
                Actor destino = em.find(Actor.class, Long.valueOf(idDestino));
                destinos.add(destino);
            }

            for (String id : ids) {
                if (!destinos.isEmpty()) {
                    Documento documento = em.find(Documento.class, Long.valueOf(id));
                    documento.setProcedencia(destinos);
                    em.merge(documento);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void migrarDestinos(List<String> ids) {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection();

            for (String id : ids) {
                Documento doc = em.find(Documento.class, Long.valueOf(id));
                if (doc.getDestinos().isEmpty()) {

                    List<Actor> procedencia = new ArrayList<>();
                    List<Actor> destinos = new ArrayList<>();
                    List<Actor> revisores = new ArrayList<>();
                    StringBuilder queryDestino = new StringBuilder("SELECT iddatosbasicos from destinodocumento where iddocumento=?");
                    pstmt = conn.prepareStatement(queryDestino.toString()); // create a statement
                    pstmt.setString(1, id);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        String idDestino = rs.getString(1);
                        if (StringUtils.isNotBlank(idDestino) && StringUtils.isNumeric(idDestino)) {
                            Actor destino = em.find(Actor.class, Long.valueOf(idDestino));
                            destinos.add(destino);
                        }
                    }

                    pstmt = null;
                    rs = null;

                    if (!destinos.isEmpty()) {
                        doc.setDestinos(destinos);
                        em.merge(doc);
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void migrarDOcTrd(List<String> ids) {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection();

            for (String id : ids) {

                List<TrdOld> lista = em.createQuery("select t from TrdOld t where t.documento.id=:id").setParameter("id", Long.valueOf(id)).getResultList();

                if (lista.isEmpty()) {

                    StringBuilder queryId = new StringBuilder("SELECT idoficina,idserie,idsubserie,idtipo from documentotipo where iddocumento=?");
                    pstmt = conn.prepareStatement(queryId.toString()); // create a statement
                    pstmt.setString(1, id);
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        String idoficina = rs.getString(1);
                        String idserie = rs.getString(2);
                        String idsubserie = rs.getString(3);
                        String idtipo = rs.getString(4);
                        OficinaOld oficina = null;
                        if (StringUtils.isNotBlank(idoficina) && StringUtils.isNumeric(idoficina)) {
                            oficina = em.find(OficinaOld.class, Long.valueOf(idoficina));
                        }
                        if (oficina != null) {
                            SerieOld serie = null;
                            if (StringUtils.isNotBlank(idserie) && StringUtils.isNumeric(idserie)) {
                                serie = em.find(SerieOld.class, Long.valueOf(idserie));
                            }
                            if (serie != null) {
                                SubSerieOld subserie = null;
                                if (StringUtils.isNotBlank(idsubserie) && StringUtils.isNumeric(idsubserie)) {
                                    subserie = em.find(SubSerieOld.class, Long.valueOf(idsubserie));
                                }
                                TipoDocumentoOld tipo = null;
                                if (StringUtils.isNotBlank(idtipo) && StringUtils.isNumeric(idtipo)) {
                                    tipo = em.find(TipoDocumentoOld.class, Long.valueOf(idtipo));
                                }

                                Documento documento = em.find(Documento.class, Long.valueOf(id));
                                TrdOld trd = new TrdOld();
                                trd.setDocumento(documento);
                                trd.setOficinaOld(oficina);
                                trd.setSerieOld(serie);
                                trd.setSubSerieOld(subserie);
                                trd.setTipodocumentoOld(tipo);
                                em.persist(trd);
                            }
                        }
                    }
                    pstmt = null;
                    rs = null;
                }
            }
            conn.close();
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> consultarIdsRevisiones() {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;
            List<String> ids = new ArrayList<>();

            conn = getConnection();

            try {
                StringBuilder queryId = new StringBuilder("SELECT id from revisordocumento");
                pstmt = conn.prepareStatement(queryId.toString()); // create a statement
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    try {
                        String id = rs.getString(1);
                        ids.add(id);
                    } catch (SQLException ex) {
                        Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                return ids;
            } catch (SQLException ex) {
                Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void migrarRevisiones(List<String> ids) {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;
            conn = getConnection();

            for (String id : ids) {
                StringBuilder queryId = new StringBuilder("SELECT iddocumento,aprobado,idrevisor,comentario,reviso,pendiente,fecha from revisordocumento where id=?");
                pstmt = conn.prepareStatement(queryId.toString()); // create a statement
                pstmt.setString(1, id);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    String iddocumento = rs.getString(1);
                    Boolean aprobado = rs.getBoolean(2);
                    String idrevisor = rs.getString(3);
                    String comentario = rs.getString(4);
                    Boolean reviso = rs.getBoolean(5);
                    Boolean pendiente = rs.getBoolean(6);
                    Date fecha = rs.getDate(7);
                    RevisionDocumento revision = new RevisionDocumento();
                    revision.setAprobado(aprobado);
                    revision.setComentario(comentario);
                    revision.setPendiente(pendiente);
                    revision.setRevisado(reviso);
                    revision.setFechaComentario(fecha);
                    Documento documento = em.find(Documento.class, Long.valueOf(iddocumento));
                    Actor actor = em.find(Actor.class, Long.valueOf(idrevisor));
                    if (documento != null && actor != null) {
                        revision.setDocumento(documento);
                        revision.setActor(actor);
                        em.persist(revision);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<String> consultarIdsPorAño(String año) {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;
            List<Documento> listaDocumentos = new ArrayList<>();
            Boolean isWhere = false;

            conn = getConnection();

            StringBuilder queryId = new StringBuilder("SELECT DISTINCT (documento.id) from documento where fechasistema between '01/01//" + año + "' and '31/12//" + año + "' order by id");
            pstmt = conn.prepareStatement(queryId.toString()); // create a statement
            rs = pstmt.executeQuery();
            List<String> ids = new ArrayList<>();
            while (rs.next()) {
                ids.add(rs.getString(1));
            }
            pstmt = null;
            rs = null;
            return ids;
        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void migrarImagenes(List<String> lista, String ruta) {

        for (String archivo : lista) {
            List<ArchivoDocumento> archivos = new ArrayList<>();
            //BufferedReader br = new BufferedReader(new FileReader(path + ficheros[i]));
            int dotPosition = (archivo).lastIndexOf('.');
            String fileExtension = (archivo).substring(dotPosition + 1);
            String id = archivo.substring(0, dotPosition);
            System.out.println("documento id: " + id);
            Documento documento = em.find(Documento.class, Long.valueOf(id));
            ArchivoDocumento archivoD = new ArchivoDocumento();
            archivoD.setNombreArchivo(documento.getRadicacion() + "." + fileExtension);
            byte[] bytesArray = convertirArchivo(new File(ruta + File.separator + archivo));
            archivoD.setArchivo(bytesArray);
            em.persist(archivoD);
            archivos.add(archivoD);
            if (!archivos.isEmpty()) {
                documento.setArchivos(archivos);
                em.merge(documento);
            }
        }
    }

    @Override
    public void migrarImagenes(String nombre, ArchivoDocumento archivo) {
        List<ArchivoDocumento> archivos = new ArrayList<>();
        int dotPosition = (nombre).lastIndexOf('.');
        String fileExtension = (nombre).substring(dotPosition + 1);
        String id = nombre.substring(0, dotPosition);
        System.out.println("documento id: " + id);
        Documento documento = em.find(Documento.class, Long.valueOf(id));
        em.persist(archivo);
        archivos.add(archivo);
        if (!archivos.isEmpty()) {
            documento.setArchivos(archivos);
            em.merge(documento);
        }
    }

    public byte[] convertirArchivo(File file) {
        try {
            byte[] bytesArray = new byte[(int) file.length()];

            FileInputStream fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();

            return bytesArray;
        } catch (IOException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Long> consultarDocumentosIncompletos() {
        List<Long> lista = em.createQuery("select d.id from Documento d where d.destinos is empty").getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        }
        return null;
    }

    @Override
    public void migrarFaltantesIncompletos(List<Long> lista) {
        if (lista != null && !lista.isEmpty()) {

            for (Long idDocumento : lista) {
                List<Actor> procedencia = new ArrayList<>();
                Documento documento = em.find(Documento.class, idDocumento);
                Actor actor = em.find(Actor.class, 33623L);
                procedencia.add(actor);
                documento.setDestinos(procedencia);
                em.merge(documento);
            }
        }
    }

    @Override
    public List<String> consultarCorreosUsuarios() {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection();

            StringBuilder queryId = new StringBuilder("SELECT email from usuariobeesoft");
            pstmt = conn.prepareStatement(queryId.toString()); // create a statement
            rs = pstmt.executeQuery();
            List<String> ids = new ArrayList<>();
            while (rs.next()) {
                ids.add(rs.getString(1));
            }
            pstmt = null;
            rs = null;
            return ids;
        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void migrarUsuarios(List<String> correos) {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection();

            for (String correo : correos) {

                StringBuilder queryId = new StringBuilder("SELECT perfil from usuarioperfil where email=?");
                pstmt = conn.prepareStatement(queryId.toString()); // create a statement
                pstmt.setString(1, correo);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    String perfil = rs.getString(1);
                    if (StringUtils.isNotBlank(perfil)) {
                        List<Actor> lista = em.createQuery("select a from Actor a where a.email=:email").setParameter("email", correo).getResultList();
                        if (lista != null && !lista.isEmpty()) {
                            Actor actor = lista.get(0);
                            Usuario usuario = new Usuario();
                            usuario.setActor(actor);
                            usuario.setUserName(correo);
                            BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
                            String Encryptedpassword = passwordEncryptor.encryptPassword("beesoft");
                            usuario.setPassword(Encryptedpassword);
                            Perfil profile = null;
                            switch (perfil) {
                                case "ADMINISTRADOR":
                                    profile = em.find(Perfil.class, 1L);
                                    usuario.setPerfil(profile);
                                    break;
                                case "RADICADOR":
                                    profile = em.find(Perfil.class, 2L);
                                    usuario.setPerfil(profile);
                                    break;
                                case "GESTIONADOR":
                                    profile = em.find(Perfil.class, 3L);
                                    usuario.setPerfil(profile);
                                    break;
                                default:
                                    break;
                            }
                            em.persist(usuario);
                        }
                    }

                }
            }
            pstmt = null;
            rs = null;
            conn.close();
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void completarTRD() {
        List<SubSerie> lista = em.createQuery("select s from SubSerie s where s.id not in (select t.subserie.id from TipoDocumento t)").getResultList();
        for (SubSerie subSerie : lista) {
            TipoDocumento tipo = new TipoDocumento();
            tipo.setDescripcion("N/A");
            tipo.setSubserie(subSerie);
            tipo.setActivo(Boolean.TRUE);
            em.persist(tipo);
        }

    }

    @Override
    public List<Documento> consultarReferenciasCruzadas(String año) {
        List<Documento> lista = em.createQuery("select d from Documento d where d.documentoReferencia!=null and d.fechaSistema between '01/01/" + año + "' and '31/12/" + año + "'").getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public void organizarReferenciasCruzadas(List<Documento> lista) {
        if (lista != null && !lista.isEmpty()) {
            for (Documento documento : lista) {
                List<Documento> referencias = new ArrayList<>();
                referencias.add(documento.getDocumentoReferencia());
                documento.setDocumentosReferencia(referencias);
                em.merge(documento);

                Documento res = em.find(Documento.class, documento.getDocumentoReferencia().getId());
                res.setDocumentoRespuesta(documento);
                em.merge(res);
                
    			/*List<Documento> lRespuesta = new ArrayList();
    			for (Documento doc : documentosRespuesta) {
    				Documento dr = em.find(Documento.class, doc.getId());
    				lRespuesta.add(dr);
    				em.merge(dr);					
    				}
    			documento.setDocumentosRespuesta(lRespuesta);*/
            }
        }
    }

    @Override
    public void migrarClases() {
        try {
            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement pstmt = null;

            conn = getConnection2();

            StringBuilder queryId = new StringBuilder("SELECT id,clasedocumento_id from documento where fechasistema between '01/04/2017' and '01/10/2017'");
            pstmt = conn.prepareStatement(queryId.toString()); // create a statement

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Long id = rs.getLong(1);
                Long clase = rs.getLong(2);

                if (clase != null && clase > 0) {
                    Documento doc = em.find(Documento.class, id);
                    if (doc != null) {
                        switch (clase.intValue()) {
                            case 1:
                                doc.setClasedocumento("PQR");
                                break;
                            case 2:
                                doc.setClasedocumento("DENUNCIA");
                                break;
                            case 10:
                                doc.setClasedocumento("TRAMITE");
                                break;
                            default:
                                doc.setClasedocumento("OTROS");

                        }
                        em.merge(doc);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MigracionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
