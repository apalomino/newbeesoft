/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author SUMSET
 */
public class ClaseDocumentoDTO {
    private String nombre;
    private String estado;
    private Long id;
    
    public ClaseDocumentoDTO(){
        
    }
    
    public ClaseDocumentoDTO(String nombre, String estado, Long id){
        this.nombre = nombre;
        this.estado= estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   
    
    
}
