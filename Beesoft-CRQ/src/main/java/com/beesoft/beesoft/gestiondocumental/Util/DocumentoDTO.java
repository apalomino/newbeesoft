/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import java.util.Date;

/**
 *
 * @author ISSLUNO
 */
public class DocumentoDTO {

    private Documento documento;
    private String id;
    private String radicacion;
    private Date fechaEmision;
    private Date fechaRecepcion;
    private Date fechaSistema;
    private String procedencia;
    private String destino;
    private String asunto;
    private String recibido;
    private String guia;
    private String enviado;
    private String funcionario;
    private String prueba;
    private String notificacion;
    private String observacion;
    private String clase;
    private String claseAux;
    private Boolean aprobado;
    private Boolean revisado;
    private Boolean pendiente;
    private Date fechaControl;
    private String responsable;
    private String revisor;
    private String radicador;
    private String ubicacionFisica;
    private Boolean activo;
    private Boolean old;

    public DocumentoDTO() {
    }

    public DocumentoDTO(String id, String radicacion, Date fechaEmision, Date fechaRecepcion, Date fechaSistema, String procedencia, String destino, String asunto, String recibido, String guia, String enviado, String funcionario, String prueba, String notificacion, String observacion, String clase, Boolean aprobado, Boolean revisado, Boolean pendiente, Date fechaControl, String responsable, String revisor, String radicador, Boolean activo, Boolean old) {
        this.id = id;
        this.radicacion = radicacion;
        this.fechaEmision = fechaEmision;
        this.fechaRecepcion = fechaRecepcion;
        this.fechaSistema = fechaSistema;
        this.procedencia = procedencia;
        this.destino = destino;
        this.asunto = asunto;
        this.recibido = recibido;
        this.guia = guia;
        this.enviado = enviado;
        this.funcionario = funcionario;
        this.prueba = prueba;
        this.notificacion = notificacion;
        this.observacion = observacion;
        this.clase = clase;
        this.aprobado = aprobado;
        this.revisado = revisado;
        this.pendiente = pendiente;
        this.fechaControl = fechaControl;
        this.responsable = responsable;
        this.revisor = revisor;
        this.radicador = radicador;
        this.activo = activo;
        this.old = old;
    }

    public DocumentoDTO(Documento documento, String radicacion, Date fechaEmision, Date fechaSistema, String asunto, Boolean activo, Boolean old) {
        this.documento = documento;
        this.radicacion = radicacion;
        this.fechaEmision = fechaEmision;
        this.fechaSistema = fechaSistema;
        this.asunto = asunto;
        this.activo = activo;
        this.old = old;
    }

    public DocumentoDTO(String id, String radicacion, String ubicacionFisica, Date fechaEmision, Date fechaSistema, String destino, String asunto, String observacion, String clase, Date fechaControl, String radicador, Boolean old) {
        this.id = id;
        this.radicacion = radicacion;
        this.ubicacionFisica = ubicacionFisica;
        this.fechaEmision = fechaEmision;
        this.fechaSistema = fechaSistema;
        this.destino = destino;
        this.asunto = asunto;
        this.observacion = observacion;
        this.clase = clase;
        this.fechaControl = fechaControl;
        this.radicador = radicador;
        this.old = old;
    }

    public DocumentoDTO(Documento documento, String radicacion, Date fechaEmision, Date fechaSistema, String asunto, String observacion, String clase, Boolean old) {
        this.documento = documento;
        this.radicacion = radicacion;
        this.fechaEmision = fechaEmision;
        this.fechaSistema = fechaSistema;
        this.asunto = asunto;
        this.observacion = observacion;
        this.old = old;
        this.claseAux = clase;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getRecibido() {
        return recibido;
    }

    public void setRecibido(String recibido) {
        this.recibido = recibido;
    }

    public String getGuia() {
        return guia;
    }

    public void setGuia(String guia) {
        this.guia = guia;
    }

    public String getEnviado() {
        return enviado;
    }

    public void setEnviado(String enviado) {
        this.enviado = enviado;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public String getPrueba() {
        return prueba;
    }

    public void setPrueba(String prueba) {
        this.prueba = prueba;
    }

    public String getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(String notificacion) {
        this.notificacion = notificacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public Boolean getAprobado() {
        return aprobado;
    }

    public void setAprobado(Boolean aprobado) {
        this.aprobado = aprobado;
    }

    public Boolean getRevisado() {
        return revisado;
    }

    public void setRevisado(Boolean revisado) {
        this.revisado = revisado;
    }

    public Date getFechaControl() {
        return fechaControl;
    }

    public void setFechaControl(Date fechaControl) {
        this.fechaControl = fechaControl;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getRevisor() {
        return revisor;
    }

    public void setRevisor(String revisor) {
        this.revisor = revisor;
    }

    public String getRadicador() {
        return radicador;
    }

    public void setRadicador(String radicador) {
        this.radicador = radicador;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getOld() {
        return old;
    }

    public void setOld(Boolean old) {
        this.old = old;
    }

    public Boolean getPendiente() {
        return pendiente;
    }

    public void setPendiente(Boolean pendiente) {
        this.pendiente = pendiente;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public Date getFechaSistema() {
        return fechaSistema;
    }

    public void setFechaSistema(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    public String getUbicacionFisica() {
        return ubicacionFisica;
    }

    public void setUbicacionFisica(String ubicacionFisica) {
        this.ubicacionFisica = ubicacionFisica;
    }

    public String getClaseAux() {
        return claseAux;
    }

    public void setClaseAux(String claseAux) {
        this.claseAux = claseAux;
    }

}
