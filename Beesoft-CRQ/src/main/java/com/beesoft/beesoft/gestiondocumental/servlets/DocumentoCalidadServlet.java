/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.servlets;

import com.beesoft.beesoft.gestiondocumental.entidades.DocumentoCalidad;
import java.io.IOException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristhian
 */
public class DocumentoCalidadServlet extends HttpServlet {

    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idCalidad = request.getParameter("idCalidad");
        String tipo = request.getParameter("tipo");
        // Manejar cuando no se encuentra
        DocumentoCalidad calidad = em.find(DocumentoCalidad.class, Long.valueOf(idCalidad));

        ServletOutputStream out = response.getOutputStream();
        String nombreArchivo = null;
        byte[] archivo = null;
        switch (tipo) {
            case "true":
                nombreArchivo = calidad.getNombreDocumento();
                archivo = calidad.getDocumento();
                break;
            case "false":
                nombreArchivo = calidad.getNombreImagen();
                archivo = calidad.getImagen();
                break;
        }

        try {
            System.out.print("revisando extensiones");

            int dotPosition = nombreArchivo.lastIndexOf('.');
            String extension[] = {"tif", "tiff", "txt", "htm", "jpg", "gif", "pdf", "doc", "ppt", "xls", "docx", "pptx", "xlsx", "zip", "rar", "7z"};
            String mimeType[] = {"image/tiff", "image/tiff", "text/plain", "text/html", "image/jpg", "image/gif", "application/pdf", "application/msword", "application/vnd.ms-powerpoint", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/zip", "application/x-rar-compressed", "application/x-7z-compressed"};
            String fileExtension = nombreArchivo.substring(dotPosition + 1);
            System.out.print("extension" + fileExtension);

            boolean encontro = false;
            for (int index = 0; index < mimeType.length; index++) {

                if (fileExtension.equalsIgnoreCase(extension[index])) {
                    response.setContentType(mimeType[index]);
                    String disposition = "attachment; filename=\"" + nombreArchivo + "\"";
                    response.setHeader("Content-disposition", disposition);
                    index = mimeType.length;
                    encontro = true;
                }
            }
            if (encontro == false) {
                response.setContentType("text/plain");
            }
            System.out.print(response.toString());
            out.write(archivo);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
