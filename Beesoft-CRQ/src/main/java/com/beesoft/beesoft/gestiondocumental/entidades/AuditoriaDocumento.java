package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;


/**
 * The persistent class for the auditoria_documento database table.
 * 
 */
@Entity
public class AuditoriaDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String accion;

	private Date fechaHora;

	private Actor usuario;

	private Documento documento;

	public AuditoriaDocumento() {
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String acccion) {
		this.accion = acccion;
	}

        @Temporal(javax.persistence.TemporalType.TIMESTAMP)
	public Date getFechaHora() {
		return this.fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

        @OneToOne
	public Actor getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Actor usuario) {
		this.usuario = usuario;
	}

	@ManyToOne
	public Documento getDocumento() {
		return this.documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

}