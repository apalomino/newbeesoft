/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.PerfilLocal;
import com.beesoft.beesoft.gestiondocumental.Local.PermisoLocal;
import com.beesoft.beesoft.gestiondocumental.Util.PermisoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import static com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil.addMessageInfo;
import com.beesoft.beesoft.gestiondocumental.entidades.Menu;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import com.beesoft.beesoft.gestiondocumental.entidades.Permiso;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "permisoAction")
@SessionScoped
public class PermisoAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(PermisoAction.class);

    @EJB
    private PermisoLocal permisoBean;

    @EJB
    private PerfilLocal perfilBean;

    private TreeNode root;

    private Map<Long, String> mapaPerfil = new HashMap<>();
    private List<String> listaPerfil;
    private String perfilCadena;
    private Long perfilId;
    private Perfil perfil;
    private final int goCreate = 1;
    private List<Menu> sub;
    private List<Menu> listaMenu;

    private String tipo;
    private String etiqueta;
    private String descripcion;
    private Long padreId;
    private Integer orden;
    private List<Menu> listaPadres;

    @PostConstruct
    public void inicializar() {
        cargarPerfiles();
        cargarSub(null);
        cargarMenuCompleto();
        cargarListaPadres();
    }

    public void crearMenu() {
        try {
            root = new DefaultTreeNode(new Menu(), null);

//            TreeMap<Menu, List<TreeMap<Menu, List<Menu>>>> lista = permisoBean.cargarMenu(null);
//
//            if (lista != null && !lista.isEmpty()) {
//                for (Map.Entry<Menu, List<TreeMap<Menu, List<Menu>>>> menu : lista.entrySet()) {
//                    TreeNode Raiz = new DefaultTreeNode(menu.getKey(), root);
//                    for (Map<Menu, List<Menu>> listaSub : menu.getValue()) {
//                        for (Map.Entry<Menu, List<Menu>> entry : listaSub.entrySet()) {
//                            TreeNode subMenu = new DefaultTreeNode(entry.getKey(), Raiz);
//                            for (Menu url : entry.getValue()) {
//                                TreeNode menuItem = new DefaultTreeNode(url, subMenu);
//                            }
//                        }
//                    }
//                }
//
//            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public List<Menu> cargarRaiz() {
        return permisoBean.cargarRaiz(null);
    }

    public void cargarSub(Long parent) {
        obtenerPerfil();
        if (perfil != null) {
            sub = permisoBean.cargarSubMenu(perfil.getId(), parent);

        } else {
            sub = permisoBean.cargarSubMenu(null, parent);
        }
    }

    public List<PermisoDTO> cargarUrl(Long parent) {
        obtenerPerfil();
        if (perfil != null) {
            List<PermisoDTO> lista = new ArrayList<PermisoDTO>();
            List<Menu> listaPermisos = permisoBean.cargarUrl(perfil.getId(), parent);
            List<Menu> listaCompleta = permisoBean.cargarUrl(null, parent);
            if (listaCompleta != null && !listaCompleta.isEmpty()) {
                for (Menu menu : listaCompleta) {
                    PermisoDTO dto = new PermisoDTO();
                    dto.setMenu(menu);
                    dto.setPermiso(Boolean.FALSE);
                    lista.add(dto);
                }

                if (listaPermisos != null && !listaPermisos.isEmpty()) {
                    for (Menu menu : listaPermisos) {
                        for (PermisoDTO dto : lista) {
                            if (dto.getMenu().getId().equals(menu.getId())) {
                                dto.setPermiso(Boolean.TRUE);
                            }
                        }
                    }
                }
            }
            return lista;
        } else {
            List<PermisoDTO> lista = new ArrayList<PermisoDTO>();
            List<Menu> listaCompleta = permisoBean.cargarUrl(null, parent);
            for (Menu menu : listaCompleta) {
                PermisoDTO dto = new PermisoDTO();
                dto.setMenu(menu);
                dto.setPermiso(Boolean.FALSE);
                lista.add(dto);
            }
            return lista;
        }
    }

    public void cargarMenuCompleto() {
        listaMenu = permisoBean.cargarMenuCompleto();
    }

    public void crearPermiso(Menu menu) {

        int validate = 0;
        validate += ValidacionesUtil.validarCadena(perfilCadena, "Perfil");

        if (goCreate == validate) {

            obtenerPerfil();

            List<Menu> selectedNodes = new ArrayList<Menu>();
            selectedNodes.add(menu);
            //Menu parent = menu.getParent();
//            while (parent != null) {
//                selectedNodes.add(parent);
//                parent = parent.getParent();
//            }

            for (Menu nodo : selectedNodes) {
                permisoBean.gestionarPermisos(nodo, perfil, Boolean.TRUE);
            }

            cargarUrl(menu.getParent().getId());
            addMessageInfo("Se ha asignado un permiso al perfil: " + perfil.getDescripcion());
        }

    }

    public void buscarPermisosPerfil() {

    }

//    public void buscarPermisosPerfil() {
//
//        obtenerPerfil();
//        crearMenu();
//        TreeMap<Menu, List<TreeMap<Menu, List<Menu>>>> lista = permisoBean.cargarMenu(perfil.getId());
//        if (lista != null && !lista.isEmpty()) {
//            for (Map.Entry<Menu, List<TreeMap<Menu, List<Menu>>>> menu : lista.entrySet()) {
//                for (TreeNode raiz : root.getChildren()) {
//                    if (menu.getKey().getId().equals(((Menu) raiz.getData()).getId())) {
//                        raiz.setSelected(true);
//                    }
//                    for (Map<Menu, List<Menu>> listaSub : menu.getValue()) {
//                        for (Map.Entry<Menu, List<Menu>> entry : listaSub.entrySet()) {
//                            for (TreeNode submenu : raiz.getChildren()) {
//                                if (entry.getKey().getId().equals(((Menu) submenu.getData()).getId())) {
//                                    submenu.setSelected(true);
//                                }
//
//                                for (Menu url : entry.getValue()) {
//                                    for (TreeNode node : submenu.getChildren()) {
//                                        if (url.getId().equals(((Menu) node.getData()).getId())) {
//                                            node.setSelected(true);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
    public void eliminarPermiso(Menu menu) {
        try {
            obtenerPerfil();

            List<Menu> selectedNodes = new ArrayList<Menu>();
            selectedNodes.add(menu);
            while (menu.getParent() != null && menu.getParent().getSubMenu() != null) {
                selectedNodes.add(menu.getParent());
                menu = menu.getParent();
            }
            for (Menu nodo : selectedNodes) {
                Permiso permiso = permisoBean.buscarMenuPerfil(perfil.getId(), nodo.getId());
                permisoBean.eliminarPermiso(permiso.getId());
            }
            cargarUrl(menu.getParent().getId());
            addMessageInfo("Se eliminó un permiso para el perfil: " + perfil.getDescripcion());

        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void obtenerPerfil() {

        perfilId = ValidacionesUtil.obtenerIdFromMap(mapaPerfil, perfilCadena);
        perfil = null;

        try {
            if (perfilId != null) {
                perfil = perfilBean.buscarPerfil(perfilId);

            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarPerfiles() {
        listaPerfil = new ArrayList<>();
        try {
            mapaPerfil = perfilBean.cargarPerfiles();
            for (Map.Entry<Long, String> p : mapaPerfil.entrySet()) {
                listaPerfil.add(p.getValue());
            }
            Collections.sort(listaPerfil);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public String mostrarTipoMenu(Menu menu) {
        if (menu.getRaiz() != null && menu.getRaiz()) {
            return "Raiz";
        } else if (menu.getSubMenu() != null && menu.getSubMenu()) {
            return "Sub Menu";
        } else {
            return "Url";
        }
    }

    public void cargarListaPadres() {
        listaPadres = permisoBean.cargarPadresMenu();
    }

    public void ingresarNuevoMenu() {
        permisoBean.crearMenu(etiqueta, descripcion, tipo, padreId, orden);
        cargarListaPadres();
        cargarMenuCompleto();
        etiqueta = "";
        descripcion = "";
        tipo = "";
        padreId = null;
        orden = null;
    }

    @PreDestroy
    public void limpiar() {
        crearMenu();
        perfilCadena = "";
    }

    //--------------------getters----------------------------------------
    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public Map<Long, String> getMapaPerfil() {
        return mapaPerfil;
    }

    public void setMapaPerfil(Map<Long, String> mapaPerfil) {
        this.mapaPerfil = mapaPerfil;
    }

    public List<String> getListaPerfil() {
        cargarPerfiles();
        return listaPerfil;
    }

    public void setListaPerfil(List<String> listaPerfil) {
        this.listaPerfil = listaPerfil;
    }

    public String getPerfilCadena() {
        return perfilCadena;
    }

    public void setPerfilCadena(String perfilCadena) {
        this.perfilCadena = perfilCadena;
    }

    public Long getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(Long perfilId) {
        this.perfilId = perfilId;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public List<Menu> getSub() {
        return sub;
    }

    public void setSub(List<Menu> sub) {
        this.sub = sub;
    }

    public List<Menu> getListaMenu() {
        return listaMenu;
    }

    public void setListaMenu(List<Menu> listaMenu) {
        this.listaMenu = listaMenu;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPadreId() {
        return padreId;
    }

    public void setPadreId(Long padre) {
        this.padreId = padre;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public List<Menu> getListaPadres() {
        return listaPadres;
    }

    public void setListaPadres(List<Menu> listaPadres) {
        this.listaPadres = listaPadres;
    }

}
