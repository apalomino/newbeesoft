/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Bean.ActorBean;
import com.beesoft.beesoft.gestiondocumental.Bean.NotificationMailBean;
import com.beesoft.beesoft.gestiondocumental.Bean.ReportesBean;
import com.beesoft.beesoft.gestiondocumental.Bean.UsuarioBean;
import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.PerfilLocal;
import com.beesoft.beesoft.gestiondocumental.Local.PermisoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ReportesLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UsuarioLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.NotificationMailDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RecorridoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import com.beesoft.beesoft.gestiondocumental.entidades.DatosConfiguracion;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Menu;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import com.beesoft.beesoft.gestiondocumental.entidades.Permiso;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import com.ibm.icu.text.DateFormat;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "usuario")
@SessionScoped
public class UsuarioAction implements Serializable {

	private static final Log LOG = LogFactory.getLog(UsuarioAction.class);

	@EJB
	private UsuarioLocal session;

	@EJB
	private UtilidadesLocal utilidades;

	@EJB
	private ActorLocal actorBean;

	@EJB
	private PerfilLocal perfilBean;

	@EJB
	private PermisoLocal permisoBean;

	@EJB
	private DocumentoLocal documentoBean;

	@EJB
	private ReportesLocal reportesBean;

	@EJB
	private UtilidadesLocal utilidadBean;

	@Inject
	private ApplicationAction application;

	private String username;
	private String password;
	private String newPassword;
	private String confirmPassword;
	private Long perfilId;
	private Perfil perfil;
	private Usuario user;
	private String tipoNavi;
	private String fechaIngreso;
	private String nombreInfo;
	private Perfil perfilInfo;
	private Actor actor;
	private Long actorCadena;
	private String perfilCadena;
	private String email;
	private List<String> listaPerfil;
	private List<String> listaActores;

	private Map<Long, String> mapaPerfil = new HashMap<Long, String>();
	private Map<Long, String> mapaActor = new HashMap<Long, String>();

	private Boolean showAdmin;

	private String radicacion;
	private Boolean sesionCorreo;
	private Boolean sesionResponsable;
	private String methodi;

	private final int goCreate = 2;

	MenuModel model;

	private String nombreUsuario;
	private String userName;
	private String perfilConsultaCadena;
	private Perfil perfilConsulta;
	private Perfil perfilEditar;
	private String perfilEditarCadena;
	private List<Usuario> listaUsuarios;
	List<Documento> documentosInbox;
	List<Documento> documentosInboxFiltered;
	private Boolean gestionBuscar;
	private Boolean gestionInbox;
	private Boolean inbox;
	private List<String> listaImagenes;
	private Date fechaCron;

	public UsuarioAction() {

	}

	public String getMethodi() {
		radicacion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("radicacion");
		sesionCorreo = Boolean.parseBoolean(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sesionCorreo"));
		sesionResponsable = Boolean.parseBoolean(FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get("sesionResponsable"));
		return methodi;
	}

	public void limpiar() {
		user = new Usuario();
		username = "";
		password = "";
		perfilCadena = null;
		actorCadena = null;
	}

	public String login() {
		try {
			user = session.login(username, password);
			if (user != null) {
				nombreInfo = user.getActor().getNombreYApellido();
				perfilInfo = user.getPerfil();
				username = "";
				password = "";
				SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("ES"));
				Date fecha = new Date();
				fechaIngreso = formateador.format(fecha);

				DatosConfiguracion d = new DatosConfiguracion();
				d = utilidadBean.mostrarConfiguracionCorreo();
				fechaCron = d.getFechaCron();
				String fc = fechaCron.toString();
				SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
				Date inicio = formatoFecha.parse(fc);

				Date fhoy = new Date();
				// ****CRON
				int dias = (int) ((inicio.getTime() - fhoy.getTime()) / 86500000);
				if (dias != 0)
					correrCron(d);
				showMenu();
				crearMenu();
				// buscarDocumentoInbox();
				return "success";
			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}

		return null;
	}

	public void correrCron(DatosConfiguracion d) {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					enviarCorreoPQRSDVencidos();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, 3000, 86400000);
		// Acá se actualiza la tabla ConfiguracionCorreo
		fechaCron = new Date();
		String emailEmisor = d.getEmailEmisor();
		String host = d.getHost();
		String puerto = d.getPuerto();
		String starttls = d.getStarttls();
		String authetication = d.getAuthetication();
		String newPassword = d.getPassword();
		utilidadBean.editarConfigCorreos(emailEmisor, host, newPassword, puerto, starttls, authetication, fechaCron);
	}

	public void enviarCorreoPQRSDVencidos() throws Exception {
		Oficina oficina = null;
		NotificationMailBean smail = new NotificationMailBean();
		NotificationMailDTO mail = new NotificationMailDTO();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaI = (Date) formato.parse("01/01/2021");
		Calendar cal = Calendar.getInstance();
		Date fechaF = (Date) cal.getTime();
		List<Trd> listaDocumentoVencidos = reportesBean.reportePQRSDVencidos(fechaI, fechaF, oficina);
		if (listaDocumentoVencidos == null) {
			return;
		}
		for (Trd trd : listaDocumentoVencidos) {
			try {

				RecorridoDTO dto = new RecorridoDTO();
				Date fechaFinHabil = ReportesAction.fechaDiasHabiles(trd.getDocumento().getFechaRecepcion());
				dto.setDiasVencimiento(ReportesAction.numeroDiasHabiles(fechaF, fechaFinHabil));

				if (dto.getDiasVencimiento() > 0) {
					dto.setRadicacion(trd.getDocumento().getRadicacion());
					dto.setFechalimite(ValidacionesUtil.formatearFechaHora(trd.getDocumento().getFechaControl()));
					List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(trd.getDocumento().getId());
					List<Actor> destinos = documentoBean.consultarDestinoDocumento(trd.getDocumento().getId());
					dto.setProcedencia(ReportesAction.listToCadena(procedencia));
					dto.setDestino(ReportesAction.listToCadena(destinos));
					dto.setDependencia(trd.getOficina().getDescripcion());
					dto.setClase(trd.getDocumento().getClasedocumento());
					List<String> lDestino = new ArrayList<>();
					for (int i = 0; i < destinos.size(); i++) {
						lDestino.add(destinos.get(i).getEmail());
					}
					dto.setMails(lDestino);

					mail.setDestinatarios(lDestino);
					String mensajeMail = documentoBean.mensajeCorreoDocVencido(destinos, trd.getDocumento(),
							dto.getDiasVencimiento());
					mail.setMensaje(mensajeMail);
					List<ArchivosAdjuntos> adjuntos = mail.getAdjuntos();
					String mensaje = mail.getMensaje();
					List<String> correos = new ArrayList<>(mail.getDestinatarios());

					DatosConfiguracion configuracion = new DatosConfiguracion();
					javax.mail.Session session = null;
					Properties props = new Properties();
					configuracion = utilidadBean.mostrarConfiguracionCorreo();
					mail.setEmailEmisor(configuracion.getEmailEmisor());
					String emisor = configuracion.getEmailEmisor();

					props.put(ConstantsMail.AUTH, configuracion.getAuthetication());
					props.put(ConstantsMail.STARTTLS, configuracion.getStarttls());
					props.put(ConstantsMail.HOST, configuracion.getHost());
					props.put(ConstantsMail.PORT, configuracion.getPuerto());
					props.put(ConstantsMail.USER, configuracion.getEmailEmisor());
					props.put(ConstantsMail.PASSWORD, configuracion.getPassword());
					session = javax.mail.Session.getInstance(props);
					MimeMessage message = new MimeMessage(session);
					message.setFrom(new InternetAddress(emisor));
					message.addRecipients(Message.RecipientType.TO, NotificationMailBean.correosDestino(correos));
					message.setSubject(mail.getAsunto());
					if (adjuntos != null) {
						message.setContent(NotificationMailBean.procesarAdjuntosCompletos(adjuntos, mensaje),
								"multipart/form-data");
					} else {
						message.setContent(NotificationMailBean.procesarAdjuntosCompletos(null, mensaje),
								"multipart/form-data");
					}
					Transport t = session.getTransport("smtp");
					t.connect((String) props.get(ConstantsMail.HOST), (String) props.get(ConstantsMail.USER),
							(String) props.get(ConstantsMail.PASSWORD));
					t.sendMessage(message, message.getAllRecipients());
					t.close();
				}
			} catch (Exception e) {
				LOG.info(e);
			}
		}

	}

	public void reestablecerPassword() {
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			boolean loggedIn = false;
			if (ValidacionesUtil.validarPatronCorreo(email)) {
				session.enviarCorreoCambioPassword(email);
				ValidacionesUtil.addMessageInfo("La nueva contraseña se envio a su correo");
				loggedIn = true;
				context.addCallbackParam("loggedIn", loggedIn);
			} else {
				ValidacionesUtil.addMessage("La estructura del email es incorrecta");
			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
	}

	public void buscarDocumentoInbox() {
		try {
			gestionBuscar = false;
			gestionInbox = true;
			Actor actorRevisor = user.getActor();
			documentosInbox = new ArrayList<>();
			documentosInbox = documentoBean.buscarDocumentosPendientes(actorRevisor);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String validarFechaControl(Documento doc) {
		if (doc != null) {
			int dias = (int) ((doc.getFechaControl().getTime() - new Date().getTime()) / 86400000);
			if (doc.getActivo()) {
				if (dias >= 10) {
					return "background-color: green;padding: 5px 5px 5px 5px;";
				} else if (dias <= 10 && dias >= 3) {
					return "background-color: yellow;padding: 5px 5px 5px 5px;";
				} else if (dias < 3) {
					return "background-color: red;padding: 5px 5px 5px 5px;";
				}
			}
		}
		return "";
	}

	public String volver() {
		if (gestionInbox) {
			buscarDocumentoInbox();
			return "volverInbox";
		}
		return "";
	}

	public Boolean getInbox() {
		buscarDocumentoInbox();
		Perfil perfil = user.getPerfil();
		Permiso p = permisoBean.buscarMenuPerfil(perfil.getId(), 11l);
		if (p != null) {
			inbox = true;
		}
		return inbox;
	}

	public void cambiarEstado(Long id, Boolean estado) {
		session.cambiarEstado(id, estado);
		application.buscarResponsable();
		application.cargarRevisores();
		application.cargarListaActores();
		buscarUsuario();
		cargarExternos();
	}

	@PostConstruct
	public void iniciar() {
		listaImagenes = new ArrayList<>();
		listaImagenes.add("edeqprincipal.png");
		// listaImagenes.add("trabajadores.png");
		// listaImagenes.add("clientes.png");
	}

	@PreDestroy
	public String cerrarSession() {
		user = null;
		perfil = null;
		nombreInfo = "";
		perfilInfo = null;
		session.logout();
		return ConstantsMail.LOGOUT;
	}

	public void crearUsuario() {
		int validate = 0;

		validate += ValidacionesUtil.validarNulo(perfilCadena, "Perfil");
		validate += ValidacionesUtil.validarNulo(actorCadena, "Actor");

		if (goCreate == validate) {
			try {
				obtenerActor();
				perfil = obtenerPerfil(perfilCadena);
				if (StringUtils.isNotBlank(actor.getEmail())) {
					username = actor.getEmail();
					password = "beesoft";
					session.crearUsuario(username, password, perfil, actor);
					ValidacionesUtil.addMessageInfo("El usuario " + username + " fue creado!");
					cargarExternos();
					limpiar();
				} else {
					ValidacionesUtil
							.addMessage("La usuario " + actor.getNombreYApellido() + " no tiene correo electronico");
				}

			} catch (Exception ex) {
				LOG.info(ex);
				ValidacionesUtil.addMessage(ex.getMessage());
			}
		}
	}

	public void cargarExternos() {
		application.cargarListaActoresSinUsuario();
		application.buscarResponsable();
		application.cargarRevisores();
		application.buscarDestinatarios();
		application.cargarJefes();
		application.buscarDestinatarios();
		application.llenarInformacionDestinoDto();
	}

	public Perfil obtenerPerfil(String cadena) {
		Long perfilId = ValidacionesUtil.obtenerIdFromMap(application.getMapaPerfil(), cadena);
		Perfil perfil = null;

		try {
			if (perfilId != null) {
				perfil = perfilBean.buscarPerfil(perfilId);
				return perfil;
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public void obtenerActor() {
		try {
			actor = actorBean.buscarActor(actorCadena);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cambiarPasswordUsuario() {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean loggedIn = false;
		FacesMessage msg = null;
		if (user != null) {
			if (StringUtils.isNotBlank(newPassword)) {
				if (StringUtils.isNotBlank(confirmPassword)) {
					if (newPassword.equals(confirmPassword)) {
						session.cambiarPasswordUsuario(user, newPassword);
						ValidacionesUtil.addMessageInfo("Su contraseña ha sido editada");
						newPassword = "";
						confirmPassword = "";
						loggedIn = true;
					} else {
						ValidacionesUtil.addMessage("Las contraseñas ingresadas no coinciden");
					}
				} else {
					ValidacionesUtil.addMessage("Campo obligatorio: Confirmar Contraseña");
				}
			} else {
				ValidacionesUtil.addMessage("Campo obligatorio: Nueva Contraseña");
			}
			context.addCallbackParam("loggedIn", loggedIn);
		}
	}

	public void showMenu() {
		if (perfilInfo.getDescripcion().equals(ConstantsMail.ADMIN)) {
			showAdmin = true;
		} else {
			showAdmin = false;
		}
	}

	public void crearMenu() {
		try {
			model = new DefaultMenuModel();

			List<Menu> lista = permisoBean.cargarMenu(user.getPerfil().getId());
			Map<Long, Menu> subMenuMap = new TreeMap<>();
			Map<Long, Menu> raizMenuMap = new TreeMap<>();
			for (Menu sub : lista) {
				subMenuMap.put(sub.getParent().getId(), sub.getParent());
			}
			for (Entry<Long, Menu> sub : subMenuMap.entrySet()) {
				raizMenuMap.put(sub.getValue().getParent().getId(), sub.getValue().getParent());
			}

			if (lista != null && !lista.isEmpty()) {
				for (Entry<Long, Menu> raiz : raizMenuMap.entrySet()) {
					DefaultSubMenu MenuRaiz = new DefaultSubMenu();
					MenuRaiz.setLabel(raiz.getValue().getEtiqueta());
					for (Entry<Long, Menu> sub : subMenuMap.entrySet()) {
						DefaultSubMenu subMenu = new DefaultSubMenu();
						subMenu.setLabel(sub.getValue().getEtiqueta());
						for (Menu menu : lista) {
							if (menu.getParent().getId().equals(sub.getKey())) {
								DefaultMenuItem menuItem = new DefaultMenuItem();
								menuItem.setValue(menu.getEtiqueta());
								menuItem.setUrl(menu.getDescripcion());
								subMenu.addElement(menuItem);
							}
						}
						if (raiz.getValue().getId().equals(sub.getValue().getParent().getId())) {
							MenuRaiz.addElement(subMenu);
						}
					}
					model.addElement(MenuRaiz);
				}

			}

		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}

	}

	public void buscarUsuario() {
		try {
			if (perfilConsultaCadena != null) {
				perfilConsulta = obtenerPerfil(perfilConsultaCadena);
			}
			listaUsuarios = new ArrayList<>();
			listaUsuarios = session.buscarUsuarios(nombreUsuario, userName, perfilConsulta);
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
	}

	public void asignarDatos(Perfil perfil) {
		perfilEditar = perfil;
		perfilEditarCadena = perfilEditar.getDescripcion();

	}

	public void onRowEdit(RowEditEvent event) {
		try {
			Usuario usuario = (Usuario) event.getObject();
			perfilEditar = obtenerPerfil(perfilEditarCadena);
			session.editarUsuario(usuario, perfilEditar);
			buscarUsuario();
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
	}

	public void reestablecerPassword(Usuario user) {
		session.editarUsuario(user, "beesoft");
	}

	public void reset() {
		perfilEditarCadena = null;
		perfilConsultaCadena = null;
		perfilEditar = null;
		perfilConsulta = null;
		nombreUsuario = "";
		userName = "";
		listaUsuarios = new ArrayList<Usuario>();
	}

	public void cancelarPass() {
		email = "";
	}

	public List<String> getListaActores() {
		return listaActores;
	}

	public List<String> getListaPerfil() {
		return listaPerfil;
	}

	public void setListaPerfil(List<String> listaPerfil) {
		this.listaPerfil = listaPerfil;
	}

	public void setListaActores(List<String> listaActores) {
		this.listaActores = listaActores;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getPerfilId() {
		return perfilId;
	}

	public void setPerfilId(Long perfil) {
		this.perfilId = perfil;
	}

	public String getTipoNavi() {
		return tipoNavi;
	}

	public void setTipoNavi(String tipoNavi) {
		this.tipoNavi = tipoNavi;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNombreInfo() {
		return nombreInfo;
	}

	public void setNombreInfo(String nombreInfo) {
		this.nombreInfo = nombreInfo;
	}

	public Perfil getPerfilInfo() {
		return perfilInfo;
	}

	public void setPerfilInfo(Perfil perfilInfo) {
		this.perfilInfo = perfilInfo;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Long getActorCadena() {
		return actorCadena;
	}

	public void setActorCadena(Long actorCadena) {
		this.actorCadena = actorCadena;
	}

	public String getPerfilCadena() {
		return perfilCadena;
	}

	public void setPerfilCadena(String perfilCadena) {
		this.perfilCadena = perfilCadena;
	}

	public Map<Long, String> getMapaPerfil() {
		return mapaPerfil;
	}

	public void setMapaPerfil(Map<Long, String> mapaPerfil) {
		this.mapaPerfil = mapaPerfil;
	}

	public Map<Long, String> getMapaActor() {
		return mapaActor;
	}

	public void setMapaActor(Map<Long, String> mapaActor) {
		this.mapaActor = mapaActor;
	}

	public Boolean getShowAdmin() {
		return showAdmin;
	}

	public void setShowAdmin(Boolean showAdmin) {
		this.showAdmin = showAdmin;
	}

	public String getRadicacion() {
		return radicacion;
	}

	public void setRadicacion(String radicacion) {
		this.radicacion = radicacion;
	}

	public Boolean getSesionCorreo() {
		return sesionCorreo;
	}

	public void setSesionCorreo(Boolean sesionCorreo) {
		this.sesionCorreo = sesionCorreo;
	}

	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}

	public Boolean getSesionResponsable() {
		return sesionResponsable;
	}

	public void setSesionResponsable(Boolean sesionResponsable) {
		this.sesionResponsable = sesionResponsable;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPerfilConsultaCadena() {
		return perfilConsultaCadena;
	}

	public void setPerfilConsultaCadena(String perfilConsultaCadena) {
		this.perfilConsultaCadena = perfilConsultaCadena;
	}

	public Perfil getPerfilConsulta() {
		return perfilConsulta;
	}

	public void setPerfilConsulta(Perfil perfilConsulta) {
		this.perfilConsulta = perfilConsulta;
	}

	public Perfil getPerfilEditar() {
		return perfilEditar;
	}

	public void setPerfilEditar(Perfil perfilEditar) {
		this.perfilEditar = perfilEditar;
	}

	public String getPerfilEditarCadena() {
		return perfilEditarCadena;
	}

	public void setPerfilEditarCadena(String perfilEditarCadena) {
		this.perfilEditarCadena = perfilEditarCadena;
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Boolean getGestionBuscar() {
		return gestionBuscar;
	}

	public void setGestionBuscar(Boolean gestionBuscar) {
		this.gestionBuscar = gestionBuscar;
	}

	public Boolean getGestionInbox() {
		return gestionInbox;
	}

	public void setGestionInbox(Boolean gestionInbox) {
		this.gestionInbox = gestionInbox;
	}

	public void setInbox(Boolean inbox) {
		this.inbox = inbox;
	}

	public List<Documento> getDocumentosInbox() {
		return documentosInbox;
	}

	public void setDocumentosInbox(List<Documento> documentosInbox) {
		this.documentosInbox = documentosInbox;
	}

	public List<String> getListaImagenes() {
		return listaImagenes;
	}

	public void setListaImagenes(List<String> listaImagenes) {
		this.listaImagenes = listaImagenes;
	}

	public List<Documento> getDocumentosInboxFiltered() {
		return documentosInboxFiltered;
	}

	public void setDocumentosInboxFiltered(List<Documento> documentosInboxFiltered) {
		this.documentosInboxFiltered = documentosInboxFiltered;
	}

	public Date getFechaCron() {
		return fechaCron;
	}

	public void setFechaCron(Date fechaCron) {
		this.fechaCron = fechaCron;
	}

}
