package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 * The persistent class for the actor database table.
 *
 */
@Entity
public class Actor implements Serializable, Comparable<Actor> {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Boolean estado;

    private String cedula;

    private String direccion1;

    private String email;

    private String nombreYApellido;

    private Ciudad ciudad;

    private TipoPersona tipopersona;

    private String telefono;

    private String telefonoExt;

    private Actor Jefe;

    private Actor Empresa;

    private String Cargo;

    private Oficina oficina;

    private String firma;
//    private byte[] firma;

    public Actor() {
    }

    public Actor(String cedula, String direccion1, String email, String nombreYApellido, Ciudad ciudad, TipoPersona tipopersona) {
        this.cedula = cedula;
        this.direccion1 = direccion1;
        this.email = email;
        this.nombreYApellido = nombreYApellido;
        this.ciudad = ciudad;
        this.tipopersona = tipopersona;
    }
    
    public Actor(Long id, String cedula, String direccion1, String email, String nombreYApellido, Ciudad ciudad, TipoPersona tipopersona, Boolean estado) {
        this.id = id;
        this.cedula = cedula;
        this.direccion1 = direccion1;
        this.email = email;
        this.nombreYApellido = nombreYApellido;
        this.ciudad = ciudad;
        this.tipopersona = tipopersona;
        this.estado = estado;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCedula() {
        return this.cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getNombreYApellido() {
        return this.nombreYApellido;
    }

    public void setNombreYApellido(String nombreYApellido) {
        this.nombreYApellido = nombreYApellido;
    }

    @ManyToOne
    public Ciudad getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    @OneToOne
    public TipoPersona getTipopersona() {
        return this.tipopersona;
    }

    public void setTipopersona(TipoPersona tipopersona) {
        this.tipopersona = tipopersona;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Boolean getEstado() {
        return estado;
    }

    @OneToOne
    public Actor getJefe() {
        return Jefe;
    }

    public void setJefe(Actor jefe) {
        Jefe = jefe;
    }

    @OneToOne
    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return nombreYApellido;
    }

    @OneToOne
    public Actor getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(Actor Empresa) {
        this.Empresa = Empresa;
    }

    public String getTelefonoExt() {
        return telefonoExt;
    }

    public void setTelefonoExt(String telefonoExt) {
        this.telefonoExt = telefonoExt;
    }

    public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}
    
//    @Lob
//    @Type(type = "org.hibernate.type.BinaryType")
//    @Column(length = 100000)
//    public byte[] getFirma() {
//        return this.firma;
//    }
//
//    public void setFirma(byte[] firma) {
//        this.firma = firma;
//    }


	@Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Actor o) {
        return id.compareTo(o.getId());
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
}
