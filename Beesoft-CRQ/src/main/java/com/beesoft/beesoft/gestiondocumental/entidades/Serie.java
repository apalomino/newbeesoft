package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * The persistent class for the serie database table.
 *
 */
@Entity
public class Serie implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Boolean activo;

    private String descripcion;

    private String codigo;

    private Oficina oficina;
    
    private Boolean esretencion;

    public Serie() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ManyToOne
    public Oficina getOficina() {
        return this.oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    public Boolean getEsretencion() {
    	return esretencion;
    }
    
    public void setEsretencion(Boolean estado) {
    	this.esretencion = estado;
    }

}
