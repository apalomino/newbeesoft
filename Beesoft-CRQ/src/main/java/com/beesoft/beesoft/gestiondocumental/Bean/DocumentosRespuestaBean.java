/**
 * 
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.beesoft.beesoft.gestiondocumental.Local.DocumentosRespuestaLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentosRespuesta;
import com.beesoft.beesoft.gestiondocumental.entidades.Permiso;

/**
 * @author Sumset
 *
 */
@Stateless
public class DocumentosRespuestaBean implements DocumentosRespuestaLocal {
	
private static final Log LOG = LogFactory.getLog(DocumentosRespuestaBean.class);
	
	@PersistenceContext(unitName = "BeesoftPU")
	private EntityManager em;
	
	@Override
	public void crearDocumentoRespuesta(Long docResp, Long docOrigen, String radicado) throws Exception {
		
		DocumentosRespuesta dr = new DocumentosRespuesta();
		dr.setDocumentoRespuesta(docResp);
		dr.setDocumentoOrigen(docOrigen);
		dr.setRadicado(radicado);
		em.persist(dr);
	}
	
//	@Override
//	public List<DocumentosRespuesta> consultarRespuestasDocumento(Long idDoc) throws Exception {
//		List<DocumentosRespuesta> lista = em.createQuery("Select d from DocumentosRespuesta d"
//				+" where d.documentoOrigen = :id").setParameter("id", idDoc).getResultList();
//		if (lista != null && !lista.isEmpty()) {
//			return lista;
//		} else {
//			return null;
//		}
//	}
	
	@Override
	public List<DocumentosRespuesta> consultarRespuesta(Long idDoc) throws Exception {
		List<DocumentosRespuesta> doc = em.createQuery("Select d from DocumentosRespuesta d"
				+ " where d.documentoRespuesta = :id").setParameter("id", idDoc).getResultList();
		if (doc != null && !doc.isEmpty()) {
			return doc;
		} else {
			return null;
		}
	}
	
	@Override
	public Long consultaIdRespuesta(Long idOrigen, Long idResp) throws Exception {
		List<Long> idR = em.createQuery("Select d.id from DocumentosRespuesta d"
				+ " where d.documentoOrigen =:idO and d.documentoRespuesta =:idR")
				.setParameter("idO", idOrigen).setParameter("idR", idResp) .getResultList();
		if (idR.size() > 0) {
			Long id = idR.get(0);
			return id;
		} else {
			return null;
		}
	}
	
	@Override
	public void eliminarRegRespuesta(Long idR) throws Exception {
		DocumentosRespuesta doc = em.find(DocumentosRespuesta.class, idR);

        if (doc != null) {
            em.remove(doc);
        }
	}
	
}
