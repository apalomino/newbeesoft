/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.DatosConfiguracion;
import com.beesoft.beesoft.gestiondocumental.entidades.LetraRadicacion;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "utilidadAction")
@SessionScoped
public class UtilidadAction implements Serializable {
    
    private static final Log LOG = LogFactory.getLog(UtilidadAction.class);
    
    @EJB
    private UtilidadesLocal utilidadBean;
    
    private Usuario usuarioLogin;
    private List<LetraRadicacion> listaLetras;
    private String emailEmisor;
    private String host;
    private String password;
    private String puerto;
    private String starttls;
    private String authetication;
    private Date fechaCron;
    
    private String letra;
    private Long numeroSecuencia;
    
    @PostConstruct
    public void init() {
        if (FacesContext.getCurrentInstance() != null) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                    .getExternalContext().getSession(false);
            usuarioLogin = (Usuario) session.getAttribute("username");
        }
        listarLetras();
        obtenerConfigCorreo();
    }
    
    public void listarLetras() {
        listaLetras = utilidadBean.listarLetrasRad();
    }
    
    public void obtenerConfigCorreo() {
        DatosConfiguracion config = utilidadBean.mostrarConfiguracionCorreo();
        if (config != null) {
            host = config.getHost();
            emailEmisor = config.getEmailEmisor();
            puerto = config.getPuerto();
            starttls = config.getStarttls();
            authetication = config.getAuthetication();
            fechaCron = config.getFechaCron();
        }
    }
    
    public void onRowEdit(RowEditEvent event) {
        try {
            LetraRadicacion letraEdit = (LetraRadicacion) event.getObject();
            utilidadBean.editarLetrasRad(letraEdit.getLetra(), letraEdit.getNumeroSecuencia());
        } catch (Exception ex) {
            LOG.info(ex);
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }
    
    public void editarCorreo() {
        utilidadBean.editarConfigCorreos(emailEmisor, host, password, puerto, starttls, authetication, fechaCron);
    }
    
    public void crearLetra() {
        utilidadBean.crearLetrasRad(letra);
        listarLetras();
    }
    
    public void quitarLetra(LetraRadicacion letra) {
        utilidadBean.quitarLetrasRad(letra.getId());
        listarLetras();
    }
    //--------getter and setter-------------------------------

    public Usuario getUsuarioLogin() {
        return usuarioLogin;
    }
    
    public void setUsuarioLogin(Usuario usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }
    
    public List<LetraRadicacion> getListaLetras() {
        return listaLetras;
    }
    
    public void setListaLetras(List<LetraRadicacion> listaLetras) {
        this.listaLetras = listaLetras;
    }
    
    public String getEmailEmisor() {
        return emailEmisor;
    }
    
    public void setEmailEmisor(String emailEmisor) {
        this.emailEmisor = emailEmisor;
    }
    
    public String getHost() {
        return host;
    }
    
    public void setHost(String host) {
        this.host = host;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPuerto() {
        return puerto;
    }
    
    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }
    
    public String getStarttls() {
        return starttls;
    }
    
    public void setStarttls(String starttls) {
        this.starttls = starttls;
    }
    
    public String getAuthetication() {
        return authetication;
    }
    
    public void setAuthetication(String authetication) {
        this.authetication = authetication;
    }
    
    public String getLetra() {
        return letra;
    }
    
    public void setLetra(String letra) {
        this.letra = letra;
    }
    
    public Long getNumeroSecuencia() {
        return numeroSecuencia;
    }
    
    public void setNumeroSecuencia(Long numeroSecuencia) {
        this.numeroSecuencia = numeroSecuencia;
    }

	public Date getFechaCron() {
		return fechaCron;
	}

	public void setFechaCron(Date fechaCron) {
		this.fechaCron = fechaCron;
	}
    
}
