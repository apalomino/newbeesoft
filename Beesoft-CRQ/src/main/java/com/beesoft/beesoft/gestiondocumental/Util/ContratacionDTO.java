/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author Cristhian
 */
public class ContratacionDTO {

    private String contratacionInfo;
    private String numero;
    private String fecha;
    private String radicacion;
    private String procedencia;
    private String destino;
    private String asunto;
    private String anexos;

    public ContratacionDTO() {
    }

    public ContratacionDTO(String contratacionInfo, String numero, String fecha, String radicacion, String procedencia, String destino, String asunto, String anexos) {
        this.contratacionInfo = contratacionInfo;
        this.numero = numero;
        this.fecha = fecha;
        this.radicacion = radicacion;
        this.procedencia = procedencia;
        this.destino = destino;
        this.asunto = asunto;
        this.anexos = anexos;
    }

    
    
    public String getContratacionInfo() {
        return contratacionInfo;
    }

    public void setContratacionInfo(String contratoInfo) {
        this.contratacionInfo = contratoInfo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getAnexos() {
        return anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }

    
    
}
