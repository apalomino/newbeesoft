/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Serie;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerie;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoRecorrido;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "oficinaAction")
@SessionScoped
public class OficinaAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(OficinaAction.class);

    @EJB
    private transient OficinaLocal oficinaBean;

    @Inject
    private ApplicationAction application;

    private String recorridoCadena;
    private String oficinaCadena;
    private String descripcion;
    private String oficinaDescripcion;
    private String serieCadena;
    private String subSerieCadena;
    private String tipoCadena;

    private TipoRecorrido tipoRecorrido;
    private Oficina oficina;
    private Serie serie;
    private SubSerie subserie;
    private TipoDocumento tipoDocumento;

    private List<TipoRecorrido> listaRecorridos;
    private List<Oficina> listaOficina;
    private List<Serie> listaSerie;
    private List<SubSerie> listaSubSerie;
    private List<TipoDocumento> listaTipoDocumento;
    private List<Oficina> listaOficinaFiltered;
    private List<Serie> listaSerieFiltered;
    private List<SubSerie> listaSubSerieFiltered;
    private List<TipoDocumento> listaTipoDocumentoFiltered;
    private String crear;
    private String tipo;
    private String oficinaValue;
    private String serieValue;
    private String subserieValue;
    private String tipoDocValue;
    private String oficinaCode;
    private String serieCode;
    private String subserieCode;

    @PostConstruct
    public void inicializar() {
        llenarTablaOficina(1l);
    }

    public void crearRecorrido() {
        try {
            int validacion = ValidacionesUtil.validarCadena(recorridoCadena, "Descripción");
            if (validacion == 1) {
                oficinaBean.crearRecorrido(recorridoCadena);
                llenarTablaRecorrido();
                reset();
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void crearOficina() {
        try {
            int validacion = ValidacionesUtil.validarCadena(oficinaValue, "Descripción");
            validacion += ValidacionesUtil.validarCadena(oficinaCode, "Codigo");
            if (validacion == 2) {
                tipoRecorrido = oficinaBean.buscarTipoRecorrido(1l);
                oficinaBean.crearOficina(oficinaCode, oficinaValue, tipoRecorrido);
                llenarTablaOficina(1l);
                cargarExternos();
                oficinaValue = "";
                oficinaCode = "";
                ValidacionesUtil.addMessageInfo("Se creo con exito");
            }
        } catch (Exception ex) {
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void cargarExternos() {
        application.cargarOficinas();
    }

    public void crearSerie() {
        try {
            int validacion = ValidacionesUtil.validarCadena(serieValue, "Descripción");
            validacion += ValidacionesUtil.validarCadena(serieCode, "Codigo");
            if (validacion == 2) {
                oficinaBean.crearSerie(serieCode, serieValue, oficina);
                llenarTablaSerie(oficina.getId());
                serieValue = "";
                serieCode = "";
                ValidacionesUtil.addMessageInfo("Se creo con exito");
            }
        } catch (Exception ex) {
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void crearSubSerie() {
        try {
            int validacion = ValidacionesUtil.validarCadena(subserieValue, "Descripción");
            validacion += ValidacionesUtil.validarCadena(subserieCode, "Codigo");
            if (validacion == 2) {
                oficinaBean.crearSubSerie(subserieCode, subserieValue, serie);
                llenarTablaSubSerie(serie.getId());
                subserieValue = "";
                subserieCode = "";
                ValidacionesUtil.addMessageInfo("Se creo con exito");
            }
        } catch (Exception ex) {
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void crearTipo() {
        try {

            int validacion = ValidacionesUtil.validarCadena(tipoDocValue, "Descripción");
            if (validacion == 1) {
                oficinaBean.crearTipo(tipoDocValue, subserie);
                llenarTablaTipoDoc(subserie.getId());
                tipoDocValue = "";
                ValidacionesUtil.addMessageInfo("Se creo con exito");
            }
        } catch (Exception ex) {
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public String onFlowProcess(FlowEvent event) {
        switch (event.getNewStep()) {
            case "serieTab":
                if (oficina != null) {
                    llenarTablaSerie(oficina.getId());
                    return event.getNewStep();
                } else {
                    ValidacionesUtil.addMessage("Seleccione una oficina");
                    return "oficinaTab";
                }
            case "subTab":
                if (serie != null) {
                    llenarTablaSubSerie(serie.getId());
                    return event.getNewStep();
                } else {
                    ValidacionesUtil.addMessage("Seleccione una serie");
                    return "serieTab";
                }
            case "tipoDocTab":
                if (subserie != null) {
                    llenarTablaTipoDoc(subserie.getId());
                    return event.getNewStep();
                } else {
                    ValidacionesUtil.addMessage("Seleccione una subserie");
                    return "subTab";
                }
            default:
                return event.getNewStep();
        }
    }

    public void llenarTablaRecorrido() {
        try {
            listaRecorridos = oficinaBean.listarRecorridos();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public List<Oficina> llenarTablaOficina(Long id) {
        try {
            listaOficina = new ArrayList<>();
            listaOficina = oficinaBean.listarOficinas(id);
            return listaOficina;
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;
    }

    public List<Serie> llenarTablaSerie(Long id) {
        try {
            listaSerie = new ArrayList<>();
            listaSerie = oficinaBean.listarSeries(id);
            return listaSerie;
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;
    }

    public List<SubSerie> llenarTablaSubSerie(Long id) {
        try {
            listaSubSerie = new ArrayList<>();
            listaSubSerie = oficinaBean.listarSubSeries(id);
            return listaSubSerie;
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;
    }

    public List<TipoDocumento> llenarTablaTipoDoc(Long id) {
        try {
            listaTipoDocumento = new ArrayList<>();
            listaTipoDocumento = oficinaBean.listarTipoDocumentos(id);
            return listaTipoDocumento;
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;
    }

    public void cambiarEstado(Long id, Boolean estado) {
        oficinaBean.cambiarEstado(id, estado);
        llenarTablaOficina(1l);
        cargarExternos();
    }

    public void asignarDatos(Oficina of) {
        oficinaDescripcion = of.getDescripcion();
    }

    public void onOficinaEdit(RowEditEvent event) {
        try {
            Oficina oficina = (Oficina) event.getObject();
            oficinaBean.editarOficina(oficina.getId(), oficina.getCodigo(), oficina.getDescripcion());
        } catch (Exception ex) {
            LOG.info(ex);
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void onSerieEdit(RowEditEvent event) {
        try {
            Serie serie = (Serie) event.getObject();
            oficinaBean.editarSerie(serie.getId(), serie.getCodigo(), serie.getDescripcion());
        } catch (Exception ex) {
            LOG.info(ex);
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void onSubSerieEdit(RowEditEvent event) {
        try {
            SubSerie sub = (SubSerie) event.getObject();
            oficinaBean.editarSubSerie(sub.getId(), sub.getCodigo(), sub.getDescripcion());
        } catch (Exception ex) {
            LOG.info(ex);
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void onTipoDocEdit(RowEditEvent event) {
        try {
            TipoDocumento tipoDoc = (TipoDocumento) event.getObject();
            oficinaBean.editarTipoDoc(tipoDoc.getId(), tipoDoc.getDescripcion());
        } catch (Exception ex) {
            LOG.info(ex);
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void reset() {
        recorridoCadena = "";
        oficinaCadena = "";
        serieCadena = "";
        subSerieCadena = "";
        tipoCadena = "";
    }

    //----------------------getter----------------------------
    public String getRecorridoCadena() {
        return recorridoCadena;
    }

    public void setRecorridoCadena(String recorridoCadena) {
        this.recorridoCadena = recorridoCadena;
    }

    public String getOficinaCadena() {
        return oficinaCadena;
    }

    public void setOficinaCadena(String oficinaCadena) {
        this.oficinaCadena = oficinaCadena;
    }

    public String getSerieCadena() {
        return serieCadena;
    }

    public void setSerieCadena(String serieCadena) {
        this.serieCadena = serieCadena;
    }

    public String getSubSerieCadena() {
        return subSerieCadena;
    }

    public void setSubSerieCadena(String subSerieCadena) {
        this.subSerieCadena = subSerieCadena;
    }

    public String getTipoCadena() {
        return tipoCadena;
    }

    public void setTipoCadena(String tipoCadena) {
        this.tipoCadena = tipoCadena;
    }

    public TipoRecorrido getTipoRecorrido() {
        return tipoRecorrido;
    }

    public void setTipoRecorrido(TipoRecorrido tipoRecorrido) {
        this.tipoRecorrido = tipoRecorrido;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public Serie getSerie() {
        return serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }

    public SubSerie getSubserie() {
        return subserie;
    }

    public void setSubserie(SubSerie subserie) {
        this.subserie = subserie;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public List<TipoRecorrido> getListaRecorridos() {
        return listaRecorridos;
    }

    public void setListaRecorridos(List<TipoRecorrido> listaRecorridos) {
        this.listaRecorridos = listaRecorridos;
    }

    public List<Oficina> getListaOficina() {
        return listaOficina;
    }

    public void setListaOficina(List<Oficina> listaOficina) {
        this.listaOficina = listaOficina;
    }

    public List<Serie> getListaSerie() {
        return listaSerie;
    }

    public void setListaSerie(List<Serie> listaSerie) {
        this.listaSerie = listaSerie;
    }

    public List<SubSerie> getListaSubSerie() {
        return listaSubSerie;
    }

    public void setListaSubSerie(List<SubSerie> listaSubSerie) {
        this.listaSubSerie = listaSubSerie;
    }

    public List<TipoDocumento> getListaTipoDocumento() {
        return listaTipoDocumento;
    }

    public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
        this.listaTipoDocumento = listaTipoDocumento;
    }

    public String getOficinaDescripcion() {
        return oficinaDescripcion;
    }

    public void setOficinaDescripcion(String oficinaDescripcion) {
        this.oficinaDescripcion = oficinaDescripcion;
    }

    public String getCrear() {
        llenarTablaOficina(1l);
        return crear;
    }

    public void setCrear(String crear) {
        this.crear = crear;
    }

    public String getOficinaValue() {
        return oficinaValue;
    }

    public void setOficinaValue(String oficinaValue) {
        this.oficinaValue = oficinaValue;
    }

    public String getSerieValue() {
        return serieValue;
    }

    public void setSerieValue(String serieValue) {
        this.serieValue = serieValue;
    }

    public String getTipoDocValue() {
        return tipoDocValue;
    }

    public void setTipoDocValue(String tipoDocValue) {
        this.tipoDocValue = tipoDocValue;
    }

    public String getSubserieValue() {
        return subserieValue;
    }

    public void setSubserieValue(String subserieValue) {
        this.subserieValue = subserieValue;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Oficina> getListaOficinaFiltered() {
        return listaOficinaFiltered;
    }

    public void setListaOficinaFiltered(List<Oficina> listaOficinaFiltered) {
        this.listaOficinaFiltered = listaOficinaFiltered;
    }

    public List<Serie> getListaSerieFiltered() {
        return listaSerieFiltered;
    }

    public void setListaSerieFiltered(List<Serie> listaSerieFiltered) {
        this.listaSerieFiltered = listaSerieFiltered;
    }

    public List<SubSerie> getListaSubSerieFiltered() {
        return listaSubSerieFiltered;
    }

    public void setListaSubSerieFiltered(List<SubSerie> listaSubSerieFiltered) {
        this.listaSubSerieFiltered = listaSubSerieFiltered;
    }

    public List<TipoDocumento> getListaTipoDocumentoFiltered() {
        return listaTipoDocumentoFiltered;
    }

    public void setListaTipoDocumentoFiltered(List<TipoDocumento> listaTipoDocumentoFiltered) {
        this.listaTipoDocumentoFiltered = listaTipoDocumentoFiltered;
    }

    public String getOficinaCode() {
        return oficinaCode;
    }

    public void setOficinaCode(String oficinaCode) {
        this.oficinaCode = oficinaCode;
    }

    public String getSerieCode() {
        return serieCode;
    }

    public void setSerieCode(String serieCode) {
        this.serieCode = serieCode;
    }

    public String getSubserieCode() {
        return subserieCode;
    }

    public void setSubserieCode(String subserieCode) {
        this.subserieCode = subserieCode;
    }

}
