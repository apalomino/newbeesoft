/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.ClaseDocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.ClasesDocumento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author SUMSET
 */
@Stateless
public class ClaseDocumentoBean implements ClaseDocumentoLocal {

    private static final Log LOG = LogFactory.getLog(DocumentoBean.class);

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

    @Override
    public boolean crearClaseDocumento(String name, String estado) {
        
        Query q = null;
        StringBuilder query = new StringBuilder("select cd from ClasesDocumento cd where upper(nombre)='"+name.toUpperCase()+"'");        
        q = em.createQuery(query.toString());
        List<ClasesDocumento> resultados = q.getResultList();
        
        if (resultados.size() <= 0) {
            ClasesDocumento claseDocumento = new ClasesDocumento();
            claseDocumento.setNombre(name);
            claseDocumento.setEstado(estado);
            em.persist(claseDocumento);
            return false;
        }   
        else
        return true;  
    }

    @Override
    public List<ClasesDocumento> listarClasesDocumento(String condicion) {
        Query q = null;
        StringBuilder query;
        if(condicion.equals("AI")){
            query = new StringBuilder("select cd from ClasesDocumento cd ");
        }
        else{
            query = new StringBuilder("select cd from ClasesDocumento cd where estado='activo' "); 
        }

        q = em.createQuery(query.toString());
        List<ClasesDocumento> resultados = q.getResultList();
        return resultados;
    }

    @Override
    public void CambioEstadoClaseDocumento(Long id, String estado) {
        ClasesDocumento cd = em.find(ClasesDocumento.class, id);
        cd.setEstado(estado);
        em.merge(cd);
    }

}
