/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Serie;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerie;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoRecorrido;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.TrdOld;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface OficinaLocal {

    /**
     * Metodo encargado de crear un recorrido.
     *
     * @param recorrido, descripcion del recorrido.
     * @throws Exception si hay algun error.
     */
    public void crearRecorrido(String recorrido) throws Exception;

    /**
     * Metodo encargado de crear un oficina de un recorrido.
     *
     * @param codigo
     * @param oficina, descripcion de la oficina.
     * @param recorrido, recorrido al que pertenece la oficina.
     * @throws Exception si hay algun error.
     */
    public void crearOficina(String codigo, String oficina, TipoRecorrido recorrido) throws Exception;

    /**
     * Metodo para editar oficinas.
     *
     * @param oficinaId
     * @param codigo
     * @param descripcion
     */
    public void editarOficina(Long oficinaId, String codigo, String descripcion);

    /**
     * Metodo encargado de crear una serie de una oficina.
     *
     * @param codigo
     * @param serie, descripcion de la serie.
     * @param oficina, oficina a la que pertenece la serie.
     * @throws Exception si hay algun error.
     */
    public void crearSerie(String codigo, String serie, Oficina oficina) throws Exception;

    /**
     * Metodo encargado de crear una subserie.
     *
     * @param codigo
     * @param subserie, descripcion de la subserie.
     * @param serie, serie a la que pertenece la subserie.
     * @throws Exception si hay algun error.
     */
    public void crearSubSerie(String codigo, String subserie, Serie serie) throws Exception;

    /**
     * Metodo encargado de crear tipo documental.
     *
     * @param tipo, descripcion del tipo.
     * @param subserie, subserie a la que pertenece el tipo.
     * @throws Exception si hay algun error.
     */
    public void crearTipo(String tipo, SubSerie subserie) throws Exception;

    /**
     * Metodo encargado de cargar las oficinas existentes.
     *
     * @return mapa que contiene el id y la descripcion de cada oficina.
     * @throws Exception si hay algun error.
     */
    public Map<Long, String> cargarOficinas() throws Exception;

    public Map<Long, String> cargarOficinasRecorridos() throws Exception;

    /**
     * Metodo encargado de buscar una oficina por su identificador.
     *
     * @param id, Identificador de la oficina.
     * @return oficina.
     * @throws Exception si hay algun error.
     */
    public Oficina buscarOficina(Long id) throws Exception;

    /**
     * Metodo encargado de cargar las series de una oficina especifica.
     *
     * @param idOficina, identificador de la oficina.
     * @return mapa que contiene el id y la descripcion de cada serie.
     * @throws Exception si hay algun error.
     */
    public Map<Long, String> cargarSeries(Long idOficina) throws Exception;

    /**
     * Metodo encargado de buscar una serie especifica.
     *
     * @param id, identificador de la serie.
     * @return serie.
     * @throws Exception si hay algun error.
     */
    public Serie buscarSerie(Long id) throws Exception;

    /**
     * Metodo encargado de cargar las subseries de una serie.
     *
     * @param idSerie, identificador de la serie.
     * @return mapa que contiene el id y la descripcion de cada subserie.
     * @throws Exception si hay algun error.
     */
    public Map<Long, String> cargarSubSeries(Long idSerie) throws Exception;

    /**
     * Metodo encargado de buscar una subserie.
     *
     * @param id, identificador de la subserie.
     * @return subserie.
     * @throws Exception si hay algun error.
     */
    public SubSerie buscarSubserie(Long id) throws Exception;

    /**
     * Metodo encargado de cargar los tipos de una subserie.
     *
     * @param idSubS, identificador de una subserie.
     * @return mapa que contiene el id y descripcion de cada tipo.
     * @throws Exception si hay algun error.
     */
    public Map<Long, String> cargarTipoDoc(Long idSubS) throws Exception;

    /**
     * Metodo encargado de buscar un tipo documental.
     *
     * @param id, identificador del tipo.
     * @return tipo documental.
     * @throws Exception si hay algun error.
     */
    public TipoDocumento buscarTipodocumento(Long id) throws Exception;

    /**
     * Metodo encargado de cargar la lista de trds de un documento.
     *
     * @param radicacion, codigo del documento.
     * @return lista de trds del documento.
     * @throws Exception si hay algun error.
     */
    public List<Trd> buscarTrds(Long id) throws Exception;

    public List<TrdOld> buscarTrdsOld(Long id) throws Exception;

    /**
     * Metodo encargado de cargar la lista de recorridos.
     *
     * @return lista de recorridos.
     * @throws Exception si hay algun error.
     */
    public List<TipoRecorrido> listarRecorridos() throws Exception;

    /**
     * Metodo encargado de cargar la lista de oficinas por recorrido.
     *
     * @param id, identificador del recorrido.
     * @return lista de oficinas.
     * @throws Exception si hay algun error.
     */
    public List<Oficina> listarOficinas(Long id) throws Exception;

    /**
     * Metodo encargado de cargar la lista de de series de una oficina.
     *
     * @param id, identificador de la oficina.
     * @return lista de series.
     * @throws Exception si hay algun error.
     */
    public List<Serie> listarSeries(Long id) throws Exception;

    /**
     * Metodo encargado de cargar la lista de subseries de una serie.
     *
     * @param id, identificador de la serie.
     * @return lista de subseries.
     * @throws Exception si hay algun error.
     */
    public List<SubSerie> listarSubSeries(Long id) throws Exception;

    /**
     * Metodo encargado de cargar la lista de tipo documental de una subserie.
     *
     * @param id, identificador de la subserie.
     * @return lista de tipo documental.
     * @throws Exception si hay algun error.
     */
    public List<TipoDocumento> listarTipoDocumentos(Long id) throws Exception;

    /**
     * Metodo encargado de cargar los tipos de recorridos.
     *
     * @return @throws Exception si hay algun error.
     */
    public Map<Long, String> cargarTipoRecorrido() throws Exception;

    /**
     * Metodo encargado de buscar el tipo de recorrido.
     *
     * @param id, identificador del tipo de recorrido.
     * @return tipo de recorrido.
     * @throws Exception si hay algun error.
     */
    public TipoRecorrido buscarTipoRecorrido(Long id) throws Exception;

    /**
     * Metodo para cambiar el estado de la oficina.
     *
     * @param oficinaId
     * @param estado
     */
    public void cambiarEstado(Long oficinaId, Boolean estado);

    public void editarSerie(Long serieId, String codigo, String descripcion);

    public void editarSubSerie(Long subserieId, String codigo, String descripcion);

    public void editarTipoDoc(Long tipoDocId, String descripcion);
    
    public Oficina buscarOficinaPorNombre(String nombreOficina);
}
