/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.PerfilLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class PerfilBean implements PerfilLocal {

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

    @Override
    public void crearPerfil(String descripcion) throws Exception {

        List<Perfil> perfiles = em.createQuery("select p from Perfil p where p.descripcion=:perfil").setParameter("perfil", descripcion).getResultList();

        if (perfiles != null && !perfiles.isEmpty()) {
            throw new Exception("El permiso que intenta crear ya existe");
        } else {
            Perfil perfil = new Perfil();
            perfil.setDescripcion(descripcion);
            perfil.setEstado(Boolean.TRUE);
            em.persist(perfil);
        }
    }

    @Override
    public Perfil buscarPerfil(Long id) throws Exception {
        Perfil perfil = em.find(Perfil.class, id);

        if (null != perfil) {
            return perfil;
        } else {
            throw new Exception("No se han creado ningun Perfil con ese id");
        }
    }

    @Override
    public Map<Long, String> cargarPerfiles() throws Exception {
        Map<Long, String> lista = new HashMap<Long, String>();
        List<Perfil> perfiles = (List<Perfil>) em.createQuery(
                "select p from Perfil p").getResultList();

        if (perfiles != null && !perfiles.isEmpty()) {
            for (Perfil p : perfiles) {
                lista.put(p.getId(), p.getDescripcion());
            }

            return lista;
        } else {
            throw new Exception(
                    "No se han creado los Perfiles para este sistema");
        }
    }

    @Override
    public List<Perfil> listaPerfiles() throws Exception {
        List<Perfil> lista = em.createQuery("select p from Perfil p").getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("No se han precargados Perfiles");
        }
    }

    @Override
    public void cambiarEstado(Long perfil_id, Boolean estado) {
        Perfil perfil = em.find(Perfil.class, perfil_id);
        if (perfil != null) {
            perfil.setEstado(estado);
            em.merge(perfil);
        }
    }

}
