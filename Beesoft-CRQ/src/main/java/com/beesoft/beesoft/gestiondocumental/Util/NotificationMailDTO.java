/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Cristhian
 */
public class NotificationMailDTO implements Serializable {

    private String dominioEmail;
    private String emisor;
    private String emailEmisor;
    private String host;
    private String proveedor;
    private String puerto;
    private String starttls;
    private String authetication;
    private String user;
    private String password;
    private List<String> destinatarios;
    private String asunto;
    private String mensaje;
    private List<ArchivosAdjuntos> adjuntos;
    private String comentario;
    private Boolean certificado;
    

    public NotificationMailDTO() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(List<String> destinatarios) {
        this.destinatarios = destinatarios;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ArchivosAdjuntos> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<ArchivosAdjuntos> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public String getDominioEmail() {
        return dominioEmail;
    }

    public void setDominioEmail(String dominioEmail) {
        this.dominioEmail = dominioEmail;
    }

    public String getEmailEmisor() {
        return emailEmisor;
    }

    public void setEmailEmisor(String emailEmisor) {
        this.emailEmisor = emailEmisor;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getStarttls() {
        return starttls;
    }

    public void setStarttls(String starttls) {
        this.starttls = starttls;
    }

    public String getAuthetication() {
        return authetication;
    }

    public void setAuthetication(String authetication) {
        this.authetication = authetication;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Boolean getCertificado() {
        return certificado;
    }

    public void setCertificado(Boolean certificado) {
        this.certificado = certificado;
    }

    

}
