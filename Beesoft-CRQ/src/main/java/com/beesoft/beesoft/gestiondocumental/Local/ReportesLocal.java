/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.Util.EmpresaCorreoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RadicadoresDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RadicadosDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RecorridoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ReporteExternosDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface ReportesLocal {

    /**
     * Metodo encargado cargar la lista de documentos por recorrido.
     *
     * @param fechaIni, rango fecha inicio.
     * @param fechaFin, rango fecha fin.
     * @param recorrido, recorrido de los documentos.
     * @return lista de recorridos de los documentos.
     * @throws Exception si hay algun error.
     */
    public List<Trd> reporteDocumentoRecorrido(Date fechaIni, Date fechaFin, Oficina oficina, String clase, String ciudad) throws Exception;

    public List<EmpresaCorreoDTO> reporteEmpresaCorreo(String empresaCorreo, Date fechaIni, Date fechaFin, String ciudad) throws Exception;

    public List<Documento> reporteContratacion(String tipoContrato, String numContrato, String radicacion, Date fechaIni, Date fechaFin) throws Exception;

    public List<RadicadosDTO> reporteRadicadosPorFechas(String letra, Date fechaIni, Date fechaFin) throws Exception;

    public List<RecorridoDTO> reporteDocumentoCruzado(Date fechaIni, Date fechaFin, Oficina oficina, String ciudad) throws Exception;

    public List<RadicadoresDTO> listarRadicadores(Date fechaIni, Date fechaFin) throws Exception;
    
    public List<Trd> reportePQRSDVencidos(Date fechaIni, Date fechaFin, Oficina oficina) throws Exception;

	public List<Actor> reporteActoresExternos(Long tipoPersona) throws Exception;
	
	public List<Documento> reporteDocumento2017(Date fechaIni, Date fechaFin) throws Exception;
}
