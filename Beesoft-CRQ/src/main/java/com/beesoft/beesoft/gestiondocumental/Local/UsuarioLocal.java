/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface UsuarioLocal {

    /**
     * Metodo que se encarga de crear un usuario.
     *
     * @param username, nombre de usuario.
     * @param password, contraseña de usuario.
     * @param perfil, perfil de usuario.
     * @param actor, informacion del usuario.
     * @throws Exception si hay algun error.
     */
    public void crearUsuario(String username, String password,
            Perfil perfil, Actor actor) throws Exception;

    /**
     * Meotodo que se encarga de cambiar la contraseña del usuario.
     *
     * @param user , usuario al que se la cambiara la contraseña.
     * @param newPassword , nueva contraseña.
     */
    public void cambiarPasswordUsuario(Usuario user, String newPassword);

    /**
     * Meotodo encargado de iniciar sesion.
     *
     * @param username , nombre de usuario.
     * @param password contraseña de usuario.
     * @return cadena para la redireccion. "success".
     * @throws Exception
     */
    public Usuario login(String username, String password) throws Exception;

    public void logout();

    /**
     * Metodo encargado reestablecer la contraseña de un usuario.
     *
     * @param usuario, nombre de usuario.
     * @throws Exception si hay algun error.
     */
    public void enviarCorreoCambioPassword(String usuario) throws Exception;

    /**
     * Metodo encargado de buscar un usuario.
     *
     * @param id, identificador del usuario.
     * @return Usuario.
     * @throws Exception si hay algun error.
     */
    public Usuario buscarUsuario(Long id) throws Exception;

    public Boolean isLogueado();

    public void setLogueado(Boolean logueado);

    public List<Usuario> buscarUsuarios(String nombre, String username, Perfil perfil) throws Exception;

    public void editarUsuario(Usuario usuario, Perfil perfil);

    public void cambiarEstado(Long actor_id, Boolean estado);

    public void editarUsuario(Usuario user, String newPassword);
    
}
