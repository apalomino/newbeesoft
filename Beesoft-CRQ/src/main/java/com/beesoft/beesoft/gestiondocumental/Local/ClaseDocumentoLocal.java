/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.ClasesDocumento;
import java.util.List;
import javax.ejb.Local;
/**
 *
 * @author SUMSET
 * 
 * Metodo para crear una clase de documento
 * 
 * @param name, nombre de la clase
 * 
 */

@Local
public interface ClaseDocumentoLocal {
    
    public boolean crearClaseDocumento(String name, String estado);
    
    public List<ClasesDocumento> listarClasesDocumento(String condicion);
    
    public void CambioEstadoClaseDocumento(Long id, String estado); 
}
