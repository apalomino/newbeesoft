/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author Cristhian
 */
public class ConstantsMail {

    public static final String HOTMAIL = "hotmail";
    public static final String GMAIL = "gmail";
    public static final String STMP_HOTMAIL = "smtp-mail.outlook.com";
    public static final String STMP_GMAIL = "smtp.gmail.com";

    public static final String AUTH = "mail.smtp.auth";
    public static final String STARTTLS = "mail.smtp.starttls.enable";
    public static final String HOST = "mail.smtp.host";
    public static final String PORT = "mail.smtp.port";
    public static final String USER = "mail.smtp.user";
    public static final String PASSWORD = "mail.smtp.password";

    public static final String NOTIFICATION_QUEUE = "jms/NotificationQueue";
    public static final String CONNECTION_FACTORY = "java:/ConnectionFactory";

    public static final String CLASE_INFORMACION = "INFORMACION";
    public static final String CLASE_CONTROL = "CONTROL";
    public static final String CLASE_GESTION = "GESTION";
    public static final String CLASE_MEMORANDO = "MEMORANDO";
    public static final String CLASE_MANUAL = "MANUAL";
    public static final String CLASE_CIRCULAR = "CIRCULAR";
    public static final String OFICIO_MEMORANDO = "Memorando";
    public static final String OFICIO_MANUAL = "Manual";
    public static final String OFICIO_CIRCULAR = "Circular";

    public static final String TIPO_JEFE = "JEFE";
    public static final String TIPO_EMPLEADO = "EMPLEADO";
    public static final String TIPO_EMPRESA = "EMPRESA";
    public static final String TIPO_PERSONA = "PERSONA";

    public static final String ADMIN = "ADMINISTRADOR";
    public static final String GESTOR = "GESTOR";
    public static final String RADICADOR = "RADICADOR";
    public static final String EDITOR = "EDITOR";
    public static final String JEFE = "JEFE";
    public static final String INTERVENTOR = "INTERVENTOR";
    public static final String GESTOR_VB = "GESTOR VB";
    public static final String AUDITOR = "AUDITOR";

    public static final String LOGOUT = "logout";

    public static final String PAIS = "COLOMBIA";
    public static final String DEPTO = "QUINDÍO";
    public static final String CIUDAD = "ARMENIA";

    public static final String RESPONSABLE = "RESPONSABLE";
    public static final String REVISOR = "REVISOR";

    public static final String ACCION_CREAR = "CREAR";
    public static final String ACCION_EDITAR = "EDITAR";
    public static final String ACCION_ANULAR = "ANULAR";
    public static final String ACCION_ACTIVAR = "ACTIVAR";
    public static final String ACCION_CAMBIO_RADICADO = "CAMBIO_RADICADO";

    public static final String ACTAS = "ACTAS";
    public static final String ACTAS_COMITE = "ACTAS COMITE";
    public static final String CONTRATOS = "CONTRATOS";
    public static final String ESCRITURA = "ESCRITURA";
    public static final String INSTRUCTIVOS = "INSTRUCTIVOS";
    public static final String ARCHIVOUSUARIO = "ARCHIVOUSUARIO";
    public static final String NIU_ESPECIAL = "NIU ESPECIAL";
    public static final String NIU_MASIVO = "NIU MASIVO";
    public static final String ODC = "ORDEN DE COMPRA";
    public static final String AEF = "ACEPTACION DE OFERTA FILIAL";
    public static final String AO = "ACEPTACION DE OFERTA";
    public static final String HISTORIAS = "HISTORIAS LABORALES";
    public static final String USEDEQ = "USUARIOS EDEQ";
    public static final String EMPLEADOS = "EMPLEADOS";
    public static final String GUIAS = "GUIAS";

    //---------------------------tipo archivo usuario-------------------------
    public static final String ACTACARTERA = "Acta de depuración de cartera";
    public static final String ACTATECNICA = "Acta de inspección técnica";
    public static final String ACTATECNICAC = "Acta de inspección técnica (C)";
    public static final String ACTAVISUAL = "Acta de inspección visual";
    public static final String ACTAMEDIDORES = "Acta de instalación de medidores";
    public static final String ACTALABORATORIO = "Acta de remisión al laboratorio";
    public static final String ACTAPERSUASIVO = "Acta de visita para cobro persuasivo";
    public static final String CERTIFLABORATORIO = "Certificado calibración del Laboratorio";
    public static final String CONCPAJUSTES = "Concepto de ajustes";
    public static final String CUENTASCONG = "Cuentas congeladas (A)";
    public static final String FORMATCORTE = "Formato orden de corte";
    public static final String FORMATSERVICIO = "Formato legalización servicio";
    public static final String NORMALIZACION = "Normalización (B)";
    public static final String REPORTES = "Reportes de visita (D)";
    public static final String PQR = "Expedientes de PQR";
    public static final String PAGARE = "Pagaré";
    public static final String INTRANET = "Intranet";
    public static final String TARJETA = "Tarjeta Financiación Social";
    public static final String PROTECCION = "Usuario con protección constitucional";
    public static final String CONSTANCIAS = "Contancias y/o notificación atención-revisiones";
    public static final String INSTALACION_MEDIDA = "Acta de instalación y medida";
    public static final String TIRILLA = "Tirilla";
    public static final String SUSPENSION = "Suspensión y Recuperación";
    public static final String NORMA = "Normalización";
    public static final String REVISIONES = "Revisiones";
    public static final String DESVIACION = "Desviación significativa";
    public static final String REPARACION = "Financiación reparaciones";
    public static final String CONVENIOS = "Convenios";
    public static final String COMPROMISOS = "Compromisos Financieros";

    //---------------------empresa correo----------------------------------
    public static final String EMP_ROR = "ROR INGENIERIA";
    public static final String EMP_ROR_RECUPERACION = "ROR INGENIERIA - RECUPERACION ENERGIA";
    public static final String EMP_472 = "472";
    public static final String EMP_SERVIENTREGA = "SERVIENTREGA";
    public static final String EMP_FUNCIONARIO = "FUNCIONARIO";

    //--------------------Estados Comunicados interno-----------------------
    public static final String POR_APROBAR = "POR APROBAR";
    public static final String APROBADO = "APROBADO";
    public static final String SOL_APROBADO = "SOL APROBADO";
    public static final String RECHAZADO = "RECHAZADO";
    public static final String ANULADO = "ANULADO";

    public static final String EMISOR = "EMISOR";
    
    public static final int REPOSITORIO_DOCUMENTO = 1;
    public static final int REPOSITORIO_COMUNICADO_INTERNO = 2;
    public static final int REPOSITORIO_COMUNICADO_EXTERNO = 3;

}
