package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.AuditoriaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ClaseLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentosRespuestaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ArchivoDocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentosRespuesta;
import com.beesoft.beesoft.gestiondocumental.entidades.LetraRadicacion;
import com.beesoft.beesoft.gestiondocumental.entidades.RevisionDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.SubClase;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.postgresql.PGConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class DocumentoBean implements DocumentoLocal {

	private static final Log LOG = LogFactory.getLog(DocumentoBean.class);

	@PersistenceContext(unitName = "BeesoftPU")
	private EntityManager em;

	@EJB
	private NotificationMailLocal notificationBean;

	@EJB
	private AuditoriaLocal auditoriaBean;

	@EJB
	private ClaseLocal claseBean;

	@EJB
	private DocumentosRespuestaLocal docResBean;

	private String idCiudad;

	@Override
	public void crearDocumento(String radicacion, String pqr, String contractTipo, String contractNum, Actor radicador,
			String anexos, String asunto, String observacion, Date fechaControl, Date fechaEmision, Date fechaRecepcion,
			String guia, String mensajero, String notificacionEntrega, String empresaCorreoRecibido,
			String pruebaEntrega, String ubicacionFisica, String clasedocumento, SubClase subClase,
			List<Actor> procedencia, List<Actor> destinos, List<Documento> documentosReferencia, String empresacorreo,
			List<Actor> revisores, Boolean medioElectronico, Actor responsable, List<Trd> listaTrd,
			List<ArchivoDocumento> archivos, Ciudad ciudad) throws Exception {

		Boolean crear = false;
		if (radicacion.equals("FA") || radicacion.equals("V")) {
			crear = true;
		} else {

			List<Documento> listaDoc = em
					.createQuery(
							"select d from Documento d where d.radicacion=:rad and d.activo=true and d.anulado=false")
					.setParameter("rad", radicacion).getResultList();
			if (listaDoc != null && !listaDoc.isEmpty()) {
				crear = false;
			} else {
				crear = true;
			}
		}
		if (crear) {
			Documento documento = new Documento();
			documento.setRadicacion(radicacion);
			documento.setPqr(pqr);
			documento.setContratacionTipo(contractTipo);
			documento.setContratacionNumero(contractNum);
			documento.setRadicador(radicador);
			documento.setActivo(true);
			documento.setFechaSistema(new Date());
			documento.setAsunto(asunto);
			documento.setMedioElectronico(medioElectronico);
			documento.setAnulado(false);
			if (responsable != null || revisores != null) {
				documento.setResponsable(responsable);
				documento.setClasedocumento(clasedocumento);
				documento.setRevisado(false);
				documento.setPendiente(true);
				documento.setAprobado(false);
			} else if (responsable == null && revisores == null) {
				documento.setRevisado(true);
				documento.setActivo(false);
				documento.setAprobado(true);
				documento.setPendiente(false);
			} else if (responsable == null) {
				documento.setRevisado(true);
				documento.setActivo(false);
				documento.setAprobado(true);
				documento.setPendiente(false);
			}
			// documento.setClasedocumento(clasedocumento);
			// documento.setSubClase(subClase);
			documento.setRevisores(revisores);
			documento.setDestinos(destinos);
			documento.setDocumentosReferencia(documentosReferencia);
			documento.setAnexos(anexos);
			documento.setObservacion(observacion);
			documento.setUbicacionFisica(ubicacionFisica);
			documento.setEmpresaCorreoRecibido(empresaCorreoRecibido);
			documento.setEmpresacorreoEnviado(empresacorreo);
			documento.setGuia(guia);
			documento.setMensajero(mensajero);
			documento.setProcedencia(procedencia);
			documento.setPruebaEntrega(pruebaEntrega);
			documento.setFechaControl(fechaControl);
			documento.setFechaEmision(fechaEmision);
			documento.setFechaRecepcion(fechaRecepcion);
			documento.setNotificacionEntrega(notificacionEntrega);
			documento.setMigrado(false);
			documento.setCiudad(ciudad);

			List<ArchivoDocumento> adjuntos = new ArrayList<>();
			if (archivos != null && !archivos.isEmpty()) {
				for (ArchivoDocumento a : archivos) {
					ArchivoDocumento nuevoAdj = new ArchivoDocumento();
					nuevoAdj.setArchivo(a.getArchivo());
					nuevoAdj.setNombreArchivo(a.getNombreArchivo());
					em.persist(nuevoAdj);
					adjuntos.add(nuevoAdj);
				}
				documento.setArchivos(adjuntos);
			}

			em.persist(documento);
			auditoriaBean.crearAuditoria(ConstantsMail.ACCION_CREAR, radicador, documento);

			for (Trd trd : listaTrd) {
				// .setParameter("ser", trd.getSerie().getId()).setParameter("sb",
				// trd.getSubserie().getId()).setParameter("td", trd.getTipodocumento().getId())
				// and t.serie.id=:ser and t.subserie.id=:sb and t.tipodocumento.id=:td
				List<Trd> lista = em.createQuery(
						"select t from Trd t where t.documento.id=:doc and t.oficina.id=:of and t.serie.id=:ser and t.subserie.id=:sb and t.tipodocumento.id=:td")
						.setParameter("doc", documento.getId()).setParameter("of", trd.getOficina().getId())
						.setParameter("ser", trd.getSerie().getId()).setParameter("sb", trd.getSubserie().getId())
						.setParameter("td", trd.getTipodocumento().getId()).getResultList();

				if (lista != null && !lista.isEmpty()) {
					throw new Exception("El documento tiene asociado ese Trd");
				} else {
					Trd trdPersist = new Trd();
					trdPersist.setDocumento(documento);
					trdPersist.setOficina(trd.getOficina());
					trdPersist.setSerie(trd.getSerie());
					trdPersist.setSubserie(trd.getSubserie());
					trdPersist.setTipodocumento(trd.getTipodocumento());
					trdPersist.setMedioElectronico(trd.getMedioElectronico());
					em.persist(trdPersist);
				}
			}

			if (documentosReferencia != null && !documentosReferencia.isEmpty()) {
				for (Documento doc : documentosReferencia) {
					Documento dr = em.find(Documento.class, doc.getId());
					dr.setDocumentoRespuesta(documento);
					em.merge(dr);
				}
			}

//			if (documento.getId() != null) {
//
//				if (documento.getResponsable() != null) {
//					enviarCorreoRadicado(null, radicador, documento.getResponsable(), ConstantsMail.RESPONSABLE,
//							documento);
//				}
//
//				if (revisores != null && !revisores.isEmpty()) {
//					for (Actor rev : revisores) {
//						enviarCorreoRadicado(null, radicador, rev, ConstantsMail.REVISOR, documento);
//					}
//				}
//			}
		} else {
			throw new Exception("El radicado ya existe por favor verifique");
		}
	}

	@Override
	public void cambiarRadicacionDocumento(Long idDocumento, String radicacion, String comentario, Long idUsuario) {
		if (StringUtils.isNotBlank(radicacion)) {
			Documento doc = em.find(Documento.class, idDocumento);

			if (doc != null) {
				doc.setRadicacionOriginal(doc.getRadicacion());
				doc.setRadicacion(radicacion);
				doc.setComentarioCambio(comentario);
				em.merge(doc);
				Usuario user = em.find(Usuario.class, idUsuario);
				auditoriaBean.crearAuditoria(ConstantsMail.ACCION_CAMBIO_RADICADO, user.getActor(), doc);
			}
			// *** Modifica radicado de la tabla de DocumentosRespuesta
			try {
				List<DocumentosRespuesta> docRes = docResBean.consultarRespuesta(idDocumento);
				if (docRes != null && !docRes.isEmpty()) {
					for (DocumentosRespuesta dR : docRes) {
						dR.setRadicado(radicacion);
						em.merge(dR);
					}
				}
			} catch (Exception e) {
				LOG.error(e);
			}
		}
	}

	@Override
	public void gestionarDocumento(Long id, Date fechaControl, String clasedocumento,
			List<Documento> documentoReferencia, List<Actor> revisores, List<Actor> revisoresNuevos, Actor responsable,
			List<Trd> listaTrdEliminar, List<Trd> listaTrdNueva, List<ArchivoDocumento> archivosNuevos,
			List<ArchivoDocumento> archivosEliminar, String observacion, String empresaCorreo,
			String empresaCorreoEnviado, String funcionario, String guia, String prueba, Boolean medioElectronico,
			String notificacion, String contratacionTipo, String contratacionNum, Actor modifica) throws Exception {

		Documento documento = em.find(Documento.class, id);

		documento.setRevisores(revisores);
		documento.setFechaControl(fechaControl);
		documento.getArchivos().removeAll(archivosEliminar);
		documento.setObservacion(observacion);
		documento.setEmpresaCorreoRecibido(empresaCorreo);
		documento.setEmpresacorreoEnviado(empresaCorreoEnviado);
		documento.setMensajero(funcionario);
		documento.setGuia(guia);
		documento.setPruebaEntrega(prueba);
		documento.setNotificacionEntrega(notificacion);
		documento.setContratacionTipo(contratacionTipo);
		documento.setContratacionNumero(contratacionNum);
		documento.setMedioElectronico(medioElectronico);
		documento.setClasedocumento(clasedocumento);

		if (responsable != null) {
			documento.setResponsable(responsable);
		}

		if (archivosNuevos != null && !archivosNuevos.isEmpty()) {
			for (ArchivoDocumento a : archivosNuevos) {
				em.persist(a);
			}
			documento.getArchivos().addAll(archivosNuevos);
		}

//        if (clasedocumento.equals(ConstantsMail.CLASE_INFORMACION)) {
//            documento.setClasedocumento(clasedocumento);
//            documento.setRevisado(false);
//            documento.setPendiente(true);
//            documento.setAprobado(true);
//        } else if (clasedocumento.equals(ConstantsMail.CLASE_CONTROL)) {
//            documento.setClasedocumento(clasedocumento);
//            documento.setRevisado(false);
//            documento.setPendiente(true);
//            documento.setAprobado(true);
//        } else if (clasedocumento.equals(ConstantsMail.CLASE_GESTION)) {
//        documento.setClasedocumento(clasedocumento);
		documento.setRevisado(false);
		documento.setPendiente(true);
		documento.setAprobado(false);
//        }

		documento.setDocumentosReferencia(documentoReferencia);
		if (documento.getDocumentoReferencia() != null) {
			Query q = null;
			try {
				for (Documento doc : documento.getDocumentosReferencia()) {
					// Documento dr = em.find(Documento.class, doc.getId());
					StringBuilder query = new StringBuilder("select d from Documento d");
					q = em.createQuery(query.toString());
				}
			} catch (Exception e) {
				LOG.info(e);
			}
			/*
			 * for (Documento doc : documento.getDocumentosReferencia()) { Documento dr =
			 * em.find(Documento.class, doc.getId()); dr.setDocumentoRespuesta(null);
			 * em.merge(dr); }
			 */
		}

//		if (documentoReferencia != null && !documentoReferencia.isEmpty()) {
//			List<Documento> lRespuesta = new ArrayList();
//			for (Documento doc : documentoReferencia) {
//				Documento dr = em.find(Documento.class, doc.getId());
//				dr.setDocumentoRespuesta(documento);
//				em.merge(dr);
//			}
		/*
		 * for (Documento doc : documentoReferencia) { Documento dr =
		 * em.find(Documento.class, doc.getId()); lRespuesta.add(dr); em.merge(dr); }
		 * documento.setDocumentosRespuesta(lRespuesta);
		 */

//		}
//		documento.setDocumentosReferencia(documentoReferencia);

		em.merge(documento);
		auditoriaBean.crearAuditoria(ConstantsMail.ACCION_EDITAR, modifica, documento);

		if (listaTrdEliminar != null && !listaTrdEliminar.isEmpty()) {
			for (Trd trd : listaTrdEliminar) {
				trd = em.find(Trd.class, trd.getId());
				em.remove(trd);
			}
		}

		for (Trd trd : listaTrdNueva) {

			List<Trd> lista = em.createQuery("select t from Trd t where t.documento.id=:doc and t.oficina.id=:of")
					.setParameter("doc", documento.getId()).setParameter("of", trd.getOficina().getId())
					.getResultList();

			if (lista != null && !lista.isEmpty()) {
				throw new Exception("El documento tiene asociado ese Trd");
			} else {
				Trd trdPersist = new Trd();
				trdPersist.setDocumento(documento);
				trdPersist.setOficina(trd.getOficina());
				trdPersist.setSerie(trd.getSerie());
				trdPersist.setSubserie(trd.getSubserie());
				trdPersist.setTipodocumento(trd.getTipodocumento());
				trdPersist.setMedioElectronico(trd.getMedioElectronico());
				em.persist(trdPersist);
			}
		}

//		if (documento.getId() != null) {
//			if (responsable != null) {
//				enviarCorreoRadicado(null, documento.getRadicador(), responsable, ConstantsMail.RESPONSABLE, documento);
//			}
//			for (Actor rev : revisoresNuevos) {
//				enviarCorreoRadicado(null, documento.getRadicador(), rev, ConstantsMail.REVISOR, documento);
//			}
//		}

	}

	@Override
	public void enviarComentario(Documento documento, Date fechaControl, Boolean revisado, Boolean aprobado,
			Boolean pendiente, String comentario, Actor revisor, Boolean enviarCorreo, List<Actor> revisores,
			List<Actor> nuevosRevisores, Boolean responsable) {
		if (documento != null) {
			documento = em.find(Documento.class, documento.getId());

			if (enviarCorreo) {
				documento.setRevisores(revisores);
				em.merge(documento);
//				for (Actor nuevoRevisor : nuevosRevisores) {
//					enviarCorreoRadicado(revisor, revisor, nuevoRevisor, "REVISOR", documento);
//				}
				RevisionDocumento revision = new RevisionDocumento();
				revision.setActor(revisor);
				revision.setRevisado(revisado);
				revision.setAprobado(aprobado);
				revision.setPendiente(pendiente);
				revision.setEstado(true);
				revision.setDocumento(documento);
				revision.setComentario(comentario);
				revision.setFechaComentario(new Date());
				em.persist(revision);
			} else {
				RevisionDocumento revision = new RevisionDocumento();
				revision.setActor(revisor);
				revision.setRevisado(revisado);
				revision.setAprobado(aprobado);
				revision.setPendiente(pendiente);
				revision.setEstado(true);
				revision.setDocumento(documento);
				revision.setComentario(comentario);
				revision.setFechaComentario(new Date());
				em.persist(revision);
				editarDocumento(documento, responsable);
			}

		}
	}

	@Override
	public List<Documento> buscarDocumento(String radicacion, Date fechaIni, Date fechaFin, Actor procedencia,
			Actor destino, String asunto, String clase, String pruebaEntrega, Actor revisor, Boolean aprobado,
			Boolean revisado, Boolean pendiente, String idCiudad) throws Exception {
		Query q = null;
		try {
			StringBuilder query = new StringBuilder("select d from Documento d");
			Map<String, Object> parameters = new HashMap<String, Object>();
			boolean iswhere = false;
			boolean isprocedencia = false;
			boolean isdestino = false;
			boolean isrevisor = false;

			if (procedencia != null) {
				isprocedencia = true;
				query.append(" left join d.procedencia proc");
			}
			if (destino != null) {
				isdestino = true;
				query.append(" left join d.destinos dest");
			}
			if (revisor != null) {
				isrevisor = true;
				query.append(" left join d.revisores rev");
			}

			if (StringUtils.isNotBlank(radicacion)) {
				query.append(" where d.radicacion like :radicacion");
				parameters.put("radicacion", "%" + radicacion + "%");
				iswhere = true;
			}
			if (fechaIni != null && fechaFin != null) {
				if (iswhere) {
					query.append(" and d.fechaRecepcion BETWEEN :fechaIni AND :fechaFin");
					parameters.put("fechaIni", fechaIni);
					parameters.put("fechaFin", fechaFin);
					iswhere = true;
				} else {
					query.append(" where d.fechaRecepcion BETWEEN :fechaIni AND :fechaFin");
					parameters.put("fechaIni", fechaIni);
					parameters.put("fechaFin", fechaFin);
					iswhere = true;
				}
			}

			if (StringUtils.isNotBlank(idCiudad)) {
				if (iswhere) {
					query.append(" and d.ciudad.id= :idCiudad");
					parameters.put("idCiudad", Long.valueOf(idCiudad));
				} else {
					query.append(" where d.ciudad.id= :idCiudad");
					parameters.put("idCiudad", Long.valueOf(idCiudad));
					iswhere = true;
				}

			}

			if (StringUtils.isNotBlank(asunto)) {
				if (iswhere) {
					query.append(" and d.asunto like :asunto");
					parameters.put("asunto", "%" + asunto + "%");
				} else {
					query.append(" where d.asunto like :asunto");
					parameters.put("asunto", "%" + asunto + "%");
					iswhere = true;
				}
			}

			if (StringUtils.isNotBlank(clase)) {
				if (iswhere) {
					query.append(" and d.clasedocumento like :clase");
					parameters.put("clase", clase);
				} else {
					query.append(" where d.clasedocumento like :clase");
					parameters.put("clase", clase);
					iswhere = true;
				}
			}

			if (StringUtils.isNotBlank(pruebaEntrega)) {
				if (iswhere) {
					query.append(" and d.pruebaEntrega =:prueba");
					parameters.put("prueba", pruebaEntrega);
				} else {
					query.append(" where d.pruebaEntrega =:prueba");
					parameters.put("prueba", pruebaEntrega);
					iswhere = true;
				}
			}

			if (aprobado != null && revisado != null && pendiente != null) {
				if (aprobado != false || revisado != false || pendiente != false) {

					if (iswhere) {
						query.append(" and d.aprobado=:aprobado");
						parameters.put("aprobado", aprobado);
					} else {
						query.append(" where d.aprobado=:aprobado");
						parameters.put("aprobado", aprobado);
						iswhere = true;
					}

					if (iswhere) {
						query.append(" and d.revisado=:revisado");
						parameters.put("revisado", revisado);
					} else {
						query.append(" where d.revisado=:revisado");
						parameters.put("revisado", revisado);
						iswhere = true;
					}

					if (iswhere) {
						query.append(" and d.pendiente=:pendiente");
						parameters.put("pendiente", pendiente);
					} else {
						query.append(" where d.pendiente=:pendiente");
						parameters.put("pendiente", pendiente);
						iswhere = true;
					}
				}
			}

			if (isprocedencia) {
				if (iswhere) {
					query.append(" and proc.id=:proc");
					parameters.put("proc", procedencia.getId());
				} else {
					query.append(" where proc.id=:proc");
					parameters.put("proc", procedencia.getId());
					iswhere = true;
				}
			}

			if (isdestino) {
				if (iswhere) {
					query.append(" and dest.id=:dest");
					parameters.put("dest", destino.getId());
				} else {
					query.append(" where dest.id=:dest");
					parameters.put("dest", destino.getId());
					iswhere = true;
				}

			}

			if (isrevisor) {
				if (iswhere) {
					query.append(" and rev.id=:revi");
					parameters.put("revi", revisor.getId());
				} else {
					query.append(" where rev.id=:revi");
					parameters.put("revi", revisor.getId());
					iswhere = true;
				}

			}
			q = em.createQuery(query.toString());

			if (!parameters.isEmpty()) {
				for (Map.Entry<String, Object> p : parameters.entrySet()) {
					q.setParameter(p.getKey(), p.getValue());
				}
			}

			List<Documento> resultados = q.getResultList();
			if (resultados != null && !resultados.isEmpty()) {
				if (!resultados.isEmpty()) {
					HashSet<Documento> hash = new HashSet<>(resultados);
					resultados.clear();
					resultados.addAll(hash);
					return resultados;
				}
			}
			return null;
		} catch (Exception e) {
			ValidacionesUtil.addMessage(e.getMessage());
			return null;
		}

	}

	@Override
	public List<Documento> buscarDocumentosPendientes(Actor revisor) throws Exception {
		List<Documento> documentosRev = new ArrayList<>();
		List<Documento> documentos = new ArrayList<>();
		documentosRev = em.createQuery(
				"select DISTINCT(d) from RevisionDocumento rev join rev.documento d join d.revisores r  where rev.actor.id=:revisor and rev.pendiente=false and rev.aprobado=true order by d.fechaControl DESC")
				.setParameter("revisor", revisor.getId()).getResultList();
		documentos = em.createQuery(
				"select DISTINCT(d) from Documento d left join d.revisores r where (r.id=:revisor or d.responsable.id=:resp) and d.pendiente=true and d.activo=true order by d.fechaControl DESC")
				.setParameter("revisor", revisor.getId()).setParameter("resp", revisor.getId()).getResultList();

		if (documentos != null && !documentos.isEmpty()) {

//            HashSet<Documento> hash = new HashSet<>(documentos);
//            documentos.clear();
//            documentos.addAll(hash);
			if (documentosRev != null && !documentosRev.isEmpty()) {
				for (Documento rev : documentosRev) {
					documentos.remove(rev);
				}
			}
			return documentos;
		} else {
			throw new Exception("Los parametros de busqueda no obtinen resultados");
		}

	}

	@Override
	public void editarDocumento(Documento documento, Boolean responsable) {

		Map<Actor, Date> mapRevision = new HashMap<>();
		int countRevDoc = 0;

		List<RevisionDocumento> revisiones = em
				.createQuery("select r from RevisionDocumento r where r.documento.id=:documento")
				.setParameter("documento", documento.getId()).getResultList();

		if (revisiones != null && !revisiones.isEmpty()) {

			List<String> revisoresDoc = new ArrayList<>();
			List<String> revisionesDoc = new ArrayList<>();

			if (documento.getRevisores() != null) {
				for (Actor rev : documento.getRevisores()) {
					revisoresDoc.add(rev.getNombreYApellido());
				}
			}
			if (documento.getResponsable() != null) {
				revisoresDoc.add(documento.getResponsable().getNombreYApellido());
			}

			for (RevisionDocumento rev : revisiones) {
				if (rev.getActor() != null) {
					revisionesDoc.add(rev.getActor().getNombreYApellido());
				}
			}

			int revisadoSI = 0;
			int aprobadoSI = 0;

			List<String> revisorAnterior = new ArrayList<>();
			for (String revisor : revisoresDoc) {
				for (RevisionDocumento revisiondocumento : revisiones) {
					if (!revisorAnterior.contains(revisiondocumento.getActor().getNombreYApellido())) {
						if (revisiondocumento.getRevisado()
								&& revisiondocumento.getActor().getNombreYApellido().equals(revisor)) {
							revisadoSI += 1;
							revisorAnterior.add(revisiondocumento.getActor().getNombreYApellido());
						}
					}

				}
			}

			revisorAnterior = new ArrayList<>();
			for (String revisor : revisoresDoc) {
				for (RevisionDocumento revisiondocumento : revisiones) {
					if (!revisorAnterior.contains(revisiondocumento.getActor().getNombreYApellido())) {
						if (revisiondocumento.getAprobado()
								&& revisiondocumento.getActor().getNombreYApellido().equals(revisor)) {
							aprobadoSI += 1;
							mapRevision.put(revisiondocumento.getActor(), revisiondocumento.getFechaComentario());
							revisorAnterior.add(revisiondocumento.getActor().getNombreYApellido());
						}
					}

				}
			}

			countRevDoc = revisoresDoc.size();
			if (revisadoSI >= countRevDoc) {
				documento.setRevisado(true);
				documento.setPendiente(true);
				documento.setAprobado(false);
				em.merge(documento);
			}

			if (aprobadoSI >= countRevDoc) {
				documento.setRevisado(true);
				documento.setPendiente(false);
				documento.setAprobado(true);
				documento.setActivo(false);
				em.merge(documento);

//				enviarCorreoRadicadoFinalizado(documento.getRadicador(), mapRevision, documento);
			}

		}
	}

	@Override
	public List<RevisionDocumento> comentariosDocumento(Long id) throws Exception {

		List<RevisionDocumento> revisiones = em
				.createQuery("select r from RevisionDocumento r where r.documento.id=:documento")
				.setParameter("documento", id).getResultList();
		if (revisiones != null && !revisiones.isEmpty()) {
			return revisiones;
		} else {
			throw new Exception("El documento aun no se ha revisado");
		}
	}

	@Override
	public List<String> consecutivoRadicacion() throws Exception {
		List<LetraRadicacion> concecutivos = em.createQuery("select l from LetraRadicacion l").getResultList();
		List<String> letras = new ArrayList<>();
		if (concecutivos != null && !concecutivos.isEmpty()) {
			for (LetraRadicacion letraradicacion : concecutivos) {
				letras.add(letraradicacion.getLetra() + letraradicacion.getNumeroSecuencia());
			}
			return letras;
		} else {
			throw new Exception("Los consecutivos de para radicar no se han precargado");
		}
	}

	@Override
	public String consecutivoRadicacionXLetra(String letra) throws Exception {
		List<LetraRadicacion> concecutivos = em.createQuery("select l from LetraRadicacion l where l.letra=:letra")
				.setParameter("letra", letra).getResultList();
		String radicado = "";
		if (concecutivos != null && !concecutivos.isEmpty()) {
			for (LetraRadicacion letraradicacion : concecutivos) {
				radicado = letraradicacion.getLetra() + numeroConsecutivo(letraradicacion.getNumeroSecuencia());
			}
			return radicado;
		} else {
			throw new Exception("Ingrese una letra valida");
		}
	}

	@Override
	public String consecutivoRadicacionOficio(String letra) throws Exception {
		List<LetraRadicacion> concecutivos = em.createQuery("select l from LetraRadicacion l where l.letra=:letra")
				.setParameter("letra", letra).getResultList();
		String radicado = "";
		if (concecutivos != null && !concecutivos.isEmpty()) {
			for (LetraRadicacion letraradicacion : concecutivos) {
				radicado = letraradicacion.getLetra() + letraradicacion.getNumeroSecuencia();
			}
			return radicado;
		} else {
			throw new Exception("Ingrese una letra valida");
		}
	}

	public String numeroConsecutivo(Long numero) {
		String x = numero.toString();
		int cifras = x.length();
		String secuencia = "";
		switch (cifras) {
		case 1:
			secuencia = "0000" + numero;
			break;
		case 2:
			secuencia = "000" + numero;
			break;
		case 3:
			secuencia = "00" + numero;
			break;
		case 4:
			secuencia = "0" + numero;
			break;
		case 5:
			secuencia = "" + numero;
			break;
		default:
			secuencia = "" + numero;
			break;
		}
		return secuencia;
	}

	@Override
	public String buscarConsecutivo(String radicacion) {
		Map<String, Long> mapaCadena = new HashMap<String, Long>();
		mapaCadena = ValidacionesUtil.ValidarRadicado(radicacion);
		String letra = "";
		for (Map.Entry<String, Long> entry : mapaCadena.entrySet()) {
			List<LetraRadicacion> letras = em.createQuery("Select l from LetraRadicacion l where l.letra=:letra")
					.setParameter("letra", entry.getKey()).getResultList();
			letra = letras.get(0).getLetra() + letras.get(0).getNumeroSecuencia();
		}
		return letra;
	}

	public String listToCadena(List<Actor> lista) {
		int cont = 0;
		StringBuilder destinatarios = new StringBuilder();
		for (Actor actor : lista) {
			cont++;
			destinatarios.append(actor.getNombreYApellido());
			if (lista.size() > cont) {
				destinatarios.append(", ");
			}
		}
		return destinatarios.toString();
	}

	/**
	 * Método que genra el mail de notificación de un PQRDS Vencido
	 */
	@Override
	public String mensajeCorreoDocVencido(List<Actor> actort, Documento doc, Integer diasVencido) {
		Actor actor = actort.get(0);
		List<Actor> lista = new ArrayList<Actor>();
		lista.add(actor);
		String url = ValidacionesUtil.urlServer();
		StringBuilder message = new StringBuilder("Senor(a):");
		message.append("<br>");
		message.append("<strong>" + actor.getNombreYApellido() + "</strong>");
		message.append("<br>");
		message.append("<br>");
		message.append("El documento con el numero de radicacion: ");
		message.append("<a href=\"http://" + url + ":8080/Beesoft/Gestionar?radicacion=" + doc.getId()
				+ "&sesionResponsable=false\">" + doc.getRadicacion() + "</a>");
		message.append(" Se ha vencido desde hace " + diasVencido + " dias");
		message.append("<br>");
		message.append("<br>");
		message.append("Caracteristicas del Documento:");
		message.append("<br>");
		message.append("<br>");
		message.append("Origen(es):     <strong>");
		message.append(listToCadena(consultarProcedenciaDocumento(doc.getId())));
		message.append("</strong><br>");
		message.append("Destino(s):	<strong>");
		message.append(listToCadena(consultarDestinoDocumento(doc.getId())));
		message.append("</strong><br>");
		message.append("Fecha de Control:	<strong>");
		message.append(formatearFecha(doc.getFechaControl()));
		message.append("</strong><br>");
		message.append("Fecha de Recepcion:	<strong>");
		message.append(formatearFecha(doc.getFechaRecepcion()));
		message.append("</strong><br>");
		message.append("Asunto:	         <strong>");
		message.append(doc.getAsunto() + "</strong>");
		message.append("<br>");
		message.append("<br>");

		String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message.toString());
		return mailCompleto;
	}

//	public void enviarCorreoRadicado(Actor actorReenvio, Actor emisor, Actor actor, String tipoUsuario, Documento doc) {
//		List<Actor> lista = new ArrayList<Actor>();
//		lista.add(actor);
//		String url = ValidacionesUtil.urlServer();
//		StringBuilder message = new StringBuilder("Se\u00F1or(a):");
//		message.append("<br>");
//		message.append("<strong>" + actor.getNombreYApellido() + "</strong>");
//		message.append("<br>");
//		message.append("<br>");
//		message.append("Usted es el <strong>" + tipoUsuario + "</strong> del documento con el número de radicación: ");
//		message.append("<a href=\"http://" + url + ":8080/Beesoft/Gestionar?radicacion=" + doc.getId()
//				+ "&sesionResponsable=false\">" + doc.getRadicacion() + "</a>");
//		message.append("<br>");
//		message.append("<br>");
//		message.append("Caracteristicas del Documento:");
//		message.append("<br>");
//		message.append("<br>");
//		message.append("Origen(es):     <strong>");
//		message.append(listToCadena(consultarProcedenciaDocumento(doc.getId())));
//		message.append("</strong><br>");
//		message.append("Destino(s):	<strong>");
//		message.append(listToCadena(consultarDestinoDocumento(doc.getId())));
//		message.append("</strong><br>");
//		message.append("Fecha de Control:	<strong>");
//		message.append(formatearFecha(doc.getFechaControl()));
//		message.append("</strong><br>");
//		message.append("Fecha de Recepción:	<strong>");
//		message.append(formatearFecha(doc.getFechaRecepcion()));
//		message.append("</strong><br>");
//		message.append("Asunto:	         <strong>");
//		message.append(doc.getAsunto() + "</strong>");
//		message.append("<br>");
//		message.append("<br>");
//		if (actorReenvio != null) {
//			message.append("Reenviado por: " + actorReenvio.getNombreYApellido());
//		}
//
//		String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message.toString());
//
//		notificationBean.guardarCorreo(emisor, lista, doc.getAsunto(), mailCompleto, null, "RADICACION", false);
//
//	}

//	public void enviarCorreoRadicadoFinalizado(Actor emisor, Map<Actor, Date> actores, Documento doc) {
//
//		List<Actor> lista = new ArrayList<Actor>();
//		String url = ValidacionesUtil.urlServer();
//		StringBuilder message = new StringBuilder();
//		message.append("El documento <a href=\"http://" + url + ":8080/Beesoft/Gestionar?radicacion=" + doc.getId()
//				+ "&sesionResponsable=false\">" + doc.getRadicacion() + "</a>");
//		message.append(" ha sido revisado por todos los responsables a continuacion nombrados");
//		message.append("<br>");
//
//		for (Map.Entry<Actor, Date> actor : actores.entrySet()) {
//			message.append(actor.getKey() + " : " + formatearFecha(actor.getValue()));
//			message.append("<br>");
//			lista.add(actor.getKey());
//		}
//		message.append("<br>");
//
//		String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message.toString());
//
//		notificationBean.guardarCorreo(emisor, lista, "Documento revisado", mailCompleto, null, "RADICACION", false);
//
//	}

	public String formatearFecha(Date fecha) {
		SimpleDateFormat formateador = new SimpleDateFormat("dd'/'MMMM'/'yyyy", new Locale("ES"));

		String fechaFormat = formateador.format(fecha);
		return fechaFormat;
	}

	@Override
	public void generarRadicado(String cadena) {
		Map<String, Long> mapaCadena = ValidacionesUtil.ValidarRadicado(cadena);
		if (!mapaCadena.isEmpty()) {
			String letras = "";
			Long numeroFormateado = null;
			for (Map.Entry<String, Long> entry : mapaCadena.entrySet()) {
				letras = entry.getKey();
				numeroFormateado = entry.getValue();
			}

			List<LetraRadicacion> listaLetras = em.createQuery("select l from LetraRadicacion l where l.letra=:letra")
					.setParameter("letra", letras).getResultList();
			if (listaLetras != null && !listaLetras.isEmpty()) {
				for (LetraRadicacion letraradicacion : listaLetras) {
					letraradicacion.setNumeroSecuencia(numeroFormateado + 1);
					em.merge(letraradicacion);
				}
			}
		}
	}

	private boolean isNumeric(char caracter) {
		try {
			Integer.parseInt(String.valueOf(caracter));
			return true;
		} catch (NumberFormatException ex) {
			LOG.info(ex);
			return false;
		}
	}

	/**
	 *
	 */
//	@Schedule(year = "*", month = "*", dayOfWeek = "1", hour = "7", minute = "40")
	@Override
	public void validarFechaControl() {
//		List<Documento> listaDocumentos = em.createQuery("select d from Documento d where d.activo=:pendiente")
//				.setParameter("pendiente", true).getResultList();
//
//		if (listaDocumentos != null && !listaDocumentos.isEmpty()) {
//			for (Documento doc : listaDocumentos) {
//				enviarCorreoResponsable(doc.getResponsable(), doc);
//			}
//		}
	}

//	public void enviarCorreoResponsable(Actor actor, Documento doc) {
//		String url = ValidacionesUtil.urlServer();
//		List<Actor> lista = new ArrayList<>();
//		StringBuilder message = new StringBuilder();
//		message.append("El documento <a href=\"http://" + url + ":8080/Beesoft/Gestionar?radicacion=" + doc.getId()
//				+ "&sesionResponsable=true\">" + doc.getRadicacion() + "</a>");
//		message.append(" del cual usted es responsable no ha sido revisado por los revisores asignados ");
//		message.append(" por favor ingrese al aplicativo y realice la revision pertinente");
//		message.append("<br>");
//
//		lista.add(actor);
//
//		message.append("<br>");
//
//		String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message.toString());
//
//		notificationBean.guardarCorreo(doc.getRadicador(), lista, "Documento Pendiente", mailCompleto, null,
//				"RADICACION", false);
//
//	}

//	public void enviarCorreoFinalizadoResponsable(List<Actor> actores, Documento doc) {
//
//		String url = ValidacionesUtil.urlServer();
//		StringBuilder message = new StringBuilder();
//		message.append("El documento <a href=\"http://" + url + ":8080/Beesoft/Gestionar?radicacion=" + doc.getId()
//				+ "&sesionResponsable=true\">" + doc.getRadicacion() + "</a>");
//		message.append(" fue revisado por el responsable en la fecha de control: ");
//		message.append(formatearFecha(doc.getFechaControl()));
//		message.append("<br>");
//		message.append("<br>");
//
//		String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message.toString());
//
//		notificationBean.guardarCorreo(doc.getRadicador(), actores, "Documento Finalizado", mailCompleto, null,
//				"RADICACION", false);
//
//	}

	@Override
	public void cambiarEstado(Long idDocumento, Actor usuario, Boolean estado) {
		Documento documento = em.find(Documento.class, idDocumento);
		if (documento != null) {
			documento.setActivo(estado);
			if (!estado) {
				documento.setAnulado(!estado);
			}
			em.merge(documento);
			if (estado) {
				auditoriaBean.crearAuditoria(ConstantsMail.ACCION_ACTIVAR, usuario, documento);
			} else {
				auditoriaBean.crearAuditoria(ConstantsMail.ACCION_ANULAR, usuario, documento);
			}
		}
	}

	@Override
	public Boolean esResponsable(String radicado, Actor responsable) {
		List<Documento> lista = em.createQuery(
				"select d from Documento d  join d.revisores r where d.radicacion=:rad and r.nombreYApellido=:res")
				.setParameter("rad", radicado).setParameter("res", responsable.getNombreYApellido()).getResultList();
		if (lista != null && !lista.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void editarPruebaEntregaDocumento(Long idDocumento, String prueba, String observacion) {
		Documento documento = em.find(Documento.class, idDocumento);
		if (documento != null) {
			documento.setPruebaEntrega(prueba);
			documento.setObservacion(observacion);
			em.merge(documento);
		}
	}

	public Connection getConnection() throws Exception {
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/crq";
		String username = "postgres";
		String password = "12345";
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}

	@Override
	public List<DocumentoDTO> buscarDocumentoOld(String radicacion, Date fechaIni, Date fechaFin, Actor procedencia,
			Actor destino, String asunto, Actor revisor, Boolean aprobado, Boolean revisado, Boolean pendiente) {
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		List<DocumentoDTO> listaDTO = new ArrayList<>();
		Boolean isWhere = false;
		try {
			conn = getConnection();
			StringBuilder query = new StringBuilder("SELECT DISTINCT (documento.id),");
			query.append("documento.radicacion,");
			query.append("documento.fechaemision,");
			query.append("documento.fechasistema,");
			query.append("documento.fecharecepcion,");
			query.append("documento.comentario,");
			query.append("documento.recibido,");
			query.append("documento.guia,");
			query.append("documento.enviado,");
			query.append("documento.funcionario,");
			query.append("documento.prueba,");
			query.append("documento.notificacion,");
			query.append("documento.observacion,");
			query.append("documento.clase,");
			query.append("documento.aprobado,");
			query.append("documento.reviso,");
			query.append("documento.pendiente,");
			query.append("documento.fechacontrol,");
			query.append("documento.responsable,");
			query.append("documento.estado,");
			query.append("documento.usuario ");
			query.append("FROM documento ");
			query.append("left join destinodocumento ON documento.id=destinodocumento.iddocumento ");
			query.append("left join origendocumento ON documento.id=origendocumento.iddocumento ");

			if (StringUtils.isNotBlank(radicacion)) {
				query.append(" WHERE documento.radicacion like ? ");
				isWhere = true;
			}
			if (fechaIni != null && fechaFin != null) {
				if (isWhere) {
					query.append(" AND documento.fechaemision BETWEEN ? AND ? ");
				} else {
					query.append(" WHERE documento.fechaemision BETWEEN ? AND ? ");
					isWhere = true;
				}
			}
			if (StringUtils.isNotBlank(asunto)) {
				if (isWhere) {
					query.append(" AND documento.comentario like ?");
				} else {
					query.append(" WHERE documento.comentario like ?");
					isWhere = true;
				}
			}

			if (procedencia != null) {
				if (isWhere) {
					query.append(" AND origendocumento.iddatosbasicos=?");
				} else {
					query.append(" WHERE origendocumento.iddatosbasicos=?");
					isWhere = true;
				}
			}

			if (destino != null) {
				if (isWhere) {
					query.append(" AND destinodocumento.iddatosbasicos=?");
				} else {
					query.append(" WHERE destinodocumento.iddatosbasicos=?");
					isWhere = true;
				}
			}

			if (revisor != null) {
				if (isWhere) {
					query.append(" AND revisordocumento.idrevisor=?");
				} else {
					query.append(" WHERE revisordocumento.idrevisor=?");
					isWhere = true;
				}
			}

			query.append(" ORDER BY documento.radicacion");

			pstmt = conn.prepareStatement(query.toString()); // create a statement
			int numero = 1;
			if (StringUtils.isNotBlank(radicacion)) {
				pstmt.setString(numero, "%" + radicacion + "%");
				numero++;
			}
			if (fechaIni != null && fechaFin != null) {
				pstmt.setDate(numero, new java.sql.Date(fechaIni.getTime()));
				numero++;
				pstmt.setDate(numero, new java.sql.Date(fechaFin.getTime()));
				numero++;
			}
			if (StringUtils.isNotBlank(asunto)) {
				pstmt.setString(numero, "%" + asunto + "%");
				numero++;
			}

			if (procedencia != null) {
				pstmt.setString(numero, "" + buscarDatosBasicosXNombre(procedencia.getNombreYApellido()));
				numero++;
			}
			if (destino != null) {
				pstmt.setString(numero, "" + buscarDatosBasicosXNombre(destino.getNombreYApellido()));
				numero++;
			}
			if (revisor != null) {
				pstmt.setString(numero, "" + buscarDatosBasicosXNombre(revisor.getNombreYApellido()));
				numero++;
			}

			rs = pstmt.executeQuery();
			pstmt.setMaxRows(600);
			// extract data from the ResultSet
			while (rs.next()) {
				String id = rs.getString(1);
				String rad = rs.getString(2);
				Date fechaEmision = rs.getDate(3);
				Date fechaRecepcion = rs.getDate(4);
				Date fechaSistema = rs.getDate(5);
				String asu = rs.getString(6);
				String recibido = rs.getString(7);
				String guia = rs.getString(8);
				String enviado = rs.getString(9);
				String funcionario = rs.getString(10);
				String prueba = rs.getString(11);
				String notifi = rs.getString(12);
				String observ = rs.getString(13);
				String clase = rs.getString(14);
				Boolean aprob = rs.getBoolean(15);
				Boolean rev = rs.getBoolean(16);
				Boolean pendient = rs.getBoolean(17);
				Date fechaControl = rs.getDate(18);
				String responsable = rs.getString(19);
				String estado = rs.getString(20);
				String radicador = rs.getString(21);
				Boolean activo = false;
				DocumentoDTO dto = new DocumentoDTO(id, rad, fechaEmision, fechaRecepcion, fechaSistema, "", "", asu,
						recibido, guia, enviado, funcionario, prueba, notifi, observ, clase, aprob, rev, pendient,
						fechaControl, responsable, "", radicador, activo, true);
				listaDTO.add(dto);

			}
			if (rs != null) {
				rs.close();
				pstmt.close();
				conn.close();
			}
		} catch (Exception e) {
			LOG.info(e);
			return new ArrayList<>();
		}
		System.out.println("Los registros son. " + listaDTO.size());
		return listaDTO;
	}

	@Override
	public List<ArchivoDocumentoDTO> consultarArchivo(String id) throws Exception {
		int i;
		List<ArchivoDocumentoDTO> lista = new ArrayList<ArchivoDocumentoDTO>();
		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArchivoDocumentoDTO archivo = null;
		try {
			conn.setAutoCommit(false);
			LargeObjectManager lobj;
			lobj = ((PGConnection) conn).getLargeObjectAPI();
			pstmt = conn.prepareStatement("SELECT archivo,nombrearchivo FROM archivodocumento WHERE id_archivo=?");
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					long oid = rs.getLong(1);
					String nombre = rs.getString(2);
					LargeObject obj = lobj.open(oid, LargeObjectManager.READ);
					byte buf[] = new byte[obj.size()];
					obj.read(buf, 0, obj.size());
					archivo = new ArchivoDocumentoDTO(id, nombre, buf);
					lista.add(archivo);
					obj.close();
				}
				rs.close();
			}
			pstmt.close();
			conn.setAutoCommit(true);
		} catch (SQLException sqle) {
			LOG.info(sqle);
			return Collections.emptyList();
		} catch (Exception e) {
			LOG.info(e);
			return Collections.emptyList();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (conn.getAutoCommit() == false) {
					conn.setAutoCommit(true);
				}
			} catch (SQLException se) {
				LOG.info(se);
			}
		}
		return lista; // fin del try
	}

	@Override
	public List<String> buscarDestinosOld(String id) throws Exception {
		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> destinos = new ArrayList<>();
		try {
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement("SELECT iddatosbasicos FROM destinodocumento WHERE iddocumento=?");
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();

			if (rs != null) {
				ResultSet rs1 = null;
				while (rs.next()) {
					String idDestino = rs.getString(1);
					pstmt = conn.prepareStatement("SELECT nombre,apellido FROM datosbasicos WHERE id=?");
					pstmt.setString(1, idDestino);
					rs1 = pstmt.executeQuery();
					if (rs1 != null) {
						while (rs1.next()) {
							String nombre = rs1.getString(1);
							String apellido = rs1.getString(2);
							destinos.add(nombre + " " + apellido);
						}
					}
				}
			}
			if (rs != null) {
				rs.close();
				pstmt.close();
				conn.close();
			}

		} catch (SQLException sqle) {
			LOG.info(sqle);
			return Collections.emptyList();
		} catch (Exception e) {
			LOG.info(e);
			return Collections.emptyList();
		}
		return destinos;
	}

	@Override
	public List<String> buscarProcedenciaOld(String id) throws Exception {
		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> procedencia = new ArrayList<>();
		try {
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement("SELECT iddatosbasicos FROM origendocumento WHERE iddocumento=?");
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();

			if (rs != null) {
				ResultSet rs1 = null;
				while (rs.next()) {
					String idDestino = rs.getString(1);
					pstmt = conn.prepareStatement("SELECT nombre,apellido FROM datosbasicos WHERE id=?");
					pstmt.setString(1, idDestino);
					rs1 = pstmt.executeQuery();
					if (rs1 != null) {
						while (rs1.next()) {
							String nombre = rs1.getString(1);
							String apellido = rs1.getString(2);
							procedencia.add(nombre + " " + apellido);
						}
					}
				}
			}

			if (rs != null) {
				rs.close();
				pstmt.close();
				conn.close();
			}
		} catch (SQLException sqle) {
			LOG.info(sqle);
			return Collections.emptyList();
		} catch (Exception e) {
			LOG.info(e);
			return Collections.emptyList();
		}
		return procedencia;
	}

	public Long buscarDatosBasicosXNombre(String nombre) {
		try {
			Connection conn = getConnection();
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			Long id = null;
			try {
				conn.setAutoCommit(false);
				pstmt = conn.prepareStatement("SELECT id FROM vistadatosbasicos WHERE nombre like ?");
				pstmt.setString(1, "%" + nombre + "%");
				rs = pstmt.executeQuery();

				if (rs != null) {
					while (rs.next()) {
						id = rs.getLong(1);
					}
				}
			} catch (SQLException sqle) {
				LOG.info(sqle);
				return null;
			} catch (Exception e) {
				LOG.info(e);
				return null;
			} finally {
				try {
					if (rs != null) {
						rs.close();
					}
					if (pstmt != null) {
						pstmt.close();
					}
					if (conn.getAutoCommit() == false) {
						conn.setAutoCommit(true);
					}
				} catch (SQLException se) {
					LOG.info(se);
				}
			}
			return id;

		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;

	}

	@Override
	public String buscarResponsableOld(String id) throws Exception {
		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String radicador = "";
		try {
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement("SELECT nombre,apellido FROM datosbasicos WHERE id=?");
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					String nombre = rs.getString(1);
					String apellido = rs.getString(2);
					radicador = nombre + " " + apellido;
				}
			}
		} catch (SQLException sqle) {
			LOG.info(sqle);
			return null;
		} catch (Exception e) {
			LOG.info(e);
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (conn.getAutoCommit() == false) {
					conn.setAutoCommit(true);
				}
			} catch (SQLException se) {
				LOG.info(se);
			}
		}
		return radicador;
	}

	@Override
	public List<Documento> buscarDocumentoMercurio(String radicacionMercurio, String radicacion, Date fechaIni,
			Date fechaFin, Actor procedencia, Actor destino, String asunto, Actor revisor, Boolean aprobado,
			Boolean revisado, Boolean pendiente) throws Exception {
		Query q = null;
		StringBuilder query = new StringBuilder("select d from Documento d");
		Map<String, Object> parameters = new HashMap<String, Object>();
		boolean iswhere = false;
		boolean isprocedencia = false;
		boolean isdestino = false;
		boolean isrevisor = false;

		if (procedencia != null) {
			isprocedencia = true;
			query.append(" left join d.procedencia proc");
		}
		if (destino != null) {
			isdestino = true;
			query.append(" left join d.destinos dest");
		}
		if (revisor != null) {
			isrevisor = true;
			query.append(" left join d.revisores rev");
		}

		if (StringUtils.isNotBlank(radicacion)) {
			query.append(" where d.radicacionMercurio=:radicacionMercurio");
			parameters.put("radicacionMercurio", radicacionMercurio);
			iswhere = true;
		} else {
			query.append(" where d.radicacionMercurio is not null");
			iswhere = true;
		}

		if (StringUtils.isNotBlank(radicacion)) {
			if (iswhere) {
				query.append(" and d.radicacion=:radicacion");
				parameters.put("radicacion", radicacion);
			} else {
				query.append(" where d.radicacion=:radicacion");
				parameters.put("radicacion", radicacion);
				iswhere = true;
			}
		}
		if (fechaIni != null && fechaFin != null) {
			if (iswhere) {
				query.append(" and d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
				iswhere = true;
			} else {
				query.append(" where d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
				iswhere = true;
			}
		}

		if (StringUtils.isNotBlank(asunto)) {
			if (iswhere) {
				query.append(" and d.asunto like :asunto");
				parameters.put("asunto", "%" + asunto + "%");
			} else {
				query.append(" where d.asunto like :asunto");
				parameters.put("asunto", "%" + asunto + "%");
				iswhere = true;
			}
		}

		if (aprobado != null && revisado != null && pendiente != null) {
			if (aprobado != false || revisado != false || pendiente != false) {

				if (iswhere) {
					query.append(" and d.aprobado=:aprobado");
					parameters.put("aprobado", aprobado);
				} else {
					query.append(" where d.aprobado=:aprobado");
					parameters.put("aprobado", aprobado);
					iswhere = true;
				}

				if (iswhere) {
					query.append(" and d.revisado=:revisado");
					parameters.put("revisado", revisado);
				} else {
					query.append(" where d.revisado=:revisado");
					parameters.put("revisado", revisado);
					iswhere = true;
				}

				if (iswhere) {
					query.append(" and d.pendiente=:pendiente");
					parameters.put("pendiente", pendiente);
				} else {
					query.append(" where d.pendiente=:pendiente");
					parameters.put("pendiente", pendiente);
					iswhere = true;
				}
			}
		}

		if (isprocedencia) {
			if (iswhere) {
				query.append(" and proc.id=:proc");
				parameters.put("proc", procedencia.getId());
			} else {
				query.append(" where proc.id=:proc");
				parameters.put("proc", procedencia.getId());
				iswhere = true;
			}
		}

		if (isdestino) {
			if (iswhere) {
				query.append(" and dest.id=:dest");
				parameters.put("dest", destino.getId());
			} else {
				query.append(" where dest.id=:dest");
				parameters.put("dest", destino.getId());
				iswhere = true;
			}

		}

		if (isrevisor) {
			if (iswhere) {
				query.append(" and rev.id=:revi");
				parameters.put("revi", revisor.getId());
			} else {
				query.append(" where rev.id=:revi");
				parameters.put("revi", revisor.getId());
				iswhere = true;
			}

		}

		q = em.createQuery(query.toString());

		if (!parameters.isEmpty()) {
			for (Map.Entry<String, Object> p : parameters.entrySet()) {
				q.setParameter(p.getKey(), p.getValue());
			}
		}

		List<Documento> resultados = q.getResultList();
		List<Documento> doc = new ArrayList<>();
		if (resultados != null && !resultados.isEmpty()) {
			for (Documento docum : resultados) {
				if (docum.getClasedocumento().equals(ConstantsMail.CLASE_GESTION)
						|| docum.getClasedocumento().equals(ConstantsMail.CLASE_CONTROL)
						|| docum.getClasedocumento().equals(ConstantsMail.CLASE_INFORMACION)) {
					doc.add(docum);
				}
			}

			if (!doc.isEmpty()) {
				HashSet<Documento> hash = new HashSet<>(doc);
				doc.clear();
				doc.addAll(hash);
				return doc;
			}
		} else {
			// throw new Exception("Los parametros de busqueda no obtinen resultados");
		}
		return null;
	}

	@Override
	public List<DocumentosRespuesta> consultarRespuestasDocumento(Long idDoc) throws Exception {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			List<DocumentosRespuesta> lista = em
					.createQuery("Select d from DocumentosRespuesta d" + " where d.documentoOrigen = :id")
					.setParameter("id", idDoc).getResultList();
			if (lista != null && !lista.isEmpty()) {
				return lista;
			} else {
				return null;
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	@Override
	public List<Actor> consultarProcedenciaDocumento(Long idDocumento) {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			Documento documento = em.find(Documento.class, idDocumento);
			if (!documento.getProcedencia().isEmpty()) {
				conn.close();
				return documento.getProcedencia();
			} else {
				conn.close();
				return new ArrayList<>();
			}
		} catch (Exception ex) {
			LOG.info("Exception en Consultar Procedencia, "+ex);
		}
		return null;
	}

	@Override
	public List<Actor> consultarDestinoDocumento(Long idDocumento) {
		Documento documento = em.find(Documento.class, idDocumento);
		if (!documento.getDestinos().isEmpty()) {
			return documento.getDestinos();
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<ArchivoDocumento> consultarArchivoDocumento(Long idDocumento) {
		Documento documento = em.find(Documento.class, idDocumento);
		if (!documento.getArchivos().isEmpty()) {
			return documento.getArchivos();
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<Actor> consultarRevisorDocumento(Long idDocumento) {
		Documento documento = em.find(Documento.class, idDocumento);
		if (!documento.getRevisores().isEmpty()) {
			return documento.getRevisores();
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<Documento> consultarReferenciasDocumento(Long idDocumento) {
		Documento documento = em.find(Documento.class, idDocumento);
		if (!documento.getDocumentosReferencia().isEmpty()) {
			return documento.getDocumentosReferencia();
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public Documento buscarDocumentoById(Long id) {
		List<Documento> lista = em.createQuery("select d from Documento d where d.id=:id").setParameter("id", id)
				.getResultList();
		if (lista != null && !lista.isEmpty()) {
			return lista.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void extraccionImagenes(List<Documento> documentos, String ruta) {

//		for (Documento documento : documentos) {
//			List<ArchivoDocumento> archivos = consultarArchivoDocumento(documento.getId());
//			for (int i = 0; i < archivos.size(); i++) {
//				try {
//					int size = archivos.size();
//					String fileName = "";
//					ArchivoDocumento archivo = archivos.get(i);
//
//					String directory = System.getenv("JBOSS_HOME") + File.separator + "rootEdeq" + File.separator
//							+ "DOCUMENTOS" + File.separator + ruta;
//					File folder = new File(directory);
//					if (!folder.exists()) {
//						folder.mkdirs();
//					}
//					int dotPosition = (archivo.getNombreArchivo()).lastIndexOf('.');
//					String fileExtension = (archivo.getNombreArchivo()).substring(dotPosition + 1);
//
//					if (size > 1) {
//						fileName = directory + File.separator + documento.getId() + "-" + i + "." + fileExtension;
//					} else {
//						fileName = directory + File.separator + documento.getId() + "." + fileExtension;
//					}
//
//					File file = new File(fileName);
//
//					// convert array of bytes into file
//					FileOutputStream fileOuputStream = new FileOutputStream(file);
//					fileOuputStream.write(archivo.getArchivo());
//					fileOuputStream.close();
//
//				} catch (IOException ex) {
//					Logger.getLogger(DocumentoBean.class.getName()).log(Level.SEVERE, null, ex);
//				}
//			}
//		}
	}

	@Override
	public void extraccionImagenesOld(List<String> documentos, String ruta) {

//		try {
//			for (String id : documentos) {
//				List<ArchivoDocumentoDTO> archivos = consultarArchivosOld(id);
//				for (int i = 0; i < archivos.size(); i++) {
//
//					int size = archivos.size();
//					String fileName = "";
//					ArchivoDocumentoDTO archivo = archivos.get(i);
//
//					String directory = System.getenv("JBOSS_HOME") + File.separator + "rootEdeq" + File.separator
//							+ "DOCUMENTOS_OLD" + File.separator + ruta;
//					File folder = new File(directory);
//					if (!folder.exists()) {
//						folder.mkdirs();
//					}
//					int dotPosition = (archivo.getNombreArchivo()).lastIndexOf('.');
//					String fileExtension = (archivo.getNombreArchivo()).substring(dotPosition + 1);
//
//					if (size > 1) {
//						fileName = directory + File.separator + id + "-" + i + "." + fileExtension;
//					} else {
//						fileName = directory + File.separator + id + "." + fileExtension;
//					}
//
//					File file = new File(fileName);
//
//					// convert array of bytes into file
//					FileOutputStream fileOuputStream = new FileOutputStream(file);
//					fileOuputStream.write(archivo.getArchivo());
//					fileOuputStream.close();
//
//				}
//			}
//		} catch (IOException ex) {
//			Logger.getLogger(DocumentoBean.class.getName()).log(Level.SEVERE, null, ex);
//		}
	}

	public List<ArchivoDocumentoDTO> consultarArchivosOld(String id) {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
		} catch (Exception ex) {
			LOG.info(ex);
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<ArchivoDocumentoDTO> archivos = new ArrayList<>();
		try {

			pstmt = conn.prepareStatement("SELECT archivo,nombrearchivo FROM archivodocumento WHERE id_archivo=?");
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			LargeObjectManager lobj = ((org.postgresql.PGConnection) rs.getStatement().getConnection())
					.getLargeObjectAPI();
			if (rs != null) {
				while (rs.next()) {
					long oid = rs.getLong(1);
					String nombre = rs.getString(2);
					LargeObject obj = lobj.open(oid, LargeObjectManager.READ);
					byte buf[] = new byte[obj.size()];
					obj.read(buf, 0, obj.size());
					obj.close();
					ArchivoDocumentoDTO archivo = new ArchivoDocumentoDTO(id, nombre, buf);
					archivos.add(archivo);
				}
				rs.close();
			} else {
				System.out.println("El archivo no pudo ser descargado del servidor...");
			}
			pstmt.close();
			conn.setAutoCommit(true);
		} catch (SQLException sqle) {
			LOG.info(sqle);
		} catch (Exception e) {
			LOG.info(e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se) {
				LOG.info(se);
			}
		}
		return archivos;
	}

	public RevisionDocumento ultimaRevisionDocumentoPorUsuario(Long idDocumento, Long idUsuario) {
		List<RevisionDocumento> lista = em.createQuery(
				"Select r from RevisionDocumento r where r.documento.id=:documento and r.actor.id=:usuario ORDER BY r.fechaComentario ASC LIMIT 1")
				.setParameter("documento", idDocumento).setParameter("usuario", idUsuario).getResultList();

		if (lista != null && !lista.isEmpty()) {
			return lista.get(0);
		} else {
			return null;
		}
	}
}
