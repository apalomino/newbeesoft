/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;

import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.webapp.ReportesAction;
import com.itextpdf.text.log.SysoCounter;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.TIFFEncodeParam;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.media.jai.NullOpImage;
import javax.media.jai.OpImage;
import javax.media.jai.PlanarImage;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.logging.LogFactory;
import org.jfree.util.Log;

import javax.servlet.ServletContext;
import javax.faces.context.FacesContext;
import javax.faces.application.*;
import javax.faces.application.FacesMessage;
import javax.faces.event.*;

/**
 *
 * @author Cristhian
 */
public class ValidacionesUtil {
	
	public static int validarNulo(Object valor, String campo) {
		if (valor != null) {
			return 1;
		} else {
			addMessage("El campo " + campo + " es obligatorio");
			return -1;
		}
	}

	public static int validarMedioRespuesta(String valor, String valorLetra, String campo) {
		if (valorLetra.equalsIgnoreCase("R")) {
			if (valor != null && !"".equals(valor)) {
				return 1;
			} else {
				addMessage("El campo " + campo + " es obligatorio");
				return -1;
			}
		}
		return 1;
	}

	public static int validarCadena(String valor, String campo) {
		if (StringUtils.isNotBlank(valor)) {
			return 1;
		} else {
			addMessage("El campo " + campo + " es obligatorio");
			return -1;
		}		
	}
	
	public static int validarCedulaNit(String valor, String campo) {
		int cont = validarCadena(valor, campo);
		if (cont <= 0)
			return cont;

		if (isInteger(valor))
			return 1;
		else
			addMessage("Solo se pueden ingresar n\u00FAmeros");
		return -1;
	}

	public static int validarFechasIniFinObligatorio(Date fechaIni, Date fechaFin, String campoIni, String campoFin) {
		if (fechaIni != null && fechaFin != null) {
			if (fechaFin.after(fechaIni) || fechaFin.equals(fechaIni)) {
				return 1;
			} else {
				addMessage("La fecha de " + campoIni + " debe ser menor a la fecha de " + campoFin);
				return -1;
			}
		} else {
			addMessage("Los campos " + campoIni + " y " + campoFin + " son obligatorios");
			return -1;
		}
	}

	public static Boolean validarFechasIniFin(Date fechaIni, Date fechaFin) {
		if (fechaIni != null && fechaFin != null) {
			if (fechaFin.after(fechaIni) || fechaFin.equals(fechaIni)) {
				return true;
			} else {
				addMessage("La fecha inicial debe ser menor a la fecha final");
				return false;
			}
		} else {
			return false;
		}
	}

	public static int validarFecha(Date fecha, String campo) {
		if (fecha != null) {
			if (fecha.after(new Date())) {
				return 1;
			} else {
				addMessage("El campo Fecha " + campo + " debe ser mayor a la fecha actual");
				return -1;
			}
		} else {
			addMessage("El campo Fecha " + campo + " es obligatorio");
			return -1;
		}
	}

	public static int validarListas(List<?> lista, String campoLista) {
		if (lista != null && !lista.isEmpty()) {
			return 1;
		} else {
			addMessage("La lista de " + campoLista + " no debe estar vacia");
			return -1;
		}
	}

	public static int validarCorreo(String email) {
		if (StringUtils.isNotBlank(email)) {
			if (validarPatronCorreo(email)) {
				return 1;
			} else {
				addMessage("La estructura del email es incorrecto");
				return -1;
			}
		} else {
			addMessage("El campo Email es obligatorio");
			return -1;
		}
	}

	public static Boolean validarPatronCorreo(String email) {
		String patron = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pat = Pattern.compile(patron);
		if (StringUtils.isNotBlank(email)) {
			Matcher mat = pat.matcher(email);
			return mat.matches();
		}
		return Boolean.FALSE;
	}

	public static String listToCadena(List<Actor> lista) {
		int cont = 0;
		StringBuilder cadena = new StringBuilder();
		for (Actor actor : lista) {
			cont++;
			cadena.append(actor.getNombreYApellido());
			if (lista.size() > cont) {
				cadena.append(", ");
			}
		}
		return cadena.toString();
	}
	
	public static String listToCadena2017(List<Actor> lista) {
		int cont = 0;
		StringBuilder cadena = new StringBuilder();
		for (Actor actor : lista) {
			cont++;
			cadena.append(actor.getCiudad().getDescripcion());
			if (lista.size() > cont) {
				cadena.append(", ");
			}
		}
		return cadena.toString();
	}

	public static String listStringToCadena(List<String> lista) {
		int cont = 0;
		StringBuilder cadena = new StringBuilder();
		for (String correo : lista) {
			cont++;
			cadena.append(correo);
			if (lista.size() > cont) {
				cadena.append(", ");
			}
		}
		return cadena.toString();
	}

	public static Long obtenerIdFromMap(Map<Long, String> mapa, String cadena) {
		Long id = null;
		if (StringUtils.isNotBlank(cadena)) {

			for (Map.Entry<Long, String> re : mapa.entrySet()) {
				if (re.getValue().equals(cadena)) {
					id = re.getKey();
				}
			}

		}
		return id;
	}

	public static void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public static void addMessageInfo(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	/**
	 * Método que permite crear un archivo temporal de imagen
	 * @param bytes
	 * @param nombreArchivo
	 * @return
	 */
//	public static String guardarImagenEnArchivoTemporal(byte[] bytes, String nombreArchivo) {
//		String ubicacionImagen = null;
//		ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
//		
//		File folder = new File("src/main/resources/firmas/");
//		if (!folder.exists()) {
//			folder.mkdirs();
//		}
//		
//		String path = sc.getRealPath("src/main/resources/firmas/" + nombreArchivo);
//		
//		File f = null;
//		InputStream in = null;
//		try {
//			f = new File(path);
//			in = new ByteArrayInputStream(bytes);
//			
//			FileOutputStream out = new FileOutputStream(f.getAbsolutePath());
//			
//			int c = 0;
//			while ((c = in.read()) >= 0) {
//				out.write(c);
//			}
//			out.flush();
//			out.close();
//			ubicacionImagen = "fsrc/main/resources/firmas/" + nombreArchivo;
//		} catch (Exception e) {
//			System.err.println("No se pudo cargar la imagen");
//			e.printStackTrace();
//		}
//		return ubicacionImagen;
//	}

	/**
	 * Método que convierte un array de bytes en archivo
	 * @param archivo
	 * @param nombre
	 * @return
	 */
	public static File arraytoFile(byte[] archivo, String nombre) {
		File file = null;
		try {
			FileInputStream fileInputStream = null;
			String directory = System.getenv("JBOSS_HOME") + File.separator + "standalone" 
					+ File.separator + "tmp" + File.separator + nombre;
			file = new File(directory);

			// convert array of bytes into file
			FileOutputStream fileOuputStream = new FileOutputStream(file);
			fileOuputStream.write(archivo);
			fileOuputStream.close();

		} catch (IOException ex) {
			Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return file;

	}

	public static byte[] readFully(InputStream input) throws IOException {
		byte[] contenidoDelFichero = null;
		BufferedInputStream theBIS = null;
		byte[] buffer = new byte[8 * 1024];
		int leido = 0;
		ByteArrayOutputStream theBOS = new ByteArrayOutputStream();

		try {
			theBIS = new BufferedInputStream(input);
			while ((leido = theBIS.read(buffer)) >= 0) {
				theBOS.write(buffer, 0, leido);
			}
			// Fichero leido del todo, pasamos el contenido
			// del BOS al byte[]
			contenidoDelFichero = theBOS.toByteArray();
			// Liberamos y cerramos para ser eficientes
			theBOS.reset();
			// Este close no va dentro de un try/catch por que
			// BOS es un Stream especial y close no hace nada
			theBOS.close();
		} catch (IOException e1) {
			// Error leyendo el fichero así que no tenemos
			// en memoria el fichero.
			e1.printStackTrace();
		} finally {
			if (theBIS != null) {
				try {
					theBIS.close();
				} catch (IOException e) {
					// Error cerrando stream del fichero
					e.printStackTrace();
				}
			}
		}
		return contenidoDelFichero;
	}

	public static String formatearFecha(Date fecha) {

		SimpleDateFormat formateador = new SimpleDateFormat("dd'/'MM'/'yyyy");
		formateador.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
		String fechaFormat = formateador.format(fecha);
		return fechaFormat;
	}

	public static String formatearHora(Date hora) {

		SimpleDateFormat formateador = new SimpleDateFormat("hh:mm a");
		formateador.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
		String fechaFormat = formateador.format(hora);
		return fechaFormat;
	}

	public static String formatearFechaElectronico(Date fecha) {

		SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("ES"));
		formateador.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
		String fechaFormat = formateador.format(fecha);
		return fechaFormat;
	}

	public static Date formatearCadenaFecha(String fecha) {

		SimpleDateFormat formateador = null;
		if (fecha.contains("/")) {
			formateador = new SimpleDateFormat("dd'/'MM'/'yyyy");
		} else if (fecha.contains("-")) {
			formateador = new SimpleDateFormat("dd'-'MM'-'yyyy");
		}
		formateador.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
		Date fechaFormat;
		try {
			fechaFormat = formateador.parse(fecha);
			return fechaFormat;
		} catch (ParseException ex) {
			Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return new Date();
	}

	public static String formatearFechaHora(Date fecha) {
		SimpleDateFormat formateador = new SimpleDateFormat("dd'/'MM'/'yyyy   hh:mm a");
		formateador.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
		String fechaFormat = formateador.format(fecha);
		return fechaFormat;
	}

	public static Map<String, Long> ValidarRadicado(String cadena) {
		String letras = "";
		String numeros = "";
		Long numeroFormateado = null;
		Map<String, Long> mapaCadena = new HashMap<>();
		for (short indice = 0; indice < cadena.length(); indice++) {
			char caracter = cadena.charAt(indice);
			if (isNumeric(caracter)) {
				numeros += caracter;
			} else {
				letras += caracter;
			}
		}
		if (letras.contains("-")) {
			String[] l = letras.split("-");
			letras = l[0];
		}
		numeroFormateado = new Long(numeros);
		mapaCadena.put(letras, numeroFormateado);
		return mapaCadena;
	}

	public static Boolean validarCadenaRadicado(String cadena) {
		String letras = "";
		String numeros = "";
		for (short indice = 0; indice < cadena.length(); indice++) {
			char caracter = cadena.charAt(indice);
			if (isNumeric(caracter)) {
				numeros += caracter;
			} else {
				letras += caracter;
			}
		}
		if (StringUtils.isNotBlank(numeros)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isNumeric(char caracter) {
		try {
			Integer.parseInt(String.valueOf(caracter));
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	public static <K extends Comparable, V extends Comparable> Map<K, V> sortByKeys(Map<K, V> map) {
		List<K> keys = new LinkedList<K>(map.keySet());
		Collections.sort(keys);

		// LinkedHashMap will keep the keys in the order they are inserted
		// which is currently sorted on natural ordering
		Map<K, V> sortedMap = new LinkedHashMap<K, V>();
		for (K key : keys) {
			sortedMap.put(key, map.get(key));
		}

		return sortedMap;
	}

	public static String organizarCadenaXComas(List<String> lista) {
		int cont = 0;
		StringBuilder cadena = new StringBuilder();
		for (String nombre : lista) {
			cont++;
			cadena.append(nombre);
			if (lista.size() > cont) {
				cadena.append(",");
			}
		}
		return cadena.toString();
	}

	public static Boolean contains(Collection<Actor> list, Actor actor) {
		for (Actor actor1 : list) {
			if (actor != null && actor1.getId().equals(actor.getId())) {
				return true;
			}
		}
		return false;
	}

	public static void concateTiff(String[] inputTiffs, String outputTiff) {
		OutputStream out = null;
		try {
			// 2 single page TIF to be in a multipage
//            String[] tifs = {
//                "C:/temp/test01.tif",
//                "C:/temp/test02.tif"
//            };  
			int numTifs = inputTiffs.length; // 2 pages
			BufferedImage image[] = new BufferedImage[numTifs];
			for (int i = 0; i < numTifs; i++) {
				SeekableStream ss = null;
				try {
					ss = new FileSeekableStream(inputTiffs[i]);
					com.sun.media.jai.codec.ImageDecoder decoder = ImageCodec.createImageDecoder("tiff", ss, null);
					PlanarImage pi = new NullOpImage(decoder.decodeAsRenderedImage(0), null, null, OpImage.OP_IO_BOUND);
					image[i] = pi.getAsBufferedImage();
					ss.close();
				} catch (IOException ex) {
					Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
				} finally {
					try {
						ss.close();
					} catch (IOException ex) {
						Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
			TIFFEncodeParam params = new TIFFEncodeParam();
			params.setCompression(TIFFEncodeParam.COMPRESSION_DEFLATE);
			out = new FileOutputStream("D:/multipage.tif");
			com.sun.media.jai.codec.ImageEncoder encoder = ImageCodec.createImageEncoder("tiff", out, params);
			List<BufferedImage> list = new ArrayList<BufferedImage>(image.length);
			for (int i = 1; i < image.length; i++) {
				list.add(image[i]);
			}
			params.setExtraImages(list.iterator());
			encoder.encode(image[0]);
			out.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				out.close();
			} catch (IOException ex) {
				Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public static boolean dinamicQueryString(String variableLabel, String variableValue, String consulta,
			boolean iswhere, StringBuilder query, Map<String, Object> parameters) {
		if (iswhere) {
			query.append(" and ").append(consulta);
			parameters.put(variableLabel, variableValue);
		} else {
			query.append(" where ").append(consulta);
			parameters.put(variableLabel, variableValue);
			iswhere = true;
		}
		return iswhere;
	}

	public static boolean dinamicQueryObject(String variableLabel, Object variableValue, String consulta,
			boolean iswhere, StringBuilder query, Map<String, Object> parameters) {
		if (iswhere) {
			query.append(" and ").append(consulta);
			parameters.put(variableLabel, variableValue);
		} else {
			query.append(" where ").append(consulta);
			parameters.put(variableLabel, variableValue);
			iswhere = true;
		}
		return iswhere;
	}

	public static boolean dinamicQueryDate(String fechaInicioLabel, Date fechaInicioValue, String fechaFinLabel,
			Date fechaFinValue, String consulta, boolean iswhere, StringBuilder query, Map<String, Object> parameters) {
		if (iswhere) {
			query.append(" and ").append(consulta);
			parameters.put(fechaInicioLabel, fechaInicioValue);
			parameters.put(fechaFinLabel, fechaFinValue);
		} else {
			query.append(" where ").append(consulta);
			parameters.put(fechaInicioLabel, fechaInicioValue);
			parameters.put(fechaFinLabel, fechaFinValue);
			iswhere = true;
		}
		return iswhere;
	}

	public static String textoCapitalCompleto(String texto) {
		if (StringUtils.isNotBlank(texto)) {
			return WordUtils.capitalizeFully(texto);
		} else {
			return "";
		}
	}

	static private final int BASELENGTH = 255;
	static private final int LOOKUPLENGTH = 16;
	static private byte[] hexNumberTable = new byte[BASELENGTH];
	static private byte[] lookUpHexAlphabet = new byte[LOOKUPLENGTH];

	static {
		for (int i = 0; i < BASELENGTH; i++) {
			hexNumberTable[i] = -1;
		}
		for (int i = '9'; i >= '0'; i--) {
			hexNumberTable[i] = (byte) (i - '0');
		}
		for (int i = 'F'; i >= 'A'; i--) {
			hexNumberTable[i] = (byte) (i - 'A' + 10);
		}
		for (int i = 'f'; i >= 'a'; i--) {
			hexNumberTable[i] = (byte) (i - 'a' + 10);
		}

		for (int i = 0; i < 10; i++) {
			lookUpHexAlphabet[i] = (byte) ('0' + i);
		}
		for (int i = 10; i <= 15; i++) {
			lookUpHexAlphabet[i] = (byte) ('A' + i - 10);
		}
	}

	static public byte[] decode(byte[] binaryData) {
		if (binaryData == null) {
			return null;
		}
		int lengthData = binaryData.length;
		if (lengthData % 2 != 0) {
			return null;
		}

		int lengthDecode = lengthData / 2;
		byte[] decodedData = new byte[lengthDecode];
		for (int i = 0; i < lengthDecode; i++) {
			if (!isHex(binaryData[i * 2]) || !isHex(binaryData[i * 2 + 1])) {
				return null;
			}
			decodedData[i] = (byte) ((hexNumberTable[binaryData[i * 2]] << 4) | hexNumberTable[binaryData[i * 2 + 1]]);
		}
		return decodedData;
	}

	/**
	 * byte to be tested if it is Base64 alphabet
	 *
	 * @param octect
	 * @return
	 */
	static boolean isHex(byte octect) {
		return (hexNumberTable[octect] != -1);
	}

	public static Date removeHourToDate(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static String urlServer() {
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream(System.getenv("JBOSS_HOME") + File.separator + "properties"
					+ File.separator + "certificado.properties"));
			String server = properties.getProperty("urlServer");
			if (StringUtils.isNotBlank(server)) {
				return server;
			}
		} catch (IOException ex) {
			Logger.getLogger(ValidacionesUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "localhost";
	}

	public static String obtenerCuerpoCorreo(String cuerpo) {
		StringBuilder message = new StringBuilder("");

		message.append("<html>");
		message.append("<head>");
		message.append("<meta charset=\"utf-8\">");
		message.append("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
		message.append("<title>email admin</title>");
		message.append("<link rel=\"stylesheet\" >");
		message.append("<style type=\"text/css\">");
		message.append("hr {border: 1px solid #fff; height: 2px; background-color: #ECEFF1;}");
		message.append("h2{");
		message.append("font-weight: normal;");
		message.append("}");
		message.append("</style>");
		message.append("</head>");
		message.append("<body >");
		message.append("<table width=\"900\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		message.append("<tr>");
		message.append(
				"<td align=\"left\" valign=\"top\" background=\"https://s3.amazonaws.com/Beesoft-logos/tempo-correo.png\" style=\"background-repeat: no-repeat;\"><br>");
		message.append("<br>");
		message.append(
				"<div style=\"margin-top: 20px; top: 0; z-index: 2; padding:10px !important; margin-bottom: 20px; width: 800px;\">");
		message.append(
				"<div style=\"margin-top: 40px; margin-bottom: 220px; color: #424242; padding: 80px 40px 0px 100px !important;\" id=\"text-styles\">");

		message.append(cuerpo);

		message.append("</div>");
		message.append("</div>");
		message.append("</td>");
		message.append("</tr>");
		message.append("</tbody>");
		message.append("</table>");
		message.append("</td>");
		message.append("</tr>");
		message.append("</table>");
		message.append("</body>");
		message.append("</html>");

		return message.toString();
	}

	public static String mostrarNombreJefe(Oficina oficina) {
		String nombre = "";
		if (oficina != null) {
			if (oficina.getJefeACargo()) {
				nombre = oficina.getJefe() + " - " + oficina.getDescripcion();
			} else {
				nombre = oficina.getJefeEncargado() + " - " + oficina.getDescripcion() + " (E)";
			}
		}
		return nombre;
	}

	public static Long mostrarIdJefe(Oficina oficina) {
		Long id = null;
		if (oficina != null) {
			if (oficina.getJefeACargo()) {
				id = oficina.getJefe().getId();
			} else {
				id = oficina.getJefeEncargado().getId();
			}
		}
		return id;
	}

	public static Object[] mostrarJefe(Oficina oficina) {
		Actor jefe = null;
		Object[] resultado = new Object[2];
		if (oficina != null) {
			if (oficina.getJefeACargo() != null) {
				if (oficina.getJefeACargo()) {
					jefe = oficina.getJefe();
					resultado[0] = jefe;
					resultado[1] = oficina.getCargoJefe();
				} else {
					jefe = oficina.getJefeEncargado();
					resultado[0] = jefe;
					resultado[1] = oficina.getCargoJefe() + " (E)";
				}
			}
		}
		return resultado;
	}

}
