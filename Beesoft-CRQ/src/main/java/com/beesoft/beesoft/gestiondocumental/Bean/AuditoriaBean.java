/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.AuditoriaLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.AuditoriaDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Cristhian
 */
@Stateless
public class AuditoriaBean implements AuditoriaLocal {

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

    @Override
    public void crearAuditoria(String accion, Actor usuario, Documento documento) {
        AuditoriaDocumento auditoria = new AuditoriaDocumento();
        auditoria.setFechaHora(new Date());
        auditoria.setUsuario(usuario);
        auditoria.setAccion(accion);
        auditoria.setDocumento(documento);
        em.persist(auditoria);
    }

    @Override
    public List<AuditoriaDocumento> buscarDocumentoEliminados(String radicacion, Date fechaIni, Date fechaFin, Long usuario,  String idCiudad) {
    	try {
    		Query q = null;
            StringBuilder query = new StringBuilder("select a from AuditoriaDocumento a where a.documento.activo=false and a.documento.anulado=true ");
            Map<String, Object> parameters = new HashMap<>();
            
            if (StringUtils.isNotBlank(idCiudad)) {
    			query.append(" and a.documento.ciudad.id=:idCiudad");
    			parameters.put("idCiudad", Long.parseLong(idCiudad));
    		}

            if (StringUtils.isNotBlank(radicacion)) {
                query.append(" and a.documento.radicacion=:radicacion");
                parameters.put("radicacion", radicacion);
            }

            if (fechaIni != null && fechaFin != null) {

                query.append(" and a.documento.fechaEmision BETWEEN :fechaIni AND :fechaFin");
                parameters.put("fechaIni", fechaIni);
                parameters.put("fechaFin", fechaFin);
            }

            if (usuario != null) {
                query.append(" and a.usuario.id=:usuario");
                parameters.put("usuario", usuario);
            }
            q = em.createQuery(query.toString());

            if (!parameters.isEmpty()) {
                for (Map.Entry<String, Object> p : parameters.entrySet()) {
                    q.setParameter(p.getKey(), p.getValue());
                }
            }

            List<AuditoriaDocumento> doc = q.getResultList();

            if (doc != null && !doc.isEmpty()) {
                return doc;
            }

            return null;
		} catch (Exception e) {
			ValidacionesUtil.addMessage("Error:"+e.getMessage());
			return null;
		}
        
    }

    @Override
    public List<AuditoriaDocumento> buscarDocumentoAcciones(Date fechaIni, Date fechaFin, Long usuario, String accion) {
        Query q = null;
        StringBuilder query = new StringBuilder("select a from AuditoriaDocumento a ");
        Map<String, Object> parameters = new HashMap<String, Object>();
        Boolean isWhere = false;

        if (fechaIni != null && fechaFin != null) {
            String query0 = "a.documento.fechaEmision BETWEEN :fechaIni AND :fechaFin";
            isWhere = ValidacionesUtil.dinamicQueryDate("fechaIni", fechaIni, "fechaFin", fechaFin, query0, isWhere, query, parameters);
        }

        if (usuario != null) {
            String query1 = "a.usuario.id=:usuario";
            isWhere = ValidacionesUtil.dinamicQueryObject("usuario", usuario, query1, isWhere, query, parameters);
        }

        String query2 = "a.accion=:accion";
        isWhere = ValidacionesUtil.dinamicQueryString("accion", accion, query2, isWhere, query, parameters);

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        List<AuditoriaDocumento> doc = q.getResultList();

        if (doc != null && !doc.isEmpty()) {
            return doc;
        }

        return null;
    }

}
