/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.converters;

import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;

/**
 *
 * @author Cristhian Camilo Fernandez Valencia <cfernandez@sumset.com>
 */
@Named(value = "oficinaConverter")
public class OficinaConverter implements Converter {

    @EJB
    private OficinaLocal oficinaBean;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                Oficina oficina = oficinaBean.buscarOficina(Long.valueOf(value));
                return oficina;
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            } catch (Exception ex) {
                Logger.getLogger(OficinaConverter.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            return null;
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            return String.valueOf(((Oficina) object).getId());
        } else {
            return null;
        }
    }

}
