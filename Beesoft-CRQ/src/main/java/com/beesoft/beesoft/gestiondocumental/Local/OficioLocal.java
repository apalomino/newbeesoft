/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Cristhian
 */
@Local
public interface OficioLocal {

    public void crearOficio(String radicacion, String clase, Date fechaEmision, String titulo, String asunto, String contenido, List<Trd> listaTrd, String elaboro, String aprobo, String version, String modificacion, Actor radicador, Documento documentoReferencia, List<Actor> destinos, List<ArchivoDocumento> archivos) throws Exception;

    public void editarOficio(Long idOficio, String titulo, String asunto, String contenido, List<Trd> listaTrdNueva, List<Trd> listaTrdEliminar, String elaboro, String aprobo, String version, String modificacion, Documento documentoReferencia, List<Actor> destinos, List<ArchivoDocumento> archivosNuevos, List<ArchivoDocumento> archivosEliminados) throws Exception;

    public List<Documento> buscarOficios(String radicacion, Date fechaIni, Date fechaFin, String contenido, String clase) throws Exception;

    public List<Actor> consultarDestinoDocumento(Long idDocumento);

    public List<String> buscarDestinosOld(String id) throws Exception;

    public List<DocumentoDTO> buscarOficioOld(String radicacion, Date fechaIni, Date fechaFin, String contenido, String clase);
}
