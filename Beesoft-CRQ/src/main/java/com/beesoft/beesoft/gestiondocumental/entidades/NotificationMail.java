/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Cristhian
 */
@Entity
public class NotificationMail implements Serializable {

    private Long id;
    private Actor fromAdress;
    private List<Actor> destinatarios;
    private String asunto;
    private String mensaje;
    private Date fechaEnvio;
    private String moduloEnvio;

    public NotificationMail() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public Actor getFromAdress() {
        return fromAdress;
    }

    public void setFromAdress(Actor fromAdress) {
        this.fromAdress = fromAdress;
    }

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.DETACH)
    @JoinTable(
            name = "destinations",
            joinColumns = @JoinColumn(name = "notification", unique = false),
            inverseJoinColumns = @JoinColumn(name = "destination", unique = false)
    )
    public List<Actor> getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(List<Actor> destinatarios) {
        this.destinatarios = destinatarios;
    }

    @Column(length = 10000)
    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    @Column(length = 100000)
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getModuloEnvio() {
        return moduloEnvio;
    }

    public void setModuloEnvio(String moduloEnvio) {
        this.moduloEnvio = moduloEnvio;
    }
    
}
