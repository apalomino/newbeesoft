/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import javax.ejb.Local;

/**
 *
 * @author ISSLUNO
 */
@Local
public interface GeneradorLocal {

    public String consecutivoRadicacionCorreoXLetra(String letra) throws Exception;

}
