/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author Cristhian
 */
public class EmpresaCorreoDTO {

    private String empresa;
    private String numero;
    private String destino;
    private String direccion1;
    private String ciudad;
    private String asunto;
    private String area;
    private String radicacion;
    private String radicacionSol;
    private String telefono;
    private String fEntrega;
    private String anexos;
    private String notif;
    private String guia;

    public EmpresaCorreoDTO() {
    }

    public EmpresaCorreoDTO(String empresa, String numero, String destino, String direccion1, String ciudad, String asunto, String area, String radicacion, String anexos, String notif, String guia) {
        this.empresa = empresa;
        this.numero = numero;
        this.destino = destino;
        this.direccion1 = direccion1;
        this.ciudad = ciudad;
        this.asunto = asunto;
        this.area = area;
        this.radicacion = radicacion;
        this.anexos = anexos;
        this.notif = notif;
        this.guia = guia;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public String getAnexos() {
        return anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }

    public String getNotif() {
        return notif;
    }

    public void setNotif(String notif) {
        this.notif = notif;
    }

    public String getGuia() {
        return guia;
    }

    public void setGuia(String guia) {
        this.guia = guia;
    }

    public String getRadicacionSol() {
        return radicacionSol;
    }

    public void setRadicacionSol(String radicacionSol) {
        this.radicacionSol = radicacionSol;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getfEntrega() {
        return fEntrega;
    }

    public void setfEntrega(String fEntrega) {
        this.fEntrega = fEntrega;
    }

    @Override
    public boolean equals(Object obj) {
        return radicacion.equals(((EmpresaCorreoDTO) obj).getRadicacion()); //To change body of generated methods, choose Tools | Templates.
    }

}
