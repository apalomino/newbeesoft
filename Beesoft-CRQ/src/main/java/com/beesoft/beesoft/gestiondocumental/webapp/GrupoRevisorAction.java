/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.GrupoRevisorLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.GrupoRevisor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author cfernandez@sumset.com
 */
@Named("grupoAction")
@SessionScoped
public class GrupoRevisorAction implements Serializable {

    @EJB
    private GrupoRevisorLocal grupoBean;

    @Inject
    private ApplicationAction application;

    //----------------crear-------------------
    private String nombreGrupo;
    private List<Actor> revisoresGrupo;
    private List<GrupoRevisor> grupos;
    //----------------editar-------------------
    private Long idGrupo;
    private String nombreEditar;
    private List<Actor> listaIntegrantes;

    private List<Actor> destinations;

    @PostConstruct
    public void iniciar() {
        cargarDestinatarios();
        listarGrupos();
    }

    public void cargarDestinatarios() {
        destinations = new ArrayList<>();
        destinations = application.getResponsableIn();
    }

    public List<Actor> completeAsistentes(String query) {
        List<Actor> filtrados = new ArrayList<>();

        for (Actor actor : destinations) {
            if (StringUtils.startsWithIgnoreCase(actor.getNombreYApellido(), query)) {
                filtrados.add(actor);
            }
        }
        return filtrados;
    }

    public void crearGrupo() {
        int val = 0;
        val = ValidacionesUtil.validarCadena(nombreGrupo, "Nombre");
        val = ValidacionesUtil.validarListas(revisoresGrupo, "Lista revisores");
        if (val == 2) {
            grupoBean.crearGrupo(nombreGrupo, revisoresGrupo);
            listarGrupos();
            ValidacionesUtil.addMessageInfo("Se creo el grupo " + nombreGrupo);
        }
    }

    public void limpiar() {
        nombreGrupo = "";
        revisoresGrupo = new ArrayList<>();
    }

    public void listarGrupos() {
        try {
            grupos = grupoBean.listarGrupos();
        } catch (Exception ex) {
            Logger.getLogger(GrupoRevisorAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Actor> listarRevisores(Long id) {
        try {
            return grupoBean.consultarRevisoresGrupo(id);
        } catch (Exception ex) {
            Logger.getLogger(GrupoRevisorAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void seleccionarGrupo(GrupoRevisor grupoSeleccionado) {
        idGrupo = grupoSeleccionado.getId();
        nombreEditar = grupoSeleccionado.getNombreGrupo();
        try {
            listaIntegrantes = grupoBean.consultarRevisoresGrupo(grupoSeleccionado.getId());
        } catch (Exception ex) {
            Logger.getLogger(GrupoRevisorAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editarGrupo() {
        int val = 0;
        val += ValidacionesUtil.validarCadena(nombreEditar, "Nombre");
        val += ValidacionesUtil.validarListas(listaIntegrantes, "Lista revisores");
        if (val == 2) {
            grupoBean.editarGrupo(idGrupo, nombreEditar, listaIntegrantes);
            listarGrupos();
            ValidacionesUtil.addMessageInfo("Se actualizo el grupo " + nombreEditar);
        }
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public List<Actor> getRevisoresGrupo() {
        return revisoresGrupo;
    }

    public void setRevisoresGrupo(List<Actor> revisoresGrupo) {
        this.revisoresGrupo = revisoresGrupo;
    }

    public List<GrupoRevisor> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoRevisor> grupos) {
        this.grupos = grupos;
    }

    public List<Actor> getListaIntegrantes() {
        return listaIntegrantes;
    }

    public void setListaIntegrantes(List<Actor> listaIntegrantes) {
        this.listaIntegrantes = listaIntegrantes;
    }

    public String getNombreEditar() {
        return nombreEditar;
    }

    public void setNombreEditar(String nombreEditar) {
        this.nombreEditar = nombreEditar;
    }

}
