package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * The persistent class for the trd database table.
 *
 */
@Entity
public class TrdOld implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private TipoDocumentoOld tipodocumento;

    private Documento documento;

    private OficinaOld oficina;

    private SerieOld serie;

    private SubSerieOld subserie;
    
    private Boolean medioElectronico;

    public TrdOld() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public TipoDocumentoOld getTipodocumentoOld() {
        return this.tipodocumento;
    }

    public void setTipodocumentoOld(TipoDocumentoOld tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    
    @ManyToOne
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @ManyToOne
    public SerieOld getSerieOld() {
        return this.serie;
    }

    public void setSerieOld(SerieOld serie) {
        this.serie = serie;
    }

    @ManyToOne
    public SubSerieOld getSubSerieOld() {
        return this.subserie;
    }

    public void setSubSerieOld(SubSerieOld subserie) {
        this.subserie = subserie;
    }

    @ManyToOne
    public OficinaOld getOficinaOld() {
        return oficina;
    }

    public void setOficinaOld(OficinaOld oficina) {
        this.oficina = oficina;
    }

    public Boolean getMedioElectronico() {
        return medioElectronico;
    }

    public void setMedioElectronico(Boolean medioElectronico) {
        this.medioElectronico = medioElectronico;
    }
    
}
