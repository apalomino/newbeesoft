/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author SUMSET
 */

@Entity
public class ClasesDocumento implements Serializable{
  private static final long serialVersionUID = 1L;
    
    private Long id;  
    private String nombre;
    private String estado;
    
    public ClasesDocumento()
    {
        
    }
    
    public ClasesDocumento(String nombre, String estado) {
        this.nombre = nombre;
        this.estado = estado;
        System.out.println("****************** CLASE DOCUMENTO ********************");
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }    
    
}
