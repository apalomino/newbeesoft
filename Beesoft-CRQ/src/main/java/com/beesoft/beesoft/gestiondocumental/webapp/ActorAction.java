/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Departamento;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoPersona;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "actorAction")
@SessionScoped
public class ActorAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(ActorAction.class);

    @EJB
    private UtilidadesLocal utilidades;

    @EJB
    private OficinaLocal oficinaBean;

    @EJB
    private ActorLocal actorBean;

    @Inject
    private ApplicationAction application;

    /**
     * Datos del actor
     */
    private String nombres;
    private String apellidos;
    private String cedula;
    private String direccion;
    private String direccion1;
    private String direccion2;
    private String email;
    private String firma;
    private Date fechaNacimiento;
    private String ciudad;
    private TipoPersona tipo;
    private Actor jefe;
    private Oficina oficina;
    private Boolean estado;
    private Map<Long, String> listaPaises;
    private String pais;
    private Map<Long, String> listaDeptos;
    private String depto;
    private Map<Long, String> listaCiudades;

    private Long jefeCadena;
    private Map<Long, String> listaTipoPersona;
    private Long tipoCadena;
    private Map<Long, String> listaOficina;
    private Long oficinaCadena;
    private Long empresa;
    private Boolean showJefe = false;
    private Boolean showFirmaJefe = false;
    private Boolean showOficina = false;
    private Boolean showEmails = false;
    private String cargo;
    private String telefono;
    private String telefonoExt;
    
//    private StreamedContent graphicImage;
    private UploadedFile fileFirmaImg;
    
    //-----------------Buscar---------------------------------
    private String nombrePersona;
    private String cedulaPersona;
    private Long tipoCadenaPersona;
    private TipoPersona tipoPersona;
    private List<Actor> listaPersonal;
    private Actor actorSelected;

    private Boolean opcionVer = false;
    private Boolean opcionHab;
    private String crear;
    private Usuario usuarioLogin;
    private String graphicText;

    //------------------------comunicados-------------------------
    private String nombresComunicado;
    private String cedulaComunicado;
    private String ciudadComunicado;
    private String cargoComunicado;
    private String emailComunicado;
    private Long empresaComunicado;

    private String nombreEmp;
    private String ciudadEmp;
    private String direccionEmp;
    private String nitEmp;

    public ActorAction() {
    }

    /**
     * Metodo para crear un actor se hacen algunas validaciones.
     *
     * @return cadena para redireccion.
     */
    public String crearActor() {
        try {
            obtenerTipoPersona();
            Boolean create = validaciones();
            Actor Empresa = null;
            
            if (empresa != null) {
                Empresa = actorBean.buscarActor(empresa);
            }
            if (create) {
                Ciudad city = obtenerCiudad(ciudad);
                obtenerJefe();
                obtenerOficina();
                if (opcionVer != null && !opcionVer) {
                    actorBean.editarActor(actorSelected.getId(), nombres, cedula, direccion1, city, email,
                            tipo, telefono, telefonoExt, jefe, Empresa, cargo, oficina, firma, estado);
                    cargarExternos();
                    buscarPersonal();
                    limpiar();
                    return "volver";
                } else {
                    actorBean.crearActor(nombres, cedula, direccion1, city, email, tipo, telefono,
                            telefonoExt, jefe, Empresa, cargo, oficina, firma);
                    ValidacionesUtil.addMessageInfo(nombres + " fue ingresado(a) en el sistema");
                    limpiar();
                    cargarExternos();
                }
            }

        } catch (Exception e) {
            LOG.info(e);
        }
        return "";
    }

    public void editarActorDocumento() {
        obtenerTipoPersona();
        Boolean create = validaciones();
        Actor Empresa = null;
        if (empresa != null) {
            try {
                Empresa = actorBean.buscarActor(empresa);
            } catch (Exception ex) {
                Logger.getLogger(ActorAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (create) {
            Ciudad city = obtenerCiudad(ciudad);
            obtenerJefe();
            obtenerOficina();
            actorBean.editarActor(actorSelected.getId(), nombres, cedula, direccion1, city, email,
                    tipo, telefono, telefonoExt, jefe, Empresa, cargo, oficina, firma, estado);
            cargarExternos();
            limpiar();
        }
    }

    public void crearActorDocumento() {
        try {
            Actor Empresa = null;
            if (empresa != null) {
                Empresa = actorBean.buscarActor(empresa);
            }
            if (tipoCadena != null) {
                obtenerTipoPersona();
                Ciudad city = obtenerCiudad(ciudad);
                actorBean.crearActor(nombres, cedula, direccion1, city, email, tipo, telefono,
                        telefonoExt, jefe, Empresa, cargo, oficina, firma);
                application.buscarActoresEmpresa();
                limpiar();
            } else {
                ValidacionesUtil.addMessage("Seleccione el tipo de persona");
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void crearActorComunicado() {
        try {

            Actor Empresa = null;

            int val = 0;
            val += ValidacionesUtil.validarCadena(nombresComunicado, "Nombre del contacto");
            val += ValidacionesUtil.validarCadena(emailComunicado, "Email");
            Ciudad city = obtenerCiudad(ciudadComunicado);
            if (empresaComunicado > 0) {
                Empresa = actorBean.buscarActor(empresaComunicado);
                if (Empresa != null) {
                    val += 3;
                }
            } else {
                val += ValidacionesUtil.validarCadena(nombreEmp, "Nombre nueva empresa");
                val += ValidacionesUtil.validarCadena(nitEmp, "Nit nueva empresa");
                val += ValidacionesUtil.validarCadena(direccionEmp, "Dirección nueva empresa");
                tipoCadena = 4L;
                obtenerTipoPersona();
                if (val == 5) {
                    Empresa = actorBean.crearActor(nombreEmp, nitEmp, direccionEmp, city, "",
                            tipo, "", "", null, null, "", null, firma);
                } else {
                    ValidacionesUtil.addMessage("Los campos para la nueva Empresa son obligatorios");
                }
            }

            if (val == 5) {
                tipoCadena = 3L;
                obtenerTipoPersona();
                actorBean.crearActor(nombresComunicado, cedulaComunicado, "", city, emailComunicado,
                        tipo, null, null, null, Empresa, cargoComunicado, null, firma);
                application.destinatariosExternosInternos();
                limpiarComunicado();
                application.buscarActoresEmpresa();
                RequestContext.getCurrentInstance().execute("PF('dlgDestino').hide()");
            } else {
                ValidacionesUtil.addMessage("Los campos de Empresa Nombre del contacto e email son obligatorios");
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    /**
     * Método que se encarga de subir la imagen al servidor
     * @param event
     * @throws Exception
     */
//    public void cargarFirma(FileUploadEvent event) throws Exception {
//        firma = event.getFile().getContents();
//        actor.setFirma(event.getFile().getContents());
//        imagenFirma = ValidacionesUtil.arraytoFile(actorSelected.getFirma(), event.getFile().getFileName());
//    }


    public Boolean validaciones() {
        int validate = 0;
        validate += ValidacionesUtil.validarNulo(tipoCadena, "Tipo persona");
        validate += ValidacionesUtil.validarCadena(ciudad, "Ciudad");
        validate += ValidacionesUtil.validarCadena(nombres, "Nombres");
        validate += ValidacionesUtil.validarCedulaNit(cedula, "Cedula/Nit");
//        if (tipo.getDescripcion().equalsIgnoreCase("jefe")) {
//            if (firma != null) {
//                validate++;
//            } else {
//                validate--;
//            }
//        }
        return validate >= 4;
    }

    /**
     * Metodo encargado de buscar personas.
     */
    public void buscarPersonal() {
        try {
            listaPersonal = new ArrayList<>();
            obtenerTipoPersonaBuscar();
            listaPersonal = actorBean.listaProcedencia(nombrePersona, cedulaPersona, tipoPersona, true);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    /**
     * Metodo encargado de seleccionar el actor y obtener la informacion para
     * mostrar en pantalla.
     *
     * @param opcion, la opcion para redireccionar la pantalla.
     * @param actorSelected, actor seleccionada.
     * @return retorna la regla de navegacion.
     */
    public String selectActor(Boolean opcion, Actor actorSelected) {
    	if (actorSelected != null) {
            this.actorSelected = actorSelected;
            nombres = actorSelected.getNombreYApellido();
            cedula = actorSelected.getCedula();
            direccion1 = actorSelected.getDireccion1();
            email = actorSelected.getEmail();
            telefono = actorSelected.getTelefono();
            telefonoExt = actorSelected.getTelefonoExt();
            ciudad = actorSelected.getCiudad().getDescripcion();
            Ciudad city = actorSelected.getCiudad();
            depto = city.getDepartamento().getDescripcion();
            Departamento dpo = city.getDepartamento();
            pais = dpo.getPais().getDescripcion();
            estado = actorSelected.getEstado();
            firma = actorSelected.getFirma();

            cargarDeptos();
            cargarCiudades();

            if (actorSelected.getTipopersona() != null) {
                tipoCadena = actorSelected.getTipopersona().getId();
                if (actorSelected.getTipopersona().getDescripcion().equals(ConstantsMail.TIPO_EMPLEADO)) {
                    if (actorSelected.getJefe() != null) {
                        jefeCadena = actorSelected.getJefe().getId();
                    }
                    if (actorSelected.getOficina() != null) {
                        oficinaCadena = actorSelected.getOficina().getId();
                    }
                    cargo = actorSelected.getCargo();
                    graphicText = "";
                    showFirmaJefe = false;
                    showJefe = true;
                } else if (actorSelected.getTipopersona().getDescripcion().equals(ConstantsMail.TIPO_JEFE)) {
                    if (actorSelected.getOficina() != null) {
                        oficinaCadena = actorSelected.getOficina().getId();
                    }
                    graphicText = System.getenv("JBOSS_HOME") + File.separator + "standalone" + File.separator + "tmp"
            				+ File.separator + "imgFirmas" + File.separator + actorSelected.getId();
                    showFirmaJefe = true;
                    cargo = actorSelected.getCargo();
                    showJefe = false;
                    showOficina = true;
                } else if (actorSelected.getTipopersona().getDescripcion().equals(ConstantsMail.TIPO_PERSONA)) {
                    if (actorSelected.getEmpresa() != null) {
                        empresa = actorSelected.getEmpresa().getId();
                    }
                    showEmails = false;
                    graphicText = "";
                    showFirmaJefe = false;
                } else {
                    graphicText = "";
                    showFirmaJefe = false;
                    showJefe = false;
                    showOficina = false;
                }
            }
            opcionVer = opcion;
            showCampos();
            return "edicion";
        } else {
            ValidacionesUtil.addMessageInfo("Debe Seleccionar un Actor para continuar");
            return "";
        }
    }

    public Map<Long, String> cargarDeptos() {
        try {
            listaDeptos = new HashMap<Long, String>();
            Long idPais = null;
            if (StringUtils.isNotBlank(pais)) {
                for (Entry<Long, String> p : listaPaises.entrySet()) {
                    if (p.getValue().equals(pais)) {
                        idPais = p.getKey();
                    }
                }
                listaDeptos = utilidades.cargarDeptos(idPais);
                return listaDeptos;
            } else {
                depto = "";
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;
    }

    public void cargarCiudades() {
        try {
            listaCiudades = new HashMap<Long, String>();
            Long idDepto = null;
            if (StringUtils.isNotBlank(depto)) {
                for (Entry<Long, String> d : listaDeptos.entrySet()) {
                    if (d.getValue().equals(depto)) {
                        idDepto = d.getKey();
                    }
                }
                listaCiudades = utilidades.cargarCiudades(idDepto);

            } else {
                ciudad = "";
            }

        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public Ciudad obtenerCiudad(String city) {
        try {
            Long idCiudad = null;
            for (Entry<Long, String> c : listaCiudades.entrySet()) {
                if (c.getValue().equals(city)) {
                    idCiudad = c.getKey();
                }
            }
            return utilidades.buscarCiudad(idCiudad);
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;
    }

    public void obtenerJefe() {
        try {
            jefe = null;
            jefe = actorBean.buscarActor(jefeCadena);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarPaises() {
        try {
            listaPaises = utilidades.cargarPaises();

        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarExternos() {
        application.cargarListaActores();
        application.buscarResponsable();
        application.cargarRevisores();
        application.buscarDestinatarios();
        application.buscarActoresEmpresa();
        application.cargarListaActoresSinUsuario();
        application.cargarJefes();
        application.buscarDestinatarios();
        application.llenarInformacionDestinoDto();
    }

    public void obtenerOficina() {
        try {
            oficina = null;
            oficina = oficinaBean.buscarOficina(oficinaCadena);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void obtenerTipoPersona() {
        try {
            tipo = utilidades.buscarTipo(tipoCadena);
        } catch (Exception ex) {
            LOG.info(ex);
        }

    }

    public void obtenerTipoPersonaBuscar() {
        try {
            tipoPersona = utilidades.buscarTipo(tipoCadenaPersona);
        } catch (Exception ex) {
            LOG.info(ex);
        }

    }

    public void cambiarEstado(Long id, Boolean estado) {
        actorBean.cambiarEstadoActor(id, estado);
        buscarPersonal();
        application.buscarResponsable();
        application.cargarRevisores();
        application.cargarListaActores();
        application.cargarListaActoresSinUsuario();
    }
//----------------------------utilidades------------------------------------------------------------

    public String volver() {
        limpiar();
        buscarPersonal();
        return "volver";
    }

    public void showCampos() {
        if (tipoCadena != null) {
            try {
                TipoPersona tp = utilidades.buscarTipo(tipoCadena);
                switch (tp.getDescripcion()) {
                    case ConstantsMail.TIPO_EMPLEADO:
                        showOficina = true;
                        showJefe = false;
                        showFirmaJefe = false;
                        showEmails = true;
                        break;
                    case ConstantsMail.TIPO_JEFE:
                        showOficina = true;
                        showJefe = false;
                        showFirmaJefe = false;
                        showEmails = true;
                        break;
                    case ConstantsMail.TIPO_EMPRESA:
                        showOficina = false;
                        showJefe = true;
                        showFirmaJefe = false;
                        showEmails = true;
                        jefeCadena = null;
                        oficinaCadena = null;
                        break;
                    default:
                        showOficina = false;
                        showJefe = false;
                        showEmails = false;
                        showFirmaJefe = false;
                        jefeCadena = null;
                        oficinaCadena = null;
                        break;
                }
            } catch (Exception ex) {
                LOG.info(ex);
            }
        }
    }

    @PostConstruct
    public void inicializar() {
        cargarPaises();
        pais = ConstantsMail.PAIS;
        cargarDeptos();
        depto = ConstantsMail.DEPTO;
        cargarCiudades();
        ciudad = ConstantsMail.CIUDAD;
        ciudadComunicado = ConstantsMail.CIUDAD;
        ciudadEmp = ConstantsMail.CIUDAD;
        listaPersonal = new ArrayList<Actor>();
        actorSelected = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        usuarioLogin = (Usuario) session.getAttribute("username");
    }

    public String getCrear() {
        limpiar();
        return crear;
    }

    public void reset() {
        nombrePersona = "";
        cedulaPersona = "";
        tipoCadenaPersona = null;
        tipoPersona = null;
        listaPersonal = new ArrayList<>();
    }

    public void limpiarComunicado() {
        nombresComunicado = "";
        ciudadComunicado = ConstantsMail.CIUDAD;
        cedulaComunicado = "";
        cargoComunicado = "";
        emailComunicado = "";
        empresaComunicado = 0L;
        nombreEmp = "";
        direccionEmp = "";
        ciudadEmp = ConstantsMail.CIUDAD;
        tipo = null;
        nitEmp = "";
        fileFirmaImg = null;
    }

    @PreDestroy
    public void limpiar() {
        pais = ConstantsMail.PAIS;
        depto = ConstantsMail.DEPTO;
        ciudad = ConstantsMail.CIUDAD;
        oficinaCadena = null;
        oficina = null;
        graphicText = null;
        tipoCadena = null;
        tipo = null;
        jefeCadena = null;
        jefe = null;
        nombres = "";
        fechaNacimiento = null;
        direccion1 = "";
        direccion2 = "";
        cedula = "";
        email = "";
        cargo = "";
        opcionVer = null;
        showJefe = false;
        showOficina = false;
        telefono = "";
        telefonoExt = "";
        empresa = null;
        firma = null;
        fileFirmaImg = null;
    }

    public String mostrarNombreJefe(Oficina oficina) {
        return ValidacionesUtil.mostrarNombreJefe(oficina);
    }

    public Long mostrarIdJefe(Oficina oficina) {
        return ValidacionesUtil.mostrarIdJefe(oficina);
    }

    /**
	 * Metodo que se encarga de subir y asignar la imagen de la firma
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void cargarFirma(FileUploadEvent event) throws Exception {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
				.getContext();
		String directory = System.getenv("JBOSS_HOME") + File.separator + "standalone" 
				+ File.separator + "tmp" + File.separator + "imgFirmas" + File.separator;
		fileFirmaImg = event.getFile();
		try {
			String file = saveFoto(fileFirmaImg, directory);
			firma = file;
			ValidacionesUtil.addMessageInfo("Imagen guardada correctamente");
		} catch (Exception e) {
			LOG.info(e);
		}
	}
	
	/**
	 * Metodo que guarda imagen de la firma en el servidor
	 * 
	 * @param archivo
	 *            archivo a agregar
	 * @param ruta
	 *            lugar donde se va a guardar
	 * @return el nombre del archivo
	 * @throws IOException
	 */
	public String saveFoto(UploadedFile archivo, String ruta) throws IOException {
		try (InputStream input = archivo.getInputstream()) {
			String nombreArchivo = archivo.getFileName();
			LOG.info("**********NOMBRE DE ARCHIVO A GUARDAR: "+nombreArchivo);
			Files.copy(input, new File(ruta, nombreArchivo).toPath(), StandardCopyOption.REPLACE_EXISTING);
			return nombreArchivo;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
    //----------------------------------getters-----------------------------
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getDireccion2() {
        return direccion2;
    }

    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public TipoPersona getTipo() {
        return tipo;
    }

    public void setTipo(TipoPersona tipo) {
        this.tipo = tipo;
    }

    public Actor getJefe() {
        return jefe;
    }

    public void setJefe(Actor jefe) {
        this.jefe = jefe;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public Map<Long, String> getListaPaises() {
        return listaPaises;
    }

    public void setListaPaises(Map<Long, String> listaPaises) {
        this.listaPaises = listaPaises;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Map<Long, String> getListaDeptos() {
        return listaDeptos;
    }

    public void setListaDeptos(Map<Long, String> listaDeptos) {
        this.listaDeptos = listaDeptos;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }

    public Map<Long, String> getListaCiudades() {
        return listaCiudades;
    }

    public void setListaCiudades(Map<Long, String> listaCiudades) {
        this.listaCiudades = listaCiudades;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Long getJefeCadena() {
        return jefeCadena;
    }

    public void setJefeCadena(Long jefeCadena) {
        this.jefeCadena = jefeCadena;
    }

    public Map<Long, String> getListaTipoPersona() {
        return listaTipoPersona;
    }

    public void setListaTipoPersona(Map<Long, String> listaTipoPersona) {
        this.listaTipoPersona = listaTipoPersona;
    }

    public Long getTipoCadena() {
        return tipoCadena;
    }

    public void setTipoCadena(Long tipoCadena) {
        this.tipoCadena = tipoCadena;
    }

    public Map<Long, String> getListaOficina() {
        return listaOficina;
    }

    public void setListaOficina(Map<Long, String> listaOficina) {
        this.listaOficina = listaOficina;
    }

    public Long getOficinaCadena() {
        return oficinaCadena;
    }

    public void setOficinaCadena(Long oficinaCadena) {
        this.oficinaCadena = oficinaCadena;
    }

    public Boolean getShowJefe() {
        return showJefe;
    }

    public void setShowJefe(Boolean showJefe) {
        this.showJefe = showJefe;
    }

    public Boolean getShowOficina() {
        return showOficina;
    }

    public void setShowOficina(Boolean showOficina) {
        this.showOficina = showOficina;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public List<Actor> getListaPersonal() {
        return listaPersonal;
    }

    public void setListaPersonal(List<Actor> listaPersonal) {
        this.listaPersonal = listaPersonal;
    }

    public Actor getActorSelected() {
        return actorSelected;
    }

    public void setActorSelected(Actor actorSelected) {
        this.actorSelected = actorSelected;
    }

    public Boolean getOpcionVer() {
        return opcionVer;
    }

    public void setOpcionVer(Boolean opcionEditar) {
        this.opcionVer = opcionEditar;
    }

    public Boolean getOpcionHab() {
        return opcionHab;
    }

    public void setOpcionHab(Boolean opcionHab) {
        this.opcionHab = opcionHab;
    }

    public String getCedulaPersona() {
        return cedulaPersona;
    }

    public void setCedulaPersona(String cedulaPersona) {
        this.cedulaPersona = cedulaPersona;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Long getTipoCadenaPersona() {
        return tipoCadenaPersona;
    }

    public void setTipoCadenaPersona(Long tipoCadenaPersona) {
        this.tipoCadenaPersona = tipoCadenaPersona;
    }

    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersona tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Boolean getShowEmails() {
        return showEmails;
    }

    public void setShowEmails(Boolean showEmails) {
        this.showEmails = showEmails;
    }

    public Boolean getShowFirmaJefe() {
        return showFirmaJefe;
    }

    public void setShowFirmaJefe(Boolean showFirmaJefe) {
        this.showFirmaJefe = showFirmaJefe;
    }

    public String getGraphicText() {
        return graphicText;
    }

    public void setGraphicText(String graphicText) {
        this.graphicText = graphicText;
    }

    public Long getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Long empresa) {
        this.empresa = empresa;
    }

    public String getNombresComunicado() {
        return nombresComunicado;
    }

    public void setNombresComunicado(String nombresComunicado) {
        this.nombresComunicado = nombresComunicado;
    }

    public String getCedulaComunicado() {
        return cedulaComunicado;
    }

    public void setCedulaComunicado(String cedulaComunicado) {
        this.cedulaComunicado = cedulaComunicado;
    }

    public String getCiudadComunicado() {
        return ciudadComunicado;
    }

    public void setCiudadComunicado(String ciudadComunicado) {
        this.ciudadComunicado = ciudadComunicado;
    }

    public String getCargoComunicado() {
        return cargoComunicado;
    }

    public void setCargoComunicado(String cargoComunicado) {
        this.cargoComunicado = cargoComunicado;
    }

    public String getEmailComunicado() {
        return emailComunicado;
    }

    public void setEmailComunicado(String emailComunicado) {
        this.emailComunicado = emailComunicado;
    }

    public Long getEmpresaComunicado() {
        return empresaComunicado;
    }

    public void setEmpresaComunicado(Long empresaComunicado) {
        this.empresaComunicado = empresaComunicado;
    }

    public String getNombreEmp() {
        return nombreEmp;
    }

    public void setNombreEmp(String nombreEmp) {
        this.nombreEmp = nombreEmp;
    }

    public String getCiudadEmp() {
        return ciudadEmp;
    }

    public void setCiudadEmp(String ciudadEmp) {
        this.ciudadEmp = ciudadEmp;
    }

    public String getDireccionEmp() {
        return direccionEmp;
    }

    public void setDireccionEmp(String direccionEmp) {
        this.direccionEmp = direccionEmp;
    }

    public String getNitEmp() {
        return nitEmp;
    }

    public void setNitEmp(String nitEmp) {
        this.nitEmp = nitEmp;
    }

    public String getTelefonoExt() {
        return telefonoExt;
    }

    public void setTelefonoExt(String telefonoExt) {
        this.telefonoExt = telefonoExt;
    }

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

}
