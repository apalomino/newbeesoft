package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * The persistent class for the oficina database table.
 *
 */
@Entity
public class Oficina implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Boolean activo;

    private Actor jefe;

    private Actor jefeEncargado;

    private Boolean jefeACargo;

    private String cargoJefe;

    private String descripcion;

    private String codigo;

    private TipoRecorrido tiporecorrido;
    
    private Boolean esretencion;
    
    private Long oficina_padre;
    
    private Boolean esinforme;
    

    public Oficina() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ManyToOne
    public TipoRecorrido getTiporecorrido() {
        return this.tiporecorrido;
    }

    public void setTiporecorrido(TipoRecorrido tiporecorrido) {
        this.tiporecorrido = tiporecorrido;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @OneToOne
    public Actor getJefe() {
        return jefe;
    }

    public void setJefe(Actor jefe) {
        this.jefe = jefe;
    }

    @OneToOne
    public Actor getJefeEncargado() {
        return jefeEncargado;
    }

    public void setJefeEncargado(Actor jefeEncargado) {
        this.jefeEncargado = jefeEncargado;
    }

    public Boolean getJefeACargo() {
        return jefeACargo;
    }

    public void setJefeACargo(Boolean jefeACargo) {
        this.jefeACargo = jefeACargo;
    }

    public String getCargoJefe() {
        return cargoJefe;
    }

    public void setCargoJefe(String cargoJefe) {
        this.cargoJefe = cargoJefe;
    }
    
    public Boolean getEsretencion() {
    	return esretencion;
    }
    
    public void setEsretencion(Boolean estado) {
    	this.esretencion = estado;
    }

	public Long getOficina_padre() {
		return oficina_padre;
	}

	public void setOficina_padre(Long oficina_padre) {
		this.oficina_padre = oficina_padre;
	}

	public Boolean getEsinforme() {
		return esinforme;
	}

	public void setEsinforme(Boolean esinforme) {
		this.esinforme = esinforme;
	}
    
    
}
