/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author cfernandez@sumset.com
 */
@Entity
public class GrupoRevisor implements Serializable {

    private Long id;
    private String nombreGrupo;
    private List<Actor> revisores;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany
    @JoinTable(
            name = "grupo_revisores",
            joinColumns = @JoinColumn(name = "grupo", unique = false),
            inverseJoinColumns = @JoinColumn(name = "revisor", unique = false)
    )
    public List<Actor> getRevisores() {
        return revisores;
    }

    public void setRevisores(List<Actor> revisores) {
        this.revisores = revisores;
    }

}
