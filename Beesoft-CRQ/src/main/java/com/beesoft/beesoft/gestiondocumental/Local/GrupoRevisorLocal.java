/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.GrupoRevisor;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cfernandez@sumset.com
 */
@Local
public interface GrupoRevisorLocal {

    public void crearGrupo(String nombreGrupo, List<Actor> revisores);

    public List<GrupoRevisor> listarGrupos() throws Exception;

    public List<Actor> consultarRevisoresGrupo(Long id) throws Exception;

    public void editarGrupo(Long id, String nombre, List<Actor> revisores);

    public GrupoRevisor buscarGrupo(Long id) throws Exception;
}
