/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.servlets;

import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.entidades.ComunicadosInternos;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import com.beesoft.beesoft.gestiondocumental.webapp.ComunicadosInternosAction;
import java.io.IOException;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristhian
 */
public class ComunicadoServlet extends HttpServlet {

    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    @Inject
    private ComunicadosInternosAction notification;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String radicacion = request.getParameter("radicacion");
        String tipoRedirect = request.getParameter("tipo");
        Usuario usuarioLogin = (Usuario) request.getSession().getAttribute("username");
        notification.setUsuarioLogin(usuarioLogin);
        Long id = Long.parseLong(radicacion);
        ComunicadosInternos mail = em.find(ComunicadosInternos.class, id);

        if (mail != null) {

            if (mail.getEstado().equals(ConstantsMail.APROBADO)) {
                tipoRedirect = "Env";
            }

            switch (tipoRedirect) {
                case "Env":
                    notification.infoCorreo(mail, false);
                    response.sendRedirect(request.getContextPath() + "/html/mail/Correo.xhtml");
                    break;
                case "Apr":
                    notification.selectPendientesAprobacion(mail);
                    response.sendRedirect(request.getContextPath() + "/html/mail/AprobacionComunicados.xhtml");
                    break;
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
