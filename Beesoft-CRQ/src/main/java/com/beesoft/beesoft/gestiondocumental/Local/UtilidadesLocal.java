/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.DatosConfiguracion;
import com.beesoft.beesoft.gestiondocumental.entidades.LetraRadicacion;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoContratos;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoPersona;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface UtilidadesLocal {

    /**
     * Metodo encargado de cargar los tipos de persona.
     *
     * @return lista de tipos de persona.
     * @throws Exception si hay algun error.
     */
    public List<TipoPersona> cargarTipoPersona() throws Exception;

    /**
     * Metodo encargado de cargar los paises.
     *
     * @return Lista de paises.
     * @throws Exception si hay algun error
     */
    public Map<Long, String> cargarPaises() throws Exception;

    /**
     * Metodo encargado de cargar los departamentos segun el pais.
     *
     * @param idPais, identificador del pais.
     * @return Lista de departamentos.
     * @throws Exception si hay algun error
     */
    public Map<Long, String> cargarDeptos(Long idPais) throws Exception;

    /**
     * Metodo encargado de cargar las ciudades segun el departamento.
     *
     * @param idDepto, identificador del departamento.
     * @return lista de ciudades.
     * @throws Exception si hay algun error
     */
    public Map<Long, String> cargarCiudades(Long idDepto) throws Exception;

    /**
     * Metodo encargado de buscar una ciudad por id.
     *
     * @param id, identificador de una ciudad.
     * @return CIudad.
     * @throws Exception si hay algun error
     */
    public Ciudad buscarCiudad(Long id) throws Exception;

    /**
     * Metodo encargado de cargar los tipos de persona.
     *
     * @return @throws Exception si hay algun error
     */
    public Map<Long, String> cargarTipos() throws Exception;

    /**
     * Metodo encargado de buscar tipo de persona por id.
     *
     * @param id, identificador del tipo de persona.
     * @return tipo de persona.
     * @throws Exception si hay algun error
     */
    public TipoPersona buscarTipo(Long id) throws Exception;

    public Map<String, String> cargarTiposContratosTexto(Boolean tipo) throws Exception;

    public TipoContratos buscarTiposContratos(String id);

    public void crearConfigCorreos(String emailEmisor, String password, String host, String puerto);

    public void editarConfigCorreos(String emailEmisor, String host, String password, String puerto,
            String starttls, String authetication, Date fechaCron);

    public DatosConfiguracion mostrarConfiguracionCorreo();

    public void crearLetrasRad(String letra);
    
    public void quitarLetrasRad(Long letraId);

    public void editarLetrasRad(String letra, Long valor);

    public List<LetraRadicacion> listarLetrasRad();
    
    public Map<String, Long> cargarCiudadesDepartamento(long idDeparrtamento);

}
