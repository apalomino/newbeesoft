/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

import com.beesoft.beesoft.gestiondocumental.entidades.Menu;

/**
 *
 * @author Cristhian
 */
public class PermisoDTO {

    private Menu menu;
    private Boolean permiso;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Boolean getPermiso() {
        return permiso;
    }

    public void setPermiso(Boolean permiso) {
        this.permiso = permiso;
    }

}
