/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.CategoriaDocumentoCalidad;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentoCalidad;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoOrganizacional;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author ISSLUNO
 */
@Local
public interface CalidadLocal {

    public void crearDocumentoSistema(String titulo, String descripcion, byte[] documento, String nombreDocumento, byte[] imagen,
            String nombreImagen, Date fechaCreacion, TipoOrganizacional tipo, CategoriaDocumentoCalidad categoriaDocumento, Actor usuarioUp);

    public List<DocumentoCalidad> buscarDocumentosCalidad(Long categoria, Long tipo, String titulo);

    public void editarDocumentoSistema(DocumentoCalidad calidad, String titulo, String descripcion, byte[] documento, String nombreDocumento, byte[] imagen,
            String nombreImagen, Date fechaCreacion, TipoOrganizacional tipo, CategoriaDocumentoCalidad categoriaDocumento);

    public List<DocumentoCalidad> cargarInforOrganizacional();

    public CategoriaDocumentoCalidad buscarCategoria(Long id);

    public TipoOrganizacional buscarTipoOrganizacional(Long id);

    public List<TipoOrganizacional> cargarListaTipos();

    public List<CategoriaDocumentoCalidad> cargarListaCategorias();

}
