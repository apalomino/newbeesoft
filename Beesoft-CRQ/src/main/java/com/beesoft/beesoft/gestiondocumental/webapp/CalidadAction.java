/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.CalidadLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficioLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.CategoriaDocumentoCalidad;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentoCalidad;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoOrganizacional;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "calidadAction")
@SessionScoped
public class CalidadAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(CalidadAction.class);

    @EJB
    private CalidadLocal calidadBean;

    @EJB
    private OficioLocal circularesBean;

    @EJB
    private DocumentoLocal documentoBean;

    private Usuario usuarioLogin;
    private List<CategoriaDocumentoCalidad> listaCategorias;
    private List<TipoOrganizacional> listaTipos;
    private DocumentoCalidad calidadShow;
    private Long idCalidad;
    private Long idCategoria;
    private Long idTipo;
    private String titulo;
    private String descripcion;
    private byte[] imagen;
    private byte[] documento;
    private String nombreDocumento;
    private String nombreImagen;
    private Date fechaCreacion;
    private Boolean mostrarTipos;

    //--------------------Gestionar-----------------------
    private List<DocumentoCalidad> listaDocumentos;
    private String tituloBusqueda;
    private Long idTipoBusqueda;
    private Long idCategoriaBusqueda;
    private Boolean showDocumento;
    private Boolean showImagen;

    //--------Info Org-----------------------------------
    private String info;
    private List<DocumentoCalidad> listaInfoOrg;
    private List<DocumentoCalidad> listaInfoOrgFiltered;

    //-------------Circulares-----------------------
    private String radicacionCircular;
    private Date fechaIniCircular;
    private Date fechaFinCircular;
    private String contenidoCircular;
    private List<Documento> listaOficios;
    private List<DocumentoDTO> listaOficiosOld;

    @PostConstruct
    public void init() {

        listaCategorias = new ArrayList<>();
        listaTipos = new ArrayList<>();
        listaCategorias = calidadBean.cargarListaCategorias();
        listaTipos = calidadBean.cargarListaTipos();

        if (FacesContext.getCurrentInstance() != null) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                    .getExternalContext().getSession(false);
            usuarioLogin = (Usuario) session.getAttribute("username");
        }
    }

    public void crearDocumentoCalidad() {
        if (idCategoria != null && documento != null) {
            CategoriaDocumentoCalidad categoria = calidadBean.buscarCategoria(idCategoria);
            TipoOrganizacional tipo = null;
            if (idTipo != null) {
                tipo = calidadBean.buscarTipoOrganizacional(idTipo);
            }
            calidadBean.crearDocumentoSistema(titulo, descripcion, documento, nombreDocumento, imagen, nombreImagen, fechaCreacion, tipo, categoria, usuarioLogin.getActor());
            limpiar();
            ValidacionesUtil.addMessageInfo("El documento se ha creado en el sistema");
        } else {
            ValidacionesUtil.addMessage("Los campos categoria y documento son obligatorios");
        }
    }

    public void buscarDocumentos() {
        listaDocumentos = new ArrayList<>();
        listaDocumentos = calidadBean.buscarDocumentosCalidad(idCategoriaBusqueda, idTipoBusqueda, tituloBusqueda);
    }

    public String seleccionarDocumento(Boolean accion, DocumentoCalidad calidad) {
        limpiar();
        calidadShow = calidad;
        idCalidad = calidad.getId();
        idCategoria = calidad.getCategoriaDocumento().getId();
        mostrarTiposOrg();
        if (StringUtils.isNotBlank(calidad.getTitulo())) {
            titulo = calidad.getTitulo();
        }
        if (StringUtils.isNotBlank(calidad.getDescripcion())) {
            descripcion = calidad.getDescripcion();
        }
        if (calidad.getFechaCreacion() != null) {
            fechaCreacion = calidad.getFechaCreacion();
        }
        if (calidad.getTipo() != null) {
            idTipo = calidad.getTipo().getId();
        }

        if (StringUtils.isNotBlank(calidad.getNombreDocumento())) {
            nombreDocumento = calidad.getNombreDocumento();
            showDocumento = true;
        } else {
            showDocumento = false;
        }

        if (StringUtils.isNotBlank(calidad.getNombreImagen())) {
            nombreImagen = calidad.getNombreImagen();
            showImagen = true;
        } else {
            showImagen = false;
        }

        if (accion) {
            return "si";
        } else {
            return "no";
        }

    }

    public void buscarCirculares() {
        try {
            listaOficiosOld = circularesBean.buscarOficioOld(radicacionCircular, fechaIniCircular, fechaFinCircular, contenidoCircular, ConstantsMail.OFICIO_CIRCULAR);
            listaOficios = circularesBean.buscarOficios(radicacionCircular, fechaIniCircular, fechaFinCircular, contenidoCircular, ConstantsMail.OFICIO_CIRCULAR);
            listaOficiosOld.addAll(addListaDTO(listaOficios));
        } catch (Exception ex) {
            Logger.getLogger(CalidadAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<DocumentoDTO> addListaDTO(List<Documento> lista) {
        List<DocumentoDTO> listaDTO = new ArrayList<>();
        for (Documento doc : lista) {
            DocumentoDTO dto = null;
            dto = new DocumentoDTO(doc, doc.getRadicacion(), doc.getFechaEmision(), doc.getFechaSistema(), doc.getAsunto(), doc.getObservacion(), doc.getClasedocumento(), Boolean.FALSE);
            listaDTO.add(dto);
        }
        return listaDTO;
    }

    public List<ArchivoDocumento> archivosAdj(DocumentoDTO oficio) {
        if (oficio != null) {
            return documentoBean.consultarArchivoDocumento(oficio.getDocumento().getId());
        }
        return null;
    }

    public String formatFecha(Date fecha) {
        return ValidacionesUtil.formatearFecha(fecha);
    }

    public String crear() {
        limpiar();
        return "crear";
    }

    public String limpiar() {
        idCalidad = null;
        titulo = "";
        descripcion = "";
        idTipo = null;
        idCategoria = null;
        mostrarTiposOrg();
        documento = null;
        nombreDocumento = "";
        nombreImagen = "";
        imagen = null;
        fechaCreacion = null;
        calidadShow = null;
        showImagen = false;
        showDocumento = false;

        return "cancelar";
    }

    public String mostrarTipoDoc(DocumentoCalidad calidad) {
        if (calidad.getTipo() != null) {
            return calidad.getTipo().getDescripcion();
        } else {
            return "";
        }
    }

    public void limpiarBusqueda() {
        tituloBusqueda = "";
        idTipoBusqueda = null;
        idCategoriaBusqueda = null;
        listaDocumentos = new ArrayList<>();
    }

    public void mostrarTiposOrg() {
        if (idCategoria != null && idCategoria == 1) {
            mostrarTipos = true;
        } else {
            mostrarTipos = false;
        }
    }

    public void mostrarTiposOrgBusqueda() {
        if (idCategoriaBusqueda != null && idCategoriaBusqueda == 1) {
            mostrarTipos = true;
        } else {
            mostrarTipos = false;
        }

    }

    public void imageUpload(FileUploadEvent event) throws Exception {
        String fileName = FilenameUtils.getName(event.getFile().getFileName());
        imagen = event.getFile().getContents();
        nombreImagen = fileName;
    }

    public void DocumentUpload(FileUploadEvent event) throws Exception {
        String fileName = FilenameUtils.getName(event.getFile().getFileName());
        documento = event.getFile().getContents();
        nombreDocumento = fileName;
    }

    public Usuario getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(Usuario usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public List<CategoriaDocumentoCalidad> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<CategoriaDocumentoCalidad> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public List<TipoOrganizacional> getListaTipos() {
        return listaTipos;
    }

    public void setListaTipos(List<TipoOrganizacional> listaTipos) {
        this.listaTipos = listaTipos;
    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Long getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Long idTipo) {
        this.idTipo = idTipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getMostrarTipos() {
        return mostrarTipos;
    }

    public void setMostrarTipos(Boolean mostrarTipos) {
        this.mostrarTipos = mostrarTipos;
    }

    public List<DocumentoCalidad> getListaDocumentos() {
        return listaDocumentos;
    }

    public void setListaDocumentos(List<DocumentoCalidad> listaDocumentos) {
        this.listaDocumentos = listaDocumentos;
    }

    public String getTituloBusqueda() {
        return tituloBusqueda;
    }

    public void setTituloBusqueda(String tituloBusqueda) {
        this.tituloBusqueda = tituloBusqueda;
    }

    public Long getIdTipoBusqueda() {
        return idTipoBusqueda;
    }

    public void setIdTipoBusqueda(Long idTipoBusqueda) {
        this.idTipoBusqueda = idTipoBusqueda;
    }

    public Long getIdCategoriaBusqueda() {
        return idCategoriaBusqueda;
    }

    public void setIdCategoriaBusqueda(Long idCategoriaBusqueda) {
        this.idCategoriaBusqueda = idCategoriaBusqueda;
    }

    public Long getIdCalidad() {
        return idCalidad;
    }

    public void setIdCalidad(Long idCalidad) {
        this.idCalidad = idCalidad;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public Boolean getShowDocumento() {
        return showDocumento;
    }

    public void setShowDocumento(Boolean showDocumento) {
        this.showDocumento = showDocumento;
    }

    public Boolean getShowImagen() {
        return showImagen;
    }

    public void setShowImagen(Boolean showImagen) {
        this.showImagen = showImagen;
    }

    public DocumentoCalidad getCalidadShow() {
        return calidadShow;
    }

    public void setCalidadShow(DocumentoCalidad calidadShow) {
        this.calidadShow = calidadShow;
    }

    public String getInfo() {
        listaInfoOrg = calidadBean.cargarInforOrganizacional();
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DocumentoCalidad> getListaInfoOrg() {
        return listaInfoOrg;
    }

    public void setListaInfoOrg(List<DocumentoCalidad> listaInfoOrg) {
        this.listaInfoOrg = listaInfoOrg;
    }

    public List<DocumentoCalidad> getListaInfoOrgFiltered() {
        return listaInfoOrgFiltered;
    }

    public void setListaInfoOrgFiltered(List<DocumentoCalidad> listaInfoOrgFiltered) {
        this.listaInfoOrgFiltered = listaInfoOrgFiltered;
    }

    public String getRadicacionCircular() {
        return radicacionCircular;
    }

    public void setRadicacionCircular(String radicacionCircular) {
        this.radicacionCircular = radicacionCircular;
    }

    public Date getFechaIniCircular() {
        return fechaIniCircular;
    }

    public void setFechaIniCircular(Date fechaIniCircular) {
        this.fechaIniCircular = fechaIniCircular;
    }

    public Date getFechaFinCircular() {
        return fechaFinCircular;
    }

    public void setFechaFinCircular(Date fechaFinCircular) {
        this.fechaFinCircular = fechaFinCircular;
    }

    public String getContenidoCircular() {
        return contenidoCircular;
    }

    public void setContenidoCircular(String contenidoCircular) {
        this.contenidoCircular = contenidoCircular;
    }

    public List<DocumentoDTO> getListaOficiosOld() {
        return listaOficiosOld;
    }

    public void setListaOficiosOld(List<DocumentoDTO> listaOficiosOld) {
        this.listaOficiosOld = listaOficiosOld;
    }

}
