/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author ISSLUNO
 */
@Entity
public class ComunicadosInternos implements Serializable {

    private Long id;
    private String radicacion;
    private Actor emisor;
    private String cargoActualEmisor;
    private Actor redactor;
    private Actor origen;
    private Oficina origenOficina;
    private String asunto;
    private String mensaje;
    private Date fechaEnvio;
    private Date fechaCreacion;
    private List<ArchivosAdjuntos> adjuntosComunicados;
    private List<DestinosComunicados> destinatarios;
    private String radicacionReenvio;
    private String comentario;
    private String estado;
    private Boolean leido;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    @OneToOne
    public Actor getEmisor() {
        return emisor;
    }

    public void setEmisor(Actor emisor) {
        this.emisor = emisor;
    }

    @OneToOne
    public Actor getRedactor() {
        return redactor;
    }

    public void setRedactor(Actor redactor) {
        this.redactor = redactor;
    }

    @Column(length = 10000)
    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    @Column(length = 10000)
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "adjuntos_comunicado",
            joinColumns = @JoinColumn(name = "comunicado", unique = false),
            inverseJoinColumns = @JoinColumn(name = "adjunto", unique = false)
    )
    public List<ArchivosAdjuntos> getAdjuntosComunicados() {
        return adjuntosComunicados;
    }

    public void setAdjuntosComunicados(List<ArchivosAdjuntos> adjuntos) {
        this.adjuntosComunicados = adjuntos;
    }

    public String getRadicacionReenvio() {
        return radicacionReenvio;
    }

    public void setRadicacionReenvio(String radicacionReenvio) {
        this.radicacionReenvio = radicacionReenvio;
    }

    @Column(length = 10000)
    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @OneToOne
    public Actor getOrigen() {
        return origen;
    }

    public void setOrigen(Actor origen) {
        this.origen = origen;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCargoActualEmisor() {
        return cargoActualEmisor;
    }

    public void setCargoActualEmisor(String cargoActualEmisor) {
        this.cargoActualEmisor = cargoActualEmisor;
    }

    @OneToOne
    public Oficina getOrigenOficina() {
        return origenOficina;
    }

    public void setOrigenOficina(Oficina origenOficina) {
        this.origenOficina = origenOficina;
    }

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "destinos_comunicado",
            joinColumns = @JoinColumn(name = "comunicado", unique = false),
            inverseJoinColumns = @JoinColumn(name = "destino", unique = false)
    )
    public List<DestinosComunicados> getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(List<DestinosComunicados> destinatarios) {
        this.destinatarios = destinatarios;
    }

    public Boolean getLeido() {
        return leido;
    }

    public void setLeido(Boolean leido) {
        this.leido = leido;
    }
    
}
