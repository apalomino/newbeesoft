/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author Cristhian
 */
public class DocumentoAuditoriaDTO {

    private Long idDocumento;
    private String radicacion;
    private String fecha;
    private String fechaAccion;
    private String destino;
    private String procedencia;
    private String creador;
    private String Elimino;
    private String accion;

    public Long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Long idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public String getElimino() {
        return Elimino;
    }

    public void setElimino(String Elimino) {
        this.Elimino = Elimino;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getFechaAccion() {
        return fechaAccion;
    }

    public void setFechaAccion(String fechaAccion) {
        this.fechaAccion = fechaAccion;
    }

}
