/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ClaseDocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.ClasesDocumento;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author SUMSET
 */
@Named(value = "claseDocumentoAction")
@SessionScoped
public class ClaseDocumentoAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(ClaseDocumentoAction.class);

    private String name;
    private String estado;
    private List<ClasesDocumento> listaClasesDocumento;

    @EJB
    private ClaseDocumentoLocal claseDocumentoBean;

    @Inject
    private ApplicationAction application;

    @PostConstruct
    public void inicializar() {

    }

    public void onload() {
        cargarClasesDocumento();
    }

    public void cargarClasesDocumento() {
        try {
            listaClasesDocumento = claseDocumentoBean.listarClasesDocumento("AI");
        } catch (Exception e) {
            LOG.info(e);
        }
    }

    public void crearClaseDocumento() {
        if ((name != null) && (!name.equals(""))) {
            boolean existe = claseDocumentoBean.crearClaseDocumento(name, "activo");
            if (existe) {
                ValidacionesUtil.addMessageInfo("La clase de documento " + name + " ya existe.");
            } else {
                ValidacionesUtil.addMessageInfo("La clase de documento " + name + " fue creada con éxito.");
            }
            name = "";
            cargarClasesDocumento();
        }
    }

    public void CambioEstadoClaseDocumento(Long id, String estado) {
        if (estado.equals("activo")) {
            estado = "inactivo";
        } else {
            estado = "activo";
        }

        claseDocumentoBean.CambioEstadoClaseDocumento(id, estado);
        cargarClasesDocumento();
    }

    public boolean mostrarBoton(String estado) {
        if (estado.equals("activo")) {
            return false;
        } else {
            return true;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ClasesDocumento> getListaClasesDocumento() {
        return listaClasesDocumento;
    }

    public void setListaClasesDocumento(List<ClasesDocumento> listaClasesDocumento) {
        this.listaClasesDocumento = listaClasesDocumento;
    }

}
