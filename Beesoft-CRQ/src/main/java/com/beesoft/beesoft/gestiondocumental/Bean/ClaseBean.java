/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.ClaseLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.SubClase;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author cc
 */
@Stateless
public class ClaseBean implements ClaseLocal, Serializable {

    private static final Log LOG = LogFactory.getLog(ClaseBean.class);

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

    @Override
    public List<Clase> listarClases() {
        List<Clase> clases = em.createQuery("Select c from Clase c").getResultList();
        return clases;
    }

    @Override
    public Clase buscarClase(String nombre) {
        List<Clase> clases = em.createQuery("Select c from Clase c where c.nombre like :nombre").setParameter("nombre", "%" + nombre + "%").getResultList();
        if (!clases.isEmpty()) {
            return clases.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<SubClase> buscarSubClases(Long idClase) {
        List<SubClase> subClases = em.createQuery("Select s from SubClase s where s.clase.id = :clase").setParameter("clase", idClase).getResultList();
        if (!subClases.isEmpty()) {
            return subClases;
        } else {
            return null;
        }
    }

    @Override
    public Clase buscarClasePorId(Long id) {
        List<Clase> clases = em.createQuery("Select c from Clase c where c.id = :id").setParameter("id", id).getResultList();
        if (!clases.isEmpty()) {
            return clases.get(0);
        } else {
            return null;
        }
    }

    @Override
    public SubClase buscarSubClasePorId(Long id) {
        List<SubClase> subClases = em.createQuery("Select s from SubClase s where s.id = :id").setParameter("id", id).getResultList();
        if (!subClases.isEmpty()) {
            return subClases.get(0);
        } else {
            return null;
        }
    }
}
