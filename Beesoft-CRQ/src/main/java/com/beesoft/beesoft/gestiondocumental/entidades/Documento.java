package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 * The persistent class for the documento database table.
 *
 */
@Entity
public class Documento implements Serializable {

	private static final long serialVersionUID = 1L;

    private Long id;

    private String radicacion;

    private Boolean activo;

    private String anexos;

    private Boolean aprobado;

    private String asunto;

    private String observacion;

    private Date fechaControl;

    private Date fechaEmision;

    private Date fechaRecepcion;

    private Date fechaSistema;

    private String guia;

    private String mensajero;

    private String notificacionEntrega;

    private Boolean pendiente;

    private String pruebaEntrega;

    private Boolean revisado;

    private String ubicacionFisica;

    private String clasedocumento;

    private String claseOld;

    private SubClase subClase;

    private List<Actor> procedencia;

    private List<Documento> documentosReferencia;

    private Documento documentoReferencia;
   
    private Documento documentoRespuesta;

    private String empresacorreoEnviado;

    private String empresaCorreoRecibido;

    private List<Actor> revisores;

    private List<Actor> destinos;

    private Actor responsable;

    private Actor radicador;

    private String pqr;

    private String contratacionTipo;

    private String contratacionNumero;

    private List<ArchivoDocumento> archivos;

    private Boolean prestado;

    private Boolean medioElectronico;

    private Boolean migrado;

    private Boolean anulado;

    private String comentarioCambio;

    private String radicacionOriginal;
    
    private Ciudad ciudad;

    public Documento() {
    }

    public Documento(Long id, String radicacion, Boolean activo, String anexos, 
    		Boolean aprobado, String asunto, Date fechaControl, Date fechaEmision, 
    		Date fechaRecepcion, Date fechaSistema, Boolean pendiente, Boolean revisado, 
    		String claseOld, String empresacorreoEnviado, Actor responsable, Actor radicador, 
    		Boolean migrado, Ciudad ciudad) {
        this.id = id;
        this.radicacion = radicacion;
        this.activo = activo;
        this.anexos = anexos;
        this.aprobado = aprobado;
        this.asunto = asunto;
        this.fechaControl = fechaControl;
        this.fechaEmision = fechaEmision;
        this.fechaRecepcion = fechaRecepcion;
        this.fechaSistema = fechaSistema;
        this.pendiente = pendiente;
        this.revisado = revisado;
        this.claseOld = claseOld;
        this.empresacorreoEnviado = empresacorreoEnviado;
        this.responsable = responsable;
        this.radicador = radicador;
        this.migrado = migrado;
        this.ciudad = ciudad;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRadicacion() {
        return this.radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getAnexos() {
        return this.anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }

    public Boolean getAprobado() {
        return this.aprobado;
    }

    public void setAprobado(Boolean aprobado) {
        this.aprobado = aprobado;
    }

    public String getAsunto() {
        return this.asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaControl() {
        return this.fechaControl;
    }

    public void setFechaControl(Date fechaControl) {
        this.fechaControl = fechaControl;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaRecepcion() {
        return this.fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaSistema() {
        return this.fechaSistema;
    }

    public void setFechaSistema(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    public String getGuia() {
        return this.guia;
    }

    public void setGuia(String guia) {
        this.guia = guia;
    }

    public String getNotificacionEntrega() {
        return this.notificacionEntrega;
    }

    public void setNotificacionEntrega(String notificacionEntrega) {
        this.notificacionEntrega = notificacionEntrega;
    }

    public Boolean getPendiente() {
        return this.pendiente;
    }

    public void setPendiente(Boolean pendiente) {
        this.pendiente = pendiente;
    }

    public String getPruebaEntrega() {
        return this.pruebaEntrega;
    }

    public void setPruebaEntrega(String pruebaEntrega) {
        this.pruebaEntrega = pruebaEntrega;
    }

    public Boolean getRevisado() {
        return this.revisado;
    }

    public void setRevisado(Boolean revisado) {
        this.revisado = revisado;
    }

    public String getUbicacionFisica() {
        return this.ubicacionFisica;
    }

    public void setUbicacionFisica(String ubicacionFisica) {
        this.ubicacionFisica = ubicacionFisica;
    }

    public String getClasedocumento() {
        return this.clasedocumento;
    }

    public void setClasedocumento(String clasedocumento) {
        this.clasedocumento = clasedocumento;
    }

    public String getEmpresacorreoEnviado() {
        return empresacorreoEnviado;
    }

    public void setEmpresacorreoEnviado(String empresacorreoEnviado) {
        this.empresacorreoEnviado = empresacorreoEnviado;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "documento_procedencia",
            joinColumns = @JoinColumn(name = "documento", unique = false),
            inverseJoinColumns = @JoinColumn(name = "procedencia", unique = false)
    )
    public List<Actor> getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(List<Actor> procedencia) {
        this.procedencia = procedencia;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "documento_referencia",
            joinColumns = @JoinColumn(name = "documento", unique = false),
            inverseJoinColumns = @JoinColumn(name = "documento_origen", unique = false)
    )
    public List<Documento> getDocumentosReferencia() {
        return documentosReferencia;
    }

    public void setDocumentosReferencia(List<Documento> documentosReferencia) {
        this.documentosReferencia = documentosReferencia;
    }

    @OneToOne
    public Documento getDocumentoReferencia() {
        return documentoReferencia;
    }

    public void setDocumentoReferencia(Documento documentoReferencia) {
        this.documentoReferencia = documentoReferencia;
    }

    @OneToOne
    public Documento getDocumentoRespuesta() {
        return documentoRespuesta;
    }

    public void setDocumentoRespuesta(Documento documentoRespuesta) {
        this.documentoRespuesta = documentoRespuesta;
    }

    @OneToOne
    public Actor getResponsable() {
        return responsable;
    }

    public void setResponsable(Actor responsable) {
        this.responsable = responsable;
    }

    @OneToOne
    public Actor getRadicador() {
        return radicador;
    }

    public void setRadicador(Actor radicador) {
        this.radicador = radicador;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "documento_revisor",
            joinColumns = @JoinColumn(name = "documento", unique = false),
            inverseJoinColumns = @JoinColumn(name = "revisor", unique = false)
    )
    public List<Actor> getRevisores() {
        return revisores;
    }

    public void setRevisores(List<Actor> revisores) {
        this.revisores = revisores;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "documento_destino",
            joinColumns = @JoinColumn(name = "documento", unique = false),
            inverseJoinColumns = @JoinColumn(name = "destino", unique = false)
    )
    public List<Actor> getDestinos() {
        return destinos;
    }

    public void setDestinos(List<Actor> destinos) {
        this.destinos = destinos;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "documento_archivo",
            joinColumns = @JoinColumn(name = "documento", unique = false),
            inverseJoinColumns = @JoinColumn(name = "archivo", unique = false)
    )
    public List<ArchivoDocumento> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<ArchivoDocumento> archivos) {
        this.archivos = archivos;
    }

    public String getMensajero() {
        return mensajero;
    }

    public void setMensajero(String mensajero) {
        this.mensajero = mensajero;
    }

    public String getEmpresaCorreoRecibido() {
        return empresaCorreoRecibido;
    }

    public void setEmpresaCorreoRecibido(String empresaCorreoRecibido) {
        this.empresaCorreoRecibido = empresaCorreoRecibido;
    }

    public String getPqr() {
        return pqr;
    }

    public void setPqr(String pqr) {
        this.pqr = pqr;
    }

    public String getContratacionTipo() {
        return contratacionTipo;
    }

    public void setContratacionTipo(String contratacionTipo) {
        this.contratacionTipo = contratacionTipo;
    }

    public String getContratacionNumero() {
        return contratacionNumero;
    }

    public void setContratacionNumero(String contratacionNumero) {
        this.contratacionNumero = contratacionNumero;
    }

    public Boolean getPrestado() {
        return prestado;
    }

    public void setPrestado(Boolean prestado) {
        this.prestado = prestado;
    }

    public Boolean getMedioElectronico() {
        return medioElectronico;
    }

    public void setMedioElectronico(Boolean medioElectronico) {
        this.medioElectronico = medioElectronico;
    }

    @Override
    public boolean equals(Object obj) {
        return id.equals(((Documento) obj).getId());
    }

    @ManyToOne
    public SubClase getSubClase() {
        return subClase;
    }

    public void setSubClase(SubClase subClase) {
        this.subClase = subClase;
    }

    public Boolean getMigrado() {
        return migrado;
    }

    public void setMigrado(Boolean migrado) {
        this.migrado = migrado;
    }

    public String getClaseOld() {
        return claseOld;
    }

    public void setClaseOld(String claseOld) {
        this.claseOld = claseOld;
    }

    public Boolean getAnulado() {
        return anulado;
    }

    public void setAnulado(Boolean anulado) {
        this.anulado = anulado;
    }

    public String getComentarioCambio() {
        return comentarioCambio;
    }

    public void setComentarioCambio(String comentarioCambio) {
        this.comentarioCambio = comentarioCambio;
    }

    public String getRadicacionOriginal() {
        return radicacionOriginal;
    }

    public void setRadicacionOriginal(String radicacionOriginal) {
        this.radicacionOriginal = radicacionOriginal;
    }
    @OneToOne
	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}
    
}
