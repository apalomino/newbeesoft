/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ComunicadosInternosLocal;
import com.beesoft.beesoft.gestiondocumental.Local.GeneradorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DestinoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import com.beesoft.beesoft.gestiondocumental.entidades.ComunicadosInternos;
import com.beesoft.beesoft.gestiondocumental.entidades.DestinosComunicados;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.RadicacionCorreo;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class ComunicadosInternosBean implements ComunicadosInternosLocal, Serializable {

    private static final Log LOG = LogFactory.getLog(ComunicadosInternosBean.class);

    @PersistenceContext(unitName = "BeesoftPU")
    private transient EntityManager em;

    @EJB
    private transient NotificationMailLocal notificationBean;

    @EJB
    private transient GeneradorLocal generadorBean;

    @EJB
    private transient ActorLocal actorBean;

    @Override
    public String guardarComunicadoInterno(Actor origen, Oficina origenOficina, Actor redactor, List<DestinoDTO> destinatarios,
            List<DestinoDTO> destinatariosCopia, String asunto, String mensaje, List<ArchivosAdjuntos> adjuntosGuardar,
            Boolean aprobacion, String estado) {

        ComunicadosInternos comunicado = new ComunicadosInternos();
        comunicado.setRedactor(redactor);
        comunicado.setAsunto(asunto);
        comunicado.setMensaje(mensaje);
        comunicado.setAdjuntosComunicados(adjuntosGuardar);
        comunicado.setFechaCreacion(new Date());
        comunicado.setLeido(false);

        if (!aprobacion) {
            comunicado.setFechaEnvio(new Date());
            if (origen != null) {
                Oficina oficina = mostrarOficina(origen);
                if (oficina != null) {
                    Object[] info = ValidacionesUtil.mostrarJefe(oficina);
                    comunicado.setOrigen((Actor) info[0]);
                    comunicado.setEmisor((Actor) info[0]);
                    comunicado.setOrigenOficina(oficina);
                    comunicado.setCargoActualEmisor((String) info[1]);
                } else {
                    comunicado.setOrigen(origen);
                    comunicado.setEmisor(origen);
                    comunicado.setCargoActualEmisor(origen.getCargo());
                }
            }
        } else if (origenOficina != null) {
            Object[] info = ValidacionesUtil.mostrarJefe(origenOficina);
            comunicado.setOrigen((Actor) info[0]);
            comunicado.setOrigenOficina(origenOficina);
            comunicado.setCargoActualEmisor((String) info[1]);
        }
        comunicado.setEstado(estado);

        guardarDestinatariosComunicados(destinatarios, comunicado, false);
        List<Actor> destinosCorreo = obtenerDestinatariosComunicados(destinatarios, comunicado, false);

        guardarDestinatariosComunicados(destinatariosCopia, comunicado, true);
        List<Actor> destinosCopia = obtenerDestinatariosComunicados(destinatariosCopia, comunicado, true);

        em.persist(comunicado);

        String radicacion = "";
        if (comunicado.getId() != null) {
            if (aprobacion) {
                String message = mensajeAprobacionComunicadoInterno(comunicado.getOrigen(), redactor, comunicado.getId());
                List<Actor> destinoEmisor = new ArrayList<>();
                destinoEmisor.add(comunicado.getOrigen());
                String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message);
                notificationBean.guardarCorreo(redactor, destinoEmisor, asunto, mailCompleto, null, "COMUNICADO INTERNO", false);
            } else {

                try {
                    radicacion = generadorBean.consecutivoRadicacionCorreoXLetra("C");
                    comunicado.setRadicacion(radicacion);
                } catch (Exception ex) {
                    LOG.info(ex);
                }

                em.merge(comunicado);

                List<Actor> listaFinal = new ArrayList<>(destinosCorreo);
                if (destinatariosCopia != null) {
                    listaFinal.addAll(destinosCopia);
                }
//                for (Actor destino : listaFinal) {
//                    List<Actor> destinosFinal = new ArrayList<>();
//                    destinosFinal.add(destino);
//                    String message = mensajeComunicadoInterno(destino, null, comunicado.getId(), radicacion, "COMUNICADO INTERNO");
//                    String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message);
//                    notificationBean.guardarCorreo(origen, destinosFinal, asunto, mailCompleto, null, "COMUNICADO INTERNO", false);
//                }
            }
            return radicacion;
        }
        return "";
    }

    public ComunicadosInternos editarComunicadoInterno(Long idComunicado, List<DestinoDTO> destinatarios,
            List<DestinoDTO> destinatariosCopia, String asunto, String mensaje,
            List<ArchivosAdjuntos> adjuntosGuardar, List<ArchivosAdjuntos> adjuntosEliminados, String estado) {
        ComunicadosInternos comunicados = em.find(ComunicadosInternos.class, idComunicado);
        comunicados.setEstado(estado);
        comunicados.setAsunto(asunto);
        comunicados.setMensaje(mensaje);

        eliminarDestinos(idComunicado);
        guardarDestinatariosComunicados(destinatarios, comunicados, false);

        guardarDestinatariosComunicados(destinatariosCopia, comunicados, true);

        if (adjuntosGuardar != null && !adjuntosGuardar.isEmpty()) {
            for (ArchivosAdjuntos a : adjuntosGuardar) {
                em.persist(a);
            }
            comunicados.getAdjuntosComunicados().addAll(adjuntosGuardar);
        }

        for (ArchivosAdjuntos adjuntosEliminado : adjuntosEliminados) {
            ArchivosAdjuntos adjunto = em.find(ArchivosAdjuntos.class, adjuntosEliminado.getId());
            comunicados.getAdjuntosComunicados().remove(adjunto);
            em.remove(adjunto);
        }

        return em.merge(comunicados);
    }

    @Override
    public void reenviarComunicadoInterno(Long idComunicado, Actor redactor, List<DestinoDTO> destinatarios, String comentario) {
        ComunicadosInternos comunicadoReenvio = em.find(ComunicadosInternos.class, idComunicado);
        ComunicadosInternos comunicado = new ComunicadosInternos();
        comunicado.setRadicacionReenvio(comunicadoReenvio.getRadicacion());
        comunicado.setOrigen(comunicadoReenvio.getOrigen());
        comunicado.setRedactor(comunicadoReenvio.getRedactor());
        comunicado.setEmisor(redactor);
        comunicado.setCargoActualEmisor(comunicadoReenvio.getCargoActualEmisor());

        comunicado.setAsunto(comunicadoReenvio.getAsunto());
        comunicado.setMensaje(comunicadoReenvio.getMensaje());
        List<ArchivosAdjuntos> lista = new ArrayList<>();
        for (ArchivosAdjuntos aj : comunicadoReenvio.getAdjuntosComunicados()) {
            ArchivosAdjuntos adjunto = new ArchivosAdjuntos();
            adjunto.setNombreArchivo(aj.getNombreArchivo());
            adjunto.setArchivo(aj.getArchivo());
            adjunto.setTipo(aj.getTipo());
            em.persist(adjunto);
            lista.add(adjunto);
        }

        comunicado.setAdjuntosComunicados(lista);
        comunicado.setFechaCreacion(new Date());
        comunicado.setFechaEnvio(new Date());
        comunicado.setEstado(ConstantsMail.APROBADO);
        comunicado.setLeido(false);

        em.persist(comunicado);

        guardarDestinatariosComunicados(destinatarios, comunicado, false);
        List<Actor> destinosCorreo = obtenerDestinatariosComunicados(destinatarios, comunicado, false);

//        if (comunicado.getId() != null) {
//            for (Actor destino : destinosCorreo) {
//                String message = mensajeComunicadoInterno(destino, comunicado.getEmisor(), comunicado.getId(), comunicado.getRadicacion(), "COMUNICADO INTERNO");
//                List<Actor> destinosFinal = new ArrayList<>();
//                destinosFinal.add(destino);
//                String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message);
//                notificationBean.guardarCorreo(comunicado.getEmisor(), destinosFinal, comunicado.getAsunto(), mailCompleto, null, "COMUNICADO INTERNO", false);
//            }
//        }
    }

    @Override
    public void enviarComunicadoAprobado(Long idComunicado, List<DestinoDTO> destinatarios, List<DestinoDTO> destinatariosCopia,
            String asunto, String mensaje, List<ArchivosAdjuntos> adjuntosGuardar, List<ArchivosAdjuntos> adjuntosEliminados) {

        ComunicadosInternos comunicados = editarComunicadoInterno(idComunicado, destinatarios, destinatariosCopia, asunto, mensaje, adjuntosGuardar, adjuntosEliminados, ConstantsMail.APROBADO);

        if (comunicados.getOrigen() != null) {
            comunicados.setFechaEnvio(new Date());
            if (StringUtils.isBlank(comunicados.getRadicacion())) {
                try {
                    String radicacion = generadorBean.consecutivoRadicacionCorreoXLetra("C");
                    comunicados.setRadicacion(radicacion);
                } catch (Exception ex) {
                    LOG.info(ex);
                }
            }
            comunicados.setEmisor(comunicados.getOrigen());
            em.merge(comunicados);

            List<Actor> destinosOficina = obtenerDestinatariosComunicados(destinatarios, comunicados, false);

            List<Actor> destinosOficinaCopia = obtenerDestinatariosComunicados(destinatariosCopia, comunicados, true);

            List<Actor> listaFinal = new ArrayList<>(destinosOficina);
            if (destinosOficinaCopia != null) {
                listaFinal.addAll(destinosOficinaCopia);
            }

//            for (Actor destino : listaFinal) {
//                List<Actor> destinos = new ArrayList<>();
//                destinos.add(destino);
//                String message = mensajeComunicadoInterno(destino, null, comunicados.getId(), comunicados.getRadicacion(), "COMUNICADO INTERNO");
//                String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message);
//                notificationBean.guardarCorreo(comunicados.getEmisor(), destinos, asunto, mailCompleto, null, "COMUNICADO INTERNO", false);
//            }
        }

    }

    @Override
    public void modificarEnviarComunicadoAprobado(Long idComunicado, List<DestinoDTO> destinatarios,
            List<DestinoDTO> destinatariosCopia, String asunto, String mensaje,
            List<ArchivosAdjuntos> adjuntosGuardar, List<ArchivosAdjuntos> adjuntosEliminados) {

        ComunicadosInternos comunicados = editarComunicadoInterno(idComunicado, destinatarios, destinatariosCopia, asunto, mensaje, adjuntosGuardar, adjuntosEliminados, ConstantsMail.POR_APROBAR);
//        List<Actor> lista = new ArrayList<>();
//        lista.add(comunicados.getEmisor());
//        String message = mensajeModificacionAprobacionComunicadoInterno(comunicados.getEmisor(), comunicados.getRedactor(), comunicados.getId(), comunicados.getRadicacion(), "COMUNICADO INTERNO");
//        String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(message);
//        notificationBean.guardarCorreo(comunicados.getRedactor(), lista, asunto, mailCompleto, null, "COMUNICADO INTERNO", false);
    }

    @Override
    public void modificarComunicadoInternoPorAprobar(Long idCi, String comentarios) {
        ComunicadosInternos comunicado = em.find(ComunicadosInternos.class, idCi);
        comunicado.setComentario(comentarios);
        em.merge(comunicado);
        List<Actor> destinos = new ArrayList<>();
        destinos.add(comunicado.getRedactor());

//        String mensaje = mensajeModificacionComunicadoInterno(comunicado.getRedactor(), comunicado.getEmisor(), idCi, comunicado.getRadicacion(), "COMUNICADO INTERNO", comentarios);
//        String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(mensaje);
//        notificationBean.guardarCorreo(comunicado.getEmisor(), destinos, comunicado.getAsunto(), mailCompleto, null, "COMUNICADO INTERNO", false);

    }

    @Override
    public void anularComunicadoInternoPorAprobar(Long idCi, String comentarios) {
        ComunicadosInternos comunicado = em.find(ComunicadosInternos.class, idCi);
        comunicado.setRadicacion("ANULADO");
        comunicado.setComentario(comentarios);
        comunicado.setEstado(ConstantsMail.ANULADO);
        em.merge(comunicado);
        List<Actor> destinos = new ArrayList<>();
        destinos.add(comunicado.getRedactor());

//        String mensaje = mensajeAnularComunicadoInterno(comunicado.getRedactor(), comunicado.getOrigen(), idCi, comunicado.getRadicacion(), "COMUNICADO INTERNO", comentarios);
//        String mailCompleto = ValidacionesUtil.obtenerCuerpoCorreo(mensaje);
//        notificationBean.guardarCorreo(comunicado.getOrigen(), destinos, comunicado.getAsunto(), mailCompleto, null, "COMUNICADO INTERNO", false);
    }

    @Override
    public List<ComunicadosInternos> buscarComunicadosPendientesAprobacion(Actor actor, String inActor, String estado) {
        Query q = null;
        StringBuilder query = new StringBuilder("select mail from ComunicadosInternos mail");
        Map<String, Object> parameters = new HashMap<>();
        boolean iswhere = false;

        if (actor != null) {
            if (StringUtils.isNotBlank(inActor)) {
                if (inActor.equals(ConstantsMail.EMISOR)) {
                    query.append(" where mail.origen.id=:from");
                    parameters.put("from", actor.getId());
                    iswhere = true;
                } else if (inActor.equals(ConstantsMail.RESPONSABLE)) {
                    query.append(" where mail.redactor.id=:from");
                    parameters.put("from", actor.getId());
                    iswhere = true;
                }
            }
        }

        query.append(" and mail.estado='").append(estado).append("'");

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        List<ComunicadosInternos> mails = q.getResultList();

        if (mails != null && !mails.isEmpty()) {
            return mails;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<ComunicadosInternos> buscarComunicadosPendientesAprobacionAdmin(String estado) {
        List<ComunicadosInternos> lista = em.createQuery("select c from ComunicadosInternos c "
                + "where c.estado=:estado")
                .setParameter("estado", estado)
                .getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Actor> cargarListaDestinatarios() {
        List<Actor> lista = (List<Actor>) em.createQuery("Select a.actor from Usuario a where a.estado=true").getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        }
        return null;
    }

    @Override
    public List<Actor> cargarListaJefesDestinatarios() {
        List<Actor> lista = (List<Actor>) em.createQuery("Select a.actor from Usuario a where a.estado=true and (a.perfil.descripcion like '%JEFE%' or a.perfil.descripcion='GESTOR VB') ").getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        }
        return null;
    }

//    public String mensajeComunicadoInterno(Actor actor, Actor actorReenvio, Long idCI, String radicacion, String tipoComunicado) {
//        String url = ValidacionesUtil.urlServer();
//        StringBuilder message = new StringBuilder("Sr(a). " + actor.getNombreYApellido() + " tiene asignado un(a) ");
//        message.append(" <a href=\"http://" + url + ":8080/Beesoft/Comunicado?radicacion=").append(idCI).append("&tipo=Env\">").append(tipoComunicado).append("</a>,");
//        message.append("<br> de click en el numero para visualizarlo.");
//        message.append("<br>");
//        if (actorReenvio != null) {
//            message.append("Reenviado por: ").append(actorReenvio.getNombreYApellido());
//        }
//        return message.toString();
//    }

    public String mensajeAprobacionComunicadoInterno(Actor jefe, Actor redactor, Long idCI) {
        String url = ValidacionesUtil.urlServer();
        StringBuilder message = new StringBuilder("Sr(a). <strong>" + jefe.getNombreYApellido() + "</strong> tiene pendiente de aprobacion un(a) ");
        message.append("<br> <a href=\"http://" + url + ":8080/Beesoft/Comunicado?radicacion=").append(idCI).append("&tipo=Apr\"><strong>").append("COMUNICADO INTERNO").append("</strong></a>,");
        message.append("redactado por <strong>").append(redactor.getNombreYApellido()).append("</strong>,<br> de click en el numero para visualizarlo.");
        message.append("<br>");

        return message.toString();
    }

//    public String mensajeAprobacionFinalComunicadoInterno(Actor jefe, Actor redactor, Actor revisor, Long idCI, String radicacion, String tipoComunicado) {
//        String url = ValidacionesUtil.urlServer();
//        StringBuilder message = new StringBuilder("Sr(a). <strong>" + jefe.getNombreYApellido() + "</strong> tiene pendiente de aprobacion un(a) ");
//        message.append("<br> <a href=\"http://" + url + ":8080/Beesoft/Comunicado?radicacion=").append(idCI).append("&tipo=Apr\"><strong>").append(tipoComunicado).append("</strong></a>,");
//        message.append("redactado por <strong>").append(redactor.getNombreYApellido()).append("</strong> y revisado por ").append(revisor.getNombreYApellido());
//        message.append("<br> de click en el numero para visualizarlo.");
//        message.append("<br>");
//
//        return message.toString();
//    }

//    public String mensajeModificacionAprobacionComunicadoInterno(Actor jefe, Actor redactor, Long idCI, String radicacion, String tipoComunicado) {
//        String url = ValidacionesUtil.urlServer();
//        StringBuilder message = new StringBuilder("Sr(a). <strong>" + jefe.getNombreYApellido() + "</strong> tiene pendiente de aprobacion un(a) ");
//        message.append("<br> con numero de radicacion <a href=\"http://" + url + ":8080/Beesoft/Comunicado?radicacion=").append(idCI).append("&tipo=Apr\"><strong>").append(tipoComunicado).append("</strong></a>,");
//        message.append("<br> modificado por <strong>").append(redactor.getNombreYApellido()).append("</strong>, de click en el numero para visualizarlo.");
//        message.append("<br>");
//
//        return message.toString();
//    }

//    public String mensajeModificacionComunicadoInterno(Actor redactor, Actor jefe, Long idCI, String radicacion, String tipoComunicado, String comentarios) {
//        String url = ValidacionesUtil.urlServer();
//        StringBuilder message = new StringBuilder("Sr(a). <strong>" + redactor.getNombreYApellido() + "</strong> tiene un(a) ");
//        message.append(" <a href=\"http://" + url + ":8080/Beesoft/Comunicado?radicacion=").append(idCI).append("&tipo=Apr\"><strong>").append(tipoComunicado).append("</strong></a>,");
//        message.append("<br> con una serie de modificaciones pendientes solicitadas por <strong>").append(jefe.getNombreYApellido() + "</strong>.");
//        message.append("<br>");
//        message.append("<br>");
//        message.append(comentarios);
//        message.append("<br>");
//        message.append("<br>");
//        message.append(" Para realizar las modificaciones haga click en el numero de radicacion.");
//        return message.toString();
//    }

//    public String mensajeAnularComunicadoInterno(Actor redactor, Actor jefe, Long idCI, String radicacion, String tipoComunicado, String comentarios) {
//        StringBuilder message = new StringBuilder("Sr(a). <strong>" + redactor.getNombreYApellido() + "</strong> tiene un(a) <strong>");
//        message.append(tipoComunicado);
//        message.append("</strong> que ha sido anulado por <strong>").append(jefe.getNombreYApellido() + "</strong>.");
//        message.append("<br>");
//        message.append("<br>");
//        message.append(comentarios);
//        message.append("<br>");
//        return message.toString();
//    }

    @Override
    public List<ComunicadosInternos> buscarCorreo(String radicacion, List<DestinoDTO> destinatarios, String asunto, Date fechaIni, Date fechaFin) {

        Query q = null;
        StringBuilder query = new StringBuilder("select mail from ComunicadosInternos mail");
        Map<String, Object> parameters = new HashMap<>();
        boolean iswhere = false;
        boolean isdestinations = false;

        if (destinatarios != null && !destinatarios.isEmpty()) {
            query.append(" left join mail.destinatarios destination");
            isdestinations = true;
        }

        List<ComunicadosInternos> mails;

        if (StringUtils.isNotBlank(radicacion)) {
            query.append(" where mail.radicacion=:radicacion");
            parameters.put("radicacion", radicacion);
            iswhere = true;
        }

        if (fechaIni != null && fechaFin != null) {
            if (iswhere) {
                query.append(" and mail.fechaEnvio BETWEEN :fechaIni AND :fechaFin");
                parameters.put("fechaIni", fechaIni);
                parameters.put("fechaFin", fechaFin);
            } else {
                query.append(" where mail.fechaEnvio BETWEEN :fechaIni AND :fechaFin");
                parameters.put("fechaIni", fechaIni);
                parameters.put("fechaFin", fechaFin);
                iswhere = true;
            }
        }

        if (StringUtils.isNotBlank(asunto)) {
            if (iswhere) {
                query.append(" and mail.asunto like :asunto");
                parameters.put("asunto", "%" + asunto + "%");
            } else {
                query.append(" where mail.asunto like :asunto");
                parameters.put("asunto", "%" + asunto + "%");
                iswhere = true;
            }
        }

        if (isdestinations) {
            if (iswhere) {
                if (destinatarios.get(0).getTipoDestino().equals("JEFE")) {
                    query.append(" and (destination.oficina.id=:desti)");
                    parameters.put("desti", destinatarios.get(0).getId());
                } else {
                    query.append(" and (destination.destino.id=:desti)");
                    parameters.put("desti", destinatarios.get(0).getId());
                }
            } else if (destinatarios.get(0).getTipoDestino().equals("JEFE")) {
                query.append(" where (destination.oficina.id=:desti)");
                parameters.put("desti", destinatarios.get(0).getId());
                iswhere = true;
            } else {
                query.append(" where (destination.destino.id=:desti)");
                parameters.put("desti", destinatarios.get(0).getId());
                iswhere = true;
            }
        }

        query.append(" and mail.estado=:estado");
        parameters.put("estado", ConstantsMail.APROBADO);

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        mails = q.getResultList();

        if (mails != null) {
            HashSet<ComunicadosInternos> hash = new HashSet<>(mails);
            mails.clear();
            mails.addAll(hash);
        }
        return mails;
    }

    @Override
    public List<ComunicadosInternos> buscarCorreoRepositorio(String radicacion, Actor procedencia, Actor destinatarios, String asunto, Date fechaIni, Date fechaFin) {

        Query q = null;
        StringBuilder query = new StringBuilder("select mail from ComunicadosInternos mail");
        Map<String, Object> parameters = new HashMap<>();
        boolean iswhere = false;
        boolean isdestinations = false;

        if (destinatarios != null) {
            query.append(" left join mail.destinatarios destination left join mail.destinatariosCopia copia");
            isdestinations = true;
        }

        List<ComunicadosInternos> mails;

        if (StringUtils.isNotBlank(radicacion)) {
            query.append(" where mail.radicacion=:radicacion and mail.radicacion != ''");
            parameters.put("radicacion", radicacion);
            iswhere = true;
        }

        if (fechaIni != null && fechaFin != null) {
            if (iswhere) {
                query.append(" and mail.fechaEnvio BETWEEN :fechaIni AND :fechaFin");
                parameters.put("fechaIni", fechaIni);
                parameters.put("fechaFin", fechaFin);
            } else {
                query.append(" where mail.fechaEnvio BETWEEN :fechaIni AND :fechaFin");
                parameters.put("fechaIni", fechaIni);
                parameters.put("fechaFin", fechaFin);
                iswhere = true;
            }
        }

        if (procedencia != null) {
            if (iswhere) {
                query.append(" and mail.emisor := procedencia ");
                parameters.put("procedencia", procedencia);
            } else {
                query.append(" and mail.emisor := procedencia ");
                parameters.put("procedencia", procedencia);
                iswhere = true;
            }
        }

        if (StringUtils.isNotBlank(asunto)) {
            if (iswhere) {
                query.append(" and mail.asunto like :asunto");
                parameters.put("asunto", "%" + asunto + "%");
            } else {
                query.append(" where mail.asunto like :asunto");
                parameters.put("asunto", "%" + asunto + "%");
            }
        }

        if (isdestinations) {
            if (iswhere) {
                query.append(" and (destination in :desti or copia in :desti)");
                parameters.put("desti", destinatarios);
            } else {
                query.append(" where (destination in :desti or copia in :desti)");
                parameters.put("desti", destinatarios);
                iswhere = true;
            }
        }

        query.append(" and mail.estado=:estado");
        parameters.put("estado", ConstantsMail.APROBADO);
        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        mails = q.getResultList();

        if (mails != null) {
            HashSet<ComunicadosInternos> hash = new HashSet<>(mails);
            mails.clear();
            mails.addAll(hash);
        }
        System.out.println("size" + mails.size());
        return mails;
    }

    @Override
    public List<ComunicadosInternos> buscarTodosComunicadosInternos(Date fechaIni, Date fechaFin, String estado) {

        Query q = null;
        StringBuilder query = new StringBuilder("select mail from ComunicadosInternos mail");
        Map<String, Object> parameters = new HashMap<>();
        boolean iswhere = false;

        List<ComunicadosInternos> mails;

        if (fechaIni != null && fechaFin != null) {

            query.append(" where mail.fechaCreacion BETWEEN :fechaIni AND :fechaFin");
            parameters.put("fechaIni", fechaIni);
            parameters.put("fechaFin", fechaFin);
            iswhere = true;

        }

        if (StringUtils.isNotBlank(estado)) {
            if (iswhere) {
                query.append(" and mail.estado=:estado");
                parameters.put("estado", estado);
            } else {
                query.append(" where mail.estado=:estado");
                parameters.put("estado", estado);
            }
        }

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        mails = q.getResultList();

        if (mails != null) {
            HashSet<ComunicadosInternos> hash = new HashSet<>(mails);
            mails.clear();
            mails.addAll(hash);
        }
        return mails;
    }

    @Override
    public List<ComunicadosInternos> buscarComunicadosInOut(Actor actor, String mensaje, Boolean inOut) throws Exception {
        Query q = null;
        StringBuilder query = new StringBuilder("select mail from ComunicadosInternos mail");
        Map<String, Object> parameters = new HashMap<>();
        boolean iswhere = false;

        if (actor != null) {
            if (inOut != null) {
                if (inOut) {
                    query.append(" where mail.emisor.id=:from");
                    parameters.put("from", actor.getId());
                    iswhere = true;
                } else {
                    query.append(" join mail.destinatarios d where d.destino.id=:dest and (d.copia=true or d.copia=false)");
                    parameters.put("dest", actor.getId());
                    iswhere = true;
                }
            }
        }

        if (StringUtils.isNotBlank(mensaje)) {
            if (iswhere) {
                query.append(" and mail.mensaje like :mensaje");
                parameters.put("mensaje", "%" + mensaje + "%");
            } else {
                query.append(" where mail.mensaje like :mensaje");
                parameters.put("mensaje", "%" + mensaje + "%");
            }
        }

        if (iswhere) {
            query.append(" and mail.estado=:aprobado");
            parameters.put("aprobado", "APROBADO");
        } else {
            query.append(" where mail.estado=:aprobado");
            parameters.put("aprobado", "APROBADO");
        }

        query.append(" order by mail.fechaEnvio DESC");

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        List<ComunicadosInternos> mails = q.getResultList();

        if (mails != null && !mails.isEmpty()) {
            return mails;
        } else {
            throw new Exception("Los parametros de busqueda no arrojan resltados");
        }
    }

    @Override
    public ComunicadosInternos buscarCorreoId(Long id) {
        ComunicadosInternos mail = em.find(ComunicadosInternos.class, id);
        return mail;
    }

    @Override
    public List<ComunicadosInternos> buscarRenviados(String radicacion) throws Exception {
        List<ComunicadosInternos> lista = em.createQuery("select n from ComunicadosInternos n where n.radicacionReenvio=:rad").setParameter("rad", radicacion).getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("Este comunicado no ha sido reenviado");
        }
    }

    @Override
    public List<String> consecutivoRadicacionCorreo() throws Exception {
        List<RadicacionCorreo> concecutivos = em.createQuery("select r from RadicacionCorreo r").getResultList();
        List<String> letras = new ArrayList<>();
        if (concecutivos != null && !concecutivos.isEmpty()) {
            for (RadicacionCorreo radicacion : concecutivos) {
                letras.add(radicacion.getLetra() + "00" + radicacion.getNumeroSecuencia());
            }
            return letras;
        } else {
            throw new Exception("Los consecutivos de para radicar correo no se han precargado");
        }
    }

    @Override
    public String consecutivoRadicacionCorreoXLetra(String letra) throws Exception {
        List<RadicacionCorreo> concecutivos = em.createQuery("select r from RadicacionCorreo r where r.letra=:letra").setParameter("letra", letra).getResultList();
        String radicado = "";
        if (concecutivos != null && !concecutivos.isEmpty()) {
            for (RadicacionCorreo letraradicacion : concecutivos) {
                radicado = letraradicacion.getLetra() + "-00" + letraradicacion.getNumeroSecuencia();
            }
            return radicado;
        } else {
            throw new Exception("Ingrese una letra valida");
        }
    }

    @Override
    public void generarRadicado(String cadena) {
        Map<String, Long> mapaCadena = ValidacionesUtil.ValidarRadicado(cadena);
        if (!mapaCadena.isEmpty()) {
            String letras = "";
            Long numeroFormateado = null;
            for (Map.Entry<String, Long> entry : mapaCadena.entrySet()) {
                letras = entry.getKey();
                numeroFormateado = entry.getValue();
            }

            List<RadicacionCorreo> listaLetras = em.createQuery("select l from RadicacionCorreo l where l.letra=:letra").setParameter("letra", letras).getResultList();
            if (listaLetras != null && !listaLetras.isEmpty()) {
                for (RadicacionCorreo letraradicacion : listaLetras) {
                    letraradicacion.setNumeroSecuencia(numeroFormateado + 1);
                    em.merge(letraradicacion);
                }
            }
        }
    }

    public Boolean extraerDestinosComunicado(ComunicadosInternos comunicado, boolean copia, Oficina oficina) {
        List<DestinosComunicados> lista = consultarDestinosComunicados(comunicado.getId());
        List<Long> destinos = new ArrayList<>();
        for (DestinosComunicados dc : lista) {
            if (copia) {
                if (dc.getCopia()) {
                    destinos.add(dc.getOficina().getId());
                }
            } else if (!dc.getCopia()) {
                destinos.add(dc.getOficina().getId());
            }
        }

        return destinos.contains(oficina.getId());
    }

    @Override
    public String buscarConsecutivoCorreo(String radicacion) {
        Map<String, Long> mapaCadena = new HashMap<String, Long>();
        mapaCadena = ValidacionesUtil.ValidarRadicado(radicacion);
        String letra = "";
        for (Map.Entry<String, Long> entry : mapaCadena.entrySet()) {
            List<RadicacionCorreo> letras = em.createQuery("Select r from RadicacionCorreo r where r.letra=:letra").setParameter("letra", entry.getKey()).getResultList();
            letra = letras.get(0).getLetra() + "00" + letras.get(0).getNumeroSecuencia();
        }
        return letra;
    }

    public void stringTONumber(String cadena) {
        String letras = "";
        String numeros = "";
        Long numeroFormateado = null;
        for (short indice = 0;
                indice < cadena.length();
                indice++) {
            char caracter = cadena.charAt(indice);
            if (ValidacionesUtil.isNumeric(caracter)) {
                numeros += caracter;
            } else {
                letras += caracter;
            }
        }

        numeroFormateado = new Long(numeros);

        List<RadicacionCorreo> listaLetras = em.createQuery("select r from RadicacionCorreo r where r.letra=:letra").setParameter("letra", letras).getResultList();
        if (listaLetras != null && !listaLetras.isEmpty()) {
            for (RadicacionCorreo letraradicacion : listaLetras) {
                letraradicacion.setNumeroSecuencia(numeroFormateado + 1);
                em.merge(letraradicacion);
            }
        }

    }

    @Override
    public List<DestinosComunicados> consultarDestinosComunicados(Long idCI) {
        List<DestinosComunicados> lista = em.createQuery("select dc from ComunicadosInternos ci join ci.destinatarios dc where ci.id=:comunicado and dc.copia=false").setParameter("comunicado", idCI).getResultList();

        if (lista != null && !lista.isEmpty()) {
            return lista;
        }

        return new ArrayList<>();
    }

    @Override
    public List<DestinosComunicados> consultarDestinosCopiaComunicados(Long idCI) {
        List<DestinosComunicados> lista = em.createQuery("select dc from ComunicadosInternos ci join ci.destinatarios dc where ci.id=:comunicado and dc.copia=true").setParameter("comunicado", idCI).getResultList();

        if (lista != null && !lista.isEmpty()) {
            return lista;
        }
        return new ArrayList<>();
    }

    @Override
    public List<ArchivosAdjuntos> consultarArchivosComunicados(Long idCI) {
        ComunicadosInternos mail = em.find(ComunicadosInternos.class, idCI);
        if (mail.getAdjuntosComunicados() != null && !mail.getAdjuntosComunicados().isEmpty()) {
            return mail.getAdjuntosComunicados();
        }

        return new ArrayList<>();
    }

    @Override
    public void marcarComoLeido(ComunicadosInternos comunicado, Actor usuarioLogin) {
        comunicado = em.find(ComunicadosInternos.class, comunicado.getId());
        for (DestinosComunicados destino : comunicado.getDestinatarios()) {
            if (destino.getDestino().getId().equals(usuarioLogin.getId())) {
                comunicado.setLeido(Boolean.TRUE);
                em.merge(comunicado);
            }
        }
    }

    @Override
    public void extraccionImagenes(Date fechaInicial, Date fechaFinal, String ruta) {
        List<ComunicadosInternos> comInternos = em.createQuery("Select ci from ComunicadosInternos ci where ci.fechaEnvio between :fechaini and :fechafin")
                .setParameter("fechaini", fechaInicial)
                .setParameter("fechafin", fechaFinal)
                .getResultList();

        for (ComunicadosInternos ci : comInternos) {
            List<ArchivosAdjuntos> archivos = consultarArchivosComunicados(ci.getId());
            for (int i = 0; i < archivos.size(); i++) {
                try {
                    int size = archivos.size();
                    String fileName = "";
                    ArchivosAdjuntos archivo = archivos.get(i);

                    String directory = System.getenv("JBOSS_HOME") + File.separator + "rootEdeq" + File.separator + "COMINTERNOS" + File.separator + ruta;
                    File folder = new File(directory);
                    if (!folder.exists()) {
                        folder.mkdirs();
                    }
                    int dotPosition = (archivo.getNombreArchivo()).lastIndexOf('.');
                    String fileExtension = (archivo.getNombreArchivo()).substring(dotPosition + 1);

                    if (size > 1) {
                        fileName = directory + File.separator + ci.getId() + "-" + i + "." + fileExtension;
                    } else {
                        fileName = directory + File.separator + ci.getId() + "." + fileExtension;
                    }

                    File file = new File(fileName);

                    //convert array of bytes into file
                    FileOutputStream fileOuputStream
                            = new FileOutputStream(file);
                    fileOuputStream.write(archivo.getArchivo());
                    fileOuputStream.close();

                } catch (IOException ex) {
                    Logger.getLogger(DocumentoBean.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void generarInfoComunicados(Boolean interno) {
        try {
            if (interno) {
                List<ComunicadosInternos> lista = em.createQuery("select c from ComunicadosInternos c join c.destinatarios d where c.fechaEnvio between '01/01/2016' and '29/09/2016' and c.radicacion!=null order by c.id").getResultList();
                JsonObject mainObj = new JsonObject();
                JsonArray listaJson = new JsonArray();
                FileWriter file = null;

                for (ComunicadosInternos comunicadosInternos : lista) {
                    JsonObject json = new JsonObject();
                    json.addProperty("IDDOCUMENTO", comunicadosInternos.getId() + "");
                    json.addProperty("DESDOCUMENTO", comunicadosInternos.getMensaje());
                    listaJson.add(json);
                    mainObj.add("comunicados", listaJson);
                }

                String directory = System.getenv("JBOSS_HOME") + File.separator + "standalone" + File.separator + "tmp" + File.separator + "comunicadosIN.json";
                file = new FileWriter(directory);
                file.write(mainObj.toString());
                try {
                    file.close();

                } catch (IOException ex) {
                    Logger.getLogger(ComunicadosInternosBean.class
                            .getName()).log(Level.SEVERE, null, ex);

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ComunicadosInternosBean.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public List<Actor> obtenerDestinatariosComunicados(List<DestinoDTO> lista, ComunicadosInternos comunicado, boolean copia) {
        List<Actor> destinosCorreo = new ArrayList<>();

        if (lista != null && !lista.isEmpty()) {
            for (DestinoDTO destinoDTO : lista) {
                if (destinoDTO != null) {
                    if (destinoDTO.getTipoDestino().equals("JEFE")) {
                        Oficina oficina = em.find(Oficina.class, destinoDTO.getId());
                        Object[] info = ValidacionesUtil.mostrarJefe(oficina);
                        Actor destino = (Actor) info[0];
                        destinosCorreo.add(destino);

                    } else {
                        Actor destino = em.find(Actor.class, destinoDTO.getId());
                        destinosCorreo.add(destino);
                    }

                }
            }
        }
        return destinosCorreo;
    }

    public void guardarDestinatariosComunicados(List<DestinoDTO> lista, ComunicadosInternos comunicado, boolean copia) {
        List<DestinosComunicados> destinatarios = new ArrayList<>();

        if (lista != null && !lista.isEmpty()) {
            for (DestinoDTO destinoDTO : lista) {
                if (destinoDTO != null) {
                    if (destinoDTO.getTipoDestino().equals("JEFE")) {
                        Oficina oficina = em.find(Oficina.class, destinoDTO.getId());
                        Object[] info = ValidacionesUtil.mostrarJefe(oficina);
                        Actor destino = (Actor) info[0];
                        String cargo = (String) info[1];

                        DestinosComunicados dc = new DestinosComunicados();
                        dc.setOficina(oficina);
                        dc.setDestino(destino);
                        dc.setCargoActualDestino(cargo);
                        dc.setTipoDestino("JEFE");
                        dc.setCopia(copia);
                        em.persist(dc);
                        destinatarios.add(dc);

                    } else {
                        Actor destino = em.find(Actor.class, destinoDTO.getId());
                        DestinosComunicados dc = new DestinosComunicados();
                        dc.setDestino(destino);
                        dc.setTipoDestino("EMPLEADO");
                        dc.setCopia(copia);
                        em.persist(dc);
                        destinatarios.add(dc);
                    }

                }
            }

            if (!copia) {
                comunicado.setDestinatarios(destinatarios);
            } else {
                comunicado.getDestinatarios().addAll(destinatarios);
            }
        }
    }

    public void eliminarDestinos(Long idComunicado) {
        List<DestinosComunicados> lista = em.createQuery("select d from ComunicadosInternos c join c.destinatarios d where c.id=:id").setParameter("id", idComunicado).getResultList();
        if (lista != null && !lista.isEmpty()) {
            for (DestinosComunicados destinosComunicados : lista) {
                em.remove(destinosComunicados);
            }
        }
    }

    @Override
    public Oficina mostrarOficina(Actor jefe) {
        Oficina oficina = null;
        List<Oficina> lista = em.createQuery("Select o from Oficina o where o.jefeACargo=true and o.jefe.id=:jefe").setParameter("jefe", jefe.getId()).getResultList();
        if (lista != null && !lista.isEmpty()) {
            oficina = lista.get(0);
        } else {
            List<Oficina> lista1 = em.createQuery("Select o from Oficina o where o.jefeACargo=false and o.jefeEncargado.id=:jefe").setParameter("jefe", jefe.getId()).getResultList();
            if (lista1 != null && !lista1.isEmpty()) {
                oficina = lista1.get(0);
            }
        }
        return oficina;
    }
}
