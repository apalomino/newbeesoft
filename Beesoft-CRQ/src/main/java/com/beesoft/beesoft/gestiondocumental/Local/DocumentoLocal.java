package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.Util.ArchivoDocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentosRespuesta;
import com.beesoft.beesoft.gestiondocumental.entidades.RevisionDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.SubClase;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface DocumentoLocal {

    /**
     * Metodo para crear documento.
     *
     * @param radicacion, Codigo unico para cada documento.
     * @param radicador, Creador del documento en el sistema.
     * @param anexos,
     * @param asunto, Asunto del documento.
     * @param observacion, Observacion sobre el documento.
     * @param fechaControl, fecha maxima de revision del documento.
     * @param fechaEmision, fecha de creacion del documento.
     * @param fechaRecepcion, fecha de recepcion del documento.
     * @param guia, numero de guia cuando el documento llega por empresa de
     * envios.
     * @param notificacionEntrega, notificacion de entrega del documento.
     * @param pruebaEntrega
     * @param ubicacionFisica, ubicacion del documento fisico.
     * @param mensajero
     * @param clasedocumento, tipo de documento: INFORMACION, CONTROL, GESTION.
     * @param empresaCorreoRecibido
     * @param procedencia, actores que envian el documento.
     * @param destinos, actores a los que se envia el documento.
     * @param documentoReferencia, documento asociado al documento a crear.
     * @param empresacorreo, empresa por la que envian el documento.
     * @param revisores, actores encargados de revisar el documento.
     * @param responsable, actor encargado del documento.
     * @param listaTrd, informacion de recorridos del documento.
     * @param archivos, archivo escaneado para guardar en base de datos.
     * @throws Exception si ocurre un error.
     */
    public void crearDocumento(String radicacion, String pqr, String contractTipo, String contractNum,
            Actor radicador, String anexos, String asunto, String observacion,
            Date fechaControl, Date fechaEmision, Date fechaRecepcion,
            String guia, String mensajero, String notificacionEntrega, String empresaCorreoRecibido,
            String pruebaEntrega, String ubicacionFisica, String clasedocumento, SubClase subClase,
            List<Actor> procedencia, List<Actor> destinos, List<Documento> documentosReferencia,
            String empresacorreo, List<Actor> revisores, Boolean medioElectronico,
            Actor responsable, List<Trd> listaTrd, List<ArchivoDocumento> archivos, Ciudad ciudad) throws Exception;

    /**
     *
     * @param id, identificacion del documento para editarlo.
     * @param fechaControl, fecha maxima de revision del documento.
     * @param clasedocumento, tipo de documento: INFORMACION, CONTROL, GESTION.
     * @param documentoReferencia, documento asociado al documento a crear.
     * @param revisores, actores encargados de revisar el documento.
     * @param revisoresNuevos, nuevos actores encargados de revisar el
     * documento.
     * @param revisoresEliminar, revisores que se van a eliminar.
     * @param responsable, actor encargado del documento.
     * @param listaTrdEliminar, informacion de recorridos a eliminar.
     * @param listaTrdNueva, informacion de recorridos nuevas.
     * @param archivosNuevos, archivos nuevos.
     * @param archivosEliminar, archivos a eliminar.
     * @param observacion
     * @param modifica
     * @param empresaCorreo
     * @param empresaCorreoEnviado
     * @param funcionario
     * @param prueba
     * @param guia
     * @param notificacion
     *
     * @throws Exception si hay algun error.
     */
    public void gestionarDocumento(Long id, Date fechaControl,
            String clasedocumento, List<Documento> documentoReferencia, List<Actor> revisores,
            List<Actor> revisoresNuevos, Actor responsable,
            List<Trd> listaTrdEliminar, List<Trd> listaTrdNueva, List<ArchivoDocumento> archivosNuevos,
            List<ArchivoDocumento> archivosEliminar, String observacion, String empresaCorreo,
            String empresaCorreoEnviado, String funcionario, String guia, String prueba, Boolean medioElectronico,
            String notificacion, String contratacionTipo, String contratacionNum, Actor modifica) throws Exception;

    /**
     *
     * @param documento, documento al cual se le agrega un comentario.
     * @param fechaControl, fecha de control de la revision.
     * @param revisado, esta revisado el documento? si o no.
     * @param aprobado, esta aprobado el documento? si o no.
     * @param pendiente, esta pendiente el documento? si o no.
     * @param comentario, comentario que se realiza al documento.
     * @param revisor, el revisor que hizo el comentario.
     * @param enviarCorreo, es nuevo revisor? si o no.
     * @param nuevoRevisor, nuevo revisor en caso de agregar uno nuevo.
     * @param responsable, responsable del documento.
     */
    public void enviarComentario(Documento documento, Date fechaControl, Boolean revisado, Boolean aprobado, Boolean pendiente, String comentario, Actor revisor, Boolean enviarCorreo, List<Actor> revisores, List<Actor> nuevosRevisores, Boolean responsable);

    /**
     * Metodo encargado de buscar documentos.
     *
     * @param radicacion, codigo unico de documento.
     * @param fechaIni, rango fecha inicio.
     * @param fechaFin, rango fecha fin.
     * @param procedencia, actor, parte de lista de procedencia del documento.
     * @param destino, actor, parte de lista de destinos del documento.
     * @param asunto, asunto del documento.
     * @param revisor, revisor del documento.
     * @param aprobado, filtrar por documentos aprobados
     * @param revisado, filtrar por documentos aprobados
     * @param pendiente, filtrar por documentos aprobados
     * @return lista de documentos relacionados a los filtros.
     * @throws Exception si hay algun error.
     */
    public List<Documento> buscarDocumento(String radicacion, Date fechaIni, Date fechaFin, Actor procedencia, Actor destino, String asunto, String clase, String pruebaEntrega, Actor revisor, Boolean aprobado, Boolean revisado, Boolean pendiente, String idCiudad) throws Exception;

    /**
     * Metodo encargado de buscar documentos pendientes de revision segun el
     * revisor.
     *
     * @param revisor, revisor asociado a los documentos pendientes.
     * @return lista de documentos pendientes.
     * @throws Exception si hay algun error.
     */
    public List<Documento> buscarDocumentosPendientes(Actor revisor) throws Exception;

    /**
     * Metodo encargado de editar el estado del documento cuando se ha revisado.
     *
     * @param documento, documento revisado.
     * @param responsable, responsable del documento.
     */
    public void editarDocumento(Documento documento, Boolean responsable);

    /**
     * Metodo encargado de buscar comentarios segun el documento.
     *
     * @param id, identificacion del documento.
     * @return lista de comentarios de un documento.
     * @throws Exception
     */
    public List<RevisionDocumento> comentariosDocumento(Long id) throws Exception;

    /**
     * Metodo para cargar lso consecutivos de radicacion.
     *
     * @return lista de radicacion.
     * @throws Exception si hay algun error.
     */
    public List<String> consecutivoRadicacion() throws Exception;

    public String buscarConsecutivo(String radicacion);

    public Documento buscarDocumentoById(Long id);

    /**
     * Metodo para eliminar o activar un documento.
     *
     * @param idDocumento, identificador del documento.
     * @param usuario, usuario que elimina o activa el documento.
     * @param estado, eliminado o activado.
     */
    public void cambiarEstado(Long idDocumento, Actor usuario, Boolean estado);

    public Boolean esResponsable(String radicado, Actor responsable);

    public String consecutivoRadicacionXLetra(String letra) throws Exception;

    public String consecutivoRadicacionOficio(String letra) throws Exception;

    public void generarRadicado(String cadena);

    public void editarPruebaEntregaDocumento(Long idDocumento, String prueba, String observacion);

    public List<DocumentoDTO> buscarDocumentoOld(String radicacion, Date fechaIni, Date fechaFin, Actor procedencia, Actor destino, String asunto, Actor revisor, Boolean aprobado, Boolean revisado, Boolean pendiente);

    public List<ArchivoDocumentoDTO> consultarArchivo(String id) throws Exception;

    public List<String> buscarDestinosOld(String id) throws Exception;

    public List<String> buscarProcedenciaOld(String id) throws Exception;

    public String buscarResponsableOld(String id) throws Exception;

    public List<Documento> buscarDocumentoMercurio(String radicacionMercurio, String radicacion, Date fechaIni, Date fechaFin, Actor procedencia, Actor destino, String asunto, Actor revisor, Boolean aprobado, Boolean revisado, Boolean pendiente) throws Exception;

    public List<Actor> consultarProcedenciaDocumento(Long idDocumento);
    
    public List<DocumentosRespuesta> consultarRespuestasDocumento(Long idDoc) throws Exception;

    public List<Actor> consultarDestinoDocumento(Long idDocumento);

    public List<Actor> consultarRevisorDocumento(Long idDocumento);

    public List<Documento> consultarReferenciasDocumento(Long idDocumento);

    public List<ArchivoDocumento> consultarArchivoDocumento(Long idDocumento);

    public void extraccionImagenes(List<Documento> documentos, String ruta);

    public void extraccionImagenesOld(List<String> documentos, String ruta);

    public RevisionDocumento ultimaRevisionDocumentoPorUsuario(Long idDocumento, Long idUsuario);

    public void validarFechaControl();
    
    public void cambiarRadicacionDocumento(Long idDocumento, String radicacion, String comentario, Long idUsuario);
    
    public String mensajeCorreoDocVencido(List<Actor> actor, Documento doc, Integer diasVencido);
    
}
