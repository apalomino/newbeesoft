package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the menu database table.
 *
 */
@Entity
public class Menu implements Serializable,Comparable<Menu> {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Boolean raiz;

    private Boolean subMenu;

    private Boolean url;

    private String descripcion;

    private String etiqueta;

    private Menu parent;

    private Integer orden;

    public Menu() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getRaiz() {
        return this.raiz;
    }

    public void setRaiz(Boolean raiz) {
        this.raiz = raiz;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getUrl() {
        return this.url;
    }

    public void setUrl(Boolean url) {
        this.url = url;
    }

    public Boolean getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(Boolean subMenu) {
        this.subMenu = subMenu;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    @OneToOne
    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }
    
    

    @Override
    public String toString() {
        return etiqueta;
    }

    @Override
    public int compareTo(Menu o) {
        return orden.compareTo(o.getOrden());
    }

}
