/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import org.hibernate.annotations.Type;

/**
 *
 * @author ISSLUNO
 */
@Entity
public class CertificadoComunicado implements Serializable {

    private Long id;
    private String fechaHoraEnvio;
    private String fechaHoraRecibido;
    private String correoDestino;
    private String asunto;
    private byte[] certificado;
    private String certificadoNombre;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getFechaHoraEnvio() {
        return fechaHoraEnvio;
    }

    public void setFechaHoraEnvio(String fechaHoraEnvio) {
        this.fechaHoraEnvio = fechaHoraEnvio;
    }

    public String getFechaHoraRecibido() {
        return fechaHoraRecibido;
    }

    public void setFechaHoraRecibido(String fechaHoraRecibido) {
        this.fechaHoraRecibido = fechaHoraRecibido;
    }

    public String getCorreoDestino() {
        return correoDestino;
    }

    public void setCorreoDestino(String correoDestino) {
        this.correoDestino = correoDestino;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(length = 100000)
    public byte[] getCertificado() {
        return certificado;
    }

    public void setCertificado(byte[] certificado) {
        this.certificado = certificado;
    }

    public String getCertificadoNombre() {
        return certificadoNombre;
    }

    public void setCertificadoNombre(String certificadoNombre) {
        this.certificadoNombre = certificadoNombre;
    }

    
    
}
