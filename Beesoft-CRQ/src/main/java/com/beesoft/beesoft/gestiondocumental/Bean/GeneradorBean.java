/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.GeneradorLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.LetraRadicacion;
import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ISSLUNO
 */
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Singleton
public class GeneradorBean implements GeneradorLocal {

    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    @Lock(LockType.WRITE)
    @AccessTimeout(60_000)
    @Override
    public String consecutivoRadicacionCorreoXLetra(String letra) throws Exception {
        LetraRadicacion consecutivo = em.createQuery("select r from LetraRadicacion r where r.letra=:letra", LetraRadicacion.class).setParameter("letra", letra).getSingleResult();
        em.refresh(consecutivo);
        if (consecutivo != null) {
            Long viejoConsecutivo = consecutivo.getNumeroSecuencia();
            LetraRadicacion radicacionCorreo = new LetraRadicacion();
            radicacionCorreo.setLetra(letra);
            radicacionCorreo.setNumeroSecuencia(consecutivo.getNumeroSecuencia() + 1);

            em.remove(consecutivo);
            em.persist(radicacionCorreo);
            return numeroConsecutivo(viejoConsecutivo);
        } else {
            throw new Exception("Ingrese una letra valida");
        }
    }

    public String numeroConsecutivo(Long numero) {
        String x = numero.toString();
        int cifras = x.length();
        String secuencia = "";
        switch (cifras) {
            case 1:
                secuencia = "0000" + numero;
                break;
            case 2:
                secuencia = "000" + numero;
                break;
            case 3:
                secuencia = "00" + numero;
                break;
            case 4:
                secuencia = "0" + numero;
                break;
            case 5:
                secuencia = "" + numero;
                break;
            default:
                secuencia = "" + numero;
                break;
        }
        return secuencia;
    }

}
