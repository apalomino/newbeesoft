/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author cfernandez@sumset.com
 */
@Entity
public class DestinosComunicados implements Serializable {

    private Long id;
    private String tipoDestino;
    private Oficina oficina;
    private Actor destino;
    private String cargoActualDestino;
    private Boolean copia;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne
    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    @OneToOne
    public Actor getDestino() {
        return destino;
    }

    public void setDestino(Actor destino) {
        this.destino = destino;
    }

    public Boolean getCopia() {
        return copia;
    }

    public void setCopia(Boolean copia) {
        this.copia = copia;
    }

    public String getCargoActualDestino() {
        return cargoActualDestino;
    }

    public void setCargoActualDestino(String cargoActualDestino) {
        this.cargoActualDestino = cargoActualDestino;
    }

    public String getTipoDestino() {
        return tipoDestino;
    }

    public void setTipoDestino(String tipoDestino) {
        this.tipoDestino = tipoDestino;
    }

}
