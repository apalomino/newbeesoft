package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author cc
 */
@Named("jefesAction")
@SessionScoped
public class JefesAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(JefesAction.class);

    @EJB
    private ActorLocal actorBean;

    @EJB
    private OficinaLocal oficinaBean;

    @Inject
    private ApplicationAction application;

    private List<Oficina> jefes;

    private List<Actor> empleados;

    private boolean showActor;

    private Long jefeId;

    private Long jefeEncargadoId;

    private boolean opcionVer;

    private Long oficinaId;

    private Oficina oficinaSelected;

    private String cargo;

    private Map<Long, String> listaEmpleados;

    @PostConstruct
    public void inicializar() {
        try {
            cargarTodosJefesAsignados();
            listaEmpleados = actorBean.listarActores();
            showActor = true;
            opcionVer = true;
        } catch (Exception ex) {
            Logger.getLogger(JefesAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cargarTodosJefesAsignados() {
        try {
            jefes = actorBean.listaJefes();
        } catch (Exception ex) {
            LOG.info(ex);
        }

    }

    public List<Actor> cargarEmpleados(Long jefeId) {
        try {
            empleados = actorBean.listarEmpleados(jefeId);
            return empleados;
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;

    }

    public void listarEmpleados() {
        try {
            listaEmpleados = actorBean.listarActores();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cambiarJefe(Oficina oficina) {
        try {
            if (oficina != null) {
                this.oficinaSelected = oficina;
                jefeId = oficinaSelected.getJefe().getId();
                cargo = oficinaSelected.getCargoJefe();
                if (oficinaSelected.getJefeEncargado() != null) {
                    jefeEncargadoId = oficinaSelected.getJefeEncargado().getId();
                }
                //RequestContext.getCurrentInstance().execute("PF('cambiarJefe').show()");
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cambiarJefeOficina() {
        try {
            actorBean.cambiarJefeOficina(oficinaSelected.getId(), cargo, jefeId, jefeEncargadoId, oficinaSelected.getJefeACargo());
            cargarTodosJefesAsignados();
            application.cargarJefes();
            application.buscarDestinatarios();
            application.llenarInformacionDestinoDto();
            RequestContext.getCurrentInstance().execute("PF('cambiarJefe').hide()");
        } catch (Exception ex) {
            Logger.getLogger(JefesAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Oficina> getJefes() {
        return jefes;
    }

    public void setJefes(List<Oficina> jefes) {
        this.jefes = jefes;
    }

    public List<Actor> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Actor> empleados) {
        this.empleados = empleados;
    }

    public boolean isShowActor() {
        return showActor;
    }

    public void setShowActor(boolean showActor) {
        this.showActor = showActor;
    }

    public Long getJefeId() {
        return jefeId;
    }

    public void setJefeId(Long jefeId) {
        this.jefeId = jefeId;
    }

    public Long getJefeEncargadoId() {
        return jefeEncargadoId;
    }

    public void setJefeEncargadoId(Long jefeEncargadoId) {
        this.jefeEncargadoId = jefeEncargadoId;
    }

    public boolean isOpcionVer() {
        return opcionVer;
    }

    public void setOpcionVer(boolean opcionVer) {
        this.opcionVer = opcionVer;
    }

    public Long getOficinaId() {
        return oficinaId;
    }

    public void setOficinaId(Long oficinaId) {
        this.oficinaId = oficinaId;
    }

    public Map<Long, String> getListaEmpleados() {
        return listaEmpleados;
    }

    public void setListaEmpleados(Map<Long, String> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Oficina getOficinaSelected() {
        return oficinaSelected;
    }

    public void setOficinaSelected(Oficina oficinaSelected) {
        this.oficinaSelected = oficinaSelected;
    }

}
