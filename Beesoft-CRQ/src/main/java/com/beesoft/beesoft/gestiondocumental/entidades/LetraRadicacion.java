package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the letraradicacion database table.
 * 
 */
@Entity
public class LetraRadicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String letra;

	private Long numeroSecuencia;

	public LetraRadicacion() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLetra() {
		return this.letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public Long getNumeroSecuencia() {
		return this.numeroSecuencia;
	}

	public void setNumeroSecuencia(Long numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}


}