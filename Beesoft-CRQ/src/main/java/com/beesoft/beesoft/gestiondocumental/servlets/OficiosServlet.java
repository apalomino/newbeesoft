/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.servlets;

import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import com.beesoft.beesoft.gestiondocumental.webapp.OficiosAction;
import java.io.IOException;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristhian
 */
public class OficiosServlet extends HttpServlet {

    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    @Inject
    private OficiosAction oficios;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String radicacion = request.getParameter("radicacion");
        Usuario usuarioLogin = (Usuario) request.getSession().getAttribute("username");
        oficios.setUsuarioLogin(usuarioLogin);
        Long id = Long.parseLong(radicacion);
        Documento oficio = em.find(Documento.class, id);

        if (oficio != null) {
            DocumentoDTO ofi = new DocumentoDTO(oficio, oficio.getRadicacion(), oficio.getFechaEmision(), oficio.getFechaSistema(), oficio.getAsunto(), oficio.getObservacion(),oficio.getClasedocumento(), Boolean.FALSE);
            oficios.verOficio(ofi);
            response.sendRedirect(request.getContextPath() + "/html/correspondencia/verOficio.xhtml");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
