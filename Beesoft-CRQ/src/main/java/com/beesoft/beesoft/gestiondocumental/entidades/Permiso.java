package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * The persistent class for the permisos database table.
 *
 */
@Entity
public class Permiso implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Menu opcion;

    private Perfil perfil;

    private Boolean permiso;

    public Permiso() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne
    public Perfil getPerfil() {
        return this.perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    @OneToOne
    public Menu getOpcion() {
        return opcion;
    }

    public void setOpcion(Menu opcion) {
        this.opcion = opcion;
    }

    public Boolean getPermiso() {
        return permiso;
    }

    public void setPermiso(Boolean permiso) {
        this.permiso = permiso;
    }

}
