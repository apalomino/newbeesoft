/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficioLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.LetraRadicacion;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Cristhian
 */
@Stateless
public class OficioBean implements OficioLocal {

    private static final Log LOG = LogFactory.getLog(OficioBean.class);

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

//    @EJB
//    private NotificationMailLocal notificationBean;

    @Override
    public void crearOficio(String radicacion, String clase, Date fechaEmision, String titulo, String asunto, String contenido, List<Trd> listaTrd, String elaboro, String aprobo, String version, String modificacion, Actor radicador, Documento documentoReferencia, List<Actor> destinos, List<ArchivoDocumento> archivos) throws Exception {

        String cadena = "";
        Map<String, Long> mapa = ValidacionesUtil.ValidarRadicado(radicacion);
        for (Map.Entry<String, Long> entry : mapa.entrySet()) {
            cadena = entry.getValue() + "";
        }
        Boolean crear = false;
        List<Documento> listaDoc = em.createQuery("select d from Documento d where d.radicacion=:rad and d.clasedocumento=:clase").setParameter("rad", cadena).setParameter("clase", clase).getResultList();
        if (listaDoc != null && !listaDoc.isEmpty()) {
            for (Documento d : listaDoc) {
                crear = d.getFechaEmision().getYear() != new Date().getYear();
            }
        } else {
            crear = true;
        }
        if (crear) {
            Documento documento = new Documento();
            documento.setRadicacion(cadena);
            documento.setFechaEmision(fechaEmision);
            documento.setAsunto(asunto);
            documento.setUbicacionFisica(titulo);
            documento.setFechaSistema(new Date());
            documento.setObservacion(contenido);
            documento.setEmpresacorreoEnviado(aprobo);
            documento.setEmpresaCorreoRecibido(modificacion);
            documento.setAnexos(elaboro);
            documento.setGuia(version);
            documento.setRevisado(true);
            documento.setPendiente(true);
            documento.setAprobado(true);
            documento.setRadicador(radicador);
            //documento.setDocumentoReferencia(documentoReferencia);
            documento.setActivo(Boolean.TRUE);
            documento.setDestinos(destinos);

            if (archivos != null && !archivos.isEmpty()) {
                for (ArchivoDocumento a : archivos) {
                    em.persist(a);
                }
                documento.setArchivos(archivos);
            }

            em.persist(documento);

            for (Trd trd : listaTrd) {

                List<Trd> lista = em.createQuery("select t from Trd t where t.documento.id=:doc and t.oficina.id=:of").setParameter("doc", documento.getId()).setParameter("of", trd.getOficina().getId()).getResultList();

                if (lista != null && !lista.isEmpty()) {
                    throw new Exception("El documento tiene asociado ese Trd");
                } else {
                    Trd trdPersist = new Trd();
                    trdPersist.setDocumento(documento);
                    trdPersist.setOficina(trd.getOficina());
                    trdPersist.setSerie(trd.getSerie());
                    trdPersist.setSubserie(trd.getSubserie());
                    trdPersist.setTipodocumento(trd.getTipodocumento());
                    em.persist(trdPersist);
                }
            }

//            if (documento.getId() != null) {
//                stringTONumber(radicacion);
//                for (Actor destino : destinos) {
//                    enviarCorreoRadicado(radicador, destino, documento);
//                }
//            }
        } else {
            throw new Exception("El radicado ya existe por favor verifique");
        }
    }

    @Override
    public void editarOficio(Long idOficio, String titulo, String asunto, String contenido, List<Trd> listaTrdNueva, List<Trd> listaTrdEliminar, String elaboro, String aprobo, String version, String modificacion, Documento documentoReferencia, List<Actor> destinos, List<ArchivoDocumento> archivosNuevos, List<ArchivoDocumento> archivosEliminados) throws Exception {

        Documento documento = em.find(Documento.class, idOficio);
        documento.setAsunto(asunto);
        documento.setUbicacionFisica(titulo);
        documento.setObservacion(contenido);
        documento.setEmpresacorreoEnviado(aprobo);
        documento.setEmpresaCorreoRecibido(modificacion);
        documento.setAnexos(elaboro);
        documento.setGuia(version);
//        documento.setDocumentoReferencia(documentoReferencia);
        documento.setDestinos(destinos);
        documento.getArchivos().removeAll(archivosEliminados);

        if (listaTrdEliminar != null && !listaTrdEliminar.isEmpty()) {
            for (Trd trd : listaTrdEliminar) {
                trd = em.find(Trd.class, trd.getId());
                em.remove(trd);
            }
        }

        for (Trd trd : listaTrdNueva) {

            List<Trd> lista = em.createQuery("select t from Trd t where t.documento.id=:doc and t.oficina.id=:of").setParameter("doc", documento.getId()).setParameter("of", trd.getOficina().getId()).getResultList();

            if (lista != null && !lista.isEmpty()) {
                throw new Exception("El documento tiene asociado ese Trd");
            } else {
                Trd trdPersist = new Trd();
                trdPersist.setDocumento(documento);
                trdPersist.setOficina(trd.getOficina());
                trdPersist.setSerie(trd.getSerie());
                trdPersist.setSubserie(trd.getSubserie());
                trdPersist.setTipodocumento(trd.getTipodocumento());
                em.persist(trdPersist);
            }
        }

        if (archivosNuevos != null && !archivosNuevos.isEmpty()) {
            for (ArchivoDocumento a : archivosNuevos) {
                em.persist(a);
            }
            documento.getArchivos().addAll(archivosNuevos);
        }

        em.persist(documento);

    }

    @Override
    public List<Documento> buscarOficios(String radicacion, Date fechaIni, Date fechaFin, String contenido, String clase) throws Exception {
        Query q = null;
        StringBuilder query = new StringBuilder("select d from Documento d");
        Map<String, Object> parameters = new HashMap<>();
        boolean iswhere = false;

        if (StringUtils.isNotBlank(radicacion)) {
            query.append(" where d.radicacion=:radicacion");
            parameters.put("radicacion", radicacion);
            iswhere = true;
        }

        if (fechaIni != null && fechaFin != null) {
            if (iswhere) {
                query.append(" and d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
                parameters.put("fechaIni", fechaIni);
                parameters.put("fechaFin", fechaFin);
                iswhere = true;
            } else {
                query.append(" where d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
                parameters.put("fechaIni", fechaIni);
                parameters.put("fechaFin", fechaFin);
                iswhere = true;
            }
        }

        if (StringUtils.isNotBlank(contenido)) {
            if (iswhere) {
                query.append(" and d.observacion like :contenido");
                parameters.put("contenido", "%" + contenido + "%");
            } else {
                query.append(" where d.observacion like :contenido");
                parameters.put("contenido", "%" + contenido + "%");
                iswhere = true;
            }
        }

        if (StringUtils.isNotBlank(clase)) {
            if (iswhere) {
                query.append(" and UPPER(translate(d.clasedocumento, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) Like UPPER(translate(:clase, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU'))");
                parameters.put("clase", clase);
            } else {
                query.append(" where UPPER(translate(d.clasedocumento, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) Like UPPER(translate(:clase, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU'))");
                parameters.put("clase", clase);
                iswhere = true;
            }
        }

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        List<Documento> doc = q.getResultList();
        List<Documento> resultado = new ArrayList<>();
        if (doc != null && !doc.isEmpty()) {
            for (Documento docu : doc) {
                if (docu.getClasedocumento().equalsIgnoreCase(ConstantsMail.CLASE_MEMORANDO) || docu.getClasedocumento().equalsIgnoreCase(ConstantsMail.CLASE_MANUAL) || docu.getClasedocumento().equalsIgnoreCase(ConstantsMail.CLASE_CIRCULAR)) {
                    resultado.add(docu);
                }
            }
            if (!resultado.isEmpty()) {
                return resultado;
            } else {
                throw new Exception("Los parametros de busqueda no arrojan resultados.");
            }
        } else {
            throw new Exception("Los parametros de busqueda no arrojan resultados.");
        }
    }

    public void enviarCorreoRadicado(Actor emisor, Actor actor, Documento doc) {
//        List<Actor> lista = new ArrayList<Actor>();
//        lista.add(actor);
//        String url = ValidacionesUtil.urlServer();
//        StringBuilder message = new StringBuilder("Se\u00F1or(a):");
//        message.append("<br>");
//        message.append(actor.getNombreYApellido());
//        message.append("<br>");
//        message.append("<br>");
//        message.append("Usted tiene un(a) " + doc.getClasedocumento() + " con el número de radicación: ");
//        message.append("<a href=\"http://" + url + ":8080/Beesoft/Oficios?radicacion=" + doc.getId() + "\">" + doc.getRadicacion() + "</a>");
//        message.append("<br>");
//        message.append("<br>");
//        message.append("Características del Documento");
//        message.append("<br>");
//        message.append("Destino(s):	");
//        message.append(listToCadena(consultarDestinoDocumento(doc.getId())));
//        message.append("<br>");
//        message.append("Fecha:	");
//        message.append(formatearFecha(doc.getFechaEmision()));
//        message.append("<br>");
//        message.append("<br>");
//        message.append("Asunto:	         ");
//        message.append(doc.getAsunto());
//        message.append("<br>");
//        message.append("<br>");
//
//        notificationBean.guardarCorreo(emisor, lista, doc.getAsunto(), message.toString(), null,  "OFICIO", false);

    }

    public Connection getConnection() throws Exception {
        String driver = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/edeq";
        String username = "postgres";
        String password = "123456";
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }

    @Override
    public List<DocumentoDTO> buscarOficioOld(String radicacion, Date fechaIni, Date fechaFin, String contenido, String clase) {
        ResultSet rs = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        List<DocumentoDTO> listaDTO = new ArrayList<>();
        Boolean isWhere = false;
        try {
            conn = getConnection();
            StringBuilder query = new StringBuilder("SELECT DISTINCT ON (documento.id) documento.id,");
            query.append("documento.radicacion,");
            query.append("documento.ubicacionfisica,");
            query.append("documento.fechaemision,");
            query.append("documento.comentario,");
            query.append("documento.fechacontrol,");
            query.append("documento.clase,");
            query.append("documento.fechasistema,");
            query.append("documento.observacion,");
            query.append("destinodocumento.iddatosbasicos AS destino,");
            query.append("documento.usuario ");
            query.append("FROM documento left join revisordocumento ");
            query.append("ON documento.id=revisordocumento.iddocumento ");
            query.append("left join destinodocumento ON documento.id=destinodocumento.iddocumento ");

            if (StringUtils.isNotBlank(radicacion)) {
                query.append(" WHERE documento.radicacion like ? ");
                isWhere = true;
            }
            if (fechaIni != null && fechaFin != null) {
                if (isWhere) {
                    query.append(" AND documento.fechaemision BETWEEN ? AND ? ");
                } else {
                    query.append(" WHERE documento.fechaemision BETWEEN ? AND ? ");
                    isWhere = true;
                }
            }
            if (StringUtils.isNotBlank(contenido)) {
                if (isWhere) {
                    query.append(" AND documento.observacion like ?");
                } else {
                    query.append(" WHERE documento.observacion like ?");
                    isWhere = true;
                }
            }
            if (StringUtils.isNotBlank(clase)) {
                if (isWhere) {
                    query.append(" AND documento.clase = ?");
                } else {
                    query.append(" WHERE documento.clase = ?");
                    isWhere = true;
                }
            }

            pstmt = conn.prepareStatement(query.toString()); // create a statement
            int numero = 1;
            if (StringUtils.isNotBlank(radicacion)) {
                pstmt.setString(numero, "%" + radicacion + "%");
                numero++;
            }
            if (fechaIni != null && fechaFin != null) {
                pstmt.setDate(numero, new java.sql.Date(fechaIni.getTime()));
                numero++;
                pstmt.setDate(numero, new java.sql.Date(fechaFin.getTime()));
                numero++;
            }
            if (StringUtils.isNotBlank(contenido)) {
                pstmt.setString(numero, "%" + contenido + "%");
                numero++;
            }
            if (StringUtils.isNotBlank(clase)) {
                pstmt.setString(numero, clase);
                numero++;
            }

            rs = pstmt.executeQuery();
            pstmt.setMaxRows(300);
            // extract data from the ResultSet
            while (rs.next()) {
                String claseDoc = rs.getString(7);
                if (claseDoc.equalsIgnoreCase(ConstantsMail.OFICIO_MEMORANDO) || claseDoc.equalsIgnoreCase(ConstantsMail.OFICIO_MANUAL) || claseDoc.equalsIgnoreCase(ConstantsMail.OFICIO_CIRCULAR)) {
                    String id = rs.getString(1);
                    String rad = rs.getString(2);
                    String ubicacion = rs.getString(3);
                    Date fechaEmision = rs.getDate(4);
                    String asu = rs.getString(5);
                    Date fechaControl = rs.getDate(6);
                    Date fechaSistema = rs.getDate(8);
                    String observ = rs.getString(9);
                    String dest = rs.getString(10);
                    String radicador = rs.getString(11);

                    DocumentoDTO dto = new DocumentoDTO(id, rad, ubicacion, fechaEmision, fechaSistema, dest, asu, observ, claseDoc, fechaControl, radicador, true);
                    listaDTO.add(dto);
                }
            }
        } catch (Exception e) {
            LOG.info(e);
            return new ArrayList<>();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    pstmt.close();
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
        return listaDTO;
    }

    @Override
    public List<String> buscarDestinosOld(String id) throws Exception {
        Connection conn = getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<String> destinos = new ArrayList<>();
        try {
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement("SELECT iddatosbasicos FROM destinodocumento WHERE iddocumento=?");
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();

            if (rs != null) {
                ResultSet rs1 = null;
                while (rs.next()) {
                    String idDestino = rs.getString(1);
                    pstmt = conn.prepareStatement("SELECT nombre,apellido FROM datosbasicos WHERE id=?");
                    pstmt.setString(1, idDestino);
                    rs1 = pstmt.executeQuery();
                    if (rs1 != null) {
                        while (rs1.next()) {
                            String nombre = rs1.getString(1);
                            String apellido = rs1.getString(2);
                            destinos.add(nombre + " " + apellido);
                        }
                    }
                }
            }
        } catch (SQLException sqle) {
            LOG.info(sqle);
            return Collections.emptyList();
        } catch (Exception e) {
            LOG.info(e);
            return Collections.emptyList();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn.getAutoCommit() == false) {
                    conn.setAutoCommit(true);
                }
            } catch (SQLException se) {
                LOG.info(se);
            }
        }
        return destinos;
    }

    @Override
    public List<Actor> consultarDestinoDocumento(Long idDocumento) {
        Documento documento = em.find(Documento.class, idDocumento);
        if (!documento.getDestinos().isEmpty()) {
            return documento.getDestinos();
        } else {
            return new ArrayList<>();
        }
    }

    public String formatearFecha(Date fecha) {
        SimpleDateFormat formateador = new SimpleDateFormat(
                "dd'/'MMMM'/'yyyy", new Locale("ES"));

        String fechaFormat = formateador.format(fecha);
        return fechaFormat;
    }

    public void stringTONumber(String cadena) {
        Map<String, Long> mapaCadena = ValidacionesUtil.ValidarRadicado(cadena);
        if (!mapaCadena.isEmpty()) {
            String letras = "";
            Long numeroFormateado = null;
            for (Map.Entry<String, Long> entry : mapaCadena.entrySet()) {
                letras = entry.getKey();
                numeroFormateado = entry.getValue();
            }

            List<LetraRadicacion> listaLetras = em.createQuery("select l from LetraRadicacion l where l.letra=:letra").setParameter("letra", letras).getResultList();
            if (listaLetras != null && !listaLetras.isEmpty()) {
                for (LetraRadicacion letraradicacion : listaLetras) {
                    letraradicacion.setNumeroSecuencia(numeroFormateado + 1);
                    em.merge(letraradicacion);
                }
            }
        }
    }

    public String listToCadena(List<Actor> lista) {
        int cont = 0;
        StringBuilder destinatarios = new StringBuilder();
        for (Actor actor : lista) {
            cont++;
            destinatarios.append(actor.getNombreYApellido());
            if (lista.size() > cont) {
                destinatarios.append(", ");
            }
        }
        return destinatarios.toString();
    }
}
