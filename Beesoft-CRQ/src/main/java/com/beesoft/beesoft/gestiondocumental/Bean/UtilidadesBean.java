/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.DatosConfiguracion;
import com.beesoft.beesoft.gestiondocumental.entidades.Departamento;
import com.beesoft.beesoft.gestiondocumental.entidades.LetraRadicacion;
import com.beesoft.beesoft.gestiondocumental.entidades.Pais;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoContratos;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoPersona;
import com.itextpdf.text.pdf.codec.Base64.InputStream;
import com.itextpdf.text.pdf.qrcode.ByteArray;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class UtilidadesBean implements UtilidadesLocal {

	@PersistenceContext(unitName = "BeesoftPU")
	private EntityManager em;

	@Override
	public List<TipoPersona> cargarTipoPersona() throws Exception {
		List<TipoPersona> tipos = em.createQuery("Select tp from TipoPersona tp").getResultList();

		if (tipos != null) {
			return tipos;
		} else {
			throw new Exception("No se han creado los tipos de persona para este sistema");
		}

	}

	@Override
	public Map<Long, String> cargarPaises() throws Exception {
		Map<Long, String> paises = new HashMap<Long, String>();
		List<Pais> lista = (List<Pais>) em.createQuery("select p from Pais p").getResultList();

		if (lista != null) {
			for (Pais p : lista) {
				paises.put(p.getId(), p.getDescripcion());
			}

			return paises;
		} else {
			throw new Exception("No se han creado los Paises para este sistema");
		}
	}

	@Override
	public Map<Long, String> cargarDeptos(Long idPais) throws Exception {
		Map<Long, String> deptos = new HashMap<Long, String>();
		List<Departamento> lista = (List<Departamento>) em
				.createQuery("select d from Departamento d where d.pais.id=:pais").setParameter("pais", idPais)
				.getResultList();

		if (deptos != null) {

			for (Departamento d : lista) {
				deptos.put(d.getId(), d.getDescripcion());
			}

			return deptos;
		} else {
			throw new Exception("No se han creado los Departamentos para este sistema");
		}
	}

	@Override
	public Map<Long, String> cargarCiudades(Long idDepto) throws Exception {
		Map<Long, String> lista = new HashMap<Long, String>();
		List<Ciudad> ciudades = (List<Ciudad>) em.createQuery("select c from Ciudad c where c.departamento.id=:depto")
				.setParameter("depto", idDepto).getResultList();

		if (ciudades != null && !ciudades.isEmpty()) {
			for (Ciudad c : ciudades) {
				lista.put(c.getId(), c.getDescripcion());
			}
			return lista;
		} else {
			throw new Exception("No se han creado los Perfiles para este sistema");
		}
	}

	@Override
	public Ciudad buscarCiudad(Long id) throws Exception {

		Ciudad ciudad = em.find(Ciudad.class, id);
		if (ciudad != null) {
			return ciudad;
		} else {
			throw new Exception("Ciudad no existestente");
		}
	}

	@Override
	public Map<Long, String> cargarTipos() throws Exception {
		Map<Long, String> tiposMapa = new HashMap<Long, String>();
		List<TipoPersona> tipos = em.createQuery("select t from TipoPersona t").getResultList();

		if (tipos != null && !tipos.isEmpty()) {
			for (TipoPersona tipopersona : tipos) {
				tiposMapa.put(tipopersona.getId(), tipopersona.getDescripcion());
			}
			return tiposMapa;
		} else {
			throw new Exception("No se han precargado los tipos de persona");
		}
	}

	@Override
	public TipoPersona buscarTipo(Long id) throws Exception {
		TipoPersona tipo = em.find(TipoPersona.class, id);
		if (tipo != null) {
			return tipo;
		} else {
			throw new Exception("El tipo de persona no existe");
		}
	}

	@Override
	public Map<String, String> cargarTiposContratosTexto(Boolean tipo) throws Exception {
		Map<String, String> tiposContratos = new HashMap<>();
		Map<String, String> subTiposContratos = new HashMap<>();
		List<TipoContratos> tipos = em.createQuery("select t from TipoContratos t").getResultList();

		if (tipos != null && !tipos.isEmpty()) {

			boolean hayPunto = false;
			int i = 0;
			for (TipoContratos tc : tipos) {
				String cad = tc.getTipoDocumental();
				hayPunto = false;
				for (i = 0; i < cad.length(); i++) {
					if (cad.charAt(i) == '.') {
						hayPunto = true;
					}
				}
				if (!hayPunto) {
					tiposContratos.put(tc.getTipoDocumental(), tc.getDescripcion());
				} else {
					subTiposContratos.put(tc.getTipoDocumental(), tc.getDescripcion());
				}
			}
			return tipo ? tiposContratos : subTiposContratos;
		} else {
			throw new Exception("No se han precargado los tipos de contratos");
		}
	}

	@Override
	public TipoContratos buscarTiposContratos(String id) {
		TipoContratos tp = em.find(TipoContratos.class, id);
		if (tp != null) {
			return tp;
		}
		return null;
	}

	@Override
	public void crearConfigCorreos(String emailEmisor, String password, String host, String puerto) {
		DatosConfiguracion config = new DatosConfiguracion();
		config.setAuthetication("true");
		config.setEmailEmisor(emailEmisor);
		config.setHost(host);
		config.setPassword(password);
		config.setPuerto(puerto);
		config.setStarttls("true");

		em.persist(config);
	}

	@Override
	public void editarConfigCorreos(String emailEmisor, String host, String password, String puerto, String starttls,
			String authetication, Date fechaCron) {
		DatosConfiguracion config = (DatosConfiguracion) em.createQuery("select d from DatosConfiguracion d")
				.getResultList().get(0);
		if (config != null) {
			config.setAuthetication(authetication);
			config.setEmailEmisor(emailEmisor);
			config.setHost(host);
			if (StringUtils.isNotBlank(password)) {
				config.setPassword(password);
			}
			config.setPuerto(puerto);
			config.setStarttls(starttls);
			config.setFechaCron(fechaCron);
			em.merge(config);
		}
	}

	@Override
	public DatosConfiguracion mostrarConfiguracionCorreo() {
		List<DatosConfiguracion> configs = em.createQuery("select d from DatosConfiguracion d").getResultList();
		if (configs != null && !configs.isEmpty()) {
			return configs.get(0);
		}
		return null;
	}

	@Override
	public void crearLetrasRad(String letra) {
		LetraRadicacion letraNueva = new LetraRadicacion();
		letraNueva.setLetra(letra);
		letraNueva.setNumeroSecuencia(0L);
		em.persist(letraNueva);
	}

	@Override
	public void editarLetrasRad(String letra, Long valor) {
		LetraRadicacion letraNueva = (LetraRadicacion) em
				.createQuery("select l from LetraRadicacion l where l.letra=:letra").setParameter("letra", letra)
				.getResultList().get(0);
		letraNueva.setLetra(letra);
		letraNueva.setNumeroSecuencia(valor);
		em.merge(letraNueva);
	}

	@Override
	public List<LetraRadicacion> listarLetrasRad() {
		List<LetraRadicacion> lista = em.createQuery("select l from LetraRadicacion l").getResultList();
		if (lista != null && !lista.isEmpty()) {
			return lista;
		}
		return null;
	}

	@Override
	public void quitarLetrasRad(Long letraId) {
		if (letraId != null) {
			LetraRadicacion letra = em.find(LetraRadicacion.class, letraId);
			em.remove(letra);
		}
	}

	@Override
	public Map<String, Long> cargarCiudadesDepartamento(long idDeparrtamento) {
		Map<String, Long> lista = new HashMap<String, Long>();
		List<Ciudad> ciudades = (List<Ciudad>) em.createQuery("select c from Ciudad c where c.departamento.id=:depto")
				.setParameter("depto", 24L).getResultList();

		if (ciudades != null && !ciudades.isEmpty()) {
			for (Ciudad c : ciudades) {
				lista.put(c.getDescripcion(), c.getId());
			}
			return lista;
		}
		return lista;
	}

}
