/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author cfernandez@sumset.com
 */
public class ComunicadoInternoDTO {

    private String radicado;
    private String fecha;
    private String emisorNombre;
    private String emisorCargo;
    private String asunto;
    private String detalle;
    private String nombreFirma;
    private String cargoFirma;
    private String email;
    private String telefono;
    private String telExt;
    private String nombreAdjunto;

    public String getRadicado() {
        return radicado;
    }

    public void setRadicado(String radicado) {
        this.radicado = radicado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEmisorNombre() {
        return emisorNombre;
    }

    public void setEmisorNombre(String emisorNombre) {
        this.emisorNombre = emisorNombre;
    }

    public String getEmisorCargo() {
        return emisorCargo;
    }

    public void setEmisorCargo(String emisorCargo) {
        this.emisorCargo = emisorCargo;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getNombreFirma() {
        return nombreFirma;
    }

    public void setNombreFirma(String nombreFirma) {
        this.nombreFirma = nombreFirma;
    }

    public String getCargoFirma() {
        return cargoFirma;
    }

    public void setCargoFirma(String cargoFirma) {
        this.cargoFirma = cargoFirma;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelExt() {
        return telExt;
    }

    public void setTelExt(String telExt) {
        this.telExt = telExt;
    }

    public String getNombreAdjunto() {
        return nombreAdjunto;
    }

    public void setNombreAdjunto(String nombreAdjunto) {
        this.nombreAdjunto = nombreAdjunto;
    }
    

    
}
