/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.ComunicadosInternosLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentosRespuestaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ReportesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.EmpresaCorreoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RadicadoresDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RadicadosDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RecorridoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ReporteExternosDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ComunicadosInternos;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentosRespuesta;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.TrdOld;
import com.beesoft.beesoft.gestiondocumental.webapp.ReportesAction;
import com.itextpdf.text.log.SysoCounter;

import groovy.ui.SystemOutputInterceptor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.hibernate.Hibernate;
//import org.jfree.util.Log;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class ReportesBean implements ReportesLocal {

	private static final Log LOG = LogFactory.getLog(ReportesBean.class);

	@PersistenceContext(unitName = "BeesoftPU")
	private EntityManager em;

	@EJB
	private DocumentoLocal documentoBean;

	@EJB
	private ComunicadosInternosLocal internosBean;

	@EJB
	private OficinaLocal oficinaBean;

	@EJB
	private DocumentosRespuestaLocal docRespuesta;

	public List<Actor> reporteActorExterno(String tipoActExt, String nombreActExt, Long nitActExt) throws Exception {
		Query q = null;
		StringBuilder query = new StringBuilder("select a from Actor a ");
		Map<String, Object> parameters = new HashMap<String, Object>();
		boolean iswhere = false;

		if (nombreActExt != null) {
			query.append("where a.tipo LIKE :tipoActExt ");
			parameters.put("nombreExterno", tipoActExt);
			iswhere = true;
		}

		if (nombreActExt != null) {
			query.append("and a.nombreyapellido LIKE :nombreExterno ");
			parameters.put("nombreExterno", nombreActExt);
			iswhere = true;
		}

		if (nombreActExt != null) {
			query.append("and a.cedula = :cedulaExterno");
			parameters.put("cedulaExterno", nitActExt);
			iswhere = true;
		}

		q = em.createQuery(query.toString());

		if (!parameters.isEmpty()) {
			for (Map.Entry<String, Object> p : parameters.entrySet()) {
				q.setParameter(p.getKey(), p.getValue());
			}
		}

		List<Actor> resultados = q.getResultList();

		if (resultados != null && !resultados.isEmpty()) {
			return resultados;

		} else {
			throw new Exception("Los filtros de busqueda no arrojan resultados: ");
		}
	}

	public List<Documento> reporteDocumento2017(Date fechaIni, Date fechaFin) throws Exception {
		String filtroAdicional = "";

		List<Documento> lista = null;
		lista = em
				.createQuery("select d from Documento d " + "inner join d.destinos dest "
						+ "where d.radicacion not like '%R%' and d.fechaSistema "
						+ "BETWEEN :fechaini and :fechafin and d.anulado=false order by d.radicacion")
				.setParameter("fechaini", fechaIni).setParameter("fechafin", fechaFin).getResultList();

		if (lista != null && lista.size() > 0) {
			return lista;
		} else {
			throw new Exception("Los filtros de busqueda no arrojan resultados");
		}
	}

	@Override
	public List<Trd> reporteDocumentoRecorrido(Date fechaIni, Date fechaFin, Oficina oficina, String clase,
			String ciudad) throws Exception {
		String filtroAdicional = "";
		String filtroAdicional2 = "";
		Query query = null;
		if (StringUtils.isNotBlank(ciudad.toString())) {
			filtroAdicional = " and t.documento.ciudad.id=" + ciudad + " ";
		}
		if (oficina != null) {
			filtroAdicional2 = " and t.oficina.id=" + oficina.getId() + " ";
			List<Trd> lista = null;
			ArrayList<Trd> temp = new ArrayList<>();

			if (StringUtils.isNotBlank(clase)) {
				while (oficina.getId() != null) {
					lista = em.createQuery(
							"select t from Trd t where t.documento.radicacion not like '%R%' and t.documento.clasedocumento=:clase and t.documento.fechaSistema BETWEEN :fechaini and :fechafin and t.documento.anulado=false"
									+ filtroAdicional + filtroAdicional2
									+ " order by t.oficina, t.documento.radicacion")
							.setParameter("clase", clase).setParameter("fechaini", fechaIni)
							.setParameter("fechafin", fechaFin).getResultList();
					if (oficina.getOficina_padre() != null) {
						Oficina oficinaH = oficinaBean.buscarOficina(oficina.getOficina_padre());
						oficina = oficinaH;
					} else {
						break;
					}
					temp.addAll(lista);
					filtroAdicional2 = " and t.oficina.id=" + oficina.getId() + " ";
				}
				lista.addAll(temp);
			} else {
				while (oficina.getId() != null) {
					lista = em.createQuery(
							"select t from Trd t where t.documento.radicacion not like '%R%' and t.documento.fechaSistema BETWEEN :fechaini and :fechafin and t.documento.anulado=false"
									+ filtroAdicional + filtroAdicional2
									+ " order by t.oficina, t.documento.radicacion")
							.setParameter("fechaini", fechaIni).setParameter("fechafin", fechaFin).getResultList();
					if (oficina.getOficina_padre() != null) {
						Oficina oficinaH = oficinaBean.buscarOficina(oficina.getOficina_padre());
						oficina = oficinaH;
					} else {
						break;
					}
					temp.addAll(lista);
					filtroAdicional2 = " and t.oficina.id=" + oficina.getId() + " ";
				}
				lista.addAll(temp);
			}
			if (lista != null && lista.size() > 0) {
				return lista;
			} else {
				throw new Exception("Los filtros de busqueda no arrojan resultados");
			}
		}
		List<Trd> lista = null;
		if (StringUtils.isNotBlank(clase)) {
			lista = em.createQuery(
					"select t from Trd t where t.documento.radicacion not like '%R%' and t.documento.clasedocumento=:clase and t.documento.fechaSistema BETWEEN :fechaini and :fechafin and t.documento.anulado=false"
							+ filtroAdicional + " order by t.oficina, t.documento.radicacion")
					.setParameter("clase", clase).setParameter("fechaini", fechaIni).setParameter("fechafin", fechaFin)
					.getResultList();
		} else {
			lista = em.createQuery(
					"select t from Trd t where t.documento.radicacion not like '%R%' and t.documento.fechaSistema BETWEEN :fechaini and :fechafin and t.documento.anulado=false"
							+ filtroAdicional + " order by t.oficina, t.documento.radicacion")
					.setParameter("fechaini", fechaIni).setParameter("fechafin", fechaFin).getResultList();
		}

		if (lista != null && lista.size() > 0) {
			return lista;
		} else {
			throw new Exception("Los filtros de busqueda no arrojan resultados");
		}
	}

	@Override
	public List<RecorridoDTO> reporteDocumentoCruzado(Date fechaIni, Date fechaFin, Oficina oficina, String ciudad)
			throws Exception {
		// New consult
		List<RecorridoDTO> listaDTO = new ArrayList<>();
		ArrayList<Trd> temp = new ArrayList<>();
		ArrayList<String> lReferencia = new ArrayList();
		String prueba = "";
		String filtroAdicional = "";
		try {
			if (StringUtils.isNotBlank(ciudad.toString())) {
				filtroAdicional = " and t.documento.ciudad.id=" + ciudad + " ";
			}
			List<Trd> lista = new ArrayList<Trd>();
			String consulta = "select t from Trd t where t.documento.radicacion not like '%R%' "
					+ "and t.documento.fechaSistema BETWEEN :fechaini and :fechafin " + "and t.documento.anulado=false "
					+ filtroAdicional;
			if (oficina != null) {
				consulta = consulta + "and t.oficina.id= :idOficina order by t.documento.fechaSistema";
				while (oficina.getId() != null) {
					lista = em.createQuery(consulta).setParameter("fechaini", fechaIni)
							.setParameter("fechafin", fechaFin).setParameter("idOficina", oficina.getId())
							.getResultList();
					if (oficina.getOficina_padre() != null) {
						Oficina oficinaH = oficinaBean.buscarOficina(oficina.getOficina_padre());
						oficina = oficinaH;
					} else {
						break;
					}
					temp.addAll(lista);
				}
				lista.addAll(temp);
			} else {
				consulta = consulta + "order by t.documento.fechaSistema";
				lista = em.createQuery(consulta).setParameter("fechaini", fechaIni).setParameter("fechafin", fechaFin)
						.getResultList();
			}
			/* Nueva */
			if (lista != null && lista.size() > 0) {
				int i = 0;
				for (Trd trd : lista) {
					if (trd != null && trd.getDocumento() != null) {
						// prueba=trd.getDocumento().getRadicacion();//+" --- procedencia:
						// "+trd.getDocumento().getProcedencia().size()+" --- destinos:
						// "+trd.getDocumento().getDestinos().size();
						RecorridoDTO dto = new RecorridoDTO();
						dto.setFecha(ValidacionesUtil.formatearFechaHora(trd.getDocumento().getFechaSistema()));
						dto.setRadicacion(trd.getDocumento().getRadicacion());
						// long idDocumento = trd.getDocumento().getId();
						List<Actor> procedencia = documentoBean
								.consultarProcedenciaDocumento(trd.getDocumento().getId());
						dto.setProcedencia(ValidacionesUtil.listToCadena(procedencia));
						dto.setAsunto(trd.getDocumento().getAsunto());
						if (trd.getDocumento().getResponsable() != null) {
							if (trd.getDocumento().getResponsable().getOficina() != null)
								dto.setResponsable(trd.getDocumento().getResponsable().getOficina().getDescripcion());
						}
						dto.setFechalimite(ValidacionesUtil.formatearFechaHora(trd.getDocumento().getFechaControl()));
						if (trd.getDocumento().getDocumentoRespuesta() != null) {
							dto.setReferenciaCruzada(trd.getDocumento().getDocumentoRespuesta().getRadicacion());
							dto.setFechaenvio(ValidacionesUtil
									.formatearFechaHora(trd.getDocumento().getDocumentoRespuesta().getFechaSistema()));
						}

						// ********************** Listar Respuestas
						List<DocumentosRespuesta> listaResp = new ArrayList<>();
						if (trd.getDocumento().getDocumentoRespuesta() != null) {
							try {
								listaResp = documentoBean.consultarRespuestasDocumento(trd.getDocumento().getId());
								if (listaResp != null && !listaResp.isEmpty()) {
									String docs = "";
									for (DocumentosRespuesta d : listaResp) {
										docs = docs + d.getRadicado() + " ";
									}
									dto.setReferenciaCruzada(docs);
									dto.setFechaenvio(
											ValidacionesUtil.formatearFechaHora(trd.getDocumento().getFechaSistema()));
								}
							} catch (Exception e) {
								LOG.info("Exception Query. " + e);
							}
						}
						/*if (trd.getDocumento().getDocumentosRespuesta() != null) {
						dto.setReferenciaCruzada(trd.getDocumento().getDocumentosRespuesta().get(0).getRadicacion());
						dto.setFechaenvio(ValidacionesUtil
								.formatearFechaHora(trd.getDocumento().getDocumentosRespuesta().get(0).getFechaSistema()));
						}*/

						if (trd.getDocumento().getMigrado() != null && trd.getDocumento().getMigrado()) {
							dto.setClase(trd.getDocumento().getClaseOld());
						} else {
							dto.setClase(trd.getDocumento().getClasedocumento());
						}

						if (StringUtils.isNotBlank(trd.getDocumento().getAnexos())) {
							dto.setAnexos(trd.getDocumento().getAnexos());
						} else {
							dto.setAnexos("");
						}

						// Trd attributes

						if (trd.getOficina() != null && StringUtils.isNotBlank(trd.getOficina().getDescripcion())) {
							dto.setOficina(trd.getOficina().getDescripcion());
						} else {
							dto.setOficina("");
						}

						if (trd.getSerie() != null && StringUtils.isNotBlank(trd.getSerie().getDescripcion())) {
							dto.setSerie(trd.getSerie().getDescripcion());
						} else {
							dto.setSerie("");
						}

						if (trd.getSubserie() != null && StringUtils.isNotBlank(trd.getSubserie().getDescripcion())) {
							dto.setSubSerie(trd.getSubserie().getDescripcion());
						} else {
							dto.setSubSerie("");
						}

						if (trd.getTipodocumento() != null
								&& StringUtils.isNotBlank(trd.getTipodocumento().getDescripcion())) {
							dto.setTipo(trd.getTipodocumento().getDescripcion());
						} else {
							dto.setTipo("");
						}
						listaDTO.add(dto);
					}
				}
				return listaDTO;
			} else {
				throw new Exception("Los filtros de busqueda no arrojan resultados");
			}
		} catch (Exception e) {
			/* Nueva */
			LOG.info("Error al generar reporte REFERENCIA CRUZADA, " + e);
			ValidacionesUtil.addMessageInfo("Error al generar el reporte");
		}
		return listaDTO;
	}

	@Override
	public List<EmpresaCorreoDTO> reporteEmpresaCorreo(String empresaCorreo, Date fechaIni, Date fechaFin,
			String ciudad) throws Exception {

		Query q = null;
		StringBuilder query = new StringBuilder("select distinct d from Documento d ");
		query.append("inner join d.destinos dest ");
		Map<String, Object> parameters = new HashMap<String, Object>();
		boolean iswhere = false;

		if (fechaIni != null && fechaFin != null) {
			query.append("where d.fechaRecepcion BETWEEN :fechaIni AND :fechaFin");
			parameters.put("fechaIni", fechaIni);
			parameters.put("fechaFin", fechaFin);
			iswhere = true;
		}

		if (StringUtils.isNotBlank(empresaCorreo)) {
			query.append(" and d.empresacorreoEnviado=:envio");
			parameters.put("envio", empresaCorreo);
			iswhere = true;
		}

		if (StringUtils.isNotBlank(ciudad)) {
			query.append(" and dest.ciudad.id=:idCiudad");
			parameters.put("idCiudad", Long.parseLong(ciudad));
			iswhere = true;
		}

		query.append(" and d.radicacion like '%R%' and d.anulado=false order by d.radicacion");

		q = em.createQuery(query.toString());

		if (!parameters.isEmpty()) {
			for (Map.Entry<String, Object> p : parameters.entrySet()) {
				q.setParameter(p.getKey(), p.getValue());
			}
		}

		// String sql ="SELECT D FROM Documento D WHERE (D.fechaRecepcion BETWEEN
		// :FECHA_INICIO AND :FECHA_FIN) AND D.ciudad.id=:idCiudad and D.radicacion like
		// '%R%' and D.anulado=false order by D.radicacion";
		// List<Documento> resultados = em.createQuery(sql).setParameter("FECHA_INICIO",
		// fechaIni).setParameter("FECHA_FIN", fechaIni).setParameter("idCiudad",
		// Long.parseLong(ciudad) ).getResultList();

		List<Documento> resultados = q.getResultList();

		List<EmpresaCorreoDTO> documentos = new ArrayList<>();
		if (resultados != null && !resultados.isEmpty()) {
			for (Documento doc : resultados) {
				EmpresaCorreoDTO correo = ingresarDatos(doc);
				documentos.add(correo);
			}
			return documentos;
		} else {
			throw new Exception("Los filtros de busqueda no arrojan resultados: ");
		}

	}

	public EmpresaCorreoDTO ingresarDatos(Documento doc) {
		EmpresaCorreoDTO correo = new EmpresaCorreoDTO();
		if (doc.getMigrado()) {
			List<TrdOld> listaTrd = em.createQuery("select t from Trd t where t.documento.id=:id")
					.setParameter("id", doc.getId()).getResultList();
			if (listaTrd != null && !listaTrd.isEmpty()) {
				TrdOld trd = listaTrd.get(0);
				correo.setArea(trd.getOficinaOld().getDescripcion());
			}
		} else {
			List<Trd> listaTrd = em.createQuery("select t from Trd t where t.documento.id=:id")
					.setParameter("id", doc.getId()).getResultList();
			if (listaTrd != null && !listaTrd.isEmpty()) {
				Trd trd = listaTrd.get(0);
				correo.setArea(trd.getOficina().getDescripcion());
			}
		}
		correo.setAsunto(doc.getAsunto());
		ArrayList<String> lDestino = new ArrayList();
		ArrayList<String> lCiudad = new ArrayList();
		ArrayList<String> lDireccion = new ArrayList();
		ArrayList<String> lTelefono = new ArrayList();

		if (doc.getDestinos() != null && !doc.getDestinos().isEmpty()) {
			for (int i = 0; i < doc.getDestinos().size(); i++) {
				Actor destino = doc.getDestinos().get(i);
				if (destino.getCiudad() != null) {
					lDestino.add(destino.getCiudad().getDescripcion() + "-"
							+ destino.getCiudad().getDepartamento().getDescripcion());
				} else {
					lDestino.add("");
				}
				if (destino.getCiudad() != null) {
					lCiudad.add(destino.getCiudad().getDescripcion());
				} else {
					lCiudad.add("SIN CIUDAD");
				}
				lDireccion.add(destino.getDireccion1());
				if (StringUtils.isNotBlank(destino.getTelefono())) {
					lTelefono.add(destino.getTelefono());
				}
			}
			String sDestino = lDestino.toString().substring(1, lDestino.toString().length() - 1);
			correo.setDestino(sDestino);
			correo.setDestino(ValidacionesUtil.listToCadena(doc.getDestinos()));
			String sCiudad = lCiudad.toString().substring(1, lCiudad.toString().length() - 1);
			correo.setCiudad(sCiudad);
			String sDireccion = lDireccion.toString().substring(1, lDireccion.toString().length() - 1);
			correo.setDireccion1(sDireccion);
			String sTelefono = lTelefono.toString().substring(1, lTelefono.toString().length() - 1);
			correo.setTelefono(sTelefono.toString());
		}

		correo.setfEntrega(doc.getEmpresacorreoEnviado());
		correo.setRadicacion(doc.getRadicacion());
		if (doc.getDocumentoReferencia() != null) {
			String referencia = "";
			for (Documento dr : doc.getDocumentosReferencia()) {
				referencia += dr.getRadicacion() + " ";
			}
			correo.setRadicacionSol(referencia);
		}

		if (StringUtils.isNotBlank(doc.getAnexos())) {
			correo.setAnexos(doc.getAnexos());
		} else {
			correo.setAnexos("");
		}

		if (StringUtils.isNotBlank(doc.getNotificacionEntrega())) {
			correo.setNotif(doc.getNotificacionEntrega());
		} else {
			correo.setNotif("");
		}

		if (StringUtils.isNotBlank(doc.getGuia())) {
			correo.setGuia(doc.getGuia());
		} else {
			correo.setGuia("");
		}
//		if (doc.getCiudad() != null)
//			correo.setCiudad(doc.getCiudad().getDescripcion());
//		else
//			correo.setCiudad("Sin datos");

		return correo;
	}

	@Override
	public List<Documento> reporteContratacion(String tipoContrato, String numContrato, String radicacion,
			Date fechaIni, Date fechaFin) throws Exception {
		Query q = null;
		StringBuilder query = new StringBuilder("select d from Documento d");
		Map<String, Object> parameters = new HashMap<String, Object>();
		boolean iswhere = false;

		if (StringUtils.isNotBlank(tipoContrato)) {
			query.append(" where d.contratacionTipo=:tipo");
			parameters.put("tipo", tipoContrato);
			iswhere = true;
		}

		if (StringUtils.isNotBlank(numContrato)) {
			if (iswhere) {
				query.append(" and d.contratacionNumero=:num");
				parameters.put("num", numContrato);
			} else {
				query.append(" where d.contratacionNumero=:num");
				parameters.put("num", numContrato);
				iswhere = true;
			}
		}

		if (StringUtils.isNotBlank(radicacion)) {
			if (iswhere) {
				query.append(" and d.radicacion=:rad");
				parameters.put("rad", radicacion);
			} else {
				query.append(" where d.radicacion=:rad");
				parameters.put("rad", radicacion);
				iswhere = true;
			}
		}

		if (fechaIni != null && fechaFin != null) {
			if (iswhere) {
				query.append(" and d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
			} else {
				query.append(" where d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
				iswhere = true;
			}
		}

		q = em.createQuery(query.toString());

		if (!parameters.isEmpty()) {
			for (Map.Entry<String, Object> p : parameters.entrySet()) {
				q.setParameter(p.getKey(), p.getValue());
			}
		}

		List<Documento> resultados = q.getResultList();

		if (resultados != null && !resultados.isEmpty()) {

			return resultados;
		} else {
			throw new Exception("Los filtros de busqueda no arrojan resultados");
		}
	}

	@Override
	public List<RadicadosDTO> reporteRadicadosPorFechas(String letra, Date fechaIni, Date fechaFin) throws Exception {

		List<Documento> filtrado = new ArrayList<>();
		List<ComunicadosInternos> filtradoInterno = new ArrayList<>();
		Query q = null;
		StringBuilder query = new StringBuilder("select d from Documento d");
		Map<String, Object> parameters = new HashMap<>();
		boolean iswhere = false;
		boolean isLetterE = true;

		if (StringUtils.isNotBlank(letra)) {
			query.append(" where d.radicacion like :letra");
			parameters.put("letra", "%" + letra + "%");
			iswhere = true;

			if (letra.equals("R")) {
				isLetterE = false;
			}

		}

		if (fechaIni != null && fechaFin != null) {
			if (iswhere) {
				query.append(" and d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
			} else {
				query.append(" where d.fechaEmision BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
				iswhere = true;
			}

			if (isLetterE) {
				filtradoInterno = em
						.createQuery("select ci from ComunicadosInternos ci "
								+ "where ci.fechaEnvio BETWEEN :fechaIni and :fechaFin")
						.setParameter("fechaIni", fechaIni).setParameter("fechaFin", fechaFin).getResultList();

			}
		}

		q = em.createQuery(query.toString());

		if (!parameters.isEmpty()) {
			for (Map.Entry<String, Object> p : parameters.entrySet()) {
				q.setParameter(p.getKey(), p.getValue());
			}
		}

		filtrado = q.getResultList();

		if (filtrado != null && !filtrado.isEmpty()) {
			return llenarDTORadicados(filtrado, filtradoInterno);
		} else {
			throw new Exception("Los filtros de busqueda no arrojan resultados");
		}
	}

	public List<RadicadosDTO> llenarDTORadicados(List<Documento> documentos, List<ComunicadosInternos> internos) {
		List<RadicadosDTO> listaFinal = new ArrayList<>();

		for (Documento doc : documentos) {
			RadicadosDTO dto = new RadicadosDTO();
			dto.setRadicacion(doc.getRadicacion());
			dto.setAsunto(doc.getAsunto());
			dto.setProcedencia(ValidacionesUtil.listToCadena(documentoBean.consultarProcedenciaDocumento(doc.getId())));
			dto.setDestino(ValidacionesUtil.listToCadena(documentoBean.consultarDestinoDocumento(doc.getId())));
			dto.setFecha(ValidacionesUtil.formatearFecha(doc.getFechaEmision()));
			dto.setTipo("DOCUMENTO");
			listaFinal.add(dto);
		}

		for (ComunicadosInternos interno : internos) {
			RadicadosDTO dto = new RadicadosDTO();
			dto.setRadicacion(interno.getRadicacion());
			dto.setAsunto(interno.getAsunto());
			dto.setProcedencia(interno.getEmisor().getNombreYApellido());
			// dto.setDestino(ValidacionesUtil.listToCadena(internosBean.consultarDestinosComunicados(interno.getId())));
			dto.setFecha(ValidacionesUtil.formatearFecha(interno.getFechaEnvio()));
			dto.setTipo("COMUNICADO ELECTRONICO");
			listaFinal.add(dto);
		}

		return listaFinal;
	}

	@Override
	public List<RadicadoresDTO> listarRadicadores(Date fechaIni, Date fechaFin) throws Exception {
		List<Actor> lista = em
				.createQuery(
						"select d.radicador from Documento d where d.fechaRecepcion BETWEEN :fechaIni and :fechaFin ")
				.setParameter("fechaIni", fechaIni).setParameter("fechaFin", fechaFin).getResultList();
		List<RadicadoresDTO> listaRadicadores = new ArrayList<>();
		HashSet hs = new HashSet();
		hs.addAll(lista);
		lista.clear();
		lista.addAll(hs);
		for (Actor rad : lista) {
			Long e = (Long) em.createQuery(
					"select count(d) from Documento d where d.radicador.id=:rad and d.radicacion not like :radicacion and d.fechaEmision BETWEEN :fechaIni and :fechaFin")
					.setParameter("rad", rad.getId()).setParameter("radicacion", "%R%")
					.setParameter("fechaIni", fechaIni).setParameter("fechaFin", fechaFin).getSingleResult();
			Long r = (Long) em.createQuery(
					"select count(d) from Documento d where d.radicador.id=:rad and d.radicacion like :radicacion and d.fechaEmision BETWEEN :fechaIni and :fechaFin")
					.setParameter("rad", rad.getId()).setParameter("radicacion", "%R%")
					.setParameter("fechaIni", fechaIni).setParameter("fechaFin", fechaFin).getSingleResult();

			listaRadicadores.add(new RadicadoresDTO(rad, e, r, null, null, null, null, null, null, null, null));
		}

		if (!listaRadicadores.isEmpty()) {
			return listaRadicadores;
		} else {
			throw new Exception("Los parametros de busqueda no arrojan resultados");
		}
	}

	@Override
	public List<Trd> reportePQRSDVencidos(Date fechaIni, Date fechaFin, Oficina oficina) throws Exception {
		try {
			List<Trd> listaDocumentos = new ArrayList<Trd>();
			ArrayList<Trd> temp = new ArrayList<>();
			if (oficina != null) {
				while (oficina.getId() != null) {
					listaDocumentos = em
							.createQuery("SELECT doc FROM Trd doc " + "WHERE "
									+ "(doc.documento.clasedocumento = 'Peticiones' "
									+ "or doc.documento.clasedocumento = 'Quejas' "
									+ "or doc.documento.clasedocumento = 'Reclamos' "
									+ "or doc.documento.clasedocumento = 'Denuncias') "
									+ "and doc.documento.documentoRespuesta is null "
									+ "and doc.documento.fechaRecepcion >= :fechaIni "
									+ "and doc.documento.radicacion like '%E%' " + "and doc.oficina.id = :idOficina "
									+ "order by doc.documento.fechaRecepcion desc")
							.setParameter("fechaIni", fechaIni).setParameter("idOficina", oficina.getId())
							.getResultList();

					if (oficina.getOficina_padre() != null) {
						Oficina oficinaH = oficinaBean.buscarOficina(oficina.getOficina_padre());
						oficina = oficinaH;
					} else {
						break;
					}
					temp.addAll(listaDocumentos);
				}
				listaDocumentos.addAll(temp);
			} else {
				listaDocumentos = em
						.createQuery("SELECT doc FROM Trd doc WHERE " + "(doc.documento.clasedocumento = 'Peticiones' "
								+ "or doc.documento.clasedocumento = 'Quejas' "
								+ "or doc.documento.clasedocumento = 'Reclamos' "
								+ "or doc.documento.clasedocumento = 'Denuncias') "
								+ "and doc.documento.documentoRespuesta is null "
								+ "and doc.documento.radicacion like '%E%' "
								+ "and doc.documento.fechaRecepcion >= :fechaIni "
								+ "order by doc.documento.fechaRecepcion desc")
						.setParameter("fechaIni", fechaIni).getResultList();
			}

			if (!listaDocumentos.isEmpty())
				return listaDocumentos;
			else
				throw new Exception("Los filtros de busqueda no arrojan resultados");
		} catch (Exception e) {
			throw new Exception("Error: " + e.getMessage());
		}
	}

	@Override
	public List<Actor> reporteActoresExternos(Long tipoPersona) throws Exception {
		try {
			List<Actor> lista = new ArrayList<Actor>();

			if (tipoPersona != 0) {
				lista = em
						.createQuery("select a from Usuario u " + " RIGHT JOIN u.actor a" + " WHERE u.id IS NULL "
								+ " and a.estado = true and a.tipopersona.id = :tipo " + " order by a.tipopersona.id")
						.setParameter("tipo", tipoPersona).setMaxResults(9000).getResultList();
			} else {
				lista = em
						.createQuery("select a from Usuario u " + " RIGHT JOIN u.actor a" + " WHERE u.id IS NULL "
								+ " and a.estado = true " + " order by a.tipopersona.id")
						.setMaxResults(9000).getResultList();
			}

			if (!lista.isEmpty()) {
				return lista;
			} else
				throw new Exception("Los filtros de busqueda no arrojan resultados");
		} catch (Exception e) {
			throw new Exception("Error: " + e.getMessage());
		}
	}

}
