/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.AuditoriaDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * * @author Cristhian Camilo Fernandez V.
 *         cfernandez@sumset.com
 */
@Local
public interface AuditoriaLocal {

    /**
     * Metodo para crear la auditoria de las modificaciones sobre los documentos.
     * @param acccion,
     *              tipo de modificacion: CREAR,EDITAR,ELIMINAR,ACTIVAR.
     * @param usuario,
     *              usuario que realizo la modificacion.
     * @param documento, 
     *              el documento relacionado.
     */
    public void crearAuditoria(String acccion, Actor usuario, Documento documento);

    /**
     * Metodo para buscar las auditorias de documentos eliminados.
     * @param fechaIni,
     *                Rango fecha de inicio 
     * @param fechaFin,
     *                Rango fecha final
     * @param usuario,
     *                Usuario que realizo la modificacion.
     * @return lista de auditorias relacionadas.
     */
    public List<AuditoriaDocumento> buscarDocumentoEliminados(String radicacion,Date fechaIni, Date fechaFin, Long usuario, String idCiudad);

    /**
     * Metodo para buscar todas las auditorias segun los filtros de busqueda.
     * @param fechaIni,
     *                 Rango fecha inicio.
     * @param fechaFin,
     *                  Rango fecha final.
     * @param usuario,
     *                  Usuario que realizo la modificacion.
     * @param accion,
     *                 el tipo de modificacion que se realizo.
     * @return Lista de auditorias relacionadas.
     */
    public List<AuditoriaDocumento> buscarDocumentoAcciones(Date fechaIni, Date fechaFin, Long usuario, String accion);

}
