/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.AuditoriaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ReportesLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UsuarioLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.ContratacionDTO;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoAuditoriaDTO;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.EmpresaCorreoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RadicadoresDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RadicadosDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RecorridoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ReporteExternosDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.AuditoriaDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoPersona;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "reportesAction")
@SessionScoped
public class ReportesAction implements Serializable {

	private static final Log LOG = LogFactory.getLog(ReportesAction.class);

	@EJB
	private ReportesLocal reportesBean;

	@EJB
	private OficinaLocal oficinaBean;

	@EJB
	private DocumentoLocal documentoBean;

	@EJB
	private ActorLocal actorBean;

	@EJB
	private AuditoriaLocal auditoriaBean;

	@EJB
	private UsuarioLocal usuarioBean;

	@EJB
	private UtilidadesLocal utilidadesBean;

	// ---------reportes recorrido------------
	private Date fechaIni;
	private Date fechaFin;
	private String clase;
	private Oficina recorrido;
	private String recorridoCadena;
	private Map<Long, String> mapRecorrido;
	private List<Trd> listaDocumentos;
	private Map<Long, String> listaCiudades;
	private String municipio;
	String file = null;
	StreamedContent fileGuardar = null;
	Boolean isDownload;

	// ---------reportes 2017------------
	private Date fechaIni2;
	private Date fechaFin2;
	private String clase2;
	private Map<Long, String> mapRecorrido2;
	private List<Documento> listaDocumentos2;
	private Map<Long, String> listaCiudades2;
	private String municipio2;
	String file2 = null;
	StreamedContent fileGuardar2 = null;
	Boolean isDownload2;

	// ---------reportes cruzado------------
	private Date fechaCruzadoIni;
	private Date fechaCruzadoFin;
	private String radicadoIni;
	private String radicadoFin;
	private Boolean fecha;
	private List<RecorridoDTO> listaCruzados;
	String fileCruzado = null;
	StreamedContent fileGuardarCruzado = null;
	// ---------------------------------------
	// -------reporte doc eliminado----------
	private String radicacionEliminado;
	private Date fechaIniEliminado;
	private Date fechaFinEliminado;
	private List<AuditoriaDocumento> listaDocsEliminados;
	private Map<Long, String> listaResponsables;
	private String usuario;
	private Actor actor;
	// --------------------------------------
	// ----------reporte acciones-------------
	private Date fechaIniAc;
	private Date fechaFinAc;
	private List<AuditoriaDocumento> listaDocsAc;
	private Actor actorAc;
	private String accion;
	// ------------------------------------------
	// ---------------reportes empresa correo------
	private List<String> empresaCorreo;
	private String empresa;
	private Date fechaIniEmpresa;
	private Date fechaFinEmpresa;
	private List<EmpresaCorreoDTO> documentosEmpresa;
	private JasperReport reporteEmpresa = null;
	private List<EmpresaCorreoDTO> listaDTO;
	private Map<String, Object> parameters;
	// -----------------reportes contratacion------
	private List<String> listaTipos;
	private List<Documento> listaContratos;
	private String contratacionTipo;
	private String numeroContrato;
	private String radicacion;
	private Date fechaIniContra;
	private Date fechaFinContra;
	private Usuario usuarioLogin;
	// ---------------------------------------------
	// -------------------------reporte Codigo Niu-------------
	private String codigoNiu;
	private String tipoNiu;
	private Date fechaInicial;
	private Date fechaFinal;
	private Long total;

	// -----------------Radicados---------------------------
	private Date fechaIniRad;
	private Date fechaFinRad;
	private String letraRad;
	private List<RadicadosDTO> listaRadicados;

	// --------------Radicadores-----------------
	private List<RadicadoresDTO> listaRadicadores;
	private Date fechaIniRadicadores;
	private Date fechaFinRadicadores;
	// -------------- PQRSD vencidos--------------
	private List<Trd> listaDocumentoVencidos;
	
	// -----------------Dias Festivos--------
		private int year;
		private int easterMonth;
		private int easterDay;
		private ArrayList<String> holidays;

	// --------------Reporte Externos ------------
	private List<TipoPersona> listaTipoPersona;
	private TipoPersona tipoPersona = null;
	private Long tipoCadena;
	private List<Actor> listaExternos;
	StreamedContent fileGuardarExterno = null;
	
	
	@PostConstruct
	public void inicializar() {
		reset();
		cargarRecorridos();
		cargarUsuario();
		cargarEmpresaCorreo();
		cargarCiudadesQuindio();
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		usuarioLogin = (Usuario) session.getAttribute("username");
	}

//	public StreamedContent reporteActExternos(Boolean tipoDownload) {
//		try {
//				listaExternos = reportesBean.reporteActorExterno(tipoActExt, nombreActExt, nitActExt);
//
//				if (listaExternos != null && !listaExternos.isEmpty()) {
//					StreamedContent file = verReporteActorExterno(listaExternos, tipoDownload);
//					if (file != null) {
//						return file;
//					}
//					reset();
//				}					
//			}catch(Exception ex) {
//				LOG.info(ex);				
//			}
//			return null;
//	}

	public StreamedContent reporte2017(Boolean tipoDownload) {
		try {
			obtenerRecorrido();
			int validacion = ValidacionesUtil.validarFechasIniFinObligatorio(fechaIni, fechaFin, "Fecha y hora inicial",
					"Fecha y hora final");

			Oficina oficinaNombre = oficinaBean.buscarOficinaPorNombre(recorridoCadena);
			if (oficinaNombre == null && (recorridoCadena != null && recorridoCadena != ""))
				validacion = 0;

			if (validacion == 1) {
				listaDocumentos2 = reportesBean.reporteDocumento2017(fechaIni, fechaFin);

				if (listaDocumentos2 != null && !listaDocumentos2.isEmpty()) {
					StreamedContent file = verReporte2017(listaDocumentos2, tipoDownload);
					// verReporteRecorridoXLS(listaDocumentos);
					if (file != null) {
						return file;
					}
					reset();
				}
			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
		return null;
	}
	
	public StreamedContent reporteRecorrido(Boolean tipoDownload) {
		try {
			obtenerRecorrido();
			int validacion = ValidacionesUtil.validarFechasIniFinObligatorio(fechaIni, fechaFin, "Fecha y hora inicial",
					"Fecha y hora final");

			Oficina oficinaNombre = oficinaBean.buscarOficinaPorNombre(recorridoCadena);
			if (oficinaNombre == null && (recorridoCadena != null && recorridoCadena != ""))
				validacion = 0;

			if (validacion == 1) {
				listaDocumentos = reportesBean.reporteDocumentoRecorrido(fechaIni, fechaFin, oficinaNombre, clase,
						obtenerIdCiudad());

				if (listaDocumentos != null && !listaDocumentos.isEmpty()) {
					StreamedContent file = verReporteRecorrido(listaDocumentos, tipoDownload);
					// verReporteRecorridoXLS(listaDocumentos);
					if (file != null) {
						return file;
					}
					reset();
				}
			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
		return null;

	}

	public StreamedContent reporteCruzado(Boolean tipoDownload) {
		try {

			listaCruzados = new ArrayList<>();

			int validacion = ValidacionesUtil.validarFechasIniFinObligatorio(fechaCruzadoIni, fechaCruzadoFin,
					"Fecha y hora inicial", "Fecha y hora final");
			Oficina oficinaNombre = oficinaBean.buscarOficinaPorNombre(recorridoCadena);
			if (oficinaNombre == null && (recorridoCadena != null && recorridoCadena != ""))
				validacion = 0;

			if (validacion == 1) {
				listaCruzados = reportesBean.reporteDocumentoCruzado(fechaCruzadoIni, fechaCruzadoFin, oficinaNombre, obtenerIdCiudad());
				if (listaCruzados != null && !listaCruzados.isEmpty()) {
					StreamedContent file = verReporteCruzado(listaCruzados, tipoDownload);
					// verReporteRecorridoXLS(listaDocumentos);
					if (file != null) {
						return file;
					}
					reset();
				}

			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
		return null;

	}

	public void reporteEmpresaCorreo() {
		try {

			listaDocumentos = new ArrayList<>();

			int validacion = ValidacionesUtil.validarFechasIniFinObligatorio(fechaIniEmpresa, fechaFinEmpresa,
					"Fecha y hora inicial", "Fecha y hora final");

			if (validacion == 1) {
				documentosEmpresa = new ArrayList<>();
				documentosEmpresa = reportesBean.reporteEmpresaCorreo(empresa, fechaIniEmpresa, fechaFinEmpresa,
						obtenerIdCiudad());

				if (documentosEmpresa != null && !documentosEmpresa.isEmpty()) {
					verReporteEmpresaDocumento(documentosEmpresa,
							ValidacionesUtil.formatearFechaElectronico(fechaIniEmpresa));
					reset();
				}

			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
	}

	public StreamedContent reporteContratacion() {
		try {

			listaContratos = new ArrayList<>();

			ValidacionesUtil.validarFechasIniFin(fechaIniContra, fechaFinContra);
			int validacion = 0;
			validacion += ValidacionesUtil.validarCadena(contratacionTipo, "Contratación");
			validacion += ValidacionesUtil.validarCadena(numeroContrato, "Número Contratación");

			if (validacion == 2) {
				listaContratos = reportesBean.reporteContratacion(contratacionTipo, numeroContrato, radicacion,
						fechaIniContra, fechaFinContra);

				if (listaContratos != null && !listaContratos.isEmpty()) {
					StreamedContent file = verReporteContratacion(listaContratos);
					if (file != null) {
						return file;
					}
					reset();
				}

			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
		return null;

	}

	public void buscarDocumentoEliminado() {
		try {

			listaDocsEliminados = new ArrayList<>();

			ValidacionesUtil.validarFechasIniFin(fechaIniEliminado, fechaFinEliminado);

			obtenerUsuario();
			if (actor != null) {
				listaDocsEliminados = auditoriaBean.buscarDocumentoEliminados(radicacionEliminado, fechaIniEliminado,
						fechaFinEliminado, actor.getId(), obtenerIdCiudad());
			} else {
				listaDocsEliminados = auditoriaBean.buscarDocumentoEliminados(radicacionEliminado, fechaIniEliminado,
						fechaFinEliminado, null, obtenerIdCiudad());
			}

			if (listaDocsEliminados == null || listaDocsEliminados.isEmpty()) {
				ValidacionesUtil.addMessage("Los parametros de busqueda no arrojan resultados.");
			}

		} catch (Exception ex) {
			ValidacionesUtil.addMessage("Error:" + ex.getMessage());
			LOG.info(ex);
		}
	}

	public void buscarDocumentoAccion() {
		try {

			listaDocsAc = new ArrayList<>();

			ValidacionesUtil.validarFechasIniFin(fechaIniAc, fechaFinAc);

			obtenerUsuario();
			actorAc = actor;
			if (actorAc != null) {
				listaDocsAc = auditoriaBean.buscarDocumentoAcciones(fechaIniAc, fechaFinAc, actorAc.getId(), accion);
			} else {
				listaDocsAc = auditoriaBean.buscarDocumentoAcciones(fechaIniAc, fechaFinAc, null, accion);
			}

			if (listaDocsAc == null || listaDocsAc.isEmpty()) {
				ValidacionesUtil.addMessage("Los parametros de busqueda no arrojan resultados.");
			}

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public StreamedContent reporteDocumentoEliminado() {
		try {
			if (listaDocsEliminados != null && !listaDocsEliminados.isEmpty()) {
				StreamedContent file = verReporteDocEliminado(listaDocsEliminados);
				if (file != null) {
					return file;
				}
				reset();
			}

		} catch (SQLException ex) {
			LOG.info(ex);
		}
		return null;
	}

	public StreamedContent reporteDocumentoAuditoria() {
		try {
			if (listaDocsAc != null && !listaDocsAc.isEmpty()) {
				StreamedContent file = verReporteAuditoria(listaDocsAc);
				if (file != null) {
					return file;
				}
				reset();
			}

		} catch (SQLException ex) {
			LOG.info(ex);
		}
		return null;
	}

	public void activarDocumento(Documento documento) {
		documentoBean.cambiarEstado(documento.getId(), usuarioLogin.getActor(), true);
		buscarDocumentoEliminado();
	}

	public void cargarUsuario() {
		try {
			listaResponsables = actorBean.cargarListaAdminRad();
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarRecorridos() {
		try {
			mapRecorrido = new HashMap<Long, String>();
			mapRecorrido = oficinaBean.cargarOficinas();
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void obtenerUsuario() {
		actor = null;
		if (StringUtils.isNotBlank(usuario)) {
			try {
				Long id = null;
				for (Map.Entry<Long, String> re : listaResponsables.entrySet()) {
					if (re.getValue().equals(usuario)) {
						id = re.getKey();
					}
				}
				actor = usuarioBean.buscarUsuario(id).getActor();

			} catch (Exception ex) {
				LOG.info(ex);
			}
		}
	}

	public void obtenerRecorrido() {
		if (StringUtils.isNotBlank(recorridoCadena)) {
			try {
				Long id = Long.valueOf(recorridoCadena);
				recorrido = oficinaBean.buscarOficina(id);

			} catch (Exception ex) {
				LOG.info(ex);
			}
		}
	}

	
	public static String listToCadena(List<Actor> lista) {
		return ValidacionesUtil.listToCadena(lista);
	}
	
	public String listToCadena2017(List<Actor> lista) {
		return ValidacionesUtil.listToCadena2017(lista);
	}
	
	public StreamedContent verReporteActorExterno(List<Actor> lista, Boolean tipoDownload) throws SQLException {
		JasperReport reporte;
		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporteExternos.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = null;
			String type = "";
			String fileName = "";

			if (tipoDownload) {
				try {
					JasperPrint reporte_view = JasperFillManager.fillReport(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					JRXlsExporter exporter = new JRXlsExporter();
					ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, reporte_view);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
					exporter.exportReport();
					archivo = xlsReport.toByteArray();
					type = "application/vnd.ms-excel";
					fileName = "Empresas y Personas Externas-" + ValidacionesUtil.formatearFecha(new Date()) + ".xls";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			} else {
				try {
					archivo = JasperRunManager.runReportToPdf(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					type = "application/pdf";
					fileName = "Empresas y Personas Externas-" + ValidacionesUtil.formatearFecha(new Date()) + ".pdf";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			}
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			fileGuardarExterno = new DefaultStreamedContent(stream, type, fileName);
			if (fileGuardarExterno != null) {
				reporteEmpresa = null;
				return fileGuardarExterno;
			}

		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}
		return null;
		
	}

	public StreamedContent verReporte2017(List<Documento> lista, Boolean tipoDownload) throws SQLException {
		JasperReport reporte;
		List<RecorridoDTO> listaDTO = new ArrayList<>();
		ArrayList<String> lOficina = new ArrayList();
		int numero = 0;
		for (Documento trd : lista) {
			RecorridoDTO dto = new RecorridoDTO();
			dto.setNumero("" + ++numero);
			dto.setFecha(ValidacionesUtil.formatearFechaHora(trd.getFechaSistema()));
			dto.setRadicacion(trd.getRadicacion());
			List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(trd.getId());
			List<Actor> destinos = documentoBean.consultarDestinoDocumento(trd.getId());
			dto.setProcedencia(listToCadena(procedencia));
			dto.setDestino(listToCadena(destinos));
			dto.setMunicipio(listToCadena2017(destinos));
			dto.setAsunto(trd.getAsunto());
			dto.setClase(trd.getClasedocumento());
			if (trd.getMedioElectronico() != null) {
				if (trd.getMedioElectronico()) {
					dto.setMedio("Electronico");
				} else {
					dto.setMedio("");
				}
			} else if (trd.getMedioElectronico() != null) {
				if (trd.getMedioElectronico()) {
					dto.setMedio("Electronico");
				} else {
					dto.setMedio("");
				}
			} else {
				dto.setMedio("");
			}
			if (StringUtils.isNotBlank(trd.getAnexos())) {
				dto.setAnexos(trd.getAnexos());
			} else {
				dto.setAnexos("");
			}
			listaDTO.add(dto);
		}

		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporte2017.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = null;
			String type = "";
			String fileName = "";

			if (tipoDownload) {
				try {
					JasperPrint reporte_view = JasperFillManager.fillReport(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					JRXlsExporter exporter = new JRXlsExporter();
					ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, reporte_view);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
					exporter.exportReport();
					archivo = xlsReport.toByteArray();
					type = "application/vnd.ms-excel";
					fileName = "REPORTE 2017-" + ValidacionesUtil.formatearFecha(new Date()) + ".xls";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			} else {
				try {
					archivo = JasperRunManager.runReportToPdf(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					type = "application/pdf";
					fileName = "REPORTE 2017-" + ValidacionesUtil.formatearFecha(new Date()) + ".pdf";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			}
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			fileGuardar = new DefaultStreamedContent(stream, type, fileName);
			if (fileGuardar != null) {
				reporteEmpresa = null;
				return fileGuardar;
			}

		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}

		return null;
	}

	public StreamedContent verReporteRecorrido(List<Trd> lista, Boolean tipoDownload) throws SQLException {
		JasperReport reporte;
		List<RecorridoDTO> listaDTO = new ArrayList<>();
		int numero = 0;
		for (Trd trd : lista) {
			RecorridoDTO dto = new RecorridoDTO();
			dto.setNumero("" + ++numero);
			dto.setFecha(ValidacionesUtil.formatearFechaHora(trd.getDocumento().getFechaSistema()));
			dto.setRadicacion(trd.getDocumento().getRadicacion());
			List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(trd.getDocumento().getId());
			List<Actor> destinos = documentoBean.consultarDestinoDocumento(trd.getDocumento().getId());
			dto.setProcedencia(listToCadena(procedencia));
			dto.setDestino(listToCadena(destinos));
			dto.setAsunto(trd.getDocumento().getAsunto());
			dto.setDependencia(trd.getOficina().getDescripcion());
			dto.setClase(trd.getDocumento().getClasedocumento());
			if (trd.getDocumento().getCiudad() != null)
				dto.setMunicipio(trd.getDocumento().getCiudad().getDescripcion());
			if (trd.getMedioElectronico() != null) {
				if (trd.getMedioElectronico()) {
					dto.setMedio("Electronico");
				} else {
					dto.setMedio("");
				}
			} else if (trd.getDocumento().getMedioElectronico() != null) {
				if (trd.getDocumento().getMedioElectronico()) {
					dto.setMedio("Electronico");
				} else {
					dto.setMedio("");
				}
			} else {
				dto.setMedio("");
			}
			if (StringUtils.isNotBlank(trd.getDocumento().getAnexos())) {
				dto.setAnexos(trd.getDocumento().getAnexos());
			} else {
				dto.setAnexos("");
			}
			listaDTO.add(dto);
		}

		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reportePlanilla.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = null;
			String type = "";
			String fileName = "";

			if (tipoDownload) {
				try {
					JasperPrint reporte_view = JasperFillManager.fillReport(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					JRXlsExporter exporter = new JRXlsExporter();
					ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, reporte_view);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
					exporter.exportReport();
					archivo = xlsReport.toByteArray();
					type = "application/vnd.ms-excel";
					fileName = "CORRESPONDENCIA-" + ValidacionesUtil.formatearFecha(new Date()) + ".xls";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			} else {
				try {
					archivo = JasperRunManager.runReportToPdf(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					type = "application/pdf";
					fileName = "CORRESPONDENCIA-" + ValidacionesUtil.formatearFecha(new Date()) + ".pdf";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			}
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			fileGuardar = new DefaultStreamedContent(stream, type, fileName);
			if (fileGuardar != null) {
				reporteEmpresa = null;
				return fileGuardar;
			}

		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}

		return null;
	}

	public StreamedContent verReporteCruzado(List<RecorridoDTO> listaDTO, Boolean tipoDownload) throws SQLException {
		JasperReport reporte;
		ValidacionesUtil.addMessageInfo("Ingreso a verReporteCruzado");
		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporteCruzado.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = null;
			String type = "";
			String fileName = "";

			if (tipoDownload) {
				try {
					JasperPrint reporte_view = JasperFillManager.fillReport(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					JRXlsExporter exporter = new JRXlsExporter();
					ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, reporte_view);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
					exporter.exportReport();
					archivo = xlsReport.toByteArray();
					type = "application/vnd.ms-excel";
					fileName = "SEGUIMIENTO Y CONTROL-" + ValidacionesUtil.formatearFecha(new Date()) + ".xls";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			} else {
				try {
					archivo = JasperRunManager.runReportToPdf(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					type = "application/pdf";
					fileName = "SEGUIMIENTO Y CONTROL-" + ValidacionesUtil.formatearFecha(new Date()) + ".pdf";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			}
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			fileGuardarCruzado = new DefaultStreamedContent(stream, type, fileName);
			if (fileGuardarCruzado != null) {
				reporteEmpresa = null;
				return fileGuardarCruzado;
			}

		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}

		return null;
	}

	public void verReporteEmpresaDocumento(List<EmpresaCorreoDTO> lista, String fecha) throws SQLException {
		listaDTO = new ArrayList<>();
		for (EmpresaCorreoDTO dto : lista) {
			listaDTO.add(dto);
		}
		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			parameters = new HashMap<>();
			parameters.put("logo", image);
			parameters.put("fecha", fecha);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporteEmpresa.jrxml";
			reporteEmpresa = JasperCompileManager.compileReport(report);

		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}
		
	}

	public StreamedContent generar(Boolean tipoDownload) {
		StreamedContent fileGuardar = null;
		ByteArrayInputStream stream = null;
		byte[] archivo = null;
		String type = "";
		String fileName = "";
		reporteEmpresaCorreo();
		if (reporteEmpresa != null) {
			if (tipoDownload) {
				try {
					JasperPrint reporte_view = JasperFillManager.fillReport(reporteEmpresa, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					JRXlsExporter exporter = new JRXlsExporter();
					ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, reporte_view);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
					exporter.exportReport();
					archivo = xlsReport.toByteArray();
					type = "application/vnd.ms-excel";
					fileName = "Empresas.xls";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			} else {
				try {
					archivo = JasperRunManager.runReportToPdf(reporteEmpresa, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					type = "application/pdf";
					fileName = "Empresas.pdf";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			}
			stream = new ByteArrayInputStream(archivo);
			fileGuardar = new DefaultStreamedContent(stream, type, fileName);
			if (fileGuardar != null) {
				reporteEmpresa = null;
				return fileGuardar;
			}
		}
		return null;
	}

	public StreamedContent verReporteContratacion(List<Documento> lista) throws SQLException {
		JasperReport reporte;
		JasperPrint reporte_view;
		List<ContratacionDTO> listaDTO = new ArrayList<ContratacionDTO>();
		int numero = 0;
		for (Documento doc : lista) {
			ContratacionDTO dto = new ContratacionDTO();
			dto.setNumero("" + ++numero);
			dto.setFecha(ValidacionesUtil.formatearFechaHora(doc.getFechaSistema()));
			dto.setRadicacion(doc.getRadicacion());
			List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(doc.getId());
			List<Actor> destinos = documentoBean.consultarDestinoDocumento(doc.getId());
			dto.setProcedencia(listToCadena(procedencia));
			dto.setDestino(listToCadena(destinos));
			dto.setAsunto(doc.getAsunto());

			if (StringUtils.isNotBlank(doc.getAnexos())) {
				dto.setAnexos(doc.getAnexos());
			} else {
				dto.setAnexos("");
			}

			if (contratacionTipo.equals("Licitacion")) {
				dto.setContratacionInfo("LICITACIÓN" + " " + numeroContrato);
			} else if (contratacionTipo.equals("Invitacion")) {
				dto.setContratacionInfo("INVITACIÓN" + " " + numeroContrato);
			}

			listaDTO.add(dto);
		}
		List<ContratacionDTO> listaVacios = new ArrayList<ContratacionDTO>();
		while (listaVacios.size() < 6) {
			ContratacionDTO dto = new ContratacionDTO("", "" + ++numero, "", "", "", "", "", "");
			listaVacios.add(dto);
		}
		listaDTO.addAll(listaVacios);
		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporteContratacion.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = JasperRunManager.runReportToPdf(reporte, parameters,
					new JRBeanCollectionDataSource(listaDTO));
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			StreamedContent fileGuardar = new DefaultStreamedContent(stream, "application/pdf", "contratacion.pdf");
			if (fileGuardar != null) {
				return fileGuardar;
			}
		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}

		return null;
	}

	public StreamedContent verReporteDocEliminado(List<AuditoriaDocumento> lista) throws SQLException {
		JasperReport reporte;
		JasperPrint reporte_view;

		List<DocumentoAuditoriaDTO> listaDTO = new ArrayList<DocumentoAuditoriaDTO>();
		for (AuditoriaDocumento audi : lista) {
			DocumentoAuditoriaDTO dto = new DocumentoAuditoriaDTO();
			dto.setFecha(ValidacionesUtil.formatearFechaHora(audi.getDocumento().getFechaSistema()));
			dto.setRadicacion(audi.getDocumento().getRadicacion());
			List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(audi.getDocumento().getId());
			List<Actor> destinos = documentoBean.consultarDestinoDocumento(audi.getDocumento().getId());
			dto.setProcedencia(listToCadena(procedencia));
			dto.setDestino(listToCadena(destinos));
			dto.setCreador(audi.getDocumento().getRadicador().getNombreYApellido());
			dto.setElimino(audi.getUsuario().getNombreYApellido());
			listaDTO.add(dto);
		}

		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporteEliminados.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = JasperRunManager.runReportToPdf(reporte, parameters,
					new JRBeanCollectionDataSource(listaDTO));
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			StreamedContent fileGuardar = new DefaultStreamedContent(stream, "application/pdf", "eliminados.pdf");
			if (fileGuardar != null) {
				return fileGuardar;
			}
		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException e) {
			LOG.info(e);
		}
		return null;
	}

	public StreamedContent verReporteAuditoria(List<AuditoriaDocumento> lista) throws SQLException {
		JasperReport reporte;
		JasperPrint reporte_view;

		List<DocumentoAuditoriaDTO> listaDTO = new ArrayList<>();
		for (AuditoriaDocumento audi : lista) {
			DocumentoAuditoriaDTO dto = new DocumentoAuditoriaDTO();
			dto.setFecha(ValidacionesUtil.formatearFechaHora(audi.getDocumento().getFechaSistema()));
			dto.setRadicacion(audi.getDocumento().getRadicacion());
			List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(audi.getDocumento().getId());
			List<Actor> destinos = documentoBean.consultarDestinoDocumento(audi.getDocumento().getId());
			dto.setProcedencia(listToCadena(procedencia));
			dto.setDestino(listToCadena(destinos));
			dto.setCreador(audi.getDocumento().getRadicador().getNombreYApellido());
			dto.setFechaAccion(ValidacionesUtil.formatearFechaHora(audi.getFechaHora()));
			dto.setElimino(audi.getUsuario().getNombreYApellido());
			dto.setAccion(audi.getAccion());
			listaDTO.add(dto);
		}

		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporteAcciones.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			reporte_view = JasperFillManager.fillReport(reporte, parameters, new JRBeanCollectionDataSource(listaDTO));
			byte[] archivo = JasperRunManager.runReportToPdf(reporte, parameters,
					new JRBeanCollectionDataSource(listaDTO));
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			StreamedContent fileGuardar = new DefaultStreamedContent(stream, "application/vnd.ms-excel",
					"cambios_mensuales.pdf");
			if (fileGuardar != null) {
				return fileGuardar;
			}
		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException e) {
			LOG.info(e);
		}
		return null;
	}

	public void reporteRadicados() {
		try {
			ValidacionesUtil.validarFechasIniFin(fechaIniRad, fechaFinRad);
			listaRadicados = reportesBean.reporteRadicadosPorFechas(letraRad, fechaIniRad, fechaFinRad);
		} catch (Exception ex) {
			Logger.getLogger(ReportesAction.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public String mostrarProcedenciaTabla(Documento doc) {
		if (doc != null) {
			List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(doc.getId());
			return ValidacionesUtil.listToCadena(procedencia);
		}
		return "";
	}

	public String mostrarDestinoTabla(Documento doc) {
		if (doc != null) {
			List<Actor> destino = documentoBean.consultarDestinoDocumento(doc.getId());
			return ValidacionesUtil.listToCadena(destino);
		}
		return "";
	}

	public void buscarRadicadores() {
		try {
			int validate = 0;
			validate += ValidacionesUtil.validarFechasIniFinObligatorio(fechaIniRadicadores, fechaFinRadicadores,
					"Fecha de Recepcion Inicial", "Fecha de Recepcion Final");
			if (validate == 1) {
				listaRadicadores = new ArrayList<>();
				listaRadicadores = reportesBean.listarRadicadores(fechaIniRadicadores, fechaFinRadicadores);
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public StreamedContent buscarPQRSDVencidos(Boolean tipoDownload) throws Exception {
		try {
			if (fechaIni == null)
				ValidacionesUtil.addMessage("El campo Fecha inicial es obligatorio");

			Oficina oficinaNombre = oficinaBean.buscarOficinaPorNombre(recorridoCadena);
			if (oficinaNombre != null || !StringUtils.isNotBlank(recorridoCadena)) {
				Date fechaFinHabil = fechaDiasHabiles(fechaIni);
				listaDocumentoVencidos = reportesBean.reportePQRSDVencidos(fechaIni, fechaFinHabil, oficinaNombre);
				if (!listaDocumentoVencidos.isEmpty()) {
					StreamedContent file = verReportePQRSDVencido(listaDocumentoVencidos, tipoDownload);
					if (file != null)
						return file;
					else
						reset();
				}
			}

		} catch (Exception e) {
			reset();
			LOG.info(e);
			ValidacionesUtil.addMessage("Error: " + e.getMessage());
		}
		return null;
	}

	public StreamedContent verReportePQRSDVencido(List<Trd> lista, Boolean tipoDownload) throws SQLException {
		JasperReport reporte;
		List<RecorridoDTO> listaDTO = new ArrayList<>();
		int numero = 0;
		java.util.Date fechaActual = new Date();//Fecha Actual
		for (Trd trd : lista) {
			RecorridoDTO dto = new RecorridoDTO();
			dto.setNumero("" + ++numero);
			dto.setFecha(ValidacionesUtil.formatearFechaHora(trd.getDocumento().getFechaSistema()));
			dto.setFecharecepcion(ValidacionesUtil.formatearFecha(trd.getDocumento().getFechaRecepcion()));
			dto.setRadicacion(trd.getDocumento().getRadicacion());
			Date fechaFinHabil = fechaDiasHabiles(trd.getDocumento().getFechaRecepcion());
			dto.setDiasVencimiento(numeroDiasHabiles(fechaActual, fechaFinHabil));
			List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(trd.getDocumento().getId());
			List<Actor> destinos = documentoBean.consultarDestinoDocumento(trd.getDocumento().getId());
			dto.setProcedencia(listToCadena(procedencia));
			dto.setDestino(listToCadena(destinos));
			dto.setAsunto(trd.getDocumento().getAsunto());
			dto.setFechalimite(ValidacionesUtil.formatearFechaHora(fechaFinHabil));
			if(trd.getDocumento().getCiudad()!=null)
				dto.setMunicipio(trd.getDocumento().getCiudad().getDescripcion());
			dto.setClase(trd.getDocumento().getClasedocumento());
			if (trd.getDocumento().getMedioElectronico() != null) {
				if (trd.getDocumento().getMedioElectronico()) {
					dto.setMedio("Electronico");
				} else {
					dto.setMedio("");
				}
			} else {
				dto.setMedio("");
			}
			if (StringUtils.isNotBlank(trd.getDocumento().getAnexos())) {
				dto.setAnexos(trd.getDocumento().getAnexos());
			} else {
				dto.setAnexos("");
			}
			// Trd attributes
			if (trd.getOficina() != null && StringUtils.isNotBlank(trd.getOficina().getDescripcion())) {
				dto.setOficina(trd.getOficina().getDescripcion());
			} else {
				dto.setOficina("");
			}

			if (trd.getSerie() != null && StringUtils.isNotBlank(trd.getSerie().getDescripcion())) {
				dto.setSerie(trd.getSerie().getDescripcion());
			} else {
				dto.setSerie("");
			}

			if (trd.getSubserie() != null && StringUtils.isNotBlank(trd.getSubserie().getDescripcion())) {
				dto.setSubSerie(trd.getSubserie().getDescripcion());
			} else {
				dto.setSubSerie("");
			}

			if (trd.getTipodocumento() != null && StringUtils.isNotBlank(trd.getTipodocumento().getDescripcion())) {
				dto.setTipo(trd.getTipodocumento().getDescripcion());
			} else {
				dto.setTipo("");
			}
			listaDTO.add(dto);
		}

		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reportePQRSDVencidos.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = null;
			String type = "";
			String fileName = "";

			if (tipoDownload) {
				try {
					JasperPrint reporte_view = JasperFillManager.fillReport(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					JRXlsExporter exporter = new JRXlsExporter();
					ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, reporte_view);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
					exporter.exportReport();
					archivo = xlsReport.toByteArray();
					type = "application/vnd.ms-excel";
					fileName = "PQRSDVencido-" + ValidacionesUtil.formatearFecha(new Date()) + ".xls";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			} else {
				try {
					archivo = JasperRunManager.runReportToPdf(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					type = "application/pdf";
					fileName = "PQRSDVencido-" + ValidacionesUtil.formatearFecha(new Date()) + ".pdf";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			}
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			fileGuardar = new DefaultStreamedContent(stream, type, fileName);
			if (fileGuardar != null) {
				reporteEmpresa = null;
				return fileGuardar;
			}

		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}

		return null;
	}

	public static Date fechaDiasHabiles(Date fechaIni) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DiaHabil.countBusinessDays(fechaIni));
		return calendar.getTime();

	}

	public static int numeroDiasHabiles(Date fechaIni, Date fechaFin) {
		int numeroDiasHabiles = DiaHabil.numeroDiasHabiles(fechaIni, fechaFin);
		return numeroDiasHabiles;

	}

	private String obtenerIdCiudad() {
		Ciudad ciudad = new Ciudad();
		String idCiudad = "";
		listaDocumentos = new ArrayList<>();
		if (StringUtils.isNotBlank(municipio)) {
			ciudad = obtenerCiudad(municipio);
			idCiudad = ciudad.getId().toString();
		} else
			idCiudad = "";

		return idCiudad;
	}

	public void reset() {
		clase = "";
		fechaInicial = null;
		fechaFinal = null;
		tipoNiu = "";
		codigoNiu = "";
		total = null;
		fechaIni = null;
		fechaFin = null;
		fechaIniEliminado = null;
		fechaFinEliminado = null;
		fechaIniEmpresa = null;
		fechaFinEmpresa = null;
		empresa = "";
		accion = "";
		fechaFinAc = null;
		fechaIniAc = null;
		recorridoCadena = "";
		usuario = "";
		listaDocsEliminados = new ArrayList<>();
		listaDocsAc = new ArrayList<>();
		listaRadicados = new ArrayList<>();
		fechaIniRad = null;
		fechaFinRad = null;
		fechaCruzadoIni = null;
		fechaCruzadoFin = null;
		letraRad = "";
		fechaIniRadicadores = null;
		fechaFinRadicadores = null;
		listaRadicadores = new ArrayList<>();
		listaDocumentoVencidos = new ArrayList<>();
		municipio = "";
		// listaCiudades= null;
	}

	public void cargarEmpresaCorreo() {
		try {
			empresaCorreo = new ArrayList<String>();
			empresaCorreo.add(ConstantsMail.EMP_ROR);
			empresaCorreo.add(ConstantsMail.EMP_ROR_RECUPERACION);
			empresaCorreo.add(ConstantsMail.EMP_472);
			empresaCorreo.add(ConstantsMail.EMP_SERVIENTREGA);
			empresaCorreo.add(ConstantsMail.EMP_FUNCIONARIO);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String formatFecha(Date fecha) {
		return ValidacionesUtil.formatearFechaHora(fecha);
	}

	public void cargarCiudadesQuindio() {
		try {
			listaCiudades = utilidadesBean.cargarCiudades(24l); // utilidadesBean.cargarCiudadesDepartamento(24L);
		} catch (Exception e) {
			LOG.info(e);
		}
	}

	public Ciudad obtenerCiudad(String city) {
		try {
			Long idCiudad = null;
			for (Entry<Long, String> c : listaCiudades.entrySet()) {
				if (c.getValue().equals(city)) {
					idCiudad = c.getKey();
				}
			}
			return utilidadesBean.buscarCiudad(idCiudad);

		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}
	
	public StreamedContent reporteActoresExternos(Boolean tipoDownload) throws Exception {
		try {
			List<Actor> listaExternos = new ArrayList<Actor>();
			LOG.info("********VOY A REPORTESBEAN... ID: "+tipoCadena);
			listaExternos = reportesBean.reporteActoresExternos(tipoCadena);
			LOG.info("**********VUELVO DEL QUERY");
			if (listaExternos != null && !listaExternos.isEmpty()) {
				LOG.info("**********VOY A VER REPORTE");
				StreamedContent file = verReporteActoresExternos(listaExternos, tipoDownload);
				if (file != null) {
					return file;
				}
				reset();
			}
		} catch (Exception e) {
			LOG.info(e);
			ValidacionesUtil.addMessage(e.getMessage());
		}
		return null;
	}
	
	public void cargarTipoPersona() {
		try {
			listaTipoPersona = utilidadesBean.cargarTipoPersona();
		} catch (Exception e) {
			LOG.info(e);
		}
	}
	
	public StreamedContent verReporteActoresExternos(List<Actor> lista, Boolean tipoDownload) throws SQLException {
		JasperReport reporte;
//		LOG.info("******TAMAÑO LISTA"+lista.size());
//		List<ReporteExternosDTO> listaDTO = new ArrayList<ReporteExternosDTO>();
//		try {
//			for (Actor a : lista) {
//				LOG.info("*********ENTRO AL FOR");
//				ReporteExternosDTO dto = new ReporteExternosDTO();
//				if (a.getCedula() == null || StringUtils.isBlank(a.getCedula())) dto.setIdentificacion(""); else dto.setIdentificacion(a.getCedula().toString());
//				if (a.getNombreYApellido() == null || StringUtils.isBlank(a.getNombreYApellido())) dto.setNombre(""); else dto.setNombre(a.getNombreYApellido().toString());
//				if (a.getDireccion1() == null || StringUtils.isBlank(a.getDireccion1())) dto.setDireccion(""); else dto.setDireccion(a.getDireccion1().toString());
//				if (a.getTelefono() == null || StringUtils.isBlank(a.getTelefono())) dto.setTelefono(""); else dto.setTelefono(a.getTelefono().toString());
//				if (a.getEmail() == null || StringUtils.isBlank(a.getEmail())) dto.setCorreo(""); else dto.setCorreo(a.getEmail().toString());
//				if (a.getCiudad().getDescripcion() == null || StringUtils.isBlank(a.getCiudad().getDescripcion())) dto.setCiudad(""); else dto.setCiudad(a.getCiudad().getDescripcion());
//				if (a.getTipopersona().getDescripcion() == null || StringUtils.isBlank(a.getTipopersona().getDescripcion())) dto.setTipoPersona(""); else dto.setTipoPersona(a.getTipopersona().getDescripcion());
//				listaDTO.add(dto);
//				LOG.info("Lista: "+dto.getIdentificacion()+dto.getNombre()+dto.getDireccion()+dto.getTelefono()+dto.getCorreo()+dto.getCiudad()+dto.getTelefono());
//			}
//			
//		} catch (Exception e) {
//			LOG.error("***********ERROR " + e);
//		}

		try {
			LOG.info("*********ENTRO AL TRY DEL JASPER");
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css"
					+ File.separator + "img" + File.separator + "logoEdeq.png";
			BufferedImage image = ImageIO.read(new File(name));
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", image);
			String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes"
					+ File.separator + "reporteExternos.jrxml";
			reporte = JasperCompileManager.compileReport(report);
			byte[] archivo = null;
			String type = "";
			String fileName = "";

			LOG.info("**********SALGO DEL ENCABEZADO DEL JASPER");
			if (tipoDownload) {
				try {
					JasperPrint reporte_view = JasperFillManager.fillReport(reporte, parameters,
							new JRBeanCollectionDataSource(lista));
					JRXlsExporter exporter = new JRXlsExporter();
					ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, reporte_view);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
					exporter.exportReport();
					archivo = xlsReport.toByteArray();
					type = "application/vnd.ms-excel";
					fileName = "EXTERNOS-" + ValidacionesUtil.formatearFecha(new Date()) + ".xls";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			} else {
				try {
					archivo = JasperRunManager.runReportToPdf(reporte, parameters,
							new JRBeanCollectionDataSource(listaDTO));
					type = "application/pdf";
					fileName = "EXTERNOS-" + ValidacionesUtil.formatearFecha(new Date()) + ".pdf";
				} catch (JRException ex) {
					LOG.info(ex);
				}
			}
			ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
			fileGuardar = new DefaultStreamedContent(stream, type, fileName);
			if (fileGuardar != null) {
				reporteEmpresa = null;
				return fileGuardar;
			}
		} catch (IOException e) {
			LOG.info(e);
		} catch (JRException ex) {
			LOG.info(ex);
		}

		return null;
	}

//-------------------getter--------------------------
	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Oficina getRecorrido() {
		return recorrido;
	}

	public void setRecorrido(Oficina recorrido) {
		this.recorrido = recorrido;
	}

	public String getRecorridoCadena() {
		return recorridoCadena;
	}

	public void setRecorridoCadena(String recorridoCadena) {
		this.recorridoCadena = recorridoCadena;
	}

	public List<Trd> getListaDocumentos() {
		return listaDocumentos;
	}

	public void setListaDocumentos(List<Trd> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}

	public Map<Long, String> getMapRecorrido() {
		return mapRecorrido;
	}

	public void setMapRecorrido(Map<Long, String> mapRecorrido) {
		this.mapRecorrido = mapRecorrido;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public List<AuditoriaDocumento> getListaDocsEliminados() {
		return listaDocsEliminados;
	}

	public void setListaDocsEliminados(List<AuditoriaDocumento> listaDocsEliminados) {
		this.listaDocsEliminados = listaDocsEliminados;
	}

	public Map<Long, String> getListaResponsables() {
		return listaResponsables;
	}

	public void setListaResponsables(Map<Long, String> listaResponsables) {
		this.listaResponsables = listaResponsables;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFechaIniEliminado() {
		return fechaIniEliminado;
	}

	public void setFechaIniEliminado(Date fechaIniEliminado) {
		this.fechaIniEliminado = fechaIniEliminado;
	}

	public Date getFechaFinEliminado() {
		return fechaFinEliminado;
	}

	public void setFechaFinEliminado(Date fechaFinEliminado) {
		this.fechaFinEliminado = fechaFinEliminado;
	}

	public Date getFechaIniAc() {
		return fechaIniAc;
	}

	public void setFechaIniAc(Date fechaIniAc) {
		this.fechaIniAc = fechaIniAc;
	}

	public Date getFechaFinAc() {
		return fechaFinAc;
	}

	public void setFechaFinAc(Date fechaFinAc) {
		this.fechaFinAc = fechaFinAc;
	}

	public List<AuditoriaDocumento> getListaDocsAc() {
		return listaDocsAc;
	}

	public void setListaDocsAc(List<AuditoriaDocumento> listaDocsAc) {
		this.listaDocsAc = listaDocsAc;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public StreamedContent getFileGuardar() {
		return fileGuardar;
	}

	public void setFileGuardar(StreamedContent fileGuardar) {
		this.fileGuardar = fileGuardar;
	}

	public Boolean getIsDownload() {
		return isDownload;
	}

	public void setIsDownload(Boolean isDownload) {
		this.isDownload = isDownload;
	}

	public List<String> getEmpresaCorreo() {
		return empresaCorreo;
	}

	public void setEmpresaCorreo(List<String> empresaCorreo) {
		this.empresaCorreo = empresaCorreo;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Date getFechaIniEmpresa() {
		return fechaIniEmpresa;
	}

	public void setFechaIniEmpresa(Date fechaIniEmpresa) {
		this.fechaIniEmpresa = fechaIniEmpresa;
	}

	public Date getFechaFinEmpresa() {
		return fechaFinEmpresa;
	}

	public void setFechaFinEmpresa(Date fechaFinEmpresa) {
		this.fechaFinEmpresa = fechaFinEmpresa;
	}

	public List<String> getListaTipos() {
		return listaTipos;
	}

	public void setListaTipos(List<String> listaTipos) {
		this.listaTipos = listaTipos;
	}

	public String getContratacionTipo() {
		return contratacionTipo;
	}

	public void setContratacionTipo(String contratacionTipo) {
		this.contratacionTipo = contratacionTipo;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getRadicacion() {
		return radicacion;
	}

	public void setRadicacion(String radicacion) {
		this.radicacion = radicacion;
	}

	public Date getFechaIniContra() {
		return fechaIniContra;
	}

	public void setFechaIniContra(Date fechaIniContra) {
		this.fechaIniContra = fechaIniContra;
	}

	public Date getFechaFinContra() {
		return fechaFinContra;
	}

	public void setFechaFinContra(Date fechaFinContra) {
		this.fechaFinContra = fechaFinContra;
	}

	public String getRadicacionEliminado() {
		return radicacionEliminado;
	}

	public void setRadicacionEliminado(String radicacionEliminado) {
		this.radicacionEliminado = radicacionEliminado;
	}

	public String getCodigoNiu() {
		return codigoNiu;
	}

	public void setCodigoNiu(String codigoNiu) {
		this.codigoNiu = codigoNiu;
	}

	public String getTipoNiu() {
		return tipoNiu;
	}

	public void setTipoNiu(String tipoNiu) {
		this.tipoNiu = tipoNiu;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Date getFechaIniRad() {
		return fechaIniRad;
	}

	public void setFechaIniRad(Date fechaIniRad) {
		this.fechaIniRad = fechaIniRad;
	}

	public Date getFechaFinRad() {
		return fechaFinRad;
	}

	public void setFechaFinRad(Date fechaFinRad) {
		this.fechaFinRad = fechaFinRad;
	}

	public String getLetraRad() {
		return letraRad;
	}

	public void setLetraRad(String letraRad) {
		this.letraRad = letraRad;
	}

	public List<RadicadosDTO> getListaRadicados() {
		return listaRadicados;
	}

	public void setListaRadicados(List<RadicadosDTO> listaRadicados) {
		this.listaRadicados = listaRadicados;
	}

	public List<RecorridoDTO> getListaCruzados() {
		return listaCruzados;
	}

	public void setListaCruzados(List<RecorridoDTO> listaCruzados) {
		this.listaCruzados = listaCruzados;
	}

	public Date getFechaCruzadoIni() {
		return fechaCruzadoIni;
	}

	public void setFechaCruzadoIni(Date fechaCruzadoIni) {
		this.fechaCruzadoIni = fechaCruzadoIni;
	}

	public Date getFechaCruzadoFin() {
		return fechaCruzadoFin;
	}

	public void setFechaCruzadoFin(Date fechaCruzadoFin) {
		this.fechaCruzadoFin = fechaCruzadoFin;
	}

	public String getFileCruzado() {
		return fileCruzado;
	}

	public void setFileCruzado(String fileCruzado) {
		this.fileCruzado = fileCruzado;
	}

	public StreamedContent getFileGuardarCruzado() {
		return fileGuardarCruzado;
	}

	public void setFileGuardarCruzado(StreamedContent fileGuardarCruzado) {
		this.fileGuardarCruzado = fileGuardarCruzado;
	}

	public List<RadicadoresDTO> getListaRadicadores() {
		return listaRadicadores;
	}

	public void setListaRadicadores(List<RadicadoresDTO> listaRadicadores) {
		this.listaRadicadores = listaRadicadores;
	}

	public Date getFechaIniRadicadores() {
		return fechaIniRadicadores;
	}

	public void setFechaIniRadicadores(Date fechaIniRadicadores) {
		this.fechaIniRadicadores = fechaIniRadicadores;
	}

	public Date getFechaFinRadicadores() {
		return fechaFinRadicadores;
	}

	public void setFechaFinRadicadores(Date fechaFinRadicadores) {
		this.fechaFinRadicadores = fechaFinRadicadores;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public List<Trd> getListaDocumentoVencidos() {
		return listaDocumentoVencidos;
	}

	public void setListaDocumentoVencidos(List<Trd> listaDocumentoVencidos) {
		this.listaDocumentoVencidos = listaDocumentoVencidos;
	}

	public Map<Long, String> getListaCiudades() {
		return listaCiudades;
	}

	public void setListaCiudades(Map<Long, String> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public List<TipoPersona> getListaTipoPersona() {
		return listaTipoPersona;
	}

	public void setListaTipoPersona(List<TipoPersona> listaTipoPersona) {
		this.listaTipoPersona = listaTipoPersona;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Long getTipoCadena() {
		return tipoCadena;
	}

	public void setTipoCadena(Long tipoCadena) {
		this.tipoCadena = tipoCadena;
	}

	public List<Actor> getListaExternos() {
		return listaExternos;
	}

	public void setListaExternos(List<Actor> listaExternos) {
		this.listaExternos = listaExternos;
	}

}
