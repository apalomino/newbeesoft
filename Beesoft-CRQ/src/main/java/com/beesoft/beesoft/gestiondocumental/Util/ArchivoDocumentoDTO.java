/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author ISSLUNO
 */
public class ArchivoDocumentoDTO {
    
    private String id;
    private String nombreArchivo;
    private byte[] archivo;

    public ArchivoDocumentoDTO() {
    }

    public ArchivoDocumentoDTO(String id, String nombreArchivo, byte[] archivo) {
        this.id = id;
        this.nombreArchivo = nombreArchivo;
        this.archivo = archivo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }
    
}
