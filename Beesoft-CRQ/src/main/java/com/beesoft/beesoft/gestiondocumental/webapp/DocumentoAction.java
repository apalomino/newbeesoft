package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ClaseDocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ClaseLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentosRespuestaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.GeneradorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.GrupoRevisorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.ClasesDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentosRespuesta;
import com.beesoft.beesoft.gestiondocumental.entidades.GrupoRevisor;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Serie;
import com.beesoft.beesoft.gestiondocumental.entidades.SubClase;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerie;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoCorreo;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.PrimeFacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.UploadedFile;

/*
 * Fernandez V. cfernandez@sumset.com
 */
@Named(value = "documentoAction")
@SessionScoped
public class DocumentoAction implements Serializable {

	private static final Log LOG = LogFactory.getLog(DocumentoAction.class);

	@EJB
	private DocumentoLocal documentoBean;

	@EJB
	private ActorLocal actorBean;

	@EJB
	private UtilidadesLocal utilidadesBean;

	@EJB
	private OficinaLocal oficinaBean;

	@EJB
	private GeneradorLocal generadorBean;

	@EJB
	private GrupoRevisorLocal grupoBean;
	
	@EJB
	private DocumentosRespuestaLocal documentoRespuesta;

	@Inject
	private ApplicationAction application;

    @EJB
    private ClaseLocal claseBean;

    @EJB
    private ClaseDocumentoLocal claseDocumentoBean;
    
    @EJB
	private DocumentosRespuestaLocal docResBean;

	private String sticker;
	private String radicacion;
	private String contractTipo;
	private String contractNum;
	private String radicacionEtiqueta;
	private String pqr;
	private Boolean activo;
	private String anexos;
	private Boolean aprobado = null;
	private String asunto;
	private String radicadoPor;
	private String comentario;
	private Date fechaControl;
	private Date fechaEmision;
	private Date fechaRecepcion;
	private Date fechaSistema;
	private String guia;
	private String mensajero;
	private TipoCorreo tipoCorreo;
	private String notificacionEntrega;
	private String empresaCorreoRecibido;
	private Boolean pendiente = null;
	private String pruebaEntrega;
	private Boolean revisado = null;
	private String observacion;
	private String ubicacionFisica;
	private String claseDocumento;
	private String popupClaseDocumento;
    private List<ClasesDocumento> listaClasesDocumento;
    private Map<String, String> mapClasesDocumentos;
	private Long claseId;
	private List<Actor> procedenciaGuardar;
	private List<Actor> destinosGuardar;
	private List<String> procedencia;
	private List<String> destinos;
	private Documento documentoReferencia;
	private List<Documento> listaRef;
	private Documento documento;
	private String empresacorreo;
	private List<Actor> revisores;
	private Actor responsable;
	private Actor radicador;

	private String nombre;
	private String nombre1;
	private String codigoNiuProcedencia;
	private String codigoNiuDestino;
	private List<Actor> destinoIn;
	private List<Actor> procedenciaIn;
	private List<Actor> revisoresIn;
	private List<Actor> revisoresList;
	private List<Actor> revisoresTarget;
	private List<Actor> responsableIn;

	private Map<Long, String> mapaClaseDocumento = new HashMap<>();
	private List<Clase> clasesDocumento;

	private Date fechaEmisionInicial;
	private Date fechaEmisionFinal;
	private Actor actorProcedencia;
	private Actor actorDestino;
	private Actor actorRevisor;
	private Long responsableValue;
	private String destinoName;
	private String procedenciaName;
	private String cedula;
	private String cedulaDestino;

	private Long oficinaValue;
	private Map<Long, String> oficinas;
	private Oficina oficina;
	private String serieValue;
	private Map<Long, String> series;
	private Serie serie;
	private String subserieValue;
	private Map<Long, String> subseries;
	private SubSerie subserie;
	private String tipoDocValue;
	private Map<Long, String> tiposDoc;
	private TipoDocumento tipoDoc;
	private Map<Long, String> listaResponsables;
	private Map<Long, String> revisorMap;

	private List<Documento> resultadoDocs;
	private StringBuilder procedenciaText;
	private StringBuilder destinosText;

	private String radicacionConsulta;
	private Actor actorProcedenciaConsulta;
	private Actor actorDestinoConsulta;
	private Actor actorRevisorConsulta;
	private String asuntoConsulta;

	private List<Trd> trds = new ArrayList<>();

	private Actor revisorComentario;
	private String revisorName;

	private String notificacion;
	private String empresaCorreoNombre;
	private List<String> empresaCorreo;

	private String outProcedencia;
	private String outDestino;
	private Actor outRevisor;
	private Trd outTrd;
	private String radicadoReferencia;
	private List<ArchivoDocumento> archivos = new ArrayList<>();
	private int goCreate = 7;
	private Boolean addPersonal = false;
	private String fechaEtiqueta;
	private Boolean showInfoCorreo;
	private String directory = "";
	private String fileName = "";
	private Boolean factura;
	private Boolean medioElectronico;
	private String radicar;
	private List<String> listaAsunto;
	private String direccionEditar;
	private String nombreYApellidoEditar;
	private Long ciudadEditar;
	private Map<Long, String> ciudades;
	private String letra;
	private String letraPqr;
	private Usuario usuarioLogin;
	private Boolean panelEditar;
	private Actor actorEditar;
	private TabChangeEvent event;
	private int activeIndex;
	private boolean ocultarMensajero = true;
    private boolean ocultarCorreoCertificado = true;
    private String tipoCertificado;
	// ----------radicado niu desviacion----
	private Boolean desviacion;
	private String codigoNiu;
	private String nroActa;
	private Date fechaActa;
	private String volumen;
	private String entrega;
	private String rutaDesviacion;
	private UploadedFile archivoDesviacion;
	private String urlServer = "";
	// -------grupos--------------------------
	private List<GrupoRevisor> grupos;
	private Long grupoSeleccionado;
	private SubClase subClase;
	private Long idsubClase;
	private List<SubClase> subClases;
	// -----trds-------------------
	private List<Oficina> listaOficina;
	private List<Serie> listaSerie;
	private List<SubSerie> listaSubSerie;
	private List<TipoDocumento> listaTipoDocumento;
	private List<Oficina> listaOficinaFiltered;
	private List<Serie> listaSerieFiltered;
	private List<SubSerie> listaSubSerieFiltered;
	private List<TipoDocumento> listaTipoDocumentoFiltered;
	private Map<Long, String> listaCiudades;
	private String municipio;

	public DocumentoAction() {

    }

    public void cargarClasesDocumento() {
        try {
            if (popupClaseDocumento != null && !popupClaseDocumento.equals("")) {
                boolean existe = claseDocumentoBean.crearClaseDocumento(popupClaseDocumento, "activo");
                if (existe) {
                    ValidacionesUtil.addMessageInfo("La clase de documento " + popupClaseDocumento + " ya existe.");
                } else {
                    ValidacionesUtil.addMessageInfo("La clase de documento " + popupClaseDocumento + " fue creada con éxito.");
                }
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("ntf.show(),setTimeout('ntf.hide()',5000)");
            }

            listaClasesDocumento = claseDocumentoBean.listarClasesDocumento("A");

            mapClasesDocumentos = new TreeMap<>();
            for (ClasesDocumento clase : listaClasesDocumento) {
                mapClasesDocumentos.put(clase.getNombre(), clase.getNombre());
            }
            popupClaseDocumento = "";
        } catch (Exception e) {
            LOG.info(e);
        }
    }

	public void listarGrupos() {
		try {
			grupos = grupoBean.listarGrupos();
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarCiudades() {
		try {
			ciudades = utilidadesBean.cargarCiudades(24l);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarClases() {

		try {
			clasesDocumento = claseBean.listarClases();
		} catch (Exception e) {
			LOG.info(e);
		}
	}

	public List<Actor> completeRevisores(String query) {
		List<Actor> filtrados = new ArrayList<>();

		for (Actor actor : revisoresList) {
			if (StringUtils.containsIgnoreCase(actor.getNombreYApellido(), query)) {
				filtrados.add(actor);
			}
		}
		return filtrados;
	}

	public void obtenerRevisoresFinal() {
		if (revisoresTarget == null) {
			revisoresTarget = new ArrayList<>();
		}
		if (grupoSeleccionado != null && grupoSeleccionado > 0) {
			try {
				List<Actor> lista = grupoBean.consultarRevisoresGrupo(grupoSeleccionado);
				revisoresTarget.addAll(lista);
			} catch (Exception ex) {
				Logger.getLogger(DocumentoAction.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public void crearDocumento() {
		try {
			Boolean validate = validaciones();

			if (validate) {
				// obtenerResponsable();
				Ciudad municipioQuindio = obtenerCiudad(municipio);
				Long idActor = usuarioLogin.getActor().getId();
				radicador = actorBean.buscarActor(idActor);
				obtenerRevisoresFinal();
				if (!trds.isEmpty()) {

					Object[] info = ValidacionesUtil.mostrarJefe(trds.get(0).getOficina());
					responsable = (Actor) info[0];
					if (trds.size() > 1) {
						for (Trd trd : trds) {
							Object[] info1 = ValidacionesUtil.mostrarJefe(trd.getOficina());
							Actor revisor = (Actor) info1[0];
							if (!revisor.getId().equals(responsable.getId())) {
								revisoresTarget.add(revisor);
							}
						}
					}
				}
				
				documentoBean.crearDocumento(radicacion, pqr, contractTipo, contractNum, radicador, anexos, asunto,
						observacion, fechaControl, fechaEmision, fechaRecepcion, guia, mensajero, notificacionEntrega,
						empresaCorreoRecibido, pruebaEntrega, ubicacionFisica, claseDocumento, subClase,
						procedenciaGuardar, destinosGuardar, listaRef, empresaCorreoNombre, revisoresTarget, null,
						responsable, trds, archivos, municipioQuindio);

				//Acá se guarda en la tabla DocumentosRespuesta
				LOG.info("**********VOY A GRABAR DOCUMENTOSRESPUESTA EN DOCUMENTO ACTION");
				List<Documento> lista = new ArrayList<Documento>();
				lista = documentoBean.buscarDocumento(radicacion, null, null, null, null,"", "", "", null, null, null, null, "");
				if (lista != null ) {
					for (Documento doc : listaRef) {
						documentoRespuesta.crearDocumentoRespuesta(lista.get(0).getId(), doc.getId(), lista.get(0).getRadicacion());
					}
				}
				ValidacionesUtil.addMessageInfo("El documento con radicado " + radicacion + " fue creado.");
				limpiar();
			}

		} catch (Exception e) {
			LOG.info(e);
			ValidacionesUtil.addMessage(e.getMessage());
		}
	}

	public Ciudad obtenerCiudad(String city) {
		try {
			Long idCiudad = null;
			for (Entry<Long, String> c : listaCiudades.entrySet()) {
				if (c.getValue().equals(city)) {
					idCiudad = c.getKey();
				}
			}
			return utilidadesBean.buscarCiudad(idCiudad);

		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public Boolean validaciones() {
		int validacion = 0;
		goCreate = 9;
		Boolean valRad = ValidacionesUtil.validarCadenaRadicado(letra);
		if (valRad) {
			radicacion = letra;
		}
		validacion += ValidacionesUtil.validarCadena(radicacion, "Radicacion");
		validacion += ValidacionesUtil.validarFechasIniFinObligatorio(fechaEmision, fechaRecepcion, "Fecha de emisión",
				"Fecha de recepción");
		validacion += ValidacionesUtil.validarListas(procedenciaGuardar, "procedencias");
		validacion += ValidacionesUtil.validarListas(destinosGuardar, "destinos");
		validacion += ValidacionesUtil.validarCadena(asunto, "asunto");
		validacion += ValidacionesUtil.validarListas(archivos, "archivos");
		validacion += ValidacionesUtil.validarListas(trds, "Tablas de retencion");
		validacion += ValidacionesUtil.validarCadena(claseDocumento, "Clase documento");
		validacion += ValidacionesUtil.validarMedioRespuesta(empresaCorreoNombre, letra, "Medio de Respuesta");
		if (claseDocumento.toUpperCase().equals("DENUNCIAS")) {
			validacion += ValidacionesUtil.validarCadena(municipio, "Municipio");
		}
		

		return validacion >= goCreate;
	}

	public void completeConsecutivo() {
		try {
			if (letra.equals("FA")) {
				radicacion = "FA";
			} else if (letra.equals("V")) {
				radicacion = "V";
			} else if (letra.equals("E") || letra.equals("R") || letra.equals("CJ") || letra.equals("DP")
					|| letra.equals("I")) {
				String rad = generadorBean.consecutivoRadicacionCorreoXLetra(letra);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy");

				if (letra.equals("E") || letra.equals("R")) {
					radicacion = letra + rad + "-" + sdf.format(new Date()).substring(2);
				}
				/**
				 * if (letra.equals("E")) { radicacion = rad + "-18"; } else if
				 * (letra.equals("R")) { radicacion = letra + rad + "-18"; }
				 **/
			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}

	}

	public void completeConsecutivoPqr() {
		try {
			pqr = documentoBean.consecutivoRadicacionXLetra("P");
			documentoBean.generarRadicado(pqr);
			cambiarFechaControl();

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarDocumento() {
		try {
			obtenerRevisor();
			resultadoDocs = documentoBean.buscarDocumento(radicacionConsulta, fechaEmisionInicial, fechaEmisionFinal,
					actorProcedenciaConsulta, actorDestinoConsulta, asuntoConsulta, "", "", actorRevisorConsulta,
					aprobado, revisado, pendiente, "");

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String mostrarProcedenciaTabla(Documento doc) {
		List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(doc.getId());
		return ValidacionesUtil.listToCadena(procedencia);
	}

	public void handleFileUpload(FileUploadEvent event) throws Exception {
		archivos.add(fileToAdjunto(event.getFile()));
		archivoDesviacion = event.getFile();
	}

	public File copyFile(String fileAbsolutePath, InputStream in) {

		try {
			OutputStream out = new FileOutputStream(new File(fileAbsolutePath));
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			in.close();

		} catch (IOException e) {
			LOG.info(e);
		}
		return new File(fileAbsolutePath);

	}

	public ArchivoDocumento fileToAdjunto(UploadedFile file) {
		ArchivoDocumento aa = new ArchivoDocumento();
		aa.setNombreArchivo(FilenameUtils.getName(file.getFileName()));
		aa.setArchivo(file.getContents());
		return aa;
	}

	public void generarEtiqueta() {
		int validacion = 0;
		if (!letra.equals("FA")) {
			Boolean valRad = ValidacionesUtil.validarCadenaRadicado(letra);
			if (valRad) {
				radicacion = letra;
			}
		}

		validacion += ValidacionesUtil.validarCadena(radicacion, "Radicacion");
		validacion += ValidacionesUtil.validarListas(procedenciaGuardar, "procedencias");
		validacion += ValidacionesUtil.validarListas(destinosGuardar, "destinos");
		validacion += ValidacionesUtil.validarCadena(asunto, "asunto");

		radicadoPor = usuarioLogin.getActor().getNombreYApellido();
		if (validacion == 4) {
			fechaEtiqueta = ValidacionesUtil.formatearFechaHora(new Date());
		}
	}

	public void selectProcedencia(Actor actorProcedencia) {
		if (actorProcedencia != null) {
			if (procedencia != null && !procedencia.isEmpty()) {
				if (!procedencia.contains(actorProcedencia.getNombreYApellido())) {
					procedencia.add(actorProcedencia.getNombreYApellido());
					procedenciaGuardar.add(actorProcedencia);
				}
			} else {
				procedencia.add(actorProcedencia.getNombreYApellido());
				procedenciaGuardar.add(actorProcedencia);
			}
			iniciarDialog();
		}
	}

	public void selectDestino(Actor actorDestino) {
		if (actorDestino != null) {
			if (destinos != null && !destinos.isEmpty()) {
				if (!destinos.contains(actorDestino.getNombreYApellido())) {
					destinos.add(actorDestino.getNombreYApellido());
					destinosGuardar.add(actorDestino);
				}
			} else {
				destinos.add(actorDestino.getNombreYApellido());
				destinosGuardar.add(actorDestino);
			}

			iniciarDialog();
		}
	}

	public void asuntoObservacionFactura() {
		if (asunto.equals("ED-CXP-COC")) {
			observacion = "NUMERO DE FACTURA:\n" + "NUMERO DE ORDEN DE COMPRA:\n" + "CONCEPTO DEL PAGO:\n" + "NIT:\n"
					+ "PROVEEDOR:";
		} else if (asunto.equals("ED-CXP-SOC")) {
			observacion = "PAGO POR VENCIMIENTO O PAGO INMEDIATO:\n" + "FECHA DE VENCIMIENTO:";
		} else if (asunto.equals("ED-NOVPP")) {
			observacion = "EQEQ - RECEPCIÓN Y REGISTRO DE PRONTO PAGO";
		} else {
			observacion = "";
		}
	}

	public void selectProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		if (actorProcedenciaConsulta != null) {
			this.actorProcedenciaConsulta = actorProcedenciaConsulta;
			procedenciaName = actorProcedenciaConsulta.getNombreYApellido();
			iniciarDialogSearch();
		}
	}

	public void selectDestinoConsulta(Actor actorDestinoConsulta) {
		if (actorDestinoConsulta != null) {
			this.actorDestinoConsulta = actorDestinoConsulta;
			destinoName = actorDestinoConsulta.getNombreYApellido();
			iniciarDialogSearch();
		}
	}

	public void selectResponsable(Actor responsable) {
		if (responsable != null) {
			this.responsable = responsable;
			responsableValue = responsable.getId();
			iniciarDialog();
		}
	}

	public void selectDocumentoReferencia(Documento documentoReferencia) {
		if (documentoReferencia != null) {
			this.documentoReferencia = documentoReferencia;
			listaRef.add(documentoReferencia);
			limpiarModal();
		}
	}

	public String selectDocumento() {
		if (documento != null) {
			radicacion = documento.getRadicacion();
			fechaEmision = documento.getFechaEmision();
			fechaRecepcion = documento.getFechaRecepcion();
			asunto = documento.getAsunto();
			observacion = documento.getObservacion();
			guia = documento.getGuia();
			claseDocumento = documento.getClasedocumento();
			responsableValue = documento.getResponsable().getId();
			procedenciaGuardar = documento.getProcedencia();
			destinosGuardar = documento.getDestinos();
			procedenciaText = new StringBuilder(ValidacionesUtil.listToCadena(procedenciaGuardar));
			destinosText = new StringBuilder(ValidacionesUtil.listToCadena(destinosGuardar));
			cargarTrds(documento.getId());
		}
		return "success";
	}

	public void cargarTrds(Long id) {
		try {
			trds = oficinaBean.buscarTrds(id);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarProcedencia() {
		try {
			procedenciaIn = new ArrayList<>();
			procedenciaIn = actorBean.listaProcedencia(nombre, cedula, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void editar(Actor actor) {
		panelEditar = true;
		actorEditar = actor;
		activeIndex = 2;
	}

	public void buscarDestinos() {
		try {
			destinoIn = new ArrayList<>();
			destinoIn = actorBean.listaProcedencia(nombre1, cedulaDestino, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarRevisores() {
		try {
			revisoresList = application.getRevisoresIn();
			revisoresTarget = new ArrayList<>();
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarRevisoresSearch() {
		try {
			revisoresIn = actorBean.listaUsuarios();
			revisorMap = new HashMap<>();
			for (Actor r : revisoresIn) {
				revisorMap.put(r.getId(), r.getNombreYApellido());
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void obtenerRevisor() {
		try {
			if (StringUtils.isNotBlank(revisorName)) {
				Long id = ValidacionesUtil.obtenerIdFromMap(revisorMap, revisorName);
				actorRevisorConsulta = actorBean.buscarActor(id);
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void obtenerResponsable() {
		try {
			responsable = actorBean.buscarActor(responsableValue);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarResponsable() {
		try {
			responsableIn = actorBean.listaUsuarios();
			listaResponsables = new HashMap<Long, String>();
			for (Actor actor : responsableIn) {
				listaResponsables.put(actor.getId(), actor.getNombreYApellido());
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarOficinas() {
		try {
			oficinas = oficinaBean.cargarOficinas();
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarSeries() {
		try {
			series = new HashMap<>();
			oficina = oficinaBean.buscarOficina(oficinaValue);
			series = oficinaBean.cargarSeries(oficinaValue);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarSubClases() {
//        try {
//            subClases = claseBean.buscarSubClases(claseId);
//            claseDocumento = claseBean.buscarClasePorId(claseId);
//            int contador = 0;
//            Date date = new Date();
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(date);
//            int limite = claseDocumento.getTiempoDias();
//            do {
//                if (calendar.get(Calendar.DAY_OF_WEEK) != 7 && calendar.get(Calendar.DAY_OF_WEEK) != 1) {
//                    calendar.add(Calendar.DATE, 1);
//                    contador++;
//                } else {
//                    calendar.add(Calendar.DATE, 1);
//                }
//            } while (contador < limite);
//            fechaControl = calendar.getTime();
//        } catch (Exception ex) {
//            LOG.info(ex);
//        }
	}

	public void cargarSubSeries() {
		try {
			subseries = new HashMap<>();
			Long idserie = ValidacionesUtil.obtenerIdFromMap(series, serieValue);
			serie = oficinaBean.buscarSerie(idserie);
			subseries = oficinaBean.cargarSubSeries(idserie);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarTiposDoc() {
		try {
			tiposDoc = new HashMap<>();
			Long id = ValidacionesUtil.obtenerIdFromMap(subseries, subserieValue);
			subserie = oficinaBean.buscarSubserie(id);
			tiposDoc = oficinaBean.cargarTipoDoc(id);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarEmpresaCorreo() {
		try {
			empresaCorreo = new ArrayList<>();
			empresaCorreo.add(ConstantsMail.EMP_ROR);
			empresaCorreo.add(ConstantsMail.EMP_ROR_RECUPERACION);
			empresaCorreo.add(ConstantsMail.EMP_472);
			empresaCorreo.add(ConstantsMail.EMP_SERVIENTREGA);
			empresaCorreo.add(ConstantsMail.EMP_FUNCIONARIO);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String onFlowProcess(FlowEvent event) {
		switch (event.getNewStep()) {
		case "serieTab":
			if (oficina != null) {
				listaSerieFiltered = null;
				llenarTablaSerie(oficina.getId());
				return event.getNewStep();
			} else {
				ValidacionesUtil.addMessage("Seleccione una oficina");
				return "oficinaTab";
			}
		case "subTab":
			if (serie != null) {
				listaSubSerieFiltered = null;
				llenarTablaSubSerie(serie.getId());
				return event.getNewStep();
			} else {
				ValidacionesUtil.addMessage("Seleccione una serie");
				return "serieTab";
			}
		case "tipoDocTab":
			if (subserie != null) {
				listaTipoDocumentoFiltered = null;
				llenarTablaTipoDoc(subserie.getId());
				return event.getNewStep();
			} else {
				ValidacionesUtil.addMessage("Seleccione una subserie");
				return "subTab";
			}
		default:
			return event.getNewStep();
		}
	}

	public List<Oficina> llenarTablaOficina(Long id) {
		try {
			listaOficina = new ArrayList<>();
			listaOficina = oficinaBean.listarOficinas(id);
			return listaOficina;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public List<Serie> llenarTablaSerie(Long id) {
		try {
			listaSerie = new ArrayList<>();
			listaSerie = oficinaBean.listarSeries(id);
			return listaSerie;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public List<SubSerie> llenarTablaSubSerie(Long id) {
		try {
			listaSubSerie = new ArrayList<>();
			listaSubSerie = oficinaBean.listarSubSeries(id);
			return listaSubSerie;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public List<TipoDocumento> llenarTablaTipoDoc(Long id) {
		try {
			listaTipoDocumento = new ArrayList<>();
			listaTipoDocumento = oficinaBean.listarTipoDocumentos(id);
			return listaTipoDocumento;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public void crearTrd() {
		try {
//            oficina = oficinaBean.buscarOficina(oficinaValue);
			Trd trd = new Trd();
			trd.setOficina(oficina);
			trd.setSerie(serie);
			trd.setSubserie(subserie);
//            Long id = ValidacionesUtil.obtenerIdFromMap(tiposDoc, tipoDocValue);
//            tipoDoc = oficinaBean.buscarTipodocumento(id);
			trd.setTipodocumento(tipoDoc);
//                trd.setMedioElectronico(medioElectronico);
			trds.add(trd);
			RequestContext.getCurrentInstance().execute("PF('wzTRD').loadStep(PF('wzTRD').cfg.steps[0], true);");
			iniciarDialog();
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void quitarProcedencia() {

		Actor out = null;
		for (Actor actor : procedenciaGuardar) {
			if (actor.getNombreYApellido().equals(outProcedencia)) {
				procedencia.remove(outProcedencia);
				out = actor;
			}
		}
		procedenciaGuardar.remove(out);
	}

	public void quitarDestino() {
		Actor out = null;
		for (Actor actor : destinosGuardar) {
			if (actor.getNombreYApellido().equals(outDestino)) {
				destinos.remove(outDestino);
				out = actor;
			}
		}
		destinosGuardar.remove(out);

	}

	public void quitarDocumentoReferencia(Documento documento) {
		listaRef.remove(documento);
	}

	public void quitarRevisor() {
		revisores.remove(outRevisor);
	}

	public void quitarTrd(Trd trd) {
		trds.remove(trd);
	}

	public void cambiarFechaControl() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date()); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, 12); // numero de días a añadir, o restar en caso de días<0
		fechaControl = calendar.getTime();
	}

	public void iniciarDialog() {
		destinoIn = new ArrayList<Actor>();
		procedenciaIn = new ArrayList<Actor>();
		revisoresIn = new ArrayList<Actor>();
		responsableIn = new ArrayList<Actor>();
		resultadoDocs = new ArrayList<Documento>();
		radicacionConsulta = "";
		fechaEmisionFinal = null;
		fechaEmisionInicial = null;
		medioElectronico = false;
		nombre = "";
		nombre1 = "";
		codigoNiuProcedencia = "";
		codigoNiuDestino = "";
		subserie = null;
		serie = null;
		tipoDoc = null;
		oficina = null;
		oficinaValue = null;
		serieValue = "N.A";
		subserieValue = "N.A";
		tipoDocValue = "N.A";
		addPersonal = false;
		listaOficinaFiltered = listaOficina;
		listaSerieFiltered = null;
		listaSubSerieFiltered = null;
		municipio = "";
	}

	public void limpiarModal() {
		fechaEmisionInicial = null;
		fechaEmisionFinal = null;
		radicacionConsulta = "";
		asuntoConsulta = "";
		destinoIn = new ArrayList<Actor>();
		codigoNiuProcedencia = "";
		codigoNiuDestino = "";
		nombre1 = "";
		procedenciaName = "";
		destinoName = "";
		procedenciaIn = new ArrayList<Actor>();
		resultadoDocs = new ArrayList<Documento>();
		aprobado = null;
		revisado = null;
		pendiente = null;
	}

	public void iniciarDialogSearch() {
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
		revisoresIn = new ArrayList<>();
		nombre = "";
	}

	@PostConstruct
	public void inicializar() {
		listaRef = new ArrayList<>();
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
		revisoresIn = new ArrayList<>();
		revisores = new ArrayList<>();
		destinosGuardar = new ArrayList<>();
		procedenciaGuardar = new ArrayList<>();
		destinos = new ArrayList<>();
		procedencia = new ArrayList<>();
		clasesDocumento = new ArrayList<>();
		responsableIn = new ArrayList<>();
		codigoNiuProcedencia = "";
		codigoNiuDestino = "";
		buscarRevisoresSearch();
		cargarAsuntos();
		cargarCiudades();
		listarGrupos();
		cargarCiudadesQuindio();
		fechaEmision = new Date();
		fechaRecepcion = new Date();
		fechaControl = new Date();
		cambiarFechaControl();
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		usuarioLogin = (Usuario) session.getAttribute("username");
	}
	
	 public void onload() {
        cargarClasesDocumento();
    }

	public void limpiar() {
		medioElectronico = false;
		factura = false;
		radicacion = "";
		fechaEmision = new Date();
		fechaRecepcion = new Date();
		fechaControl = new Date();
		cambiarFechaControl();
		procedencia = new ArrayList<>();
		destinos = new ArrayList<>();
		destinosGuardar = new ArrayList<>();
		procedenciaGuardar = new ArrayList<>();
		grupoSeleccionado = null;
		asunto = "";
		observacion = "";
		pruebaEntrega = "";
		notificacionEntrega = "";
		guia = "";
		mensajero = "";
		documentoReferencia = null;
		listaRef = new ArrayList<>();
		trds = new ArrayList<>();
		claseDocumento = "";
		responsable = null;
		responsableValue = null;
		buscarRevisores();
		empresaCorreoNombre = "";
		empresacorreo = null;
		responsable = null;
		oficina = null;
		serie = null;
		subserie = null;
		tipoDoc = null;
		oficinaValue = null;
		serieValue = null;
		subserieValue = null;
		tipoDocValue = null;
		destinoName = "";
		procedenciaName = "";
		nombre = "";
		nombre1 = "";
		codigoNiuProcedencia = "";
		codigoNiuDestino = "";
		radicadoReferencia = "";
		archivos = new ArrayList<>();
		anexos = "";
		empresaCorreoRecibido = "";
		pqr = "";
		contractTipo = "";
		contractNum = "";
		letra = "";
		letraPqr = "";
		volumen = "";
		entrega = "";
		fechaActa = null;
		nroActa = "";
		codigoNiu = "";
		rutaDesviacion = "";
		desviacion = false;
		idsubClase = null;
		claseId = null;
		claseDocumento = null;
		subClase = null;
		municipio = "";
	}
	
	 public void validarRenderedCampos() {
        if (empresaCorreoNombre != null && empresaCorreoNombre.equals("MENSAJERO")) {
            ocultarMensajero = false;
            ocultarCorreoCertificado = true;
        } else if (empresaCorreoNombre != null && empresaCorreoNombre.equals("CERTIFICADO")) {
            ocultarMensajero = true;
            ocultarCorreoCertificado = false;
        } else {
            ocultarMensajero = true;
            ocultarCorreoCertificado = true;
        }
	 }

	public void cargarAsuntos() {
		listaAsunto = new ArrayList<String>();
		listaAsunto.add("ED-CPAGOS");
		listaAsunto.add("ED-CXP-COC");
		listaAsunto.add("ED-CXP-IMPORTACION");
		listaAsunto.add("ED-CXP-IMPRMA");
		listaAsunto.add("ED-CXP-RP01");
		listaAsunto.add("ED-CXP-SOC");
		listaAsunto.add("ED-DEP-CXP");
		listaAsunto.add("ED-NOVCDE");
		listaAsunto.add("ED-NOVEMB");
		listaAsunto.add("ED-NOVPP");
		listaAsunto.add("ED-NOVSPOCP");
		listaAsunto.add("ED-NOVVDF");
		listaAsunto.add("ED-NOVVDFYR");
		listaAsunto.add("ED-PPAGOS");
		listaAsunto.add("ED-SPNR-CXP");

	}

	public void cargarCiudadesQuindio() {
		try {
			listaCiudades = utilidadesBean.cargarCiudades(24l);
		} catch (Exception e) {
			LOG.info(e);
		}
	}

	public void onRowEdit(RowEditEvent event) {
		try {
			Actor actor = (Actor) event.getObject();
			Ciudad ciudad = null;
			if (ciudadEditar != null && ciudadEditar != 0) {
				ciudad = utilidadesBean.buscarCiudad(ciudadEditar);
			}
			actorBean.cambiarDireccionActor(actor.getId(), direccionEditar, nombreYApellidoEditar, ciudad);
			nombre = actor.getNombreYApellido();
			nombre1 = actor.getNombreYApellido();
			buscarProcedencia();
			buscarDestinos();
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
	}

	public String asignarDatosDireccion(Actor actor) {
		direccionEditar = actor.getDireccion1();
		return direccionEditar;
	}

	public String asignarDatosNombreApelldio(Actor actor) {
		if (StringUtils.isNotBlank(actor.getNombreYApellido())) {
			nombreYApellidoEditar = actor.getNombreYApellido();
		}
		return nombreYApellidoEditar;
	}

	public String asignarDatosCiudad(Actor actor) {
		if (actor.getCiudad() != null) {
			ciudadEditar = actor.getCiudad().getId();
			return actor.getCiudad().getDescripcion();
		}
		return "";
	}

	public String validarLista(List<Actor> lista) {
		return ValidacionesUtil.listToCadena(lista);
	}

	public String getRadicar() {
		buscarRevisores();
		llenarTablaOficina(1l);
		urlServer = ValidacionesUtil.urlServer();
		return radicar;
	}

	public void postEditarActor(Boolean edito) {
		activeIndex = 0;
		panelEditar = false;
		if (edito) {
			nombre = actorEditar.getNombreYApellido();
			nombre1 = actorEditar.getNombreYApellido();
			buscarDestinos();
			buscarProcedencia();
		}
	}

	/// -------------------Getters--------------------------------------------------
	public String getRadicacion() {
		return radicacion;
	}

	public void setRadicacion(String radicacion) {
		this.radicacion = radicacion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getAnexos() {
		return anexos;
	}

	public void setAnexos(String anexos) {
		this.anexos = anexos;
	}

	public Boolean getAprobado() {
		return aprobado;
	}

	public void setAprobado(Boolean aprobado) {
		this.aprobado = aprobado;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Date getFechaControl() {
		return fechaControl;
	}

	public void setFechaControl(Date fechaControl) {
		this.fechaControl = fechaControl;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public Date getFechaSistema() {
		return fechaSistema;
	}

	public void setFechaSistema(Date fechaSistema) {
		this.fechaSistema = fechaSistema;
	}

	public String getGuia() {
		return guia;
	}

	public void setGuia(String guia) {
		this.guia = guia;
	}

	public TipoCorreo getTipoCorreo() {
		return tipoCorreo;
	}

	public void setTipoCorreo(TipoCorreo tipoCorreo) {
		this.tipoCorreo = tipoCorreo;
	}

	public String getNotificacionEntrega() {
		return notificacionEntrega;
	}

	public void setNotificacionEntrega(String notificacionEntrega) {
		this.notificacionEntrega = notificacionEntrega;
	}

	public Boolean getPendiente() {
		return pendiente;
	}

	public void setPendiente(Boolean pendiente) {
		this.pendiente = pendiente;
	}

	public String getPruebaEntrega() {
		return pruebaEntrega;
	}

	public void setPruebaEntrega(String pruebaEntrega) {
		this.pruebaEntrega = pruebaEntrega;
	}

	public String getUbicacionFisica() {
		return ubicacionFisica;
	}

	public void setUbicacionFisica(String ubicacionFisica) {
		this.ubicacionFisica = ubicacionFisica;
	}

	public String getClaseDocumento() {
		return claseDocumento;
	}

	public void setClaseDocumento(String claseDocumento) {
		this.claseDocumento = claseDocumento;
	}

	public List<Actor> getProcedenciaGuardar() {
		return procedenciaGuardar;
	}

	public void setProcedenciaGuardar(List<Actor> procedencia) {
		this.procedenciaGuardar = procedencia;
	}

	public Documento getDocumentoReferencia() {
		return documentoReferencia;
	}

	public void setDocumentoReferencia(Documento documentoReferencia) {
		this.documentoReferencia = documentoReferencia;
	}

	public String getEmpresacorreo() {
		return empresacorreo;
	}

	public void setEmpresacorreo(String empresacorreo) {
		this.empresacorreo = empresacorreo;
	}

	public List<Actor> getRevisores() {
		return revisores;
	}

	public void setRevisores(List<Actor> revisores) {
		this.revisores = revisores;
	}

	public Actor getResponsable() {
		return responsable;
	}

	public void setResponsable(Actor responsable) {
		this.responsable = responsable;
	}

	public Actor getRadicador() {
		return radicador;
	}

	public void setRadicador(Actor radicador) {
		this.radicador = radicador;
	}

	public Map<Long, String> getMapaClaseDocumento() {
		return mapaClaseDocumento;
	}

	public void setMapaClaseDocumento(Map<Long, String> mapaClaseDocumento) {
		this.mapaClaseDocumento = mapaClaseDocumento;
	}

	public List<Clase> getClasesDocumento() {
		return clasesDocumento;
	}

	public void setClasesDocumento(List<Clase> clasesDocumento) {
		this.clasesDocumento = clasesDocumento;
	}

	public List<Actor> getDestinoIn() {
		return destinoIn;
	}

	public void setDestinoIn(List<Actor> destinoIn) {
		this.destinoIn = destinoIn;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaEmisionInicial() {
		return fechaEmisionInicial;
	}

	public void setFechaEmisionInicial(Date fechaEmisionInicial) {
		this.fechaEmisionInicial = fechaEmisionInicial;
	}

	public Date getFechaEmisionFinal() {
		return fechaEmisionFinal;
	}

	public void setFechaEmisionFinal(Date fechaEmisionFinal) {
		this.fechaEmisionFinal = fechaEmisionFinal;
	}

	public Actor getActorProcedencia() {
		return actorProcedencia;
	}

	public void setActorProcedencia(Actor actorProcedencia) {
		this.actorProcedencia = actorProcedencia;
	}

	public Actor getActorDestino() {
		return actorDestino;
	}

	public void setActorDestino(Actor actorDestino) {
		this.actorDestino = actorDestino;
	}

	public List<Actor> getProcedenciaIn() {
		return procedenciaIn;
	}

	public void setProcedenciaIn(List<Actor> procedenciaIn) {
		this.procedenciaIn = procedenciaIn;
	}

	public List<Actor> getDestinosGuardar() {
		return destinosGuardar;
	}

	public void setDestinosGuardar(List<Actor> destinos) {
		this.destinosGuardar = destinos;
	}

	public List<Actor> getRevisoresIn() {
		return revisoresIn;
	}

	public void setRevisoresIn(List<Actor> revisoresIn) {
		this.revisoresIn = revisoresIn;
	}

	public List<Actor> getResponsableIn() {
		return responsableIn;
	}

	public void setResponsableIn(List<Actor> responsableIn) {
		this.responsableIn = responsableIn;
	}

	public Actor getActorRevisor() {
		return actorRevisor;
	}

	public void setActorRevisor(Actor actorRevisor) {
		this.actorRevisor = actorRevisor;
	}

	public Long getResponsableValue() {
		return responsableValue;
	}

	public void setResponsableValue(Long responsableValue) {
		this.responsableValue = responsableValue;
	}

	public Long getOficinaValue() {
		return oficinaValue;
	}

	public void setOficinaValue(Long oficina) {
		this.oficinaValue = oficina;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public String getSerieValue() {
		return serieValue;
	}

	public void setSerieValue(String serie) {
		this.serieValue = serie;
	}

	public Map<Long, String> getSeries() {
		return series;
	}

	public void setSeries(Map<Long, String> series) {
		this.series = series;
	}

	public String getSubserieValue() {
		return subserieValue;
	}

	public void setSubserieValue(String subserieValue) {
		this.subserieValue = subserieValue;
	}

	public Map<Long, String> getSubseries() {
		return subseries;
	}

	public void setSubseries(Map<Long, String> subseries) {
		this.subseries = subseries;
	}

	public String getTipoDocValue() {
		return tipoDocValue;
	}

	public void setTipoDocValue(String tipoDoc) {
		this.tipoDocValue = tipoDoc;
	}

	public Map<Long, String> getTiposDoc() {
		return tiposDoc;
	}

	public void setTiposDoc(Map<Long, String> tiposDoc) {
		this.tiposDoc = tiposDoc;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public SubSerie getSubserie() {
		return subserie;
	}

	public void setSubserie(SubSerie subserie) {
		this.subserie = subserie;
	}

	public TipoDocumento getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(TipoDocumento tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public List<Trd> getTrds() {
		return trds;
	}

	public void setTrds(List<Trd> trds) {
		this.trds = trds;
	}

	public String getDestinoName() {
		return destinoName;
	}

	public void setDestinoName(String destinoName) {
		this.destinoName = destinoName;
	}

	public String getProcedenciaName() {
		return procedenciaName;
	}

	public void setProcedenciaName(String procedenciaName) {
		this.procedenciaName = procedenciaName;
	}

	public List<Documento> getResultadoDocs() {
		return resultadoDocs;
	}

	public void setResultadoDocs(List<Documento> resultadoDocs) {
		this.resultadoDocs = resultadoDocs;
	}

	public Boolean getRevisado() {
		return revisado;
	}

	public void setRevisado(Boolean revisado) {
		this.revisado = revisado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public StringBuilder getProcedenciaText() {
		return procedenciaText;
	}

	public void setProcedenciaText(StringBuilder procedenciaText) {
		this.procedenciaText = procedenciaText;
	}

	public StringBuilder getDestinosText() {
		return destinosText;
	}

	public void setDestinosText(StringBuilder destinosText) {
		this.destinosText = destinosText;
	}

	public String getOutProcedencia() {
		return outProcedencia;
	}

	public void setOutProcedencia(String outProcedencia) {
		this.outProcedencia = outProcedencia;
	}

	public String getOutDestino() {
		return outDestino;
	}

	public void setOutDestino(String outDestino) {
		this.outDestino = outDestino;
	}

	public Actor getOutRevisor() {
		return outRevisor;
	}

	public void setOutRevisor(Actor outRevisor) {
		this.outRevisor = outRevisor;
	}

	public Actor getRevisorComentario() {
		return revisorComentario;
	}

	public void setRevisorComentario(Actor revisorComentario) {
		this.revisorComentario = revisorComentario;
	}

	public Actor getActorProcedenciaConsulta() {
		return actorProcedenciaConsulta;
	}

	public void setActorProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		this.actorProcedenciaConsulta = actorProcedenciaConsulta;
	}

	public Actor getActorDestinoConsulta() {
		return actorDestinoConsulta;
	}

	public void setActorDestinoConsulta(Actor actorDestinoConsulta) {
		this.actorDestinoConsulta = actorDestinoConsulta;
	}

	public String getRadicacionConsulta() {
		return radicacionConsulta;
	}

	public void setRadicacionConsulta(String radicacionConsulta) {
		this.radicacionConsulta = radicacionConsulta;
	}

	public String getAsuntoConsulta() {
		return asuntoConsulta;
	}

	public void setAsuntoConsulta(String asuntoConsulta) {
		this.asuntoConsulta = asuntoConsulta;
	}

	public Trd getOutTrd() {
		return outTrd;
	}

	public void setOutTrd(Trd outTrd) {
		this.outTrd = outTrd;
	}

	public Map<Long, String> getListaResponsables() {
		return listaResponsables;
	}

	public void setListaResponsables(Map<Long, String> listaResponsables) {
		this.listaResponsables = listaResponsables;
	}

	public Map<Long, String> getRevisorMap() {
		return revisorMap;
	}

	public void setRevisorMap(Map<Long, String> revisorMap) {
		this.revisorMap = revisorMap;
	}

	public String getRevisorName() {
		return revisorName;
	}

	public void setRevisorName(String revisorName) {
		this.revisorName = revisorName;
	}

	public String getRadicadoReferencia() {
		return radicadoReferencia;
	}

	public void setRadicadoReferencia(String radicadoReferencia) {
		this.radicadoReferencia = radicadoReferencia;
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}

	public String getCodigoNiuProcedencia() {
		return codigoNiuProcedencia;
	}

	public void setCodigoNiuProcedencia(String codigoNiuProcedencia) {
		this.codigoNiuProcedencia = codigoNiuProcedencia;
	}

	public String getCodigoNiuDestino() {
		return codigoNiuDestino;
	}

	public void setCodigoNiuDestino(String codigoNiuDestino) {
		this.codigoNiuDestino = codigoNiuDestino;
	}

	public String getNotificacion() {
		return notificacion;
	}

	public void setNotificacion(String notificacion) {
		this.notificacion = notificacion;
	}

	public List<String> getEmpresaCorreo() {
		return empresaCorreo;
	}

	public void setEmpresaCorreo(List<String> empresaCorreo) {
		this.empresaCorreo = empresaCorreo;
	}

	public String getEmpresaCorreoNombre() {
		return empresaCorreoNombre;
	}

	public void setEmpresaCorreoNombre(String empresaCorreoNombre) {
		this.empresaCorreoNombre = empresaCorreoNombre;
	}

	public List<Documento> getListaRef() {
		return listaRef;
	}

	public void setListaRef(List<Documento> listaRef) {
		this.listaRef = listaRef;
	}

	public List<String> getProcedencia() {
		return procedencia;
	}

	public void setProcedencia(List<String> procedencia) {
		this.procedencia = procedencia;
	}

	public List<String> getDestinos() {
		return destinos;
	}

	public void setDestinos(List<String> destinos) {
		this.destinos = destinos;
	}

	public Boolean getAddPersonal() {
		return addPersonal;
	}

	public void setAddPersonal(Boolean addPersonal) {
		this.addPersonal = addPersonal;
	}

	public String getFechaEtiqueta() {
		return fechaEtiqueta;
	}

	public void setFechaEtiqueta(String fechaEtiqueta) {
		this.fechaEtiqueta = fechaEtiqueta;
	}

	public Boolean getShowInfoCorreo() {
		return showInfoCorreo;
	}

	public void setShowInfoCorreo(Boolean showInfoCorreo) {
		this.showInfoCorreo = showInfoCorreo;
	}

	public String getMensajero() {
		return mensajero;
	}

	public void setMensajero(String mensajero) {
		this.mensajero = mensajero;
	}

	public String getPqr() {
		return pqr;
	}

	public void setPqr(String pqr) {
		this.pqr = pqr;
	}

	public String getContractTipo() {
		return contractTipo;
	}

	public void setContractTipo(String contractTipo) {
		this.contractTipo = contractTipo;
	}

	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getEmpresaCorreoRecibido() {
		return empresaCorreoRecibido;
	}

	public void setEmpresaCorreoRecibido(String empresaCorreoRecibido) {
		this.empresaCorreoRecibido = empresaCorreoRecibido;
	}

	public Boolean getFactura() {
		return factura;
	}

	public void setFactura(Boolean factura) {
		this.factura = factura;
	}

	public List<String> getListaAsunto() {
		return listaAsunto;
	}

	public void setListaAsunto(List<String> listaAsunto) {
		this.listaAsunto = listaAsunto;
	}

	public String getDireccionEditar() {
		return direccionEditar;
	}

	public void setDireccionEditar(String direccionEditar) {
		this.direccionEditar = direccionEditar;
	}

	public String getRadicacionEtiqueta() {
		return radicacionEtiqueta;
	}

	public void setRadicacionEtiqueta(String radicacionEtiqueta) {
		this.radicacionEtiqueta = radicacionEtiqueta;
	}

	public String getLetra() {
		return letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public String getLetraPqr() {
		return letraPqr;
	}

	public void setLetraPqr(String letraPqr) {
		this.letraPqr = letraPqr;
	}

	public Boolean getMedioElectronico() {
		return medioElectronico;
	}

	public void setMedioElectronico(Boolean medioElectronico) {
		this.medioElectronico = medioElectronico;
	}

	public String getNombreYApellidoEditar() {
		return nombreYApellidoEditar;
	}

	public void setNombreYApellidoEditar(String nombreYApellidoEditar) {
		this.nombreYApellidoEditar = nombreYApellidoEditar;
	}

	public Long getCiudadEditar() {
		return ciudadEditar;
	}

	public void setCiudadEditar(Long ciudadEditar) {
		this.ciudadEditar = ciudadEditar;
	}

	public Map<Long, String> getCiudades() {
		return ciudades;
	}

	public void setCiudades(Map<Long, String> ciudades) {
		this.ciudades = ciudades;
	}

	public Boolean getPanelEditar() {
		return panelEditar;
	}

	public void setPanelEditar(Boolean panelEditar) {
		this.panelEditar = panelEditar;
	}

	public Actor getActorEditar() {
		return actorEditar;
	}

	public void setActorEditar(Actor actorEditar) {
		this.actorEditar = actorEditar;
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}

	public Boolean getDesviacion() {
		return desviacion;
	}

	public void setDesviacion(Boolean desviacion) {
		this.desviacion = desviacion;
	}

	public String getCodigoNiu() {
		return codigoNiu;
	}

	public void setCodigoNiu(String codigoNiu) {
		this.codigoNiu = codigoNiu;
	}

	public String getNroActa() {
		return nroActa;
	}

	public void setNroActa(String nroActa) {
		this.nroActa = nroActa;
	}

	public String getVolumen() {
		return volumen;
	}

	public void setVolumen(String volumen) {
		this.volumen = volumen;
	}

	public String getEntrega() {
		return entrega;
	}

	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}

	public Date getFechaActa() {
		return fechaActa;
	}

	public void setFechaActa(Date fechaActa) {
		this.fechaActa = fechaActa;
	}

	public List<GrupoRevisor> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<GrupoRevisor> grupos) {
		this.grupos = grupos;
	}

	public Long getGrupoSeleccionado() {
		return grupoSeleccionado;
	}

	public void setGrupoSeleccionado(Long grupoSeleccionado) {
		this.grupoSeleccionado = grupoSeleccionado;
	}

	public List<Actor> getRevisoresTarget() {
		return revisoresTarget;
	}

	public void setRevisoresTarget(List<Actor> revisoresTarget) {
		this.revisoresTarget = revisoresTarget;
	}

	public List<Actor> getRevisoresList() {
		return revisoresList;
	}

	public void setRevisoresList(List<Actor> revisoresList) {
		this.revisoresList = revisoresList;
	}

	public String getUrlServer() {
		return urlServer;
	}

	public void setUrlServer(String urlServer) {
		this.urlServer = urlServer;
	}

	public SubClase getSubClase() {
		return subClase;
	}

	public void setSubClase(SubClase subClase) {
		this.subClase = subClase;
	}

	public List<SubClase> getSubClases() {
		return subClases;
	}

	public void setSubClases(List<SubClase> subClases) {
		this.subClases = subClases;
	}

	public Long getClaseId() {
		return claseId;
	}

	public void setClaseId(Long claseId) {
		this.claseId = claseId;
	}

	public Long getIdsubClase() {
		return idsubClase;
	}

	public void setIdsubClase(Long idsubClase) {
		this.idsubClase = idsubClase;
	}

	public void actualizarFecha() {
//        claseDocumento = claseBean.buscarClasePorId(claseId);
//        subClase = claseBean.buscarSubClasePorId(this.idsubClase);
//        int contador = 0;
//        Date date = new Date();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        int limite = subClase.getTiempoDias();
//        do {
//            if (calendar.get(Calendar.DAY_OF_WEEK) != 7 && calendar.get(Calendar.DAY_OF_WEEK) != 1) {
//                calendar.add(Calendar.DATE, 1);
//                contador++;
//            } else {
//                calendar.add(Calendar.DATE, 1);
//            }
//        } while (contador < limite);
//        fechaControl = calendar.getTime();
//        System.out.println(fechaControl);
	}

	public List<Oficina> getListaOficina() {
		return listaOficina;
	}

	public void setListaOficina(List<Oficina> listaOficina) {
		this.listaOficina = listaOficina;
	}

	public List<Serie> getListaSerie() {
		return listaSerie;
	}

	public void setListaSerie(List<Serie> listaSerie) {
		this.listaSerie = listaSerie;
	}

	public List<SubSerie> getListaSubSerie() {
		return listaSubSerie;
	}

	public void setListaSubSerie(List<SubSerie> listaSubSerie) {
		this.listaSubSerie = listaSubSerie;
	}

	public List<TipoDocumento> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}

	public List<Oficina> getListaOficinaFiltered() {
		return listaOficinaFiltered;
	}

	public void setListaOficinaFiltered(List<Oficina> listaOficinaFiltered) {
		this.listaOficinaFiltered = listaOficinaFiltered;
	}

	public List<Serie> getListaSerieFiltered() {
		return listaSerieFiltered;
	}

	public void setListaSerieFiltered(List<Serie> listaSerieFiltered) {
		this.listaSerieFiltered = listaSerieFiltered;
	}

	public List<SubSerie> getListaSubSerieFiltered() {
		return listaSubSerieFiltered;
	}

	public void setListaSubSerieFiltered(List<SubSerie> listaSubSerieFiltered) {
		this.listaSubSerieFiltered = listaSubSerieFiltered;
	}

	public List<TipoDocumento> getListaTipoDocumentoFiltered() {
		return listaTipoDocumentoFiltered;
	}

	public void setListaTipoDocumentoFiltered(List<TipoDocumento> listaTipoDocumentoFiltered) {
		this.listaTipoDocumentoFiltered = listaTipoDocumentoFiltered;
	}

	public String getRadicadoPor() {
		return radicadoPor;
	}

	public void setRadicadoPor(String radicadoPor) {
		this.radicadoPor = radicadoPor;
	}

	public String getSticker() {
		return sticker;
	}

	public void setSticker(String sticker) {
		this.sticker = sticker;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getCedulaDestino() {
		return cedulaDestino;
	}

	public void setCedulaDestino(String cedulaDestino) {
		this.cedulaDestino = cedulaDestino;
	}

	public Map<Long, String> getListaCiudades() {
		return listaCiudades;
	}

	public void setListaCiudades(Map<Long, String> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	 public Map<String, String> getMapClasesDocumentos() {
        return mapClasesDocumentos;
    }

    public void setMapClasesDocumentos(Map<String, String> mapClasesDocumentos) {
        this.mapClasesDocumentos = mapClasesDocumentos;
    }
	
	public String getPopupClaseDocumento() {
        return popupClaseDocumento;
    }

    public void setPopupClaseDocumento(String popupClaseDocumento) {
        this.popupClaseDocumento = popupClaseDocumento;
    }

}
