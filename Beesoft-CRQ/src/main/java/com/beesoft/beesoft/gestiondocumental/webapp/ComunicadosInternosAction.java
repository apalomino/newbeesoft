/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ComunicadosInternosLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ComunicadoInternoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DestinoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import com.beesoft.beesoft.gestiondocumental.entidades.ComunicadosInternos;
import com.beesoft.beesoft.gestiondocumental.entidades.DestinosComunicados;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import com.lowagie.text.Document;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named("comunicadoAction")
@SessionScoped
public class ComunicadosInternosAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(ComunicadosInternosAction.class);

    @EJB
    private ComunicadosInternosLocal comunicadosBean;

    @EJB
    private ActorLocal actorBean;

    @EJB
    private OficinaLocal oficinaBean;

    @Inject
    private ApplicationAction application;
    
    @Inject RepositorioAction repositorio;

    //-----------------Enviar-----------------------
    private String radicacion;
    private Long idOficina;
    private Oficina OficinaAprueba;
    private String archFirma = "";

    //------ Actores-----------------------------
    private List<DestinoDTO> destinationsAgregados;
    private List<DestinoDTO> destinationsAgregadosConCopia;
    private List<Actor> destinationsPendientes;
    private List<Actor> destinationsPendientesNuevos;
    private List<Actor> destinationsPendientesEliminados;
    private List<Actor> destinationsPendientesConCopia;
    private List<Actor> destinationsPendientesConCopiaNuevos;
    private List<Actor> destinationsPendientesConCopiaEliminados;
    private List<DestinoDTO> oficinasAprueban;
    private List<DestinoDTO> destinations;
    private List<DestinoDTO> destinationsCopia;
    private List<DestinoDTO> destinosTarget;
    private List<DestinoDTO> destinosConCopiaTarget;

    private List<Oficina> oficinasDestinations;
    private List<Oficina> oficinasDestinationsCopia;
    private List<Oficina> sendDestination;
    private List<Oficina> sendDestinationConCopia;
    private List<Oficina> sendDestinationPendientes;
    private List<Oficina> sendDestinationPendientesConCopia;
    private Map<Long, String> mapaDestinatariosPendientes;
    private Map<Long, String> mapaDestinatariosPendientesConCopia;
    private Map<Long, String> mapaDestinatarios;
    private Map<Long, String> mapaDestinatariosConCopia;
    private String asunto;
    private String mensaje;
    private String comentario;
    private String directory = "";
    private StringBuilder destinatarios;
    private File file;
    private List<ArchivosAdjuntos> files = new ArrayList<>();
    private List<ArchivosAdjuntos> archivosGuardar = new ArrayList<>();
    private List<ArchivosAdjuntos> archivosAdjuntos = new ArrayList<>();
    private List<ArchivosAdjuntos> archivosNuevos = new ArrayList<>();
    private List<ArchivosAdjuntos> archivosEliminados = new ArrayList<>();
    private List<File> archivosEnviar = new ArrayList<>();
    private Boolean pantallaJefe;
    private List<Actor> listaJefes;
    private Long idJefes;
    private Long idJefesAprobo;
    //---------------------------------------------------
    //--------------Buscar Correo------------------------
    private List<Actor> listaDestinatariosAll;
    private String radicacionConsulta;
    private List<ComunicadosInternos> correos;
    private List<ComunicadosInternos> comunicadosAll;
    private List<DestinoDTO> destinationsAgregadosBusqueda;
    private List<DestinoDTO> destinationsBusqueda;
    private List<Actor> sendDestinationBusqueda;
    private Map<Long, String> mapaDestinatariosBusqueda;
    private Date fechaIni;
    private Date fechaFin;
    private Date fechaIniAll;
    private Date fechaFinAll;
    private String asuntoBusqueda;
    private Boolean tipoDestinatarios;
    private Usuario usuarioLogin;
    private Boolean trazaVisible;
    private Boolean accionEnviar;
    private Boolean accionReenviar;
    private Boolean accionNoJefe;
    private Boolean accionNoJefeAprobo;
    private Boolean jefeAproboFinal;
    private Boolean comunicadoAnulado;
    private Long idTipoComunicado;
    private String vistoBueno;
    private String comentarioAnulado;
    //---------------------------------------------------    
    //----------------Ver Correo-------------------------  
    private String titulo;
    private Boolean boton;
    private Actor from;
    private String fromOficina;
    private String fromJefe;
	private String radicacionShow;
    private String asuntoShow;
    private String mensajeShow;
    private String comentarioShow;
    private Map<String, String> listaDestinosText;
    private String listaDestinosCopiaText;
    private Long correo;
    private ComunicadosInternos mail;
    private Boolean isComentario;
    private Boolean isJefe;
    private StreamedContent graphicText;
    private Boolean graphicShow;
    private String fechaShow;
    private StreamedContent graphicImage = null;
    //-----------------------------------------------
    //----------------Ver Pendientes-------------------------  
    private String tituloPendiente;
    private String radicacionShowPendiente;
    private String asuntoShowPendiente;
    private String mensajeShowPendiente;
    private String redactadoPorPendiente;
    private String comentarioModiicacion;
    private String comentarioAnulacion;
    private Long correoPendiente;
    private ComunicadosInternos mailPendiente;
    private Boolean isComentarioPendiente;
    private Boolean isJefePendiente;
    private String urlServer = "";
    //-----------------------------------------------
    //---------------Reenviar-----------------------------
    private List<DestinoDTO> destinationsAgregadosReenvio;
    private List<DestinoDTO> destinationsReenvio;
    private List<ArchivosAdjuntos> archivosGuardarReenvio = new ArrayList<>();
    private List<File> archivosEnviarReenvio = new ArrayList<>();
    private Boolean reenvio = false;
    private Boolean cancelar = false;
    private Boolean conCopia;
    //------------------Inbox----------------------------------
    private List<ComunicadosInternos> listaGenerados;
    private List<ComunicadosInternos> listaGeneradosFiltered;
    private List<ComunicadosInternos> listaRecibidos;
    private List<ComunicadosInternos> listaRecibidosFiltered;
    private List<ComunicadosInternos> listaEnviados;
    private List<ComunicadosInternos> listaEnviadosFiltered;
    private List<ComunicadosInternos> listaAnulados;
    private List<ComunicadosInternos> listaAnuladosFiltered;
    private List<ComunicadosInternos> listaAprobacion;
    private List<ComunicadosInternos> listaAprobacionFiltered;

    private String volver;
    private String mensajeInbox;
    private Boolean inbox;
    //------------------crear TCI------------------------------
    private String descripcionTci;
    private List<String> estadosList;
    private String estadoReporte;
    private String inboxIn;
    private TabChangeEvent event;
    private int activeIndex;

    private String radicacionTraza;
    private String redacto;
    private String reviso;
    private String envio;

    private String carpeta;
    private Date fechaExtIni;
    private Date fechaExtFin;

    private Boolean modoEmpleado = true;
    private String tipoDestinatario = "";
    private String comunicadoHtml;

    public ComunicadosInternosAction() {

    }

    @PostConstruct
    public void iniciar() {
        tipoDestinatarios = true;
        iniciarListasDestinos();
        if (FacesContext.getCurrentInstance() != null) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                    .getExternalContext().getSession(false);
            usuarioLogin = (Usuario) session.getAttribute("username");
        }
        if (usuarioLogin != null) {
            if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.JEFE)) {
                modoEmpleado = false;
            } else {
                modoEmpleado = true;
            }
        }
    }
    
    public void iniciarListasDestinos() {
        oficinasAprueban = llenarInformacion(null, application.getListaJefes());
        cargarDestinatarios();
        cargarDestinatariosConCopia();
        cargarDestinatariosBusqueda();
        cargarDestinatariosReenvio();
        cargarEstadosComunicadoReporte();
    }

    public Boolean validacionesComunicadoInterno() {
        int validate = 0;

        validate += ValidacionesUtil.validarListas(destinationsAgregados, "Destinatarios");
        validate += ValidacionesUtil.validarCadena(asunto, "Asunto");
        validate += ValidacionesUtil.validarCadena(mensaje, "Mensaje");
        return validate >= 3;

    }

    public Boolean validacionesComunicadoInternoEdicion() {
        int validate = 0;

        validate += ValidacionesUtil.validarListas(destinosTarget, "Destinatarios");
        validate += ValidacionesUtil.validarCadena(asuntoShowPendiente, "Asunto");
        validate += ValidacionesUtil.validarCadena(mensajeShowPendiente, "Mensaje");
        return validate == 3;
    }

    public void encolarCorreos() {

        Actor responsable = usuarioLogin.getActor();

        if (validacionesComunicadoInterno()) {
            radicacion = comunicadosBean.guardarComunicadoInterno(responsable, null, responsable, destinationsAgregados,
                    destinationsAgregadosConCopia, asunto, mensaje, archivosGuardar, false, ConstantsMail.APROBADO);

            ValidacionesUtil.addMessageInfo("Se ha enviado un COMUNICADO INTERNO con numero de radicacion " + radicacion);
            limpiar();
        }

    }

    public void enviarParaAprobacion() {
        Actor responsable = usuarioLogin.getActor();
        if (validacionesComunicadoInterno()) {

            try {
                OficinaAprueba = oficinaBean.buscarOficina(idOficina);
                if (OficinaAprueba != null) {
                    radicacion = comunicadosBean.guardarComunicadoInterno(null, OficinaAprueba, responsable, destinationsAgregados,
                            destinationsAgregadosConCopia, asunto, mensaje, archivosGuardar, true, ConstantsMail.POR_APROBAR);
                    ValidacionesUtil.addMessageInfo("Se ha guardado un COMUNICADO INTERNO para aprobacion");
                    limpiar();
                }

            } catch (Exception ex) {
                LOG.info(ex);
            }

        }
    }

    public List<DestinoDTO> completeDestinos(String query) {
        List<DestinoDTO> filtrados = new ArrayList<>();

        for (DestinoDTO destino : destinations) {
            if (StringUtils.containsIgnoreCase(destino.getNombreDestino(), query)) {
                filtrados.add(destino);
            }

        }
        return filtrados;
    }

    public List<DestinoDTO> completeDestinosCopia(String query) {
        List<DestinoDTO> filtrados = new ArrayList<>();

        for (DestinoDTO destino : destinationsCopia) {
            if (StringUtils.containsIgnoreCase(destino.getNombreDestino(), query)) {
                filtrados.add(destino);
            }
        }
        return filtrados;
    }

    public String mostrar(Actor actor) {
        if (actor != null) {
            if (actor.getOficina() != null) {
                return actor.getNombreYApellido() + "-" + actor.getOficina().getDescripcion();
            } else {
                return actor.getNombreYApellido();
            }
        } else {
            return "";
        }
    }
    
    public List<DestinoDTO> completeDestinosReenvio(String query) {
        List<DestinoDTO> filtrados = new ArrayList<>();

        for (DestinoDTO destino : destinationsReenvio) {
            if (StringUtils.containsIgnoreCase(destino.getNombreDestino(), query)) {
                filtrados.add(destino);
            }
        }
        return filtrados;
    }

    public List<DestinoDTO> completeDestinosBusqueda(String query) {
        List<DestinoDTO> filtrados = new ArrayList<>();

        for (DestinoDTO destino : destinationsBusqueda) {
            if (StringUtils.containsIgnoreCase(destino.getNombreDestino(), query)) {
                filtrados.add(destino);
            }
        }
        return filtrados;
    }

    public String redactar() {
    	obtenerFirma();
    	pantallaJefe = usuarioLogin.getPerfil().getDescripcion().contains(ConstantsMail.JEFE) && !usuarioLogin.getPerfil().getDescripcion().equalsIgnoreCase(ConstantsMail.GESTOR_VB);
        if (!pantallaJefe) {
            listaJefes = comunicadosBean.cargarListaJefesDestinatarios();
        } else {
            idJefes = null;
        }
        return "enviar";
    }
    
    public void handleFileUpload(FileUploadEvent event) throws Exception {
        String fileName = FilenameUtils.getName(event.getFile().getFileName());
//        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "El archivo " + fileName + " fue cargado", null);
//        FacesContext.getCurrentInstance().addMessage(null, msg);
        archivosGuardar.add(fileToAdjunto(event.getFile()));
    }

    public void editarUpload(FileUploadEvent event) throws Exception {
        String fileName = FilenameUtils.getName(event.getFile().getFileName());
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "El archivo " + fileName + " fue cargado", null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        archivosNuevos.add(fileToAdjunto(event.getFile()));
    }

    public void quitarArchivos(ArchivosAdjuntos archivo) {
        if (archivo != null) {
            archivosEliminados.add(archivo);
            archivosAdjuntos.remove(archivo);
        }
    }

    public void mostrarTrazabilidad(ComunicadosInternos traza) {
        radicacionTraza = "";
        redacto = "";
        reviso = "";
        envio = "";
        if (traza != null) {
            radicacionTraza = traza.getRadicacion();
            redacto = traza.getRedactor().getNombreYApellido();
            reviso = traza.getEmisor().getNombreYApellido();
            if (traza.getEmisor() != null) {
                envio = traza.getEmisor().getNombreYApellido();
            }
        }
    }

    public void quitarArchivosGuardar(ArchivosAdjuntos archivo) {
        if (archivo != null) {
            archivosGuardar.remove(archivo);
        }
    }

    public ArchivosAdjuntos fileToAdjunto(UploadedFile file) {
        ArchivosAdjuntos aa = new ArchivosAdjuntos();
        aa.setNombreArchivo(FilenameUtils.getName(file.getFileName()));
        aa.setArchivo(file.getContents());
        aa.setTipo(file.getContentType());
        return aa;
    }

    public File copyFile(String fileAbsolutePath, InputStream in) {
        try {
            OutputStream out = new FileOutputStream(new File(fileAbsolutePath));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            return new File(fileAbsolutePath);
        } catch (IOException ex) {
            LOG.info(ex);
        }
        return null;
    }

    /**
     * Metodo para buscar comunicados internos.
     */
    public void correosEnviados() {
        correos = new ArrayList<>();
        List<ComunicadosInternos> lista = new ArrayList<>();
        if (validaciones()) {
            if (fechaIni != null && fechaFin != null) {
                ValidacionesUtil.validarFechasIniFin(fechaIni, fechaFin);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(fechaFin);
                calendar.add(calendar.HOUR, 24);// Configuramos la fecha que se recibe
                fechaFin = calendar.getTime();

                lista = comunicadosBean.buscarCorreo(radicacionConsulta, destinationsAgregadosBusqueda, asuntoBusqueda, fechaIni, fechaFin);
                for (ComunicadosInternos correoE : lista) {
                    if (StringUtils.isNotBlank(correoE.getRadicacion())) {
                        correos.add(correoE);
                    }
                }
            } else if (fechaIni != null && fechaFin == null) {
                ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha Final");
            } else if (fechaIni == null && fechaFin != null) {
                ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha Inicial");
            } else if (fechaIni == null && fechaFin == null) {
                lista = comunicadosBean.buscarCorreo(radicacionConsulta, destinationsAgregadosBusqueda, asuntoBusqueda, fechaIni, fechaFin);
                for (ComunicadosInternos correoE : lista) {
                    if (StringUtils.isNotBlank(correoE.getRadicacion())) {
                        correos.add(correoE);
                    }
                }
            }
            reenvio = usuarioLogin.getPerfil().getDescripcion().contains(ConstantsMail.JEFE) || usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN);
        }
    }

    public void buscarComunicadosAll() {
        comunicadosAll = new ArrayList<>();
        List<ComunicadosInternos> lista = new ArrayList<>();

        if (fechaIniAll != null && fechaFinAll != null) {
            ValidacionesUtil.validarFechasIniFin(fechaIniAll, fechaFinAll);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaFinAll);
            calendar.add(calendar.HOUR, 24);// Configuramos la fecha que se recibe
            fechaFinAll = calendar.getTime();

            lista = comunicadosBean.buscarTodosComunicadosInternos(fechaIniAll, fechaFinAll, estadoReporte);
            for (ComunicadosInternos correoE : lista) {
                if (StringUtils.isNotBlank(correoE.getRadicacion())) {
                    comunicadosAll.add(correoE);
                }
            }
        } else if (fechaIniAll != null && fechaFinAll == null) {
            ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha Final");
        } else if (fechaIniAll == null && fechaFinAll != null) {
            ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha Inicial");
        } else if (fechaIniAll == null && fechaFinAll == null) {
            lista = comunicadosBean.buscarTodosComunicadosInternos(fechaIniAll, fechaFinAll, estadoReporte);
            for (ComunicadosInternos correoE : lista) {
                if (StringUtils.isNotBlank(correoE.getRadicacion())) {
                    comunicadosAll.add(correoE);
                }
            }
        }
    }

    public Boolean validaciones() {
        if (StringUtils.isBlank(radicacionConsulta) && fechaIni == null && fechaFin == null && (destinationsAgregadosBusqueda == null || destinationsAgregadosBusqueda.isEmpty()) && StringUtils.isBlank(asuntoBusqueda)) {
            ValidacionesUtil.addMessage("Debe utilizar por lo menos un filtro de busqueda");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Metodo para ver la información del correo.
     *
     * @param correo, comunicado seleccionado.
     * @param inbox
     * @return regla de navegacion.
     */
    public String infoCorreo(ComunicadosInternos correo, Boolean inbox) {
        if (correo != null) {
            String radicacion = "";
            if (StringUtils.isNotBlank(correo.getRadicacion())) {
                radicacion = correo.getRadicacion();
            } else if (StringUtils.isNotBlank(correo.getRadicacionReenvio())) {
                radicacion = correo.getRadicacionReenvio();
            }
            urlServer = ValidacionesUtil.urlServer();
            titulo = "COMUNICADO INTERNO";
            boton = false;
            if (!correo.getLeido()) {
                comunicadosBean.marcarComoLeido(correo, usuarioLogin.getActor());
            }
            this.mail = correo;
            this.correo = correo.getId();
            from = correo.getEmisor();
            fromOficina = correo.getCargoActualEmisor();
            fromJefe = correo.getEmisor().getNombreYApellido();

            List<DestinosComunicados> destinos = comunicadosBean.consultarDestinosComunicados(correo.getId());
            listaDestinosText = convetirDestinoATexto(destinos);
            List<DestinosComunicados> destinosCopia = comunicadosBean.consultarDestinosCopiaComunicados(correo.getId());
            if (destinosCopia != null && !destinosCopia.isEmpty()) {
                listaDestinosCopiaText = convetirOficinaATexto(destinosCopia);
            }
            fechaShow = ValidacionesUtil.formatearFechaElectronico(correo.getFechaEnvio());
            asuntoShow = correo.getAsunto();
            mensajeShow = correo.getMensaje();
            if (StringUtils.isNotBlank(correo.getRadicacion())) {
                radicacionShow = correo.getRadicacion();
            } else if (StringUtils.isNotBlank(correo.getRadicacionReenvio())) {
                radicacionShow = correo.getRadicacionReenvio();
            }
            comentarioAnulado = correo.getComentario();
            List<ArchivosAdjuntos> archivos = comunicadosBean.consultarArchivosComunicados(correo.getId());
            files = archivos;

            if (usuarioLogin.getPerfil().getDescripcion().contains(ConstantsMail.JEFE) || usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN)) {
                cancelar = true;
                if (accionReenviar != null) {
                    if (accionReenviar) {
                        reenvio = !StringUtils.isNotBlank(correo.getRadicacionReenvio());
                    } else {
                        reenvio = false;
                        cancelar = false;
                    }
                } else {
                    reenvio = false;
                    cancelar = false;
                }
            } else {
                reenvio = false;
                cancelar = false;
            }
            comunicadoAnulado = false;
            comentarioAnulado = "";
            if (StringUtils.isNotBlank(correo.getComentario())) {
                comentarioShow = correo.getComentario();
                isComentario = true;
            } else {
                comentarioShow = null;
                isComentario = false;
            }

            isJefe = true;
            if (inbox) {
                this.inbox = true;
                return "Inbox";
            } else {
                this.inbox = false;
                return "success";
            }
        }
        return "not";
    }

    /**
     * Metodo para ver la información del correo.
     *
     * @param correo, comunicado seleccionado.
     * @param inbox
     * @return regla de navegacion.
     */
    public String infoCorreoAnulado(ComunicadosInternos correo, Boolean inbox) {
        if (correo != null) {
            String radicacion = "";
            if (StringUtils.isNotBlank(correo.getRadicacion())) {
                radicacion = correo.getRadicacion();
            } else if (StringUtils.isNotBlank(correo.getRadicacionReenvio())) {
                radicacion = correo.getRadicacionReenvio();
            }
            urlServer = ValidacionesUtil.urlServer();
            titulo = "COMUNICADO INTERNO";
            boton = false;

            this.mail = correo;
            this.correo = correo.getId();
            from = correo.getOrigen();
            fromOficina = correo.getCargoActualEmisor();
            fromJefe = correo.getOrigen().getNombreYApellido();

            List<DestinosComunicados> destinos = comunicadosBean.consultarDestinosComunicados(correo.getId());
            listaDestinosText = convetirDestinoATexto(destinos);
            List<DestinosComunicados> destinosCopia = comunicadosBean.consultarDestinosCopiaComunicados(correo.getId());
            if (destinosCopia != null && !destinosCopia.isEmpty()) {
                listaDestinosCopiaText = convetirOficinaATexto(destinosCopia);
            }
            fechaShow = ValidacionesUtil.formatearFechaElectronico(correo.getFechaCreacion());
            asuntoShow = correo.getAsunto();
            mensajeShow = correo.getMensaje();
            if (StringUtils.isNotBlank(correo.getRadicacion())) {
                radicacionShow = correo.getRadicacion();
            } else if (StringUtils.isNotBlank(correo.getRadicacionReenvio())) {
                radicacionShow = correo.getRadicacionReenvio();
            }

            List<ArchivosAdjuntos> archivos = comunicadosBean.consultarArchivosComunicados(correo.getId());
            files = archivos;

            if (usuarioLogin.getPerfil().getDescripcion().contains(ConstantsMail.JEFE) || usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN)) {
                cancelar = true;
                if (accionReenviar != null) {
                    if (accionReenviar) {
                        reenvio = !StringUtils.isNotBlank(correo.getRadicacionReenvio());
                    } else {
                        reenvio = false;
                        cancelar = false;
                    }
                } else {
                    reenvio = false;
                    cancelar = false;
                }
            } else {
                reenvio = false;
                cancelar = false;
            }
            if (StringUtils.isNotBlank(correo.getComentario())) {
                comentarioAnulado = correo.getComentario();
                comunicadoAnulado = true;
            } else {
                comentarioAnulado = "";
                comunicadoAnulado = false;
            }

            isJefe = true;
            if (inbox) {
                this.inbox = true;
                return "Inbox";
            } else {
                this.inbox = false;
                return "success";
            }
        }
        return "not";
    }

    public Boolean showButtons(ComunicadosInternos mail) {
        List<DestinosComunicados> destinos = comunicadosBean.consultarDestinosComunicados(mail.getId());
        List<Actor> lista = new ArrayList<>();
        for (DestinosComunicados dc : destinos) {
            lista.add(dc.getDestino());
        }
        for (Actor actor : lista) {
            if (usuarioLogin.getActor().getId().equals(actor.getId())) {
                return true;
            }
        }

        if (usuarioLogin.getActor().getId().equals(mail.getEmisor().getId())) {
            return true;
        }

        if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN)) {
            return true;
        }
        return false;
    }

    public void cargarEmail(ComunicadosInternos mail) {
        this.mail = mail;
    }

    public void reenviar() {

        if (destinationsAgregadosReenvio != null && !destinationsAgregadosReenvio.isEmpty()) {
            List<ArchivosAdjuntos> archivos = comunicadosBean.consultarArchivosComunicados(mail.getId());
            for (ArchivosAdjuntos aa : archivos) {
                ArchivosAdjuntos add = new ArchivosAdjuntos();
                add.setNombreArchivo(aa.getNombreArchivo());
                add.setArchivo(aa.getArchivo());
                archivosGuardarReenvio.add(add);
            }
            for (ArchivosAdjuntos archivo : archivos) {
                File file = ValidacionesUtil.arraytoFile(archivo.getArchivo(), archivo.getNombreArchivo());
                archivosEnviarReenvio.add(file);
            }
            comunicadosBean.reenviarComunicadoInterno(mail.getId(), usuarioLogin.getActor(), destinationsAgregadosReenvio, comentario);
            resetReenvio();
        }
    }
    
    /**
     * 
     * @throws FileNotFoundException
     */
    public void prepararImagen() {
    	graphicImage = null;
    	if (from.getFirma() == null || from.getFirma().isEmpty()) return;
		String path = System.getenv("JBOSS_HOME") + File.separator + "standalone" + File.separator + "tmp"
				+ File.separator + "imgFirmas" + File.separator + from.getFirma();
		try {
			graphicImage = new DefaultStreamedContent(new FileInputStream(path), "image/PNG", from.getFirma());
		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}

	public StreamedContent getGraphicImage() throws FileNotFoundException {
		prepararImagen();
		return graphicImage;
	}

	public void setGraphicImage(StreamedContent graphicImage) {
		this.graphicImage = graphicImage;
	}
    
///----------------------Inbox-----------------------------------------------
    public void InboxComunicados(Boolean inOut) {
        try {
            listaRecibidos = new ArrayList<>();
            listaEnviados = new ArrayList<>();
            volver = "volverInbox";
            accionEnviar = true;
            accionReenviar = true;
            if (inOut) {
                List<ComunicadosInternos> lista = comunicadosBean.buscarComunicadosInOut(usuarioLogin.getActor(), mensajeInbox, inOut);
                for (ComunicadosInternos mail : lista) {
                    if (StringUtils.isBlank(mail.getRadicacionReenvio())) {
                        listaEnviados.add(mail);
                    }
                }
            } else {
                List<ComunicadosInternos> lista = comunicadosBean.buscarComunicadosInOut(usuarioLogin.getActor(), mensajeInbox, inOut);
                for (ComunicadosInternos mail : lista) {
                    if (StringUtils.isNotBlank(mail.getRadicacion()) || StringUtils.isNotBlank(mail.getRadicacionReenvio())) {
                        listaRecibidos.add(mail);
                    }
                }
            }

//            HashSet<ComunicadosInternos> listaHash = new HashSet<>(listaComunicados);
//            listaComunicados.clear();
//            listaComunicados.addAll(listaHash);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void InboxPendientesAprobacion() {

        List<ComunicadosInternos> lista = null;
        comunicadoAnulado = false;
        if (usuarioLogin.getPerfil().getDescripcion().equalsIgnoreCase(ConstantsMail.ADMIN)) {
            lista = comunicadosBean.buscarComunicadosPendientesAprobacionAdmin(ConstantsMail.POR_APROBAR);
            inbox = true;
            trazaVisible = true;
        } else if (usuarioLogin.getPerfil().getDescripcion().contains(ConstantsMail.JEFE) || usuarioLogin.getPerfil().getDescripcion().equalsIgnoreCase(ConstantsMail.GESTOR_VB)) {
            lista = comunicadosBean.buscarComunicadosPendientesAprobacion(usuarioLogin.getActor(), ConstantsMail.EMISOR, ConstantsMail.POR_APROBAR);
            lista.addAll(comunicadosBean.buscarComunicadosPendientesAprobacion(usuarioLogin.getActor(), ConstantsMail.RESPONSABLE, ConstantsMail.POR_APROBAR));
        } else {
            lista = comunicadosBean.buscarComunicadosPendientesAprobacion(usuarioLogin.getActor(), ConstantsMail.RESPONSABLE, ConstantsMail.POR_APROBAR);
        }
        try {
            listaAprobacion = new ArrayList<>();
            volver = "volverInbox";
            accionReenviar = false;
            accionEnviar = false;
            reenvio = false;
            for (ComunicadosInternos mail : lista) {
                //if (StringUtils.isBlank(mail.getRadicacionReenvio())) {
                listaAprobacion.add(mail);
                //}
            }

//            HashSet<ComunicadosInternos> listaHash = new HashSet<>(listaComunicados);
//            listaComunicados.clear();
//            listaComunicados.addAll(listaHash);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void InboxGenerados() {

        List<ComunicadosInternos> lista = null;
        if (usuarioLogin.getPerfil().getDescripcion().equalsIgnoreCase(ConstantsMail.ADMIN)) {
            lista = comunicadosBean.buscarComunicadosPendientesAprobacionAdmin(ConstantsMail.APROBADO);
            inbox = true;
            trazaVisible = true;
        } else {
            try {
                lista = comunicadosBean.buscarComunicadosPendientesAprobacion(usuarioLogin.getActor(), ConstantsMail.RESPONSABLE, ConstantsMail.APROBADO);
                if (usuarioLogin.getPerfil().getDescripcion().equalsIgnoreCase(ConstantsMail.GESTOR_VB)) {
                    lista = comunicadosBean.buscarComunicadosPendientesAprobacion(usuarioLogin.getActor(), ConstantsMail.EMISOR, ConstantsMail.APROBADO);
                }
            } catch (Exception ex) {
                LOG.info(ex);
            }
        }
        listaGenerados = new ArrayList<>();
        volver = "volverInbox";
        accionEnviar = true;
        accionReenviar = false;
        reenvio = false;
        comunicadoAnulado = false;
        for (ComunicadosInternos mail : lista) {
            if (StringUtils.isNotBlank(mail.getRadicacion())) {
                listaGenerados.add(mail);
            }
        }

//        HashSet<ComunicadosInternos> listaHash = new HashSet<>(listaComunicados);
//        listaComunicados.clear();
//        listaComunicados.addAll(listaHash);
    }

    public void InboxAnulados() {
        comunicadoAnulado = true;
        List<ComunicadosInternos> lista = null;
        if (usuarioLogin.getPerfil().getDescripcion().equalsIgnoreCase(ConstantsMail.ADMIN)) {
            lista = comunicadosBean.buscarComunicadosPendientesAprobacionAdmin(ConstantsMail.ANULADO);
        } else {
            lista = comunicadosBean.buscarComunicadosPendientesAprobacion(usuarioLogin.getActor(), ConstantsMail.EMISOR, ConstantsMail.ANULADO);

            if (usuarioLogin.getPerfil().getDescripcion().equalsIgnoreCase(ConstantsMail.GESTOR_VB)) {
                lista = comunicadosBean.buscarComunicadosPendientesAprobacion(usuarioLogin.getActor(), ConstantsMail.REVISOR, ConstantsMail.ANULADO);
            }
        }
        try {
            inbox = true;
            listaAnulados = new ArrayList<>();
            volver = "volverInbox";
            accionEnviar = false;
            accionReenviar = false;
            reenvio = false;

            for (ComunicadosInternos mail : lista) {
                //if (StringUtils.isBlank(mail.getRadicacionReenvio())) {
                listaAnulados.add(mail);
                //}
            }

//            HashSet<ComunicadosInternos> listaHash = new HashSet<>(listaComunicados);
//            listaComunicados.clear();
//            listaComunicados.addAll(listaHash);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void onTabChange(TabChangeEvent event) {
        if (event != null) {
            Tab activeTab = event.getTab();
            TabView parent = (TabView) activeTab.getParent();
            activeIndex = parent.getActiveIndex();
            this.event = event;
            if (event.getTab().getId().equals("tab1")) {
                InboxComunicados(true);
            } else if (event.getTab().getId().equals("tab2")) {
                InboxComunicados(false);
            } else if (event.getTab().getId().equals("tab3")) {
                InboxPendientesAprobacion();
            } else if (event.getTab().getId().equals("tab4")) {
                InboxGenerados();
            } else if (event.getTab().getId().equals("tab5")) {
                InboxAnulados();
            }
        } else {
            InboxComunicados(true);
        }
    }

    public String selectPendientesAprobacion(ComunicadosInternos correo) {
        if (correo != null) {
            this.correoPendiente = correo.getId();
            tituloPendiente = "COMUNICADO INTERNO";
            List<DestinosComunicados> listaD = comunicadosBean.consultarDestinosComunicados(correo.getId());

            if (listaD != null && !listaD.isEmpty()) {
                List<DestinoDTO> destinos = new ArrayList<>();
                destinos = convertirDestinosADTO(listaD);

                buscarDestinos(destinos);
            } else {
                cargarDestinatariosPendientes();
            }
            List<DestinosComunicados> listaCopia = comunicadosBean.consultarDestinosCopiaComunicados(correo.getId());

            if (listaCopia != null && !listaCopia.isEmpty()) {
                List<DestinoDTO> destinos = new ArrayList<>();
                destinos = convertirDestinosADTO(listaCopia);
                conCopia = true;
                buscarDestinosConCopia(destinos);
            } else {
                conCopia = false;
                cargarDestinatariosPendienteConCopia();
            }
            if (usuarioLogin.getPerfil().getDescripcion().contains(ConstantsMail.JEFE) || usuarioLogin.getPerfil().getDescripcion().contains(ConstantsMail.GESTOR_VB)) {
                accionNoJefe = false;
                accionNoJefeAprobo = false;
            } else {
                accionNoJefeAprobo = true;
                accionNoJefe = true;
            }

            if (correo.getOrigen() != null) {
                if (usuarioLogin.getActor().getId().equals(correo.getOrigen().getId())) {
                    accionNoJefeAprobo = true;
                    jefeAproboFinal = true;
                    vistoBueno = correo.getOrigen().getNombreYApellido();
                }
            } else if (usuarioLogin.getActor().getId().equals(correo.getRedactor().getId())) {
                accionNoJefeAprobo = true;
                accionNoJefe = true;
            } else {
                accionNoJefeAprobo = false;
                jefeAproboFinal = false;
            }
            listaJefes = new ArrayList<>();
            List<Actor> lista = comunicadosBean.cargarListaJefesDestinatarios();
            for (Actor actor : lista) {
                if (!actor.getId().equals(usuarioLogin.getActor().getId())) {
                    listaJefes.add(actor);
                }
            }
            asuntoShowPendiente = correo.getAsunto();
            mensajeShowPendiente = correo.getMensaje();
            redactadoPorPendiente = correo.getRedactor().getNombreYApellido();
            comentarioAnulado = correo.getComentario();
            List<ArchivosAdjuntos> archivos = comunicadosBean.consultarArchivosComunicados(correo.getId());
            if (archivos != null && !archivos.isEmpty()) {
                archivosAdjuntos = archivos;
            }
            archivosEliminados = new ArrayList<>();
            archivosNuevos = new ArrayList<>();
            inbox = true;
            return "InboxAprobacion";
        }
        return "";
    }

    public void buscarDestinos(List<DestinoDTO> destinos) {
        try {
            cargarDestinatarios();
            destinosTarget = new ArrayList<>();
            destinosTarget.addAll(destinos);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void buscarDestinosConCopia(List<DestinoDTO> destinos) {
        try {
            cargarDestinatariosConCopia();
            destinosConCopiaTarget = new ArrayList<>();
            destinosConCopiaTarget.addAll(destinos);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public String enviarAprobado() {

        if (validacionesComunicadoInternoEdicion()) {
            try {
                Actor jefe = null;
                if (idJefesAprobo != null) {
                    jefe = actorBean.buscarActor(idJefesAprobo);
                }
                comunicadosBean.enviarComunicadoAprobado(correoPendiente, destinosTarget, destinosConCopiaTarget, asuntoShowPendiente, mensajeShowPendiente, archivosNuevos, archivosEliminados);
                InboxPendientesAprobacion();
                limpiar();
                limpiarEnvio();
                return "volverInboxAprobacion";
            } catch (Exception ex) {
                LOG.info(ex);
            }
        }
        return "";
    }

    public String modificarEnviarAprobado() {
        if (validacionesComunicadoInternoEdicion()) {
            comunicadosBean.modificarEnviarComunicadoAprobado(correoPendiente, destinosTarget, destinosConCopiaTarget, asuntoShowPendiente, mensajeShowPendiente, archivosNuevos, archivosEliminados);
            InboxPendientesAprobacion();
            limpiar();
            limpiarEnvio();
            return "volverInboxModificaEnvia";
        }
        return "";
    }

    public String modificacionAprobado() {
        comunicadosBean.modificarComunicadoInternoPorAprobar(correoPendiente, comentarioModiicacion);
        InboxPendientesAprobacion();
        comentarioModiicacion = "";
        return "volverInboxModificacion";
    }

    public String anulacionAprobado() {
        comunicadosBean.anularComunicadoInternoPorAprobar(correoPendiente, comentarioAnulacion);
        InboxPendientesAprobacion();
        comentarioAnulacion = "";
        return "volverInboxAnulacion";
    }

    public void limpiarEnvio() {
        correoPendiente = null;
        destinationsPendientesNuevos = new ArrayList<>();
        destinationsPendientesEliminados = new ArrayList<>();
        destinationsPendientesConCopiaNuevos = new ArrayList<>();
        destinationsPendientesConCopiaEliminados = new ArrayList<>();
        asuntoShowPendiente = "";
        mensajeShowPendiente = "";
        archivosAdjuntos = new ArrayList<>();
        archivosNuevos = new ArrayList<>();
        archivosEliminados = new ArrayList<>();
        tituloPendiente = "";
        graphicImage = null;
    }

    public List<Actor> obtenerDestinosEditar(List<String> lista) {
        List<Actor> ListaRev = new ArrayList<>();
        for (Object value : lista) {
            for (Map.Entry<Long, String> entry : mapaDestinatariosPendientes.entrySet()) {
                if (entry.getValue().equals(value)) {
                    try {
                        Long id = entry.getKey();
                        Actor actor = actorBean.buscarActor(id);
                        ListaRev.add(actor);
                    } catch (Exception ex) {
                        LOG.info(ex);
                    }
                }
            }
        }
        return ListaRev;
    }

    public List<Actor> obtenerDestinosCopiaEditar(List<String> lista) {
        List<Actor> ListaRev = new ArrayList<>();
        for (Object value : lista) {
            for (Map.Entry<Long, String> entry : mapaDestinatariosPendientesConCopia.entrySet()) {
                if (entry.getValue().equals(value)) {
                    try {
                        Long id = entry.getKey();
                        Actor actor = actorBean.buscarActor(id);
                        ListaRev.add(actor);
                    } catch (Exception ex) {
                        LOG.info(ex);
                    }
                }
            }
        }
        return ListaRev;
    }

    //--------------------Utilitarios-----------------------------------------------
    //----------------------enviar-----------------------------------
    public void autoConsecutivo() {
        try {
            radicacion = comunicadosBean.consecutivoRadicacionCorreoXLetra("CI");
            comunicadosBean.generarRadicado(radicacion);

        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    //--------------Actores---------------------------------------
    public List<DestinoDTO> llenarInformacion(List<Actor> actores, List<Oficina> oficinas) {
        List<DestinoDTO> lista = new ArrayList<>();

        if (oficinas != null && !oficinas.isEmpty()) {
            for (Oficina oficina : oficinas) {
                if (oficina.getJefeACargo()) {
                    DestinoDTO destino = new DestinoDTO();
                    destino.setId(oficina.getId());
                    destino.setNombreDestino(oficina.getJefe().getNombreYApellido() + "-" + oficina.getDescripcion());
                    destino.setTipoDestino("JEFE");
                    lista.add(destino);
                } else {
                    DestinoDTO destino = new DestinoDTO();
                    destino.setId(oficina.getId());
                    destino.setNombreDestino(oficina.getJefeEncargado().getNombreYApellido() + "-" + oficina.getDescripcion());
                    destino.setTipoDestino("JEFE");
                    lista.add(destino);
                }
            }
        }

        if (actores != null && !actores.isEmpty()) {
            for (Actor actor : actores) {
                DestinoDTO destino = new DestinoDTO();
                destino.setId(actor.getId());
                destino.setNombreDestino(actor.getNombreYApellido());
                destino.setTipoDestino("EMPLEADO");
                lista.add(destino);
            }
        }
        return lista;
    }

    public void cargarDestinatarios() {
        destinations = new ArrayList<>();
        destinations = application.getListaComunicadosInternos();
        destinationsAgregados = new ArrayList<>();
    }

    public void cargarDestinatariosConCopia() {
        try {
            destinationsCopia = new ArrayList<>();
            destinationsCopia = application.getListaComunicadosInternos();
            destinationsAgregadosConCopia = new ArrayList<>();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarDestinatariosPendientes() {
        destinations = new ArrayList<>();
        destinations = application.getListaComunicadosInternos();
        destinosTarget = new ArrayList<>();
    }

    public void cargarDestinatariosPendienteConCopia() {
        try {
            destinationsCopia = new ArrayList<>();
            destinationsCopia = application.getListaComunicadosInternos();
            destinosConCopiaTarget = new ArrayList<>();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarEstadosComunicadoReporte() {
        estadosList = new ArrayList<>();
        estadosList.add(ConstantsMail.APROBADO);
        estadosList.add(ConstantsMail.ANULADO);
        estadosList.add(ConstantsMail.POR_APROBAR);
    }

    //----------------------------------------------------------------
    //---------------------Busqueda----------------------------------
    public void cargarDestinatariosBusqueda() {
        destinationsBusqueda = new ArrayList<>();
        destinationsBusqueda = application.getListaComunicadosInternos();
        destinationsAgregadosBusqueda = new ArrayList<>();
    }

    //---------------------------------------------------------------------------
    //------------------------Reenvio--------------------------------------------
    public void cargarDestinatariosReenvio() {
        destinationsReenvio = new ArrayList<>();
        destinationsReenvio = application.getListaComunicadosInternos();
        destinationsAgregadosReenvio = new ArrayList<>();
    }

    public List<ComunicadosInternos> comunicadosReenviados(String rad) {
        try {
            return comunicadosBean.buscarRenviados(rad);
        } catch (Exception ex) {
            LOG.info(ex);
        }
        return null;
    }

    //---------------------------------------------------------------------------
    public String listaCadena(Long id) {
        StringBuffer cadena = new StringBuffer();
        List<DestinosComunicados> lista = comunicadosBean.consultarDestinosComunicados(id);
        if (!lista.isEmpty()) {
            return convetirOficinaATexto(lista);
        }
        return "";
    }

    public String volver() {
        if (inbox) {
        	graphicImage = null;
            return "volverInboxInicio";
        } else {
        	graphicImage = null;
            return "volver";
        }
    }

    public String mostrarRadicado(ComunicadosInternos mail) {
        if (mail != null) {
            if (StringUtils.isNotBlank(mail.getRadicacion())) {
                return mail.getRadicacion();
            } else if (StringUtils.isNotBlank(mail.getRadicacionReenvio())) {
                return mail.getRadicacionReenvio();
            }
        }
        return "";
    }

    public String mostrarDestinoTabla(ComunicadosInternos mail) {
        if (mail != null) {
            List<DestinosComunicados> destino = comunicadosBean.consultarDestinosComunicados(mail.getId());
            if (destino != null && !destino.isEmpty()) {
                return convetirOficinaATexto(destino);
            }
        }
        return "";
    }

    public String formatoFecha(Date fecha) {
        if (fecha != null) {
            return ValidacionesUtil.formatearFechaHora(fecha);
        } else {
            return "";
        }
    }

    public void extraccion() {
        comunicadosBean.extraccionImagenes(fechaExtIni, fechaExtFin, carpeta);
    }

    public String formatFechaHora(ComunicadosInternos mail) {
        if (mail.getFechaEnvio() != null) {
            return ValidacionesUtil.formatearFechaHora(mail.getFechaEnvio());
        } else if (mail.getFechaCreacion() != null) {
            return ValidacionesUtil.formatearFechaHora(mail.getFechaCreacion());
        } else {
            return "";
        }
    }

    public String textoFirma(String texto) {
        return ValidacionesUtil.textoCapitalCompleto(texto);
    }
    
    public String obtenerFirma() {
    	archFirma = usuarioLogin.getActor().getFirma();
    	return archFirma;
    }

    public void limpiar() {
        radicacion = "";
        archivosGuardar = new ArrayList<>();
        archivosEnviar = new ArrayList<>();
        file = null;
        mensaje = "";
        asunto = "";
        reenvio = false;
        cancelar = false;
        correos = new ArrayList<>();
        cargarDestinatarios();
        cargarDestinatariosReenvio();
        conCopia = false;
        idOficina = null;
        cargarDestinatariosConCopia();
        graphicImage = null;
        archFirma = "";
    }

    public void resetBusqueda() {
        tipoDestinatarios = true;
        cargarDestinatariosBusqueda();
        asuntoBusqueda = "";
        correos = new ArrayList<>();
        radicacionConsulta = "";
        fechaIni = null;
        fechaFin = null;
        fechaIniAll = null;
        fechaFinAll = null;
        estadoReporte = "";
        comunicadosAll = new ArrayList<>();
        sendDestinationBusqueda = new ArrayList<>();
        graphicImage = null;
    }

    public void resetReenvio() {
        cargarDestinatariosReenvio();
        this.mail = null;
        destinationsAgregadosReenvio = new ArrayList<>();
        archivosEnviarReenvio = new ArrayList<>();
        archivosGuardarReenvio = new ArrayList<>();
        comentario = null;
        comentarioShow = null;
        graphicImage = null;
    }

    public String convetirOficinaATexto(List<DestinosComunicados> lista) {
        StringBuffer cadena = new StringBuffer();

        int i = 0;
        if (lista.size() > 0) {
            for (DestinosComunicados destinosComunicados : lista) {
                i++;
                if (destinosComunicados.getTipoDestino().equals("JEFE")) {
                    cadena.append(destinosComunicados.getOficina().getDescripcion());
                } else {
                    cadena.append(destinosComunicados.getDestino().getNombreYApellido());
                }
                if (lista.size() > 1 && lista.size() > i) {
                    cadena.append(",");
                }
            }
        }
        return cadena.toString();
    }

    public Map<String, String> convetirDestinoATexto(List<DestinosComunicados> lista) {
        Map<String, String> cadena = new HashMap<>();

        if (lista != null && !lista.isEmpty()) {
            for (DestinosComunicados dc : lista) {
                if (StringUtils.isNotBlank(dc.getCargoActualDestino())) {
                    cadena.put(dc.getDestino().getNombreYApellido(), dc.getCargoActualDestino());
                } else {
                    cadena.put(dc.getDestino().getNombreYApellido(), "\n");
                }
            }
        }
        return cadena;
    }

    public List<DestinoDTO> convertirDestinosADTO(List<DestinosComunicados> lista) {
        List<DestinoDTO> listaDTO = new ArrayList<>();
        if (lista != null && !lista.isEmpty()) {
            for (DestinosComunicados destinosComunicados : lista) {
                if (destinosComunicados.getTipoDestino().equals("JEFE")) {
                    if (destinosComunicados.getOficina().getJefeACargo()) {
                        DestinoDTO destino = new DestinoDTO();
                        destino.setId(destinosComunicados.getOficina().getId());
                        destino.setNombreDestino(destinosComunicados.getOficina().getJefe().getNombreYApellido() + "-" + destinosComunicados.getOficina().getDescripcion());
                        destino.setTipoDestino("JEFE");
                        listaDTO.add(destino);
                    } else {
                        DestinoDTO destino = new DestinoDTO();
                        destino.setId(destinosComunicados.getOficina().getId());
                        destino.setNombreDestino(destinosComunicados.getOficina().getJefeEncargado().getNombreYApellido() + "-" + destinosComunicados.getOficina().getDescripcion());
                        destino.setTipoDestino("JEFE");
                        listaDTO.add(destino);
                    }
                } else {
                    DestinoDTO destino = new DestinoDTO();
                    destino.setId(destinosComunicados.getDestino().getId());
                    destino.setNombreDestino(destinosComunicados.getDestino().getNombreYApellido());
                    destino.setTipoDestino("EMPLEADO");
                    listaDTO.add(destino);
                }
            }
        }
        return listaDTO;
    }

    public StreamedContent verCorreo() {
        JasperReport reporte;
        JasperPrint reporte_view;

        ComunicadoInternoDTO dto = new ComunicadoInternoDTO();
        //dto.setRadicado(radicacionShow);
        dto.setFecha(fechaShow);

        try {
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String name = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "css" + File.separator + "img" + File.separator + "logoEdeq.png";
            BufferedImage image = ImageIO.read(new File(name));
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("logo", image);
            parameters.put("comunicado", dto);
            String report = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "reportes" + File.separator + "comunicadoInterno.jrxml";
            reporte = JasperCompileManager.compileReport(report);
            byte[] archivo = JasperRunManager.runReportToPdf(reporte, parameters, new JRBeanCollectionDataSource(listaDestinosText.entrySet()));
            ByteArrayInputStream stream = new ByteArrayInputStream(archivo);
            StreamedContent fileGuardar = new DefaultStreamedContent(stream, "application/pdf", "eliminados.pdf");
            if (fileGuardar != null) {
                return fileGuardar;
            }
        } catch (IOException e) {
            LOG.info(e);
        } catch (JRException e) {
            LOG.info(e);
        }
        return null;
    }

    public void generarPdf() {
        try {
            comunicadoHtml = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("page");
            String k = "<html \n"
                    + "    xmlns=\"http://www.w3.org/1999/xhtml\"\n"
                    + "    xmlns:ui=\"http://java.sun.com/jsf/facelets\"\n"
                    + "    xmlns:h=\"http://java.sun.com/jsf/html\"\n"
                    + "    xmlns:f=\"http://java.sun.com/jsf/core\"\n"
                    + "    xmlns:p=\"http://primefaces.org/ui\"\n"
                    + "    xmlns:c=\"http://java.sun.com/jsp/jstl/core\">\n"
                    + "\n"
                    + "    <h:head>\n"
                    + "        <link rel=\"shortcut icon\"  href=\"#{resources['css/img/favicon.ico']}\" type=\"image/x-icon\"/>\n"
                    + "        <link rel=\"icon\" href=\"#{resources['css/img/favicon.ico']}\" type=\"image/x-icon\"/>\n"
                    + "        <f:facet name=\"first\">\n"
                    + "            <meta content='text/html; charset=UTF-8' http-equiv=\"Content-Type\"/>\n"
                    + "            <title>Beesoft</title>\n"
                    + "        </f:facet>\n"
                    + "\n"
                    + "        <script type=\"text/javascript\" src=\"#{resource['js/script.js']}\"></script>\n"
                    + "        <script type=\"text/javascript\" src=\"#{resource['js/bootstrap.min.js']}\"></script>\n"
                    + "        <h:outputStylesheet name=\"css/form.css\"/>\n"
                    + "        <h:outputStylesheet name=\"css/menu.css\"/>\n"
                    + "        <h:outputStylesheet name=\"css/panel.css\"/>\n"
                    + "        <h:outputStylesheet name=\"css/particular.css\"/>\n"
                    + "        <h:outputStylesheet name=\"css/table.css\"/>\n"
                    + "        <h:outputStylesheet name=\"css/widget.css\"/>\n"
                    + "        <h:outputStylesheet name=\"css/general.css\"/>\n"
                    + "        <ui:insert name=\"head\"></ui:insert>\n"
                    + "    </h:head><body><h:form>" + comunicadoHtml + "</h:form></body></html>";
            OutputStream file = new FileOutputStream(new File("E:\\Test.pdf"));
            Document document = new Document();
            PdfWriter.getInstance(document, file);
            document.open();
            HTMLWorker htmlWorker = new HTMLWorker(document);
            htmlWorker.parse(new StringReader(k));
            htmlWorker.setMargins(113, 75, 113, 151);
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-------------------getters y setters-------------------
    public Actor getFrom() {
        return from;
    }

    public void setFrom(Actor from) {
        this.from = from;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<DestinoDTO> getDestinationsAgregados() {
        return destinationsAgregados;
    }

    public void setDestinationsAgregados(List<DestinoDTO> destinationsAgregados) {
        this.destinationsAgregados = destinationsAgregados;
    }

    public List<Oficina> getSendDestination() {
        return sendDestination;
    }

    public void setSendDestination(List<Oficina> sendDestination) {
        this.sendDestination = sendDestination;
    }

    public List<DestinoDTO> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<DestinoDTO> destinations) {
        this.destinations = destinations;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public StringBuilder getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(StringBuilder destinatarios) {
        this.destinatarios = destinatarios;
    }

    public List<ComunicadosInternos> getCorreos() {
        return correos;
    }

    public void setCorreos(List<ComunicadosInternos> correos) {
        this.correos = correos;
    }

    public Long getCorreo() {
        return correo;
    }

    public void setCorreo(Long correo) {
        this.correo = correo;
    }

    public String getAsuntoShow() {
        return asuntoShow;
    }

    public void setAsuntoShow(String asuntoShow) {
        this.asuntoShow = asuntoShow;
    }

    public String getMensajeShow() {
        return mensajeShow;
    }

    public void setMensajeShow(String mensajeShow) {
        this.mensajeShow = mensajeShow;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public String getRadicacionConsulta() {
        return radicacionConsulta;
    }

    public void setRadicacionConsulta(String radicacionConsulta) {
        this.radicacionConsulta = radicacionConsulta;
    }

    public String getRadicacionShow() {
        return radicacionShow;
    }

    public void setRadicacionShow(String radicacionShow) {
        this.radicacionShow = radicacionShow;
    }

    public List<ArchivosAdjuntos> getFiles() {
        return files;
    }

    public void setFiles(List<ArchivosAdjuntos> files) {
        this.files = files;
    }

    public String getAsuntoBusqueda() {
        return asuntoBusqueda;
    }

    public void setAsuntoBusqueda(String asuntoBusqueda) {
        this.asuntoBusqueda = asuntoBusqueda;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Boolean getBoton() {
        return boton;
    }

    public void setBoton(Boolean boton) {
        this.boton = boton;
    }

    public Boolean getReenvio() {
        return reenvio;
    }

    public void setReenvio(Boolean reenvio) {
        this.reenvio = reenvio;
    }

    public Boolean getCancelar() {
        return cancelar;
    }

    public void setCancelar(Boolean cancelar) {
        this.cancelar = cancelar;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Boolean getIsComentario() {
        return isComentario;
    }

    public void setIsComentario(Boolean isComentario) {
        this.isComentario = isComentario;
    }

    public String getComentarioShow() {
        return comentarioShow;
    }

    public void setComentarioShow(String comentarioShow) {
        this.comentarioShow = comentarioShow;
    }

    public Boolean getTipoDestinatarios() {
        return tipoDestinatarios;
    }

    public void setTipoDestinatarios(Boolean tipoDestinatarios) {
        this.tipoDestinatarios = tipoDestinatarios;
    }

    public Usuario getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(Usuario usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public Boolean getInbox() {
        return inbox;
    }

    public void setInbox(Boolean inbox) {
        this.inbox = inbox;
    }

    public Boolean getConCopia() {
        return conCopia;
    }

    public void setConCopia(Boolean conCopia) {
        this.conCopia = conCopia;
    }

    public Boolean getPantallaJefe() {
        return pantallaJefe;
    }

    public void setPantallaJefe(Boolean pantallaJefe) {
        this.pantallaJefe = pantallaJefe;
    }

    public List<Actor> getListaJefes() {
        return listaJefes;
    }

    public void setListaJefes(List<Actor> listaJefes) {
        this.listaJefes = listaJefes;
    }

    public Long getIdJefes() {
        return idJefes;
    }

    public void setIdJefes(Long idJefes) {
        this.idJefes = idJefes;
    }

    public Boolean getAccionEnviar() {
        return accionEnviar;
    }

    public void setAccionEnviar(Boolean accionEnviar) {
        this.accionEnviar = accionEnviar;
    }

    public String getRadicacionShowPendiente() {
        return radicacionShowPendiente;
    }

    public void setRadicacionShowPendiente(String radicacionShowPendiente) {
        this.radicacionShowPendiente = radicacionShowPendiente;
    }

    public String getAsuntoShowPendiente() {
        return asuntoShowPendiente;
    }

    public void setAsuntoShowPendiente(String asuntoShowPendiente) {
        this.asuntoShowPendiente = asuntoShowPendiente;
    }

    public String getMensajeShowPendiente() {
        return mensajeShowPendiente;
    }

    public void setMensajeShowPendiente(String mensajeShowPendiente) {
        this.mensajeShowPendiente = mensajeShowPendiente;
    }

    public String getTituloPendiente() {
        return tituloPendiente;
    }

    public void setTituloPendiente(String tituloPendiente) {
        this.tituloPendiente = tituloPendiente;
    }

    public List<ArchivosAdjuntos> getArchivosAdjuntos() {
        return archivosAdjuntos;
    }

    public void setArchivosAdjuntos(List<ArchivosAdjuntos> archivosAdjuntos) {
        this.archivosAdjuntos = archivosAdjuntos;
    }

    public List<ArchivosAdjuntos> getArchivosNuevos() {
        return archivosNuevos;
    }

    public void setArchivosNuevos(List<ArchivosAdjuntos> archivosNuevos) {
        this.archivosNuevos = archivosNuevos;
    }

    public List<ArchivosAdjuntos> getArchivosEliminados() {
        return archivosEliminados;
    }

    public void setArchivosEliminados(List<ArchivosAdjuntos> archivosEliminados) {
        this.archivosEliminados = archivosEliminados;
    }

    public String getComentarioModiicacion() {
        return comentarioModiicacion;
    }

    public void setComentarioModiicacion(String comentarioModiicacion) {
        this.comentarioModiicacion = comentarioModiicacion;
    }

    public String getComentarioAnulacion() {
        return comentarioAnulacion;
    }

    public void setComentarioAnulacion(String comentarioAnulacion) {
        this.comentarioAnulacion = comentarioAnulacion;
    }

    public Boolean getAccionNoJefe() {
        return accionNoJefe;
    }

    public void setAccionNoJefe(Boolean accionNoJefe) {
        this.accionNoJefe = accionNoJefe;
    }

    public List<ComunicadosInternos> getComunicadosAll() {
        return comunicadosAll;
    }

    public void setComunicadosAll(List<ComunicadosInternos> comunicadosAll) {
        this.comunicadosAll = comunicadosAll;
    }

    public Date getFechaIniAll() {
        return fechaIniAll;
    }

    public void setFechaIniAll(Date fechaIniAll) {
        this.fechaIniAll = fechaIniAll;
    }

    public Date getFechaFinAll() {
        return fechaFinAll;
    }

    public void setFechaFinAll(Date fechaFinAll) {
        this.fechaFinAll = fechaFinAll;
    }

    public Long getIdJefesAprobo() {
        return idJefesAprobo;
    }

    public void setIdJefesAprobo(Long idJefesAprobo) {
        this.idJefesAprobo = idJefesAprobo;
    }

    public Boolean getAccionNoJefeAprobo() {
        return accionNoJefeAprobo;
    }

    public void setAccionNoJefeAprobo(Boolean accionNoJefeAprobo) {
        this.accionNoJefeAprobo = accionNoJefeAprobo;
    }

    public Boolean getAccionReenviar() {
        return accionReenviar;
    }

    public void setAccionReenviar(Boolean accionReenviar) {
        this.accionReenviar = accionReenviar;
    }

    public Long getIdTipoComunicado() {
        return idTipoComunicado;
    }

    public void setIdTipoComunicado(Long idTipoComunicado) {
        this.idTipoComunicado = idTipoComunicado;
    }

    public String getDescripcionTci() {
        return descripcionTci;
    }

    public void setDescripcionTci(String descripcionTci) {
        this.descripcionTci = descripcionTci;
    }

    public StreamedContent getGraphicText() {
        return graphicText;
    }

    public void setGraphicText(StreamedContent graphicText) {
        this.graphicText = graphicText;
    }

    public String getVistoBueno() {
        return vistoBueno;
    }

    public void setVistoBueno(String vistoBueno) {
        this.vistoBueno = vistoBueno;
    }

    public Boolean getComunicadoAnulado() {
        return comunicadoAnulado;
    }

    public void setComunicadoAnulado(Boolean comunicadoAnulado) {
        this.comunicadoAnulado = comunicadoAnulado;
    }

    public String getComentarioAnulado() {
        return comentarioAnulado;
    }

    public void setComentarioAnulado(String comentarioAnulado) {
        this.comentarioAnulado = comentarioAnulado;
    }

    public List<ArchivosAdjuntos> getArchivosGuardar() {
        return archivosGuardar;
    }

    public void setArchivosGuardar(List<ArchivosAdjuntos> archivosGuardar) {
        this.archivosGuardar = archivosGuardar;
    }

    public List<String> getEstadosList() {
        return estadosList;
    }

    public void setEstadosList(List<String> estadosList) {
        this.estadosList = estadosList;
    }

    public String getEstadoReporte() {
        return estadoReporte;
    }

    public void setEstadoReporte(String estadoReporte) {
        this.estadoReporte = estadoReporte;
    }

    public String getFechaShow() {
        return fechaShow;
    }

    public void setFechaShow(String fechaShow) {
        this.fechaShow = fechaShow;
    }

    public String getInboxIn() {
        onTabChange(this.event);
        return inboxIn;
    }

    public void setInboxIn(String inboxIn) {
        this.inboxIn = inboxIn;
    }

    public int getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(int activeIndex) {
        this.activeIndex = activeIndex;
    }

    public String getRedacto() {
        return redacto;
    }

    public void setRedacto(String redacto) {
        this.redacto = redacto;
    }

    public String getReviso() {
        return reviso;
    }

    public void setReviso(String reviso) {
        this.reviso = reviso;
    }

    public String getEnvio() {
        return envio;
    }

    public void setEnvio(String envio) {
        this.envio = envio;
    }

    public String getRadicacionTraza() {
        return radicacionTraza;
    }

    public void setRadicacionTraza(String radicacionTraza) {
        this.radicacionTraza = radicacionTraza;
    }

    public Boolean getTrazaVisible() {
        return trazaVisible;
    }

    public void setTrazaVisible(Boolean trazaVisible) {
        this.trazaVisible = trazaVisible;
    }

    public String getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(String carpeta) {
        this.carpeta = carpeta;
    }

    public Date getFechaExtIni() {
        return fechaExtIni;
    }

    public void setFechaExtIni(Date fechaExtIni) {
        this.fechaExtIni = fechaExtIni;
    }

    public Date getFechaExtFin() {
        return fechaExtFin;
    }

    public void setFechaExtFin(Date fechaExtFin) {
        this.fechaExtFin = fechaExtFin;
    }

    public List<DestinoDTO> getDestinationsAgregadosReenvio() {
        return destinationsAgregadosReenvio;
    }

    public void setDestinationsAgregadosReenvio(List<DestinoDTO> destinationsAgregadosReenvio) {
        this.destinationsAgregadosReenvio = destinationsAgregadosReenvio;
    }

    public List<DestinoDTO> getDestinationsAgregadosBusqueda() {
        return destinationsAgregadosBusqueda;
    }

    public void setDestinationsAgregadosBusqueda(List<DestinoDTO> destinationsAgregadosBusqueda) {
        this.destinationsAgregadosBusqueda = destinationsAgregadosBusqueda;
    }

    public void infojson(Boolean interno) {
        comunicadosBean.generarInfoComunicados(interno);
    }

    public String getUrlServer() {
        return urlServer;
    }

    public void setUrlServer(String urlServer) {
        this.urlServer = urlServer;
    }

    public Boolean getGraphicShow() {
        return graphicShow;
    }

    public void setGraphicShow(Boolean graphicShow) {
        this.graphicShow = graphicShow;
    }

    public Long getIdOficina() {
        return idOficina;
    }

    public void setIdOficina(Long idOficina) {
        this.idOficina = idOficina;
    }

    public Map<String, String> getListaDestinosText() {
        return listaDestinosText;
    }

    public void setListaDestinosText(Map<String, String> listaDestinosText) {
        this.listaDestinosText = listaDestinosText;
    }

    public String getListaDestinosCopiaText() {
        return listaDestinosCopiaText;
    }

    public void setListaDestinosCopiaText(String listaDestinosCopiaText) {
        this.listaDestinosCopiaText = listaDestinosCopiaText;
    }

    public String getFromOficina() {
        return fromOficina;
    }

    public void setFromOficina(String fromOficina) {
        this.fromOficina = fromOficina;
    }

    public String getFromJefe() {
        return fromJefe;
    }

    public void setFromJefe(String fromJefe) {
        this.fromJefe = fromJefe;
    }

    public List<DestinoDTO> getDestinationsAgregadosConCopia() {
        return destinationsAgregadosConCopia;
    }

    public void setDestinationsAgregadosConCopia(List<DestinoDTO> destinationsAgregadosConCopia) {
        this.destinationsAgregadosConCopia = destinationsAgregadosConCopia;
    }

    public List<DestinoDTO> getOficinasAprueban() {
        return oficinasAprueban;
    }

    public void setOficinasAprueban(List<DestinoDTO> oficinasAprueban) {
        this.oficinasAprueban = oficinasAprueban;
    }

    public List<DestinoDTO> getDestinosTarget() {
        return destinosTarget;
    }

    public void setDestinosTarget(List<DestinoDTO> destinosTarget) {
        this.destinosTarget = destinosTarget;
    }

    public List<DestinoDTO> getDestinosConCopiaTarget() {
        return destinosConCopiaTarget;
    }

    public void setDestinosConCopiaTarget(List<DestinoDTO> destinosConCopiaTarget) {
        this.destinosConCopiaTarget = destinosConCopiaTarget;
    }

    public List<Actor> getListaDestinatariosAll() {
        return listaDestinatariosAll;
    }

    public void setListaDestinatariosAll(List<Actor> listaDestinatariosAll) {
        this.listaDestinatariosAll = listaDestinatariosAll;
    }

    public List<ComunicadosInternos> getListaRecibidos() {
        return listaRecibidos;
    }

    public void setListaRecibidos(List<ComunicadosInternos> listaRecibidos) {
        this.listaRecibidos = listaRecibidos;
    }

    public List<ComunicadosInternos> getListaRecibidosFiltered() {
        return listaRecibidosFiltered;
    }

    public void setListaRecibidosFiltered(List<ComunicadosInternos> listaRecibidosFiltered) {
        this.listaRecibidosFiltered = listaRecibidosFiltered;
    }

    public List<ComunicadosInternos> getListaEnviados() {
        return listaEnviados;
    }

    public void setListaEnviados(List<ComunicadosInternos> listaEnviados) {
        this.listaEnviados = listaEnviados;
    }

    public List<ComunicadosInternos> getListaEnviadosFiltered() {
        return listaEnviadosFiltered;
    }

    public void setListaEnviadosFiltered(List<ComunicadosInternos> listaEnviadosFiltered) {
        this.listaEnviadosFiltered = listaEnviadosFiltered;
    }

    public List<ComunicadosInternos> getListaAnulados() {
        return listaAnulados;
    }

    public void setListaAnulados(List<ComunicadosInternos> listaAnulados) {
        this.listaAnulados = listaAnulados;
    }

    public List<ComunicadosInternos> getListaAnuladosFiltered() {
        return listaAnuladosFiltered;
    }

    public void setListaAnuladosFiltered(List<ComunicadosInternos> listaAnuladosFiltered) {
        this.listaAnuladosFiltered = listaAnuladosFiltered;
    }

    public List<ComunicadosInternos> getListaAprobacion() {
        return listaAprobacion;
    }

    public void setListaAprobacion(List<ComunicadosInternos> listaAprobacion) {
        this.listaAprobacion = listaAprobacion;
    }

    public List<ComunicadosInternos> getListaAprobacionFiltered() {
        return listaAprobacionFiltered;
    }

    public void setListaAprobacionFiltered(List<ComunicadosInternos> listaAprobacionFiltered) {
        this.listaAprobacionFiltered = listaAprobacionFiltered;
    }

    public List<ComunicadosInternos> getListaGenerados() {
        return listaGenerados;
    }

    public void setListaGenerados(List<ComunicadosInternos> listaGenerados) {
        this.listaGenerados = listaGenerados;
    }

    public List<ComunicadosInternos> getListaGeneradosFiltered() {
        return listaGeneradosFiltered;
    }

    public void setListaGeneradosFiltered(List<ComunicadosInternos> listaGeneradosFiltered) {
        this.listaGeneradosFiltered = listaGeneradosFiltered;
    }

    public Boolean getModoEmpleado() {
        return modoEmpleado;
    }

    public void setModoEmpleado(Boolean modoEmpleado) {
        this.modoEmpleado = modoEmpleado;
    }

    public String getRedactadoPorPendiente() {
        return redactadoPorPendiente;
    }

    public void setRedactadoPorPendiente(String redactadoPorPendiente) {
        this.redactadoPorPendiente = redactadoPorPendiente;
    }

    public String getComunicadoHtml() {
        return comunicadoHtml;
    }

    public void setComunicadoHtml(String comunicadoHtml) {
        this.comunicadoHtml = comunicadoHtml;
    }

	public String getArchFirma() {
		return archFirma;
	}

	public void setArchFirma(String archFirma) {
		this.archFirma = archFirma;
	}

}
