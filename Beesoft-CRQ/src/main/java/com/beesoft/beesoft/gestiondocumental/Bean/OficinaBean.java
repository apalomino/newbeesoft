/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Serie;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerie;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoRecorrido;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.TrdOld;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class OficinaBean implements OficinaLocal {

    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    @Override
    public void crearRecorrido(String recorrido) throws Exception {
        List<TipoRecorrido> lista = em.createQuery("select r from TipoRecorrido r where r.descripcion=:recorrido").setParameter("recorrido", recorrido).getResultList();

        if (lista != null && !lista.isEmpty()) {
            throw new Exception("El recorrido ya existe");
        } else {
            TipoRecorrido r = new TipoRecorrido();
            r.setDescripcion(recorrido);
            em.persist(r);
        }
    }

    @Override
    public void crearOficina(String codigo, String oficina, TipoRecorrido recorrido) throws Exception {
        List<Oficina> lista = em.createQuery("select o from Oficina o where o.descripcion=:oficina and o.codigo=:codigo and o.tiporecorrido.id=:recorrido").setParameter("oficina", oficina).setParameter("codigo", codigo).setParameter("recorrido", recorrido.getId()).getResultList();

        if (lista != null && !lista.isEmpty()) {
            throw new Exception("La Oficina ya existe en este recorrido");
        } else {
            Oficina o = new Oficina();
            o.setCodigo(codigo);
            o.setDescripcion(oficina);
            o.setTiporecorrido(recorrido);
            o.setActivo(true);
            em.persist(o);
        }
    }

    @Override
    public void editarOficina(Long oficinaId, String codigo, String descripcion) {
        Oficina oficina = em.find(Oficina.class, oficinaId);
        if (oficina != null) {
            oficina.setCodigo(codigo);
            oficina.setDescripcion(descripcion);
            em.merge(oficina);
        }
    }

    @Override
    public void editarSerie(Long serieId, String codigo, String descripcion) {
        Serie serie = em.find(Serie.class, serieId);
        if (serie != null) {
            serie.setDescripcion(descripcion);
            serie.setCodigo(codigo);
            em.merge(serie);
        }
    }

    @Override
    public void editarSubSerie(Long subserieId, String codigo, String descripcion) {
        SubSerie subserie = em.find(SubSerie.class, subserieId);
        if (subserie != null) {
            subserie.setCodigo(codigo);
            subserie.setDescripcion(descripcion);
            em.merge(subserie);
        }
    }

    @Override
    public void editarTipoDoc(Long tipoDocId, String descripcion) {
        TipoDocumento tipoDoc = em.find(TipoDocumento.class, tipoDocId);
        if (tipoDoc != null) {
            tipoDoc.setDescripcion(descripcion);
            em.merge(tipoDoc);
        }
    }

    @Override
    public void crearSerie(String codigo, String serie, Oficina oficina) throws Exception {
        List<Serie> lista = em.createQuery("select s from Serie s where s.descripcion=:serie and s.codigo=:codigo and s.oficina.id=:oficina").setParameter("serie", serie).setParameter("codigo", codigo).setParameter("oficina", oficina.getId()).getResultList();

        if (lista != null && !lista.isEmpty()) {
            throw new Exception("La Serie ya existe en esta Oficina");
        } else {
            Serie s = new Serie();
            s.setCodigo(codigo);
            s.setDescripcion(serie);
            s.setOficina(oficina);
            em.persist(s);
        }
    }

    @Override
    public void crearSubSerie(String codigo, String subserie, Serie serie) throws Exception {
        List<SubSerie> lista = em.createQuery("select s from SubSerie s where s.descripcion=:subserie and s.serie.id=:serie").setParameter("subserie", subserie).setParameter("serie", serie.getId()).getResultList();

        if (lista != null && !lista.isEmpty()) {
            throw new Exception("La SubSerie ya existe en esta Serie");
        } else {
            SubSerie s = new SubSerie();
            s.setCodigo(codigo);
            s.setDescripcion(subserie);
            s.setSerie(serie);
            em.persist(s);
        }
    }

    @Override
    public void crearTipo(String tipo, SubSerie subserie) throws Exception {
        List<TipoDocumento> lista = em.createQuery("select t from TipoDocumento t where t.descripcion=:tipo and t.subserie.id=:subserie").setParameter("tipo", tipo).setParameter("subserie", subserie.getId()).getResultList();

        if (lista != null && !lista.isEmpty()) {
            throw new Exception("El tipo de documento ya existe en esta SubSerie");
        } else {
            TipoDocumento t = new TipoDocumento();
            t.setDescripcion(tipo);
            t.setSubserie(subserie);
            em.persist(t);
        }
    }

    //-------------------------Listas--------------------------------------------
    @Override
    public Map<Long, String> cargarOficinas() throws Exception {
        Map<Long, String> oficinas = new HashMap<>();
        List<Oficina> lista = em.createQuery("select o from Oficina o where o.activo=true and o.esinforme = true").getResultList();

        if (lista != null && !lista.isEmpty()) {
            for (Oficina o : lista) {
                oficinas.put(o.getId(), o.getDescripcion());
            }
            return oficinas;
        } else {
            throw new Exception("No se han cargado oficinas");
        }
    }

    @Override
    public Map<Long, String> cargarOficinasRecorridos() throws Exception {
        Map<Long, String> oficinas = new HashMap<>();
        List<Oficina> lista = em.createQuery("select o from Oficina o and o.esretencion = true").getResultList();

        if (lista != null && !lista.isEmpty()) {
            for (Oficina o : lista) {
                oficinas.put(o.getId(), o.getDescripcion());
            }
            return oficinas;
        } else {
            throw new Exception("No se han cargado oficinas");
        }
    }

    @Override
    public Map<Long, String> cargarSeries(Long idOficina) throws Exception {
        Map<Long, String> series = new HashMap<>();
        List<Serie> lista = em.createQuery("select s from Serie s where s.oficina.id=:oficina").setParameter("oficina", idOficina).getResultList();

        if (lista != null && !lista.isEmpty()) {
            for (Serie s : lista) {
                series.put(s.getId(), s.getDescripcion());
            }
            return series;
        } else {
            throw new Exception("No se han cargado series");
        }
    }

    @Override
    public Map<Long, String> cargarSubSeries(Long idSerie) throws Exception {
        Map<Long, String> subSeries = new HashMap<>();
        List<SubSerie> lista = em.createQuery("select s from SubSerie s where s.serie.id=:serie").setParameter("serie", idSerie).getResultList();

        if (lista != null && !lista.isEmpty()) {
            for (SubSerie s : lista) {
                subSeries.put(s.getId(), s.getDescripcion());
            }
            return subSeries;
        } else {
            throw new Exception("No se han cargado subSeries");
        }
    }

    @Override
    public Map<Long, String> cargarTipoDoc(Long idSubS) throws Exception {

        Map<Long, String> tipos = new HashMap<>();
        List<TipoDocumento> lista = em.createQuery("select t from TipoDocumento t where t.subserie.id=:subserie").setParameter("subserie", idSubS).getResultList();

        if (lista != null && !lista.isEmpty()) {
            for (TipoDocumento s : lista) {
                tipos.put(s.getId(), s.getDescripcion());
            }
            return tipos;
        } else {
            throw new Exception("No se han cargado tipos de documento");
        }
    }

    //---------------------------------------------------------------------
    @Override
    public List<TipoRecorrido> listarRecorridos() throws Exception {
        List<TipoRecorrido> lista = em.createQuery("select r from TipoRecorrido r").getResultList();

        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("No hay recorridos registrados");
        }
    }

    @Override
    public List<Oficina> listarOficinas(Long idRecorrido) throws Exception {
        List<Oficina> lista = em.createQuery("select r from Oficina r where r.tiporecorrido.id=:id and r.activo=true and r.esretencion = true").setParameter("id", idRecorrido).getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("No hay Oficinas registradas");
        }
    }

    @Override
    public List<Serie> listarSeries(Long idOficina) throws Exception {
        List<Serie> lista = em.createQuery("select r from Serie r where r.oficina.id=:id and r.esretencion = true").setParameter("id", idOficina).getResultList();

        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("No hay Series registradas");
        }
    }

    @Override
    public List<SubSerie> listarSubSeries(Long idSerie) throws Exception {
        List<SubSerie> lista = em.createQuery("select r from SubSerie r where r.serie.id=:id and r.esretencion = true").setParameter("id", idSerie).getResultList();

        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("No hay SubSeries registradas");
        }
    }

    @Override
    public List<TipoDocumento> listarTipoDocumentos(Long idSub) throws Exception {
        List<TipoDocumento> lista = em.createQuery("select r from TipoDocumento r where r.subserie.id=:id").setParameter("id", idSub).getResultList();

        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("No hay Tipo de documentos registrados");
        }
    }

    @Override
    public Oficina buscarOficina(Long id) throws Exception {
        Oficina o = em.find(Oficina.class, id);

        if (o != null) {
            return o;
        } else {
            throw new Exception("Oficina no existe");
        }
    }

    @Override
    public Serie buscarSerie(Long id) throws Exception {
        Serie o = em.find(Serie.class, id);

        if (o != null) {
            return o;
        } else {
            throw new Exception("Serie no existe");
        }
    }

    @Override
    public SubSerie buscarSubserie(Long id) throws Exception {
        SubSerie o = em.find(SubSerie.class, id);

        if (o != null) {
            return o;
        } else {
            throw new Exception("Subserie no existe");
        }
    }

    @Override
    public TipoDocumento buscarTipodocumento(Long id) throws Exception {
        TipoDocumento o = em.find(TipoDocumento.class, id);

        if (o != null) {
            return o;
        } else {
            throw new Exception("Tipo de documento no existe");
        }
    }

    @Override
    public List<Trd> buscarTrds(Long id) throws Exception {
        List<Trd> trds = em.createQuery("select t from Trd t where t.documento.id=:id").setParameter("id", id).getResultList();
        if (trds != null && !trds.isEmpty()) {
            return trds;
        } else {
            throw new Exception("Este documento no se le asigno tipo documental");
        }
    }
    
    @Override
    public List<TrdOld> buscarTrdsOld(Long id) throws Exception {
        List<TrdOld> trds = em.createQuery("select t from TrdOld t where t.documento.id=:id").setParameter("id", id).getResultList();
        if (trds != null && !trds.isEmpty()) {
            return trds;
        } else {
            throw new Exception("Este documento no se le asigno tipo documental");
        }
    }

    @Override
    public Map<Long, String> cargarTipoRecorrido() throws Exception {
        Map<Long, String> recorridoMapa = new HashMap<>();
        List<TipoRecorrido> recorridos = em.createQuery("select e from TipoRecorrido e").getResultList();

        if (recorridos != null && !recorridos.isEmpty()) {
            for (TipoRecorrido ec : recorridos) {
                recorridoMapa.put(ec.getId(), ec.getDescripcion());
            }
            return recorridoMapa;
        } else {
            throw new Exception("No se han precargado los recorridos");
        }
    }

    @Override
    public TipoRecorrido buscarTipoRecorrido(Long id) throws Exception {
        TipoRecorrido ec = em.find(TipoRecorrido.class, id);
        if (ec != null) {
            return ec;
        } else {
            throw new Exception("El Recorrido de correo no existe");
        }
    }

    @Override
    public void cambiarEstado(Long oficinaId, Boolean estado) {
        Oficina oficina = em.find(Oficina.class, oficinaId);
        if (oficina != null) {
            oficina.setActivo(estado);
            em.merge(oficina);
        }
    }

	@Override
	public Oficina buscarOficinaPorNombre(String nombreOficina) {
		if (nombreOficina != null && nombreOficina!="" ) {
			String consulta = "select ofi from Oficina ofi where ofi.descripcion= :descripcion and esretencion = true";	
			List<Oficina> oficina = em.createQuery(consulta).setParameter("descripcion", nombreOficina).getResultList();
			if (oficina.size()>0) {
				return oficina.get(0);
			}else
				return null;
		}else {
			return null;
		}
	}

}
