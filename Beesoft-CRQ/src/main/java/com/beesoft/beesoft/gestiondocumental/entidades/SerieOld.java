package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * The persistent class for the serie database table.
 *
 */
@Entity
public class SerieOld implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Boolean activo;

    private String descripcion;

    private String codigo;

    private OficinaOld oficina;

    public SerieOld() {
    }

    public SerieOld(Long id, Boolean activo, String descripcion, String codigo, OficinaOld oficina) {
        this.id = id;
        this.activo = activo;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.oficina = oficina;
    }
    

    @Id
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ManyToOne
    public OficinaOld getOficinaOld() {
        return this.oficina;
    }

    public void setOficinaOld(OficinaOld oficina) {
        this.oficina = oficina;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
