/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

/**
 *
 * @author Cristhian Camilo Fernandez Valencia <cfernandez@sumset.com>
 */
public class RadicadosDTO {
    
    private String radicacion;
    private String procedencia;
    private String destino;
    private String asunto;
    private String fecha;
    private String tipo;

    public RadicadosDTO() {
    }
    
    public RadicadosDTO(String radicacion, String procedencia, String destino, String asunto, String fecha, String tipo) {
        this.radicacion = radicacion;
        this.procedencia = procedencia;
        this.destino = destino;
        this.asunto = asunto;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public String getRadicacion() {
        return radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    
    
    
}
