package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the datosconfiguracion database table.
 *
 */
@Entity
public class DatosConfiguracion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String nit;

    private String dominioEmail;

    private String emailEmisor;

    private Boolean estado;

    private String host;
    
    private String usuario;

    private String password;

    private String proveedor;

    private String puerto;

    private String starttls;

    private String authetication;
    
    private Date fechaCron;

    public DatosConfiguracion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNit() {
        return this.nit;
    }

    public String getAuthetication() {
        return authetication;
    }

    public void setAuthetication(String authetication) {
        this.authetication = authetication;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDominioEmail() {
        return this.dominioEmail;
    }

    public void setDominioEmail(String dominioEmail) {
        this.dominioEmail = dominioEmail;
    }

    public String getEmailEmisor() {
        return this.emailEmisor;
    }

    public void setEmailEmisor(String emailEmisor) {
        this.emailEmisor = emailEmisor;
    }

    public Boolean getEstado() {
        return this.estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProveedor() {
        return this.proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getPuerto() {
        return this.puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getStarttls() {
        return this.starttls;
    }

    public void setStarttls(String starttls) {
        this.starttls = starttls;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String user) {
        this.usuario = user;
    }

	public Date getFechaCron() {
		return fechaCron;
	}

	public void setFechaCron(Date fechaCron) {
		this.fechaCron = fechaCron;
	}
    
}
