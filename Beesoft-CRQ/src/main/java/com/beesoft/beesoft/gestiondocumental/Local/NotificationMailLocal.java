/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.Util.NotificationMailDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import com.beesoft.beesoft.gestiondocumental.entidades.NotificationMail;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface NotificationMailLocal {

    /**
     * Metodo para enviar los correos encolados.
     *
     * @param mail, informacion del correo.
     */
    public void enviarCorreo(NotificationMailDTO mail);

    /**
     * Metodo para encolar los correos.
     *
     * @param radicacion
     * @param emisor, persona que envia correo.
     * @param destinations, lista de destinatarios.
     * @param asunto, asunto del correo.
     * @param mensaje, mensaje del correo.
     * @param adjuntosGuardar
     * @param adjuntosEnviar
     */
    public void guardarCorreo(Actor emisor, List<Actor> destinations, String asunto,
            String mensaje, List<ArchivosAdjuntos> adjuntos, String moduloEnvio, Boolean certificacion);

    /**
     * Metodo para cargar la lista de posibles de destinatarios.
     *
     * @return lista de destinatarios.
     */
    public List<Actor> cargarListaDestinatarios();

    /**
     * Metodo para cargar la lista de todos los destinatarios .
     *
     * @return lista de destinatarios.
     */
    public List<Actor> cargarTodosDestinatarios();

    /**
     * Metodo para buscar correos segun destinos y el asunto.
     *
     * @param radicacion
     * @param actores, lista de destinos.
     * @param asunto, asunto del correo.
     * @param fechaIni, rango de fecha inicial.
     * @param fechaFin, rango de fecha final.
     * @return lista de correos relacionada.
     */
    public List<NotificationMail> buscarCorreo(String radicacion, List<Actor> actores, String asunto, Date fechaIni, Date fechaFin);

    /**
     * Metodo para buscar un correo por identificador.
     *
     * @param id, identificador del correo.
     * @return correo.
     */
    public NotificationMail buscarCorreoId(Long id);

    /**
     * Metodo para buscar los concecutivos de radicacion para correo.
     *
     * @return lista de concecutivos.
     * @throws Exception si hay algun error.
     */
    public List<String> consecutivoRadicacionCorreo() throws Exception;

    public List<NotificationMail> buscarRenviados(String radicacion) throws Exception;

    public String buscarConsecutivoCorreo(String radicacion);

    public String consecutivoRadicacionCorreoXLetra(String letra) throws Exception;

    public void generarRadicado(String cadena);

    public List<NotificationMail> buscarComunicadosInOut(Actor actor, String mensaje, Boolean inOut) throws Exception;

    public List<Oficina> cargarListaDestinatariosOficina();
    
}
