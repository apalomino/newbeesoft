package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


/**
 * The persistent class for the tipodocumento database table.
 * 
 */
@Entity
public class TipoDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private Boolean activo;

	private String descripcion;

	private SubSerie subserie;

	public TipoDocumento() {
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@ManyToOne
	public SubSerie getSubserie() {
		return this.subserie;
	}

	public void setSubserie(SubSerie subserie) {
		this.subserie = subserie;
	}

}