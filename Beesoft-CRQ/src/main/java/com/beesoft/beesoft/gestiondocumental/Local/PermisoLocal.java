/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Menu;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import com.beesoft.beesoft.gestiondocumental.entidades.Permiso;
import java.util.List;
import java.util.TreeMap;
import javax.ejb.Local;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 */
@Local
public interface PermisoLocal {

    /**
     * Metodo encargado de crear permisos.
     *
     * @param menu, menu al que se va asignar permiso de acceder.
     * @param perfil, perfil al que se le concede el acceso.
     * @param permission, tiene acceso: si o no.
     */
    public void gestionarPermisos(Menu menu, Perfil perfil, Boolean permission);

    /**
     * Metodo encargado de cargar el menu completamente dependiendo o no del
     * perfil.
     *
     * @param idPerfil, identificador del perfil.
     * @return mapa que contiene todas las rutas del menu.
     */
    public List<Menu> cargarMenu(Long idPerfil);

    /**
     * Metodo encargado de buscar un permiso por pefil y menu.
     *
     * @param idPerfil, identificador del perfil.
     * @param idMenu, identificador del menu.
     * @return permiso.
     */
    public Permiso buscarMenuPerfil(Long idPerfil, Long idMenu);

    /**
     * Metodo encargado de buscar permisos segun menu y perfil.
     *
     * @param idMenu, identificador del menu.
     * @param idPerfil, identificador del perfil.
     * @return lista de permisos.
     * @throws Exception si hay algun error.
     */
    public List<Permiso> buscarPermisos(Long idMenu, Long idPerfil) throws Exception;

    /**
     *
     * @param idPermiso
     */
    public void eliminarPermiso(Long idPermiso);

    /**
     * Metodo encargado de cargar las opciones del menu que son raiz.
     *
     * @param idPerfil, identificador del perfil.
     * @return lista de menu raiz.
     */
    public List<Menu> cargarRaiz(Long idPerfil);

    /**
     * Metodo encargado de cargar el menu que es submenu.
     *
     * @param idPerfil, identificador del perfil.
     * @param parent, identificador del menu raiz.
     * @return lista de submenu.
     */
    public List<Menu> cargarSubMenu(Long idPerfil, Long parent);

    /**
     * Metodo encargado de cargar la lista de menu de url.
     *
     * @param idPerfil, identificador del perfil.
     * @param parent, identificador del submenu.
     * @return lista de urls.
     */
    public List<Menu> cargarUrl(Long idPerfil, Long parent);

    public List<Menu> cargarMenuCompleto();

    public List<Menu> cargarPadresMenu();

    public void crearMenu(String etiqueta, String descripcion, String tipo, Long padreId, Integer orden);
}
