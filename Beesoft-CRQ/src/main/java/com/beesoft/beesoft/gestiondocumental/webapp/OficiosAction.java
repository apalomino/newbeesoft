/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ClaseLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficioLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.Serie;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerie;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Cristhian
 */
@Named("oficiosAction")
@SessionScoped
public class OficiosAction implements Serializable {

	private static final Log LOG = LogFactory.getLog(OficiosAction.class);

	@EJB
	private OficioLocal oficioBean;

	@EJB
	private DocumentoLocal documentoBean;

	@EJB
	private ActorLocal actorBean;

	@EJB
	private OficinaLocal oficinaBean;

	@EJB
	private ClaseLocal claseBean;

	// ------------atributos a guardar-------------------------
	private String radicacion;
	private Date fechaOficio;
	private String clase;
	private String titulo;
	private String asunto;
	private String contenido;
	private List<ArchivoDocumento> archivos = new ArrayList<ArchivoDocumento>();
	private Documento documentoAsociado;
	private List<Trd> listaTrd = new ArrayList<Trd>();
	private String elaboro;
	private String aprobo;
	private String version;
	private String modificacion;
	private List<Actor> listaDestino = new ArrayList<Actor>();
	private Actor radicador;
	private String letra;
	private List<Documento> listaRef;
	// ----------------------------------------------------
	// ------------atributos a editar-------------------------
	private String radicacionEditar;
	private String fechaOficioEditar;
	private String claseEditar;
	private String tituloEditar;
	private String asuntoEditar;
	private String contenidoEditar;
	private ArchivoDocumento archivoSelected;
	private List<ArchivoDocumento> archivosEditar = new ArrayList<>();
	private List<ArchivoDocumento> archivosEditarNuevos = new ArrayList<>();
	private List<ArchivoDocumento> archivosEditarEliminados = new ArrayList<>();
	private Documento documentoAsociadoEditar;
	private List<Trd> listaTrdEditar = new ArrayList<>();
	private List<Trd> listaTrdEditarNuevas = new ArrayList<>();
	private List<Trd> listaTrdEditarEliminar = new ArrayList<>();
	private String elaboroEditar;
	private String aproboEditar;
	private String versionEditar;
	private String modificacionEditar;
	private List<Actor> listaDestinoEditar = new ArrayList<Actor>();
	private Actor radicadorEditar;
	private String letraEditar;
	private List<Documento> listaRefEditar;
	private Trd outTrdEditar;
	private List<String> destinosTablaEditar = new ArrayList<String>();
	private String outDestinoEditar;
	private Long idEditar;
	private DocumentoDTO oficeOld;

	// ----------------------------------------------------
	// ------------------utilidaddes----------------------
	private List<Actor> destinosBusqueda = new ArrayList<Actor>();
	private List<String> destinosTabla = new ArrayList<String>();
	private String nombreBusqueda;
	private String outDestino;
	private List<String> clasesDocumento;
	private Trd outTrd;
	private int goCreate = 5;
	private int goEdit = 3;
	// ------------------------buscar oficio-----------------------------------
	private String radicacionOficio;
	private Date fechaConsultaIni;
	private Date fechaConsultaFin;
	private String contenidoConsulta;
	private String claseConsulta;
	private List<Documento> listaOficios;
	private List<DocumentoDTO> listaOficiosOld;
	// -----------------------ver oficio---------------------------------------
	private String radicacionVer;
	private String fechaOficioVer;
	private String claseVer;
	private String tituloVer;
	private String asuntoVer;
	private String contenidoVer;
	private List<ArchivoDocumento> archivosVer = new ArrayList<ArchivoDocumento>();
	private String documentoAsociadoVer;
	private List<Trd> listaTrdVer = new ArrayList<Trd>();
	private String elaboroVer;
	private String aproboVer;
	private String versionVer;
	private String modificacionVer;
	private String listaDestinoVer;
	private Boolean OficioOld;
	private String urlServer = "";

	// ----------------------buscar Archivo Asociado-------------------------------
	private String radicacionConsulta;
	private Actor actorProcedenciaConsulta;
	private Actor actorDestinoConsulta;
	private Actor actorRevisorConsulta;
	private Date fechaEmisionInicial;
	private Date fechaEmisionFinal;
	private String asuntoConsulta;
	private String procedenciaName;
	private String destinoName;
	private String revisorName;
	private List<Actor> revisorIn;
	private List<Actor> procedenciaIn;
	private Map<Long, String> revisorMap;
	private List<Documento> resultadosDocs;
	private Usuario usuarioLogin;
	private String oficinaValue;
	private Map<Long, String> oficinas;
	private Oficina oficina;
	private String serieValue;
	private Map<Long, String> series;
	private Serie serie;
	private String subserieValue;
	private Map<Long, String> subseries;
	private SubSerie subserie;
	private String tipoDocValue;
	private Map<Long, String> tiposDoc;
	private TipoDocumento tipoDoc;
	// --------------------------------------------------------------------

	public void crearOficio() {
		try {
			int validate = 0;
			Boolean valRad = ValidacionesUtil.validarCadenaRadicado(letra);
			if (valRad) {
				radicacion = letra;
			}

			radicador = usuarioLogin.getActor();

			validate += ValidacionesUtil.validarCadena(radicacion, "Radicación");
			validate += ValidacionesUtil.validarCadena(clase, "Clase");
			validate += ValidacionesUtil.validarCadena(asunto, "Asunto");
			validate += ValidacionesUtil.validarListas(archivos, "Archivo");
			validate += ValidacionesUtil.validarListas(listaDestino, "Destino");

			if (validate == goCreate) {
				oficioBean.crearOficio(radicacion, clase, fechaOficio, titulo, asunto, contenido, listaTrd, elaboro,
						aprobo, version, modificacion, radicador, documentoAsociado, listaDestino, archivos);
				ValidacionesUtil
						.addMessageInfo("Se creo el Oficio de tipo " + clase + " con el radicado " + radicacion);
				limpiar();
				limpiarModal();
			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
	}

	public String editarOficio() {
		try {
			int validate = 0;

			validate += ValidacionesUtil.validarCadena(asuntoEditar, "Asunto");
			validate += ValidacionesUtil.validarListas(listaDestinoEditar, "Destino");
			if (archivosEditarNuevos != null && !archivosEditarNuevos.isEmpty()
					|| archivosEditar != null && !archivosEditar.isEmpty()) {
				validate++;
			}
			if (validate == goEdit) {
				oficioBean.editarOficio(idEditar, tituloEditar, asuntoEditar, contenidoEditar, listaTrdEditarNuevas,
						listaTrdEditarEliminar, elaboroEditar, aproboEditar, versionEditar, modificacionEditar,
						documentoAsociadoEditar, listaDestinoEditar, archivosEditarNuevos, archivosEditarEliminados);
				limpiarEditar();
				limpiarModal();
				buscarOficios();
				return "editado";
			}
		} catch (Exception ex) {
			LOG.info(ex);
			ValidacionesUtil.addMessage(ex.getMessage());
		}
		return "";
	}

	public void buscarOficios() {
		try {

			if (fechaConsultaIni != null && fechaConsultaFin != null) {
				ValidacionesUtil.validarFechasIniFin(fechaEmisionInicial, fechaEmisionFinal);
				listaOficiosOld = oficioBean.buscarOficioOld(radicacionOficio, fechaConsultaIni, fechaConsultaFin,
						contenidoConsulta, claseConsulta);
				listaOficios = oficioBean.buscarOficios(radicacionOficio, fechaConsultaIni, fechaConsultaFin,
						contenidoConsulta, claseConsulta);
				listaOficiosOld.addAll(addListaDTO(listaOficios));
			} else if (fechaConsultaIni != null && fechaConsultaFin == null) {
				ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha de Emision Final");
			} else if (fechaConsultaIni == null && fechaConsultaFin != null) {
				ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha de Emision Inicial");
			} else if (fechaConsultaIni == null && fechaConsultaFin == null) {
				listaOficiosOld = oficioBean.buscarOficioOld(radicacionOficio, fechaConsultaIni, fechaConsultaFin,
						contenidoConsulta, claseConsulta);
				listaOficios = oficioBean.buscarOficios(radicacionOficio, fechaConsultaIni, fechaConsultaFin,
						contenidoConsulta, claseConsulta);
				listaOficiosOld.addAll(addListaDTO(listaOficios));
			}

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public List<DocumentoDTO> addListaDTO(List<Documento> lista) {
		List<DocumentoDTO> listaDTO = new ArrayList<>();
		for (Documento doc : lista) {
			DocumentoDTO dto = null;
			dto = new DocumentoDTO(doc, doc.getRadicacion(), doc.getFechaEmision(), doc.getFechaSistema(),
					doc.getAsunto(), doc.getObservacion(), doc.getClasedocumento(), Boolean.FALSE);
			listaDTO.add(dto);
		}
		return listaDTO;
	}

	public String verOficio(DocumentoDTO oficio) {

		oficeOld = oficio;

		String redirect = "";
		if (oficio.getOld()) {
			redirect = asignarInformacionOld(oficio);
			return redirect;
		} else if (oficio.getDocumento() != null) {
			redirect = asignarInformacion(oficio.getDocumento());
			return redirect;
		} else {
			ValidacionesUtil.addMessage("No ha seleccionado un documento");
			return "failed";
		}
	}

	public String asignarInformacion(Documento oficio) {
		radicacionVer = oficio.getRadicacion();
		fechaOficioVer = ValidacionesUtil.formatearFecha(oficio.getFechaEmision());
		claseVer = oficio.getClasedocumento();
		tituloVer = oficio.getUbicacionFisica();
		asuntoVer = oficio.getAsunto();
		contenidoVer = oficio.getObservacion();
		if (oficio.getDocumentoReferencia() != null) {
//            documentoAsociadoVer = oficio.getDocumentoReferencia().getRadicacion();
		} else {
			documentoAsociadoVer = "";
		}
		try {
			listaTrdVer = oficinaBean.buscarTrds(oficio.getId());
		} catch (Exception ex) {
			LOG.info(ex);
		}
		List<ArchivoDocumento> listaArchivos = documentoBean.consultarArchivoDocumento(oficio.getId());
		archivosVer = listaArchivos;
		listaTrd = new ArrayList<>();
		elaboroVer = oficio.getAnexos();
		aproboVer = oficio.getEmpresacorreoEnviado();
		versionVer = oficio.getGuia();
		modificacionVer = oficio.getEmpresaCorreoRecibido();
		List<Actor> listaDestinos = documentoBean.consultarDestinoDocumento(oficio.getId());
		listaDestinoVer = ValidacionesUtil.listToCadena(listaDestinos);
		OficioOld = false;
		return "verOficio";
	}

	public Boolean getOficioOld() {
		return OficioOld;
	}

	public void setOficioOld(Boolean OficioOld) {
		this.OficioOld = OficioOld;
	}

	public String asignarInformacionOld(DocumentoDTO oficio) {
		radicacionVer = oficio.getRadicacion();
		fechaOficioVer = ValidacionesUtil.formatearFecha(oficio.getFechaEmision());
		claseVer = oficio.getClase();
		tituloVer = oficio.getUbicacionFisica();
		asuntoVer = oficio.getAsunto();
		contenidoVer = oficio.getObservacion();
		elaboroVer = oficio.getRadicador();
		OficioOld = true;
		try {
			listaDestinoVer = ValidacionesUtil.organizarCadenaXComas(oficioBean.buscarDestinosOld(oficio.getId()));
		} catch (Exception ex) {
			Logger.getLogger(OficiosAction.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "verOld";
	}

	public String selectOficioEditar(DocumentoDTO oficioDto) {
		if (!oficioDto.getOld() && oficioDto.getDocumento() != null) {
			Documento oficio = oficioDto.getDocumento();
			idEditar = oficio.getId();
			radicacionEditar = oficio.getRadicacion();
			fechaOficioEditar = ValidacionesUtil.formatearFecha(oficio.getFechaEmision());
			claseEditar = oficio.getClasedocumento();
			tituloEditar = oficio.getUbicacionFisica();
			asuntoEditar = oficio.getAsunto();
			contenidoEditar = oficio.getObservacion();
			if (oficio.getDocumentoReferencia() != null) {
//                documentoAsociadoEditar = oficio.getDocumentoReferencia();
//                selectDocumentoReferenciaEditar(documentoAsociadoEditar);
			} else {
				documentoAsociadoEditar = null;
			}
			try {
				listaTrdEditar = oficinaBean.buscarTrds(oficio.getId());
			} catch (Exception ex) {
				LOG.info(ex);
			}
			List<ArchivoDocumento> listaArchivos = documentoBean.consultarArchivoDocumento(oficio.getId());
			archivosEditar = listaArchivos;

			elaboroEditar = oficio.getAnexos();
			aproboEditar = oficio.getEmpresacorreoEnviado();
			versionEditar = oficio.getGuia();
			modificacionEditar = oficio.getEmpresaCorreoRecibido();
			List<Actor> listaDestinos = documentoBean.consultarDestinoDocumento(oficio.getId());
			listaDestinoEditar = listaDestinos;
			for (Actor a : listaDestinoEditar) {
				destinosTablaEditar.add(a.getNombreYApellido());
			}
			return "editarOficio";
		}
		return "failed";
	}

	// ------------------util------------------------------------------
	@PostConstruct
	public void iniciar() {
		cargarClases();
		cargarOficinas();

		fechaOficio = new Date();
		buscarRevisoresSearch();
		if (FacesContext.getCurrentInstance() != null) {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
					.getSession(false);
			usuarioLogin = (Usuario) session.getAttribute("username");
		}
	}

	public String formatFecha(Date fecha) {
		return ValidacionesUtil.formatearFecha(fecha);
	}

	public String completeConsecutivo() {
		try {
			if (letra.equals("C") || letra.equals("M") || letra.equals("MA")) {
				radicacion = documentoBean.consecutivoRadicacionOficio(letra);
				documentoBean.generarRadicado(radicacion);
				cambiarClase(letra);
				return radicacion;
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public void cambiarClase(String letra) {
//        if (letra.equals("C")) {
//            clase = claseBean.buscarClase(ConstantsMail.CLASE_CIRCULAR);
//        } else if (letra.equals("M")) {
//            clase = claseBean.buscarClase(ConstantsMail.CLASE_MEMORANDO);
//        } else if (letra.equals("MA")) {
//            clase = claseBean.buscarClase(ConstantsMail.CLASE_MANUAL);
//        }
	}

	public void handleFileUpload(FileUploadEvent event) throws Exception {
		String fileName = FilenameUtils.getName(event.getFile().getFileName());
		archivos.add(fileToAdjunto(event.getFile()));
	}

	public void handleFileUploadEdit(FileUploadEvent event) throws Exception {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"El documento " + event.getFile().getFileName() + " se cargo", null);
		FacesContext.getCurrentInstance().addMessage("info", msg);
		archivosEditarNuevos.add(fileToAdjunto(event.getFile()));
	}

	public void quitarArchivo() {
		archivosEditar.remove(archivoSelected);
		archivosEditarEliminados.add(archivoSelected);
	}

	public ArchivoDocumento fileToAdjunto(UploadedFile file) {
		ArchivoDocumento aa = new ArchivoDocumento();
		aa.setNombreArchivo(FilenameUtils.getName(file.getFileName()));
		aa.setArchivo(file.getContents());
		return aa;
	}

	public void buscarDestinos() {
		try {
			destinosBusqueda = actorBean.listaProcedencia(nombreBusqueda, null, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void selectDestino(Actor actorDestino) {

		if (actorDestino != null) {
			if (destinosTabla != null && !destinosTabla.isEmpty()) {
				if (!destinosTabla.contains(actorDestino.getNombreYApellido())) {
					destinosTabla.add(actorDestino.getNombreYApellido());
					listaDestino.add(actorDestino);
				}
			} else {
				destinosTabla.add(actorDestino.getNombreYApellido());
				listaDestino.add(actorDestino);
			}
			iniciarDialog();
		}
	}

	public void selectDestinoEditar(Actor actorDestino) {

		if (actorDestino != null) {
			if (destinosTablaEditar != null && !destinosTablaEditar.isEmpty()) {
				if (!destinosTablaEditar.contains(actorDestino.getNombreYApellido())) {
					destinosTablaEditar.add(actorDestino.getNombreYApellido());
					listaDestinoEditar.add(actorDestino);
				}
			} else {
				destinosTablaEditar.add(actorDestino.getNombreYApellido());
				listaDestinoEditar.add(actorDestino);
			}

			iniciarDialog();
		}
	}

	public void quitarDestino() {
		Actor out = null;
		for (Actor actor : listaDestino) {
			if (actor.getNombreYApellido().equals(outDestino)) {
				destinosTabla.remove(outDestino);
				out = actor;
			}
		}
		listaDestino.remove(out);

	}

	public void quitarDestinoEditar() {
		Actor out = null;
		for (Actor actor : listaDestinoEditar) {
			if (actor.getNombreYApellido().equals(outDestinoEditar)) {
				destinosTablaEditar.remove(outDestinoEditar);
				out = actor;
			}
		}
		listaDestinoEditar.remove(out);

	}

	public void quitarDocumentoAsociado() {
		listaRef.remove(documentoAsociado);
	}

	public void quitarDocumentoAsociadoEditar() {
		listaRefEditar.remove(documentoAsociadoEditar);
	}

	public void quitarTrd() {
		listaTrd.remove(outTrd);
	}

	public void quitarTrdEditar() {
		listaTrdEditar.remove(outTrdEditar);
		listaTrdEditarEliminar.add(outTrdEditar);
	}

	public void buscarRevisoresSearch() {
		try {
			revisorIn = actorBean.listaUsuarios();
			revisorMap = new HashMap<Long, String>();
			for (Actor r : revisorIn) {
				revisorMap.put(r.getId(), r.getNombreYApellido());

			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void obtenerRevisor() {
		try {
			Long id = ValidacionesUtil.obtenerIdFromMap(revisorMap, revisorName);
			actorRevisorConsulta = actorBean.buscarActor(id);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarDocumento() {
		try {
			obtenerRevisor();
			resultadosDocs = documentoBean.buscarDocumento(radicacionConsulta, fechaEmisionInicial, fechaEmisionFinal,
					actorProcedenciaConsulta, actorDestinoConsulta, asuntoConsulta, "", "", actorRevisorConsulta, null,
					null, null, "");

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void selectDocumentoReferencia(Documento documentoAsociado) {
		if (documentoAsociado != null) {
			this.documentoAsociado = documentoAsociado;
			listaRef = new ArrayList<>();
			listaRef.add(documentoAsociado);
			limpiarModal();
		}
	}

	public void selectDocumentoReferenciaEditar(Documento documentoAsociado) {
		if (documentoAsociado != null) {
			this.documentoAsociadoEditar = documentoAsociado;
			listaRefEditar = new ArrayList<Documento>();
			listaRefEditar.add(documentoAsociadoEditar);
			limpiarModal();
		}
	}

	public void buscarProcedencia() {
		try {
			procedenciaIn = actorBean.listaProcedencia(nombreBusqueda, null, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void selectProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		if (actorProcedenciaConsulta != null) {
			this.actorProcedenciaConsulta = actorProcedenciaConsulta;
			procedenciaName = actorProcedenciaConsulta.getNombreYApellido();
			iniciarDialogSearch();
		}
	}

	public void selectDestinoConsulta(Actor actorDestinoConsulta) {
		if (actorDestinoConsulta != null) {
			this.actorDestinoConsulta = actorDestinoConsulta;
			destinoName = actorDestinoConsulta.getNombreYApellido();
			iniciarDialogSearch();
		}
	}

	public void iniciarDialogSearch() {
		destinosBusqueda = new ArrayList<Actor>();
		procedenciaIn = new ArrayList<Actor>();
		revisorIn = new ArrayList<Actor>();
		nombreBusqueda = "";
	}

	public void iniciarDialog() {
		destinosBusqueda = new ArrayList<Actor>();
		procedenciaIn = new ArrayList<Actor>();
		revisorIn = new ArrayList<Actor>();
		resultadosDocs = new ArrayList<Documento>();
		radicacionConsulta = "";
		fechaEmisionFinal = null;
		fechaEmisionInicial = null;
		nombreBusqueda = "";
		subserie = null;
		serie = null;
		tipoDoc = null;
		oficina = null;
		oficinaValue = "";
		oficinaValue = "";
		serieValue = "N.A";
		subserieValue = "N.A";
		tipoDocValue = "N.A";
	}

	public void limpiarModal() {
		fechaEmisionInicial = null;
		fechaEmisionFinal = null;
		radicacionConsulta = "";
		asuntoConsulta = "";
		destinosBusqueda = new ArrayList<Actor>();
		procedenciaName = "";
		destinoName = "";
		procedenciaIn = new ArrayList<Actor>();
		resultadosDocs = new ArrayList<Documento>();
	}

	public void cargarOficinas() {
		try {
			oficinas = oficinaBean.cargarOficinas();

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarSeries() {
		try {
			series = new HashMap<>();
			Long idOficina = ValidacionesUtil.obtenerIdFromMap(oficinas, oficinaValue);
			oficina = oficinaBean.buscarOficina(idOficina);
			series = oficinaBean.cargarSeries(idOficina);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarSubSeries() {
		try {
			subseries = new HashMap<>();
			Long idserie = ValidacionesUtil.obtenerIdFromMap(series, serieValue);
			serie = oficinaBean.buscarSerie(idserie);
			subseries = oficinaBean.cargarSubSeries(idserie);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarTiposDoc() {
		try {
			tiposDoc = new HashMap<>();
			Long id = ValidacionesUtil.obtenerIdFromMap(subseries, subserieValue);
			subserie = oficinaBean.buscarSubserie(id);
			tiposDoc = oficinaBean.cargarTipoDoc(id);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void crearTrd() {
		try {

			Long idOficina = ValidacionesUtil.obtenerIdFromMap(oficinas, oficinaValue);
			oficina = oficinaBean.buscarOficina(idOficina);
			Trd trd = new Trd();
			trd.setOficina(oficina);
			trd.setSerie(serie);
			trd.setSubserie(subserie);
			trd.setTipodocumento(tipoDoc);
			listaTrd.add(trd);
			oficina = null;
			oficinaValue = "";

		} catch (Exception ex) {
			LOG.info(ex);
		}

	}

	public void crearTrdEditar() {
		try {

			Long idOficina = ValidacionesUtil.obtenerIdFromMap(oficinas, oficinaValue);
			oficina = oficinaBean.buscarOficina(idOficina);
			Trd trd = new Trd();
			trd.setOficina(oficina);
			trd.setSerie(serie);
			trd.setSubserie(subserie);
			trd.setTipodocumento(tipoDoc);
			listaTrdEditar.add(trd);
			listaTrdEditarNuevas.add(trd);
			oficina = null;
			oficinaValue = "";

		} catch (Exception ex) {
			LOG.info(ex);
		}

	}

	public void cargarClases() {

		try {
			clasesDocumento = new ArrayList<>();
			clasesDocumento.add(ConstantsMail.OFICIO_CIRCULAR);
			clasesDocumento.add(ConstantsMail.OFICIO_MEMORANDO);
			clasesDocumento.add(ConstantsMail.OFICIO_MANUAL);

		} catch (Exception e) {
			LOG.info(e);
		}
	}

	public String mostrarFecha(Date fecha) {
		return ValidacionesUtil.formatearFecha(fecha);
	}

	public void limpiar() {
		radicacion = "";
		fechaOficio = new Date();
		destinosTabla = new ArrayList<String>();
		listaDestino = new ArrayList<Actor>();
		asunto = "";
		contenido = "";
		asunto = "";
		titulo = "";
		documentoAsociado = null;
		listaRef = new ArrayList<Documento>();
		listaTrd = new ArrayList<Trd>();
		elaboro = null;
		aprobo = "";
		aprobo = "";
		version = "";
		clase = null;
		modificacion = "";
		oficina = null;
		serie = null;
		subserie = null;
		tipoDoc = null;
		oficinaValue = null;
		serieValue = null;
		subserieValue = null;
		tipoDocValue = null;
		destinoName = "";
		procedenciaName = "";
		nombreBusqueda = "";
		archivos = new ArrayList<ArchivoDocumento>();
		letra = "";
	}

	public void limpiarEditar() {
		radicacionEditar = "";
		fechaOficioEditar = null;
		destinosTablaEditar = new ArrayList<String>();
		listaDestinoEditar = new ArrayList<Actor>();
		asuntoEditar = "";
		contenidoEditar = "";
		asuntoEditar = "";
		tituloEditar = "";
		documentoAsociadoEditar = null;
		listaRefEditar = new ArrayList<Documento>();
		listaTrdEditar = new ArrayList<Trd>();
		listaTrdEditarEliminar = new ArrayList<Trd>();
		listaTrdEditarNuevas = new ArrayList<Trd>();
		elaboroEditar = null;
		aproboEditar = "";
		aproboEditar = "";
		versionEditar = "";
		claseEditar = null;
		modificacionEditar = "";
		oficina = null;
		serie = null;
		subserie = null;
		tipoDoc = null;
		oficinaValue = null;
		serieValue = null;
		subserieValue = null;
		tipoDocValue = null;
		destinoName = "";
		procedenciaName = "";
		nombreBusqueda = "";
		archivosEditar = new ArrayList<ArchivoDocumento>();
		archivosEditarEliminados = new ArrayList<ArchivoDocumento>();
		archivosEditarNuevos = new ArrayList<ArchivoDocumento>();
		letra = "";
	}

	public void limpiarTodo() {
		limpiar();
		limpiarModal();
	}

	public String limpiarVer() {
		radicacionVer = "";
		fechaOficioVer = "";
		claseVer = "";
		tituloVer = "";
		asuntoVer = "";
		contenidoVer = "";
		archivosVer = new ArrayList<ArchivoDocumento>();
		documentoAsociadoVer = "";
		listaTrdVer = new ArrayList<Trd>();
		elaboroVer = "";
		aproboVer = "";
		versionVer = "";
		modificacionVer = "";
		listaDestinoVer = "";
		return "volverVer";
	}

	public void reset() {
		radicacionOficio = "";
		fechaConsultaIni = null;
		fechaConsultaFin = null;
		contenidoConsulta = "";
		claseConsulta = "";
		listaOficios = new ArrayList<>();
		listaOficiosOld = new ArrayList<>();
	}

	public void generarEtiqueta() {
		int validacion = 0;
		urlServer = ValidacionesUtil.urlServer();
		Boolean valRad = ValidacionesUtil.validarCadenaRadicado(letra);
		if (valRad) {
			radicacion = letra;
		}

		validacion += ValidacionesUtil.validarCadena(radicacion, "Radicacion");
		validacion += ValidacionesUtil.validarCadena(clase, "Clase");
		validacion += ValidacionesUtil.validarCadena(asunto, "asunto");
	}
//-----------------------------------------------------------------------
//------------------------------getters----------------------------------

	public String getRadicacion() {
		return radicacion;
	}

	public void setRadicacion(String radicacion) {
		this.radicacion = radicacion;
	}

	public Date getFechaOficio() {
		return fechaOficio;
	}

	public void setFechaOficio(Date fechaOficio) {
		this.fechaOficio = fechaOficio;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public String getElaboro() {
		return elaboro;
	}

	public void setElaboro(String elaboro) {
		this.elaboro = elaboro;
	}

	public String getAprobo() {
		return aprobo;
	}

	public void setAprobo(String aprobo) {
		this.aprobo = aprobo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getModificacion() {
		return modificacion;
	}

	public void setModificacion(String modificacion) {
		this.modificacion = modificacion;
	}

	public List<Actor> getDestinosBusqueda() {
		return destinosBusqueda;
	}

	public void setDestinosBusqueda(List<Actor> destinosBusqueda) {
		this.destinosBusqueda = destinosBusqueda;
	}

	public List<String> getDestinosTabla() {
		return destinosTabla;
	}

	public void setDestinosTabla(List<String> destinosTabla) {
		this.destinosTabla = destinosTabla;
	}

	public String getNombreBusqueda() {
		return nombreBusqueda;
	}

	public void setNombreBusqueda(String nombreBusqueda) {
		this.nombreBusqueda = nombreBusqueda;
	}

	public List<Trd> getListaTrd() {
		return listaTrd;
	}

	public void setListaTrd(List<Trd> listaTrd) {
		this.listaTrd = listaTrd;
	}

	public List<Actor> getListaDestino() {
		return listaDestino;
	}

	public void setListaDestino(List<Actor> listaDestino) {
		this.listaDestino = listaDestino;
	}

	public String getOutDestino() {
		return outDestino;
	}

	public void setOutDestino(String outDestino) {
		this.outDestino = outDestino;
	}

	public Documento getDocumentoAsociado() {
		return documentoAsociado;
	}

	public void setDocumentoAsociado(Documento documentoAsociado) {
		this.documentoAsociado = documentoAsociado;
	}

	public String getRadicacionConsulta() {
		return radicacionConsulta;
	}

	public void setRadicacionConsulta(String radicacionConsulta) {
		this.radicacionConsulta = radicacionConsulta;
	}

	public Actor getActorProcedenciaConsulta() {
		return actorProcedenciaConsulta;
	}

	public void setActorProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		this.actorProcedenciaConsulta = actorProcedenciaConsulta;
	}

	public Actor getActorDestinoConsulta() {
		return actorDestinoConsulta;
	}

	public void setActorDestinoConsulta(Actor actorDestinoConsulta) {
		this.actorDestinoConsulta = actorDestinoConsulta;
	}

	public Actor getActorRevisorConsulta() {
		return actorRevisorConsulta;
	}

	public void setActorRevisorConsulta(Actor actorRevisorConsulta) {
		this.actorRevisorConsulta = actorRevisorConsulta;
	}

	public Date getFechaEmisionInicial() {
		return fechaEmisionInicial;
	}

	public void setFechaEmisionInicial(Date fechaEmisionInicial) {
		this.fechaEmisionInicial = fechaEmisionInicial;
	}

	public Date getFechaEmisionFinal() {
		return fechaEmisionFinal;
	}

	public void setFechaEmisionFinal(Date fechaEmisionFinal) {
		this.fechaEmisionFinal = fechaEmisionFinal;
	}

	public String getAsuntoConsulta() {
		return asuntoConsulta;
	}

	public void setAsuntoConsulta(String asuntoConsulta) {
		this.asuntoConsulta = asuntoConsulta;
	}

	public String getProcedenciaName() {
		return procedenciaName;
	}

	public void setProcedenciaName(String procedenciaName) {
		this.procedenciaName = procedenciaName;
	}

	public String getDestinoName() {
		return destinoName;
	}

	public void setDestinoName(String destinoName) {
		this.destinoName = destinoName;
	}

	public String getRevisorName() {
		return revisorName;
	}

	public void setRevisorName(String revisorName) {
		this.revisorName = revisorName;
	}

	public List<Actor> getRevisorIn() {
		return revisorIn;
	}

	public void setRevisorIn(List<Actor> revisorIn) {
		this.revisorIn = revisorIn;
	}

	public Map<Long, String> getRevisorMap() {
		return revisorMap;
	}

	public void setRevisorMap(Map<Long, String> revisorMap) {
		this.revisorMap = revisorMap;
	}

	public List<Documento> getResultadosDocs() {
		return resultadosDocs;
	}

	public void setResultadosDocs(List<Documento> resultadosDocs) {
		this.resultadosDocs = resultadosDocs;
	}

	public List<Documento> getListaRef() {
		return listaRef;
	}

	public void setListaRef(List<Documento> listaRef) {
		this.listaRef = listaRef;
	}

	public List<Actor> getProcedenciaIn() {
		return procedenciaIn;
	}

	public void setProcedenciaIn(List<Actor> procedenciaIn) {
		this.procedenciaIn = procedenciaIn;
	}

	public String getOficinaValue() {
		return oficinaValue;
	}

	public void setOficinaValue(String oficinaValue) {
		this.oficinaValue = oficinaValue;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public String getSerieValue() {
		return serieValue;
	}

	public void setSerieValue(String serieValue) {
		this.serieValue = serieValue;
	}

	public Map<Long, String> getSeries() {
		return series;
	}

	public void setSeries(Map<Long, String> series) {
		this.series = series;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public String getSubserieValue() {
		return subserieValue;
	}

	public void setSubserieValue(String subserieValue) {
		this.subserieValue = subserieValue;
	}

	public Map<Long, String> getSubseries() {
		return subseries;
	}

	public void setSubseries(Map<Long, String> subseries) {
		this.subseries = subseries;
	}

	public SubSerie getSubserie() {
		return subserie;
	}

	public void setSubserie(SubSerie subserie) {
		this.subserie = subserie;
	}

	public String getTipoDocValue() {
		return tipoDocValue;
	}

	public void setTipoDocValue(String tipoDocValue) {
		this.tipoDocValue = tipoDocValue;
	}

	public Map<Long, String> getTiposDoc() {
		return tiposDoc;
	}

	public void setTiposDoc(Map<Long, String> tiposDoc) {
		this.tiposDoc = tiposDoc;
	}

	public TipoDocumento getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(TipoDocumento tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public Actor getRadicador() {
		return radicador;
	}

	public void setRadicador(Actor radicador) {
		this.radicador = radicador;
	}

	public List<String> getClasesDocumento() {
		return clasesDocumento;
	}

	public void setClasesDocumento(List<String> clasesDocumento) {
		this.clasesDocumento = clasesDocumento;
	}

	public Trd getOutTrd() {
		return outTrd;
	}

	public void setOutTrd(Trd outTrd) {
		this.outTrd = outTrd;
	}

	public String getRadicacionOficio() {
		return radicacionOficio;
	}

	public void setRadicacionOficio(String radicacionOficio) {
		this.radicacionOficio = radicacionOficio;
	}

	public Date getFechaConsultaIni() {
		return fechaConsultaIni;
	}

	public void setFechaConsultaIni(Date fechaConsultaIni) {
		this.fechaConsultaIni = fechaConsultaIni;
	}

	public Date getFechaConsultaFin() {
		return fechaConsultaFin;
	}

	public void setFechaConsultaFin(Date fechaConsultaFin) {
		this.fechaConsultaFin = fechaConsultaFin;
	}

	public String getContenidoConsulta() {
		return contenidoConsulta;
	}

	public void setContenidoConsulta(String contenidoConsulta) {
		this.contenidoConsulta = contenidoConsulta;
	}

	public String getClaseConsulta() {
		return claseConsulta;
	}

	public void setClaseConsulta(String claseConsulta) {
		this.claseConsulta = claseConsulta;
	}

	public List<Documento> getListaOficios() {
		return listaOficios;
	}

	public void setListaOficios(List<Documento> listaOficios) {
		this.listaOficios = listaOficios;
	}

	public String getRadicacionVer() {
		return radicacionVer;
	}

	public void setRadicacionVer(String radicacionVer) {
		this.radicacionVer = radicacionVer;
	}

	public String getFechaOficioVer() {
		return fechaOficioVer;
	}

	public void setFechaOficioVer(String fechaOficioVer) {
		this.fechaOficioVer = fechaOficioVer;
	}

	public String getClaseVer() {
		return claseVer;
	}

	public void setClaseVer(String claseVer) {
		this.claseVer = claseVer;
	}

	public String getTituloVer() {
		return tituloVer;
	}

	public void setTituloVer(String tituloVer) {
		this.tituloVer = tituloVer;
	}

	public String getAsuntoVer() {
		return asuntoVer;
	}

	public void setAsuntoVer(String asuntoVer) {
		this.asuntoVer = asuntoVer;
	}

	public String getContenidoVer() {
		return contenidoVer;
	}

	public void setContenidoVer(String contenidoVer) {
		this.contenidoVer = contenidoVer;
	}

	public String getElaboroVer() {
		return elaboroVer;
	}

	public void setElaboroVer(String elaboroVer) {
		this.elaboroVer = elaboroVer;
	}

	public String getAproboVer() {
		return aproboVer;
	}

	public void setAproboVer(String aproboVer) {
		this.aproboVer = aproboVer;
	}

	public String getVersionVer() {
		return versionVer;
	}

	public void setVersionVer(String versionVer) {
		this.versionVer = versionVer;
	}

	public String getModificacionVer() {
		return modificacionVer;
	}

	public void setModificacionVer(String modificacionVer) {
		this.modificacionVer = modificacionVer;
	}

	public String getListaDestinoVer() {
		return listaDestinoVer;
	}

	public void setListaDestinoVer(String listaDestinoVer) {
		this.listaDestinoVer = listaDestinoVer;
	}

	public String getDocumentoAsociadoVer() {
		return documentoAsociadoVer;
	}

	public void setDocumentoAsociadoVer(String documentoAsociadoVer) {
		this.documentoAsociadoVer = documentoAsociadoVer;
	}

	public List<ArchivoDocumento> getArchivosVer() {
		return archivosVer;
	}

	public void setArchivosVer(List<ArchivoDocumento> archivosVer) {
		this.archivosVer = archivosVer;
	}

	public List<Trd> getListaTrdVer() {
		return listaTrdVer;
	}

	public void setListaTrdVer(List<Trd> listaTrdVer) {
		this.listaTrdVer = listaTrdVer;
	}

	public String getLetra() {
		return letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public String getRadicacionEditar() {
		return radicacionEditar;
	}

	public void setRadicacionEditar(String radicacionEditar) {
		this.radicacionEditar = radicacionEditar;
	}

	public String getFechaOficioEditar() {
		return fechaOficioEditar;
	}

	public void setFechaOficioEditar(String fechaOficioEditar) {
		this.fechaOficioEditar = fechaOficioEditar;
	}

	public String getClaseEditar() {
		return claseEditar;
	}

	public void setClaseEditar(String claseEditar) {
		this.claseEditar = claseEditar;
	}

	public String getTituloEditar() {
		return tituloEditar;
	}

	public void setTituloEditar(String tituloEditar) {
		this.tituloEditar = tituloEditar;
	}

	public String getAsuntoEditar() {
		return asuntoEditar;
	}

	public void setAsuntoEditar(String asuntoEditar) {
		this.asuntoEditar = asuntoEditar;
	}

	public String getContenidoEditar() {
		return contenidoEditar;
	}

	public void setContenidoEditar(String contenidoEditar) {
		this.contenidoEditar = contenidoEditar;
	}

	public List<ArchivoDocumento> getArchivosEditar() {
		return archivosEditar;
	}

	public void setArchivosEditar(List<ArchivoDocumento> archivosEditar) {
		this.archivosEditar = archivosEditar;
	}

	public Documento getDocumentoAsociadoEditar() {
		return documentoAsociadoEditar;
	}

	public void setDocumentoAsociadoEditar(Documento documentoAsociadoEditar) {
		this.documentoAsociadoEditar = documentoAsociadoEditar;
	}

	public List<Trd> getListaTrdEditar() {
		return listaTrdEditar;
	}

	public void setListaTrdEditar(List<Trd> listaTrdEditar) {
		this.listaTrdEditar = listaTrdEditar;
	}

	public String getElaboroEditar() {
		return elaboroEditar;
	}

	public void setElaboroEditar(String elaboroEditar) {
		this.elaboroEditar = elaboroEditar;
	}

	public String getAproboEditar() {
		return aproboEditar;
	}

	public void setAproboEditar(String aproboEditar) {
		this.aproboEditar = aproboEditar;
	}

	public String getVersionEditar() {
		return versionEditar;
	}

	public void setVersionEditar(String versionEditar) {
		this.versionEditar = versionEditar;
	}

	public String getModificacionEditar() {
		return modificacionEditar;
	}

	public void setModificacionEditar(String modificacionEditar) {
		this.modificacionEditar = modificacionEditar;
	}

	public List<Actor> getListaDestinoEditar() {
		return listaDestinoEditar;
	}

	public void setListaDestinoEditar(List<Actor> listaDestinoEditar) {
		this.listaDestinoEditar = listaDestinoEditar;
	}

	public Actor getRadicadorEditar() {
		return radicadorEditar;
	}

	public void setRadicadorEditar(Actor radicadorEditar) {
		this.radicadorEditar = radicadorEditar;
	}

	public String getLetraEditar() {
		return letraEditar;
	}

	public void setLetraEditar(String letraEditar) {
		this.letraEditar = letraEditar;
	}

	public List<ArchivoDocumento> getArchivosEditarNuevos() {
		return archivosEditarNuevos;
	}

	public void setArchivosEditarNuevos(List<ArchivoDocumento> archivosEditarNuevos) {
		this.archivosEditarNuevos = archivosEditarNuevos;
	}

	public List<Documento> getListaRefEditar() {
		return listaRefEditar;
	}

	public void setListaRefEditar(List<Documento> listaRefEditar) {
		this.listaRefEditar = listaRefEditar;
	}

	public ArchivoDocumento getArchivoSelected() {
		return archivoSelected;
	}

	public void setArchivoSelected(ArchivoDocumento archivoSelected) {
		this.archivoSelected = archivoSelected;
	}

	public List<ArchivoDocumento> getArchivosEditarEliminados() {
		return archivosEditarEliminados;
	}

	public void setArchivosEditarEliminados(List<ArchivoDocumento> archivosEditarEliminados) {
		this.archivosEditarEliminados = archivosEditarEliminados;
	}

	public Trd getOutTrdEditar() {
		return outTrdEditar;
	}

	public void setOutTrdEditar(Trd outTrdEditar) {
		this.outTrdEditar = outTrdEditar;
	}

	public List<String> getDestinosTablaEditar() {
		return destinosTablaEditar;
	}

	public void setDestinosTablaEditar(List<String> destinosTablaEditar) {
		this.destinosTablaEditar = destinosTablaEditar;
	}

	public String getOutDestinoEditar() {
		return outDestinoEditar;
	}

	public void setOutDestinoEditar(String outDestinoEditar) {
		this.outDestinoEditar = outDestinoEditar;
	}

	public Usuario getUsuarioLogin() {
		return usuarioLogin;
	}

	public void setUsuarioLogin(Usuario usuarioLogin) {
		this.usuarioLogin = usuarioLogin;
	}

	public List<DocumentoDTO> getListaOficiosOld() {
		return listaOficiosOld;
	}

	public void setListaOficiosOld(List<DocumentoDTO> listaOficiosOld) {
		this.listaOficiosOld = listaOficiosOld;
	}

	public DocumentoDTO getOficeOld() {
		return oficeOld;
	}

	public void setOficeOld(DocumentoDTO oficeOld) {
		this.oficeOld = oficeOld;
	}

	public String getUrlServer() {
		return urlServer;
	}

	public void setUrlServer(String urlServer) {
		this.urlServer = urlServer;
	}

}
