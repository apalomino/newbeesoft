/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.PermisoLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Menu;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import com.beesoft.beesoft.gestiondocumental.entidades.Permiso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class PermisoBean implements PermisoLocal {

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

    @Override
    public void gestionarPermisos(Menu menu, Perfil perfil, Boolean permission) {

        List<Permiso> permisos = em.createQuery("select p from Permiso p where p.opcion.id=:menu and p.perfil.id=:perfil").setParameter("menu", menu.getId()).setParameter("perfil", perfil.getId()).getResultList();

        if (permisos == null || permisos.isEmpty()) {

            Permiso permiso = new Permiso();
            permiso.setOpcion(menu);
            permiso.setPerfil(perfil);
            permiso.setPermiso(permission);
            em.persist(permiso);
        }

    }

    @Override
    public List<Menu> cargarRaiz(Long idPerfil) {
        List<Menu> menuRaiz;

        if (idPerfil != null) {
            menuRaiz = em.createQuery("select p.opcion from Permiso p where p.opcion.raiz=:raiz and p.perfil.id=:perfil").setParameter("raiz", true).setParameter("perfil", idPerfil).getResultList();
        } else {
            menuRaiz = em.createQuery("select m from Menu m where m.raiz=:raiz").setParameter("raiz", true).getResultList();
        }

        if (menuRaiz != null && !menuRaiz.isEmpty()) {
            return menuRaiz;
        }
        return null;
    }

    @Override
    public List<Menu> cargarSubMenu(Long idPerfil, Long parent) {
        List<Menu> menuSub;

        if (idPerfil != null && parent != null) {
            menuSub = em.createQuery("select p.opcion from Permiso p where p.opcion.subMenu=true and p.opcion.parent.id=:parent and p.perfil.id=:perfil").setParameter("parent", parent).setParameter("perfil", idPerfil).getResultList();
        } else if (idPerfil == null && parent != null) {
            menuSub = em.createQuery("select m from Menu m where m.subMenu=true and m.parent.id=:parent ").setParameter("parent", parent).getResultList();
        } else if (idPerfil != null && parent == null) {
            menuSub = em.createQuery("select p.opcion from Permiso p where p.opcion.subMenu=true and p.perfil.id=:perfil").setParameter("perfil", idPerfil).getResultList();
        } else {
            menuSub = em.createQuery("select m from Menu m where m.subMenu=true").getResultList();
        }

        if (menuSub != null && !menuSub.isEmpty()) {
            return menuSub;
        }
        return null;
    }

    @Override
    public List<Menu> cargarUrl(Long idPerfil, Long parent) {
        List<Menu> menuUrl;

        if (idPerfil != null && parent != null) {
            menuUrl = em.createQuery("select p.opcion from Permiso p where p.opcion.url=true and p.opcion.parent.id=:parent and p.perfil.id=:perfil").setParameter("parent", parent).setParameter("perfil", idPerfil).getResultList();
        } else if (idPerfil == null && parent != null) {
            menuUrl = em.createQuery("select m from Menu m where m.url=true and m.parent.id=:parent ").setParameter("parent", parent).getResultList();
        } else if (idPerfil != null && parent == null) {
            menuUrl = em.createQuery("select p.opcion from Permiso p where p.opcion.url=true and p.perfil.id=:perfil").setParameter("perfil", idPerfil).getResultList();
        } else {
            menuUrl = em.createQuery("select p.opcion from Permiso p where p.opcion.url=true").getResultList();
        }

        if (menuUrl != null && !menuUrl.isEmpty()) {
            return menuUrl;
        }
        return null;
    }

    @Override
    public List<Menu> cargarMenu(Long idPerfil) {
        List<Menu> menuUrl = em.createQuery("select p.opcion from Permiso p where p.opcion.url=:url and p.perfil.id=:perfil").setParameter("url", true).setParameter("perfil", idPerfil).getResultList();
        if (menuUrl != null && !menuUrl.isEmpty()) {
            return menuUrl;
        }
        return null;
    }

//    @Override
//    public TreeMap<Menu, List<TreeMap<Menu, List<Menu>>>> cargarMenu(Long idPerfil) {
//
//        List<Menu> menuRaiz;
//        List<Menu> menuSub;
//        List<Menu> menuUrl;
//
//        if (idPerfil != null) {
//            menuRaiz = em.createQuery("select p.opcion from Permiso p where p.opcion.raiz=:raiz and p.perfil.id=:perfil").setParameter("raiz", true).setParameter("perfil", idPerfil).getResultList();
//            menuSub = em.createQuery("select p.opcion from Permiso p where p.opcion.subMenu=:sub and p.perfil.id=:perfil").setParameter("sub", true).setParameter("perfil", idPerfil).getResultList();
//            menuUrl = em.createQuery("select p.opcion from Permiso p where p.opcion.url=:url and p.perfil.id=:perfil").setParameter("url", true).setParameter("perfil", idPerfil).getResultList();
//
//        } else {
//
//            menuRaiz = em.createQuery("select m from Menu m where m.raiz=:raiz").setParameter("raiz", true).getResultList();
//            menuSub = em.createQuery("select m from Menu m where m.subMenu=:sub").setParameter("sub", true).getResultList();
//            menuUrl = em.createQuery("select m from Menu m where m.url=:url").setParameter("url", true).getResultList();
//        }
//
//        TreeMap<Menu, List<TreeMap<Menu, List<Menu>>>> listaMenu = null;
//        TreeMap<Menu, List<Menu>> listaUrlSub = null;
//
//        if ((menuSub != null && !menuSub.isEmpty()) && (menuUrl != null && !menuUrl.isEmpty())) {
//            listaUrlSub = new TreeMap<Menu, List<Menu>>();
//            for (Menu menuS : menuSub) {
//                List<Menu> listaUrls = new ArrayList<Menu>();
//                for (Menu menuU : menuUrl) {
//                    if (menuS.getId().equals(menuU.getParent().getId())) {
//                        listaUrls.add(menuU);
//                    }
//                }
//                listaUrlSub.put(menuS, listaUrls);
//            }
//        }
//
//        if ((menuRaiz != null && !menuSub.isEmpty()) && (listaUrlSub != null && !listaUrlSub.isEmpty())) {
//            listaMenu = new TreeMap<Menu, List<TreeMap<Menu, List<Menu>>>>();
//            for (Menu menuR : menuRaiz) {
//                List<TreeMap<Menu, List<Menu>>> lista = new ArrayList<TreeMap<Menu, List<Menu>>>();
//                for (Map.Entry<Menu, List<Menu>> menuS : listaUrlSub.entrySet()) {
//                    if (menuR.getId().equals(menuS.getKey().getParent().getId())) {
//                        TreeMap<Menu, List<Menu>> temp = new TreeMap<Menu, List<Menu>>();
//                        temp.put(menuS.getKey(), menuS.getValue());
//                        lista.add(temp);
//                    }
//                }
//                listaMenu.put(menuR, lista);
//
//            }
//        }
//
//        return listaMenu;
//    }
    @Override
    public Permiso buscarMenuPerfil(Long idPerfil, Long idMenu) {

        List<Permiso> lista = em.createQuery("select p from Permiso p where p.perfil.id=:id and p.opcion.id=:menu and p.permiso=true").setParameter("id", idPerfil).setParameter("menu", idMenu).getResultList();

        if (lista != null && !lista.isEmpty()) {
            return lista.get(0);
        }

        return null;
    }

    @Override
    public List<Permiso> buscarPermisos(Long idMenu, Long idPerfil) throws Exception {
        Query q = null;
        StringBuilder query = new StringBuilder("select p from Permiso p");
        Map<String, Object> parameters = new HashMap<String, Object>();
        boolean iswhere = false;

        if (idMenu != null) {
            query.append(" where p.opcion.id=:idMenu");
            parameters.put("idMenu", idMenu);
            iswhere = true;
        }

        if (idPerfil != null) {
            if (iswhere) {
                query.append(" and p.perfil.id=:idPerfil");
                parameters.put("idPerfil", idPerfil);

            } else {
                query.append(" where p.perfil.id=:perfil");
                parameters.put("perfil", idPerfil);
                iswhere = true;
            }
        }

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        List<Permiso> permisos = q.getResultList();

        if (permisos != null && !permisos.isEmpty()) {
            return permisos;
        } else {
            throw new Exception("No se ha precargado la lista de Permisos");
        }

    }

    @Override
    public void eliminarPermiso(Long idPermiso) {

        Permiso permiso = em.find(Permiso.class, idPermiso);

        if (permiso != null) {
            em.remove(permiso);
        }

    }

    @Override
    public List<Menu> cargarMenuCompleto() {
        List<Menu> listaMenu = em.createQuery("select m from Menu m").getResultList();
        return listaMenu;
    }

    @Override
    public List<Menu> cargarPadresMenu() {
        List<Menu> listaPadres = em.createQuery("select m from Menu m where (m.raiz=true or m.subMenu=true) order by m.orden").getResultList();
        return listaPadres;
    }

    @Override
    public void crearMenu(String etiqueta, String descripcion, String tipo, Long padreId, Integer orden) {
        Menu menu = new Menu();
        menu.setEtiqueta(etiqueta);
        menu.setDescripcion(descripcion);
        switch (tipo) {
            case "raiz":
                menu.setRaiz(Boolean.TRUE);
                break;
            case "submenu":
                menu.setSubMenu(Boolean.TRUE);
                break;
            case "url":
                menu.setUrl(Boolean.TRUE);
                break;
        }
        Menu padre = em.find(Menu.class, padreId);
        menu.setParent(padre);
        menu.setOrden(orden);
        reordenarMenu(orden);
        em.persist(menu);
    }

    public void reordenarMenu(Integer orden) {
        List<Menu> listaMenu = cargarMenuCompleto();
        Boolean cambiar = false;
        for (Menu menu : listaMenu) {
            if (menu.getOrden().equals(orden)) {
                cambiar = true;
            }
        }

        if (cambiar) {
            for (Menu menu : listaMenu) {
                if (menu.getOrden() >= orden) {
                    menu.setOrden(menu.getOrden() + 1);
                    em.merge(menu);
                }
            }
        }
    }

}
