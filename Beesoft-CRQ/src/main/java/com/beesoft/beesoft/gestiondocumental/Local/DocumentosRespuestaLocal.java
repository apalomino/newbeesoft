/**
 * 
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import java.util.List;

import javax.ejb.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.DocumentosRespuesta;

/**
 * @author Sumset
 *
 */
@Local
public interface DocumentosRespuestaLocal {
	
	public void crearDocumentoRespuesta(Long docuResp, Long docuOrigen, String radicado) throws Exception;

	//public List<DocumentosRespuesta> consultarRespuestasDocumento(Long idDoc) throws Exception;
	
	public List<DocumentosRespuesta> consultarRespuesta(Long idDoc) throws Exception;
	
	public Long consultaIdRespuesta(Long idOrigen, Long idResp) throws Exception;

	public void eliminarRegRespuesta(Long idR) throws Exception;

}
