/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author ISSLUNO
 */
@Entity
public class DocumentoCalidad implements Serializable {

    public Long id;
    public String titulo;
    public String descripcion;
    public byte[] documento;
    public String nombreDocumento;
    public byte[] imagen;
    public String nombreImagen;
    public Date fechaCreacion;
    public TipoOrganizacional tipo;
    public CategoriaDocumentoCalidad categoriaDocumento;
    public Actor usuarioCarga;
    public Boolean estado;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(length = 100000)
    public byte[] getDocumento() {
        return documento;
    }

    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(length = 100000)
    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @OneToOne
    public TipoOrganizacional getTipo() {
        return tipo;
    }

    public void setTipo(TipoOrganizacional tipo) {
        this.tipo = tipo;
    }

    @OneToOne
    public CategoriaDocumentoCalidad getCategoriaDocumento() {
        return categoriaDocumento;
    }

    public void setCategoriaDocumento(CategoriaDocumentoCalidad categoriaDocumento) {
        this.categoriaDocumento = categoriaDocumento;
    }

    @OneToOne
    public Actor getUsuarioCarga() {
        return usuarioCarga;
    }

    public void setUsuarioCarga(Actor usuarioCarga) {
        this.usuarioCarga = usuarioCarga;
    }

    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
