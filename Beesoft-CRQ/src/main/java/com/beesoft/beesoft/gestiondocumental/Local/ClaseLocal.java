/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.SubClase;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cc
 */
@Local
public interface ClaseLocal {

    public List<Clase> listarClases();

    public Clase buscarClase(String nombre);

    public List<SubClase> buscarSubClases(Long idClase);

    public Clase buscarClasePorId(Long id);

    public SubClase buscarSubClasePorId(Long id);
}
