package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ComunicadosInternosLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.RepositorioDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import com.beesoft.beesoft.gestiondocumental.entidades.ComunicadosInternos;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.RevisionDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Luis Arango
 */
@Named(value = "repositorioAction")
@SessionScoped
public class RepositorioAction implements Serializable {

	private static final Log LOG = LogFactory.getLog(RepositorioAction.class);

	@EJB
	private DocumentoLocal documentoBean;

	@EJB
	private ComunicadosInternosLocal comunicadosInternosBean;

	@EJB
	private ActorLocal actorBean;

	@EJB
	private OficinaLocal oficinaBean;

	private Usuario usuarioLogin;

	private String radicacionConsulta;
	private Date fechaEmisionInicial;
	private Date fechaEmisionFinal;
	private String procedenciaName;
	private String destinoName;
	private String asuntoConsulta;
	private List<RepositorioDTO> resultado;
	private String nombreProcedenciaBusqueda;
	private String nombreDestinoBusqueda;
	private List<Actor> destinoIn;
	private List<Actor> procedenciaIn;
	private Actor actorProcedenciaConsulta;
	private Actor actorDestinoConsulta;
	private RepositorioDTO repositorioDtoSeleccionado;
	private Documento documentoSeleccionado;
	private ComunicadosInternos comunicadosInternosSeleccionado;

	private List<ArchivoDocumento> listaArchivos = new ArrayList<>();
	private List<RevisionDocumento> revisiones = new ArrayList<>();
	private List<Trd> trds = new ArrayList<>();

	String listaDestinosCopiaText = "";

	String listaDestinosCopiaTextInterno = "";

	public String mostrarProcedenciaTabla(RepositorioDTO dto) {
		if (dto != null) {
			return ValidacionesUtil.listToCadena(dto.getEmisor());
		}
		return "";
	}

	public String mostrarDestinoTabla(RepositorioDTO dto) {
		if (dto != null) {
			return ValidacionesUtil.listToCadena(dto.getDestinatarios());
		}
		return "";
	}

	// TODO
	public void buscarDocumento() {
		try {
			resultado = new ArrayList<>();
			if (validaciones()) {
				if (fechaEmisionInicial != null && fechaEmisionFinal != null) {
					if (!ValidacionesUtil.validarFechasIniFin(fechaEmisionInicial, fechaEmisionFinal)) {
						return;
					}
				} else if (fechaEmisionInicial != null && fechaEmisionFinal == null) {
					ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha de Emision Final");
					return;
				} else if (fechaEmisionInicial == null && fechaEmisionFinal != null) {
					ValidacionesUtil
							.addMessage("El rango de fechas esta incompleto agregue la Fecha de Emision Inicial");
					return;
				}

				List<Documento> documentos = documentoBean.buscarDocumento("E" + radicacionConsulta,
						fechaEmisionInicial, fechaEmisionFinal, actorProcedenciaConsulta, actorDestinoConsulta,
						asuntoConsulta, "", "", null, null, null, null, "");
				List<ComunicadosInternos> comunicadosInternos = comunicadosInternosBean.buscarCorreoRepositorio(
						radicacionConsulta, actorProcedenciaConsulta, actorDestinoConsulta, asuntoConsulta,
						fechaEmisionInicial, fechaEmisionFinal);

				if (documentos != null && !documentos.isEmpty()) {
					for (Documento documento : documentos) {
						RepositorioDTO dto = new RepositorioDTO();
						dto.setAsunto(documento.getAsunto());
						dto.setId(documento.getId());
						dto.setRadicacion(documento.getRadicacion());
						dto.setFechaEmision(documento.getFechaSistema());
						dto.setFechaRecepcion(documento.getFechaRecepcion());
						dto.setEmisor(documentoBean.consultarProcedenciaDocumento(documento.getId()));
						dto.setDestinatarios(documentoBean.consultarDestinoDocumento(documento.getId()));
						dto.setTipo(ConstantsMail.REPOSITORIO_DOCUMENTO);
						resultado.add(dto);
					}
				}
				if (comunicadosInternos != null && !comunicadosInternos.isEmpty()) {
					for (ComunicadosInternos comunicadosInterno : comunicadosInternos) {
						if (StringUtils.isNotBlank(comunicadosInterno.getRadicacion())) {
							RepositorioDTO dto = new RepositorioDTO();
							dto.setAsunto(comunicadosInterno.getAsunto());
							dto.setId(comunicadosInterno.getId());
							dto.setRadicacion(comunicadosInterno.getRadicacion());
							dto.setFechaEmision(comunicadosInterno.getFechaCreacion());
							dto.setFechaRecepcion(null);
							ArrayList<Actor> actors = new ArrayList<>();
							actors.add(comunicadosInterno.getEmisor());
							dto.setEmisor(actors);
							// dto.setDestinatarios(comunicadosInternosBean.consultarDestinosComunicados(dto.getId()));
							dto.setTipo(ConstantsMail.REPOSITORIO_COMUNICADO_INTERNO);
							resultado.add(dto);
						}
					}
				}

			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String mostrarFecha(Date fecha) {
		return ValidacionesUtil.formatearFechaHora(fecha);
	}

	public String selectDocumento(RepositorioDTO dto) {
		String redirect = "";
		repositorioDtoSeleccionado = dto;

		if (dto == null) {
			ValidacionesUtil.addMessage("No ha seleccionado un documento");
			return "failed";
		}
		switch (repositorioDtoSeleccionado.getTipo()) {
		case ConstantsMail.REPOSITORIO_DOCUMENTO:
			documentoSeleccionado = documentoBean.buscarDocumentoById(repositorioDtoSeleccionado.getId());
			documentoSeleccionado.setDestinos(repositorioDtoSeleccionado.getDestinatarios());
			documentoSeleccionado.setProcedencia(repositorioDtoSeleccionado.getEmisor());
			listaArchivos = documentoBean.consultarArchivoDocumento(documentoSeleccionado.getId());
			cargarComentarios();
			cargarTrds(documentoSeleccionado.getId());
			redirect = asignarInformacionDocumento();
			break;
		case ConstantsMail.REPOSITORIO_COMUNICADO_INTERNO:
			comunicadosInternosSeleccionado = comunicadosInternosBean
					.buscarCorreoId(repositorioDtoSeleccionado.getId());
//                List<Actor> destinosCopiaInterno = comunicadosInternosBean.consultarDestinosCopiaComunicados(comunicadosInternosSeleccionado.getId());
//                listaDestinosCopiaTextInterno = ValidacionesUtil.listToCadena(destinosCopiaInterno);

			redirect = asignarInformacionComunicadoInterno();
		}

		return redirect;
	}

	private String asignarInformacionDocumento() {
		return "verDocumento";
	}

	private String asignarInformacionComunicadoExterno() {
		return "verCorreoExterno";
	}

	private String asignarInformacionComunicadoInterno() {
		return "verCorreo";
	}

	// TODO disabled="#{!repositorioAction.showButtons(documento)}"
	public Boolean showButtons(DocumentoDTO doc) {
		Actor actor = usuarioLogin.getActor();

		switch (usuarioLogin.getPerfil().getDescripcion()) {
		case ConstantsMail.RADICADOR:
			return true;
		case ConstantsMail.ADMIN:
			return true;
		}

		if (!doc.getOld()) {
			if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.EDITOR)) {
				return true;
			}
			Documento documento = doc.getDocumento();
			List<String> revisores = new ArrayList<>();
			List<Actor> revisor = documentoBean.consultarRevisorDocumento(documento.getId());
			if (revisor != null && !revisor.isEmpty()) {
				for (Actor a : revisor) {
					revisores.add(a.getNombreYApellido());
				}
				if (revisores.contains(actor.getNombreYApellido())) {
					return true;
				}
			}
			if (documento.getResponsable() != null) {
				if (documento.getResponsable().getId().equals(actor.getId())) {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
		return false;
	}

	public void buscarProcedencia() {
		try {
			procedenciaIn = actorBean.listaProcedencia(nombreProcedenciaBusqueda, null, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void selectProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		if (actorProcedenciaConsulta != null) {
			this.actorProcedenciaConsulta = actorProcedenciaConsulta;
			procedenciaName = actorProcedenciaConsulta.getNombreYApellido();
			iniciarDialog();
		}
	}

	public void buscarDestinos() {
		try {
			destinoIn = actorBean.listaProcedencia(nombreDestinoBusqueda, null, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void selectDestinoConsulta(Actor actorDestinoConsulta) {
		if (actorDestinoConsulta != null) {
			this.actorDestinoConsulta = actorDestinoConsulta;
			destinoName = actorDestinoConsulta.getNombreYApellido();
			iniciarDialog();
		}
	}

	public void iniciarDialog() {
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
		nombreDestinoBusqueda = "";
		nombreProcedenciaBusqueda = "";
	}

	public Boolean validaciones() {
		if (StringUtils.isBlank(radicacionConsulta) && fechaEmisionInicial == null && fechaEmisionFinal == null
				&& actorProcedenciaConsulta == null && actorDestinoConsulta == null
				&& StringUtils.isBlank(asuntoConsulta)) {
			ValidacionesUtil.addMessage("Debe utilizar por lo menos un filtro de busqueda");
			return false;
		} else {
			return true;
		}
	}

	public String actorListProcedenciaDocumentos() {
		if (documentoSeleccionado != null) {
			return ValidacionesUtil.listToCadena(documentoSeleccionado.getProcedencia());
		}
		return "";
	}

	public String actorListDestinoDocumentos() {
		if (documentoSeleccionado != null) {
			return ValidacionesUtil.listToCadena(documentoSeleccionado.getDestinos());
		}
		return "";
	}

	public String formatFecha(Date fecha) {
		if (fecha != null) {
			return ValidacionesUtil.formatearFecha(fecha);
		}
		return "";
	}

	public String getResponsable() {
		if (documentoSeleccionado.getResponsable() != null) {
			return documentoSeleccionado.getResponsable().getNombreYApellido();
		} else {
			return "DOCUMENTO SIN RESPONSABLE";
		}
	}

	public String getRevisorNameText() {
		return ValidacionesUtil.listToCadena(documentoBean.consultarRevisorDocumento(documentoSeleccionado.getId()));
	}

	public void cargarComentarios() {
		try {
			revisiones = new ArrayList<>();
			List<RevisionDocumento> revisionList = documentoBean.comentariosDocumento(documentoSeleccionado.getId());

			if (revisionList != null && !revisionList.isEmpty()) {
				for (RevisionDocumento rev : revisionList) {
					if (StringUtils.isNotBlank(rev.getComentario())) {
						revisiones.add(rev);
					}
				}
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String volver() {
		return "repositorio";
	}

	public void cargarTrds(Long id) {
		try {
			trds = new ArrayList<>();
			trds = oficinaBean.buscarTrds(id);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String textoFirma(String texto) {
		return ValidacionesUtil.textoCapitalCompleto(texto);
	}

	public String getRadicacionInterno() {
		if (StringUtils.isNotBlank(comunicadosInternosSeleccionado.getRadicacion())) {
			return comunicadosInternosSeleccionado.getRadicacion();
		} else if (StringUtils.isNotBlank(comunicadosInternosSeleccionado.getRadicacionReenvio())) {
			return comunicadosInternosSeleccionado.getRadicacionReenvio();
		}
		return "";
	}

	public String getFechaEnvioInterno() {
		return ValidacionesUtil.formatearFechaElectronico(comunicadosInternosSeleccionado.getFechaEnvio());
	}

	public String getDestinosComunicadosInterno() {
//        List<Actor> destinos = comunicadosInternosBean.consultarDestinosComunicados(comunicadosInternosSeleccionado.getId());
//        if (destinos != null && !destinos.isEmpty()) {
//            return ValidacionesUtil.listToCadena(destinos);
//        }
		return "";
	}

	public String getGraphicTextInterno() {
		if (comunicadosInternosSeleccionado.getEmisor() != null) {
			return "firmas/" + comunicadosInternosSeleccionado.getEmisor().getId() + ".PNG";
		}
		return "";
	}
	
	public String getComentarioInterno() {
		String comentario = comunicadosInternosSeleccionado.getComentario();
		if (StringUtils.isNotBlank(comentario)) {
			return comentario;
		}
		return "";
	}

	public List<ArchivosAdjuntos> getArchivosAdjuntosInterno() {
		return comunicadosInternosBean.consultarArchivosComunicados(comunicadosInternosSeleccionado.getId());
	}

	@PostConstruct
	public void inicializar() {
		fechaEmisionInicial = null;
		fechaEmisionFinal = null;
		radicacionConsulta = "";
		asuntoConsulta = "";
		actorProcedenciaConsulta = null;
		actorDestinoConsulta = null;
		nombreProcedenciaBusqueda = "";
		nombreDestinoBusqueda = "";
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
		resultado = new ArrayList<>();
		if (FacesContext.getCurrentInstance() != null) {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
					.getSession(false);
			usuarioLogin = (Usuario) session.getAttribute("username");
		}
	}

	@PreDestroy
	public void limpiar() {
		fechaEmisionInicial = null;
		fechaEmisionFinal = null;
		radicacionConsulta = "";
		procedenciaName = "";
		destinoName = "";
		asuntoConsulta = "";
		actorProcedenciaConsulta = null;
		actorDestinoConsulta = null;
		resultado = new ArrayList<>();
		nombreProcedenciaBusqueda = "";
		nombreDestinoBusqueda = "";
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
	}

	public List<RepositorioDTO> getResultado() {
		return resultado;
	}

	public void setResultado(List<RepositorioDTO> resultado) {
		this.resultado = resultado;
	}

	public DocumentoLocal getDocumentoBean() {
		return documentoBean;
	}

	public void setDocumentoBean(DocumentoLocal documentoBean) {
		this.documentoBean = documentoBean;
	}

	public String getRadicacionConsulta() {
		return radicacionConsulta;
	}

	public void setRadicacionConsulta(String radicacionConsulta) {
		this.radicacionConsulta = radicacionConsulta;
	}

	public Date getFechaEmisionInicial() {
		return fechaEmisionInicial;
	}

	public void setFechaEmisionInicial(Date fechaEmisionInicial) {
		this.fechaEmisionInicial = fechaEmisionInicial;
	}

	public Date getFechaEmisionFinal() {
		return fechaEmisionFinal;
	}

	public void setFechaEmisionFinal(Date fechaEmisionFinal) {
		this.fechaEmisionFinal = fechaEmisionFinal;
	}

	public String getAsuntoConsulta() {
		return asuntoConsulta;
	}

	public void setAsuntoConsulta(String asuntoConsulta) {
		this.asuntoConsulta = asuntoConsulta;
	}

	public Usuario getUsuarioLogin() {
		return usuarioLogin;
	}

	public void setUsuarioLogin(Usuario usuarioLogin) {
		this.usuarioLogin = usuarioLogin;
	}

	public ComunicadosInternosLocal getComunicadosInternosBean() {
		return comunicadosInternosBean;
	}

	public void setComunicadosInternosBean(ComunicadosInternosLocal comunicadosInternosBean) {
		this.comunicadosInternosBean = comunicadosInternosBean;
	}

	public String getProcedenciaName() {
		return procedenciaName;
	}

	public void setProcedenciaName(String procedenciaName) {
		this.procedenciaName = procedenciaName;
	}

	public String getDestinoName() {
		return destinoName;
	}

	public void setDestinoName(String destinoName) {
		this.destinoName = destinoName;
	}

	public List<Actor> getDestinoIn() {
		return destinoIn;
	}

	public void setDestinoIn(List<Actor> destinoIn) {
		this.destinoIn = destinoIn;
	}

	public List<Actor> getProcedenciaIn() {
		return procedenciaIn;
	}

	public void setProcedenciaIn(List<Actor> procedenciaIn) {
		this.procedenciaIn = procedenciaIn;
	}

	public ActorLocal getActorBean() {
		return actorBean;
	}

	public void setActorBean(ActorLocal actorBean) {
		this.actorBean = actorBean;
	}

	public String getNombreProcedenciaBusqueda() {
		return nombreProcedenciaBusqueda;
	}

	public void setNombreProcedenciaBusqueda(String nombreProcedenciaBusqueda) {
		this.nombreProcedenciaBusqueda = nombreProcedenciaBusqueda;
	}

	public String getNombreDestinoBusqueda() {
		return nombreDestinoBusqueda;
	}

	public void setNombreDestinoBusqueda(String nombreDestinoBusqueda) {
		this.nombreDestinoBusqueda = nombreDestinoBusqueda;
	}

	public Actor getActorProcedenciaConsulta() {
		return actorProcedenciaConsulta;
	}

	public void setActorProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		this.actorProcedenciaConsulta = actorProcedenciaConsulta;
	}

	public Actor getActorDestinoConsulta() {
		return actorDestinoConsulta;
	}

	public void setActorDestinoConsulta(Actor actorDestinoConsulta) {
		this.actorDestinoConsulta = actorDestinoConsulta;
	}

	public RepositorioDTO getRepositorioDtoSeleccionado() {
		return repositorioDtoSeleccionado;
	}

	public void setRepositorioDtoSeleccionado(RepositorioDTO repositorioDtoSeleccionado) {
		this.repositorioDtoSeleccionado = repositorioDtoSeleccionado;
	}

	public Documento getDocumentoSeleccionado() {
		return documentoSeleccionado;
	}

	public void setDocumentoSeleccionado(Documento documentoSeleccionado) {
		this.documentoSeleccionado = documentoSeleccionado;
	}

	public List<ArchivoDocumento> getListaArchivos() {
		return listaArchivos;
	}

	public void setListaArchivos(List<ArchivoDocumento> listaArchivos) {
		this.listaArchivos = listaArchivos;
	}

	public List<RevisionDocumento> getRevisiones() {
		return revisiones;
	}

	public void setRevisiones(List<RevisionDocumento> revisiones) {
		this.revisiones = revisiones;
	}

	public List<Trd> getTrds() {
		return trds;
	}

	public void setTrds(List<Trd> trds) {
		this.trds = trds;
	}

	public ComunicadosInternos getComunicadosInternosSeleccionado() {
		return comunicadosInternosSeleccionado;
	}

	public void setComunicadosInternosSeleccionado(ComunicadosInternos comunicadosInternosSeleccionado) {
		this.comunicadosInternosSeleccionado = comunicadosInternosSeleccionado;
	}

	public OficinaLocal getOficinaBean() {
		return oficinaBean;
	}

	public void setOficinaBean(OficinaLocal oficinaBean) {
		this.oficinaBean = oficinaBean;
	}

	public String getListaDestinosCopiaText() {
		return listaDestinosCopiaText;
	}

	public void setListaDestinosCopiaText(String listaDestinosCopiaText) {
		this.listaDestinosCopiaText = listaDestinosCopiaText;
	}

	public String getListaDestinosCopiaTextInterno() {
		return listaDestinosCopiaTextInterno;
	}

	public void setListaDestinosCopiaTextInterno(String listaDestinosCopiaTextInterno) {
		this.listaDestinosCopiaTextInterno = listaDestinosCopiaTextInterno;
	}

}

