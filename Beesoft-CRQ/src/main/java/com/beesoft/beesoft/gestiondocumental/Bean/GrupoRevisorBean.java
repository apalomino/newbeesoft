/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.GrupoRevisorLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.GrupoRevisor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cfernandez@sumset.com
 */
@Stateless
public class GrupoRevisorBean implements GrupoRevisorLocal {

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

    @Override
    public void crearGrupo(String nombreGrupo, List<Actor> revisores) {
        GrupoRevisor grupo = new GrupoRevisor();
        grupo.setNombreGrupo(nombreGrupo);
        grupo.setRevisores(revisores);
        em.persist(grupo);
    }

    @Override
    public List<GrupoRevisor> listarGrupos() throws Exception {
        List<GrupoRevisor> grupos = em.createQuery("select g from GrupoRevisor g").getResultList();
        if (grupos != null && !grupos.isEmpty()) {
            return grupos;
        } else {
            throw new Exception("No hay Grupos");
        }
    }

    @Override
    public List<Actor> consultarRevisoresGrupo(Long id) throws Exception {
        System.out.println("este es el id del grupo " + id);
        GrupoRevisor grupo = em.find(GrupoRevisor.class, id);
        if (grupo != null) {
            System.out.println("este es el grupo " + grupo.getNombreGrupo());
            if (grupo.getRevisores() != null && !grupo.getRevisores().isEmpty()) {
                return grupo.getRevisores();
            } else {
                throw new Exception("No tiene revisores");
            }
        } else {
            throw new Exception("No existe grupo");
        }
    }

    @Override
    public void editarGrupo(Long id, String nombre, List<Actor> revisores) {
        GrupoRevisor grupo = em.find(GrupoRevisor.class, id);
        grupo.setNombreGrupo(nombre);
        grupo.setRevisores(revisores);
        em.merge(grupo);
    }

    @Override
    public GrupoRevisor buscarGrupo(Long id) throws Exception {
        GrupoRevisor grupo = em.find(GrupoRevisor.class, id);
        if (grupo != null) {
            return grupo;
        } else {
            throw new Exception("No hay un grupo con ese identificador");
        }
    }

}
