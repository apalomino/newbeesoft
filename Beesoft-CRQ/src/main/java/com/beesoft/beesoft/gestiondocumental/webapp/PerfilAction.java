/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.PerfilLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named("perfilAction")
@SessionScoped
public class PerfilAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(PerfilAction.class);
    
    @EJB
    private PerfilLocal perfilBean;

    @Inject
    private ApplicationAction application;

    private String descripcion;
    private List<Perfil> listaPerfiles;
    private final int goCreate = 1;

    @PostConstruct
    public void iniciar() {
        obtenerPerfiles();
    }

    public void crearPerfil() {
        int validate = 0;
        try {

            validate += ValidacionesUtil.validarCadena(descripcion, "descripcion");

            if (goCreate == validate) {
                perfilBean.crearPerfil(descripcion);
                application.cargarPerfiles();
                ValidacionesUtil.addMessageInfo("EL perfil " + descripcion + " fue creado.");
                obtenerPerfiles();
                limpiar();
            }
        } catch (Exception ex) {
            LOG.info(ex);
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void obtenerPerfiles() {
        try {
            listaPerfiles = new ArrayList<>();
            listaPerfiles = perfilBean.listaPerfiles();
            application.cargarPerfiles();
        } catch (Exception ex) {
            LOG.info(ex);
            ValidacionesUtil.addMessage(ex.getMessage());
        }
    }

    public void cambiarEstado(Long id, Boolean estado) {
        perfilBean.cambiarEstado(id, estado);
        obtenerPerfiles();
    }

    public void limpiar() {
        descripcion = "";
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Perfil> getListaPerfiles() {
        return listaPerfiles;
    }

    public void setListaPerfiles(List<Perfil> listaPerfiles) {
        this.listaPerfiles = listaPerfiles;
    }

}
