/**
 * 
 */
package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Sumset
 *
 */
@Entity
public class DocumentosRespuesta implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long documentoRespuesta;
	private Long documentoOrigen;
	private String radicado;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDocumentoRespuesta() {
		return documentoRespuesta;
	}
	public void setDocumentoRespuesta(Long documentoRespuesta) {
		this.documentoRespuesta = documentoRespuesta;
	}
	public Long getDocumentoOrigen() {
		return documentoOrigen;
	}
	public void setDocumentoOrigen(Long documentoOrigen) {
		this.documentoOrigen = documentoOrigen;
	}
	public String getRadicado() {
		return radicado;
	}
	public void setRadicado(String radicado) {
		this.radicado = radicado;
	}
	
}
