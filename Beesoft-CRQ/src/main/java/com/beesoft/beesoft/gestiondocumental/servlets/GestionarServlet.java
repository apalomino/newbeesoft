/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.servlets;

import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import com.beesoft.beesoft.gestiondocumental.webapp.BuscarDocumentoAction;
import java.io.IOException;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Cristhian
 */
public class GestionarServlet extends HttpServlet {

    private static final Log LOG = LogFactory.getLog(GestionarServlet.class);
    
    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    @Inject
    private BuscarDocumentoAction buscar;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String radicacion = request.getParameter("radicacion");
        HttpSession session = request.getSession();
        Usuario usuarioLogin = (Usuario) session.getAttribute("username");
        buscar.setUsuarioLogin(usuarioLogin);
        try {
            Long idRad = Long.parseLong(radicacion);
            Documento docResult = em.find(Documento.class, idRad);
            buscar.informacionParaGestion(docResult, usuarioLogin.getActor());
            buscar.setGestionBuscar(false);
            buscar.setGestionInbox(Boolean.TRUE);
            Boolean isRevisor = buscar.usuarioPermitido(docResult, usuarioLogin);
            if (isRevisor) {
                response.sendRedirect(request.getContextPath() + "/html/documento/GestionarDocumento.xhtml");
            } else {
                response.sendRedirect(request.getContextPath() + "/html/documento/BuscarDocumento.xhtml");
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
