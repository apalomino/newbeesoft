/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UsuarioLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Perfil;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import com.beesoft.beesoft.gestiondocumental.webapp.UsuarioAction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateful
@SessionScoped
public class UsuarioBean implements UsuarioLocal {

    @PersistenceContext(name = "BeesoftPU")
    private EntityManager em;

    @EJB
    private NotificationMailLocal notificationBean;

    private Boolean logueado = false;

    @Override
    public void crearUsuario(String username, String password,
            Perfil perfil, Actor actor) throws Exception {
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        String Encryptedpassword = passwordEncryptor.encryptPassword(password);

        List<Usuario> usuarios = (List<Usuario>) em.createQuery("Select u from Usuario u where u.userName=:username and u.estado=true").setParameter("username", username).getResultList();
        if (usuarios != null && !usuarios.isEmpty()) {
            throw new Exception("Ya existe un Usuario con ese nombre de usuario");
        }
        
        Usuario user = new Usuario();
        user.setUserName(username);
        user.setPassword(Encryptedpassword);
        user.setPerfil(perfil);
        user.setActor(actor);
        user.setEstado(true);
        em.persist(user);
    }

    @Override
    public void cambiarPasswordUsuario(Usuario user, String newPassword) {
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        String Encryptedpassword = passwordEncryptor.encryptPassword(newPassword);
        Usuario usuario = em.find(Usuario.class, user.getId());
        if (null != usuario) {
            usuario.setPassword(Encryptedpassword);
            em.merge(usuario);
        }
    }

    @Override
    public void editarUsuario(Usuario user, String newPassword) {
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        String Encryptedpassword = passwordEncryptor.encryptPassword(newPassword);
        Usuario usuario = em.find(Usuario.class, user.getId());
        if (null != usuario) {
            usuario.setPassword(Encryptedpassword);
            em.merge(usuario);
        }
    }

    @Override
    public Usuario login(String username, String password) throws Exception {
        RequestContext context = RequestContext.getCurrentInstance();
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        Usuario user = null;
        List<Usuario> users = em.createQuery("Select u from Usuario u where u.userName=:username and u.estado=true").setParameter("username", username).getResultList();
        if (users != null && !users.isEmpty()) {
            user = users.get(0);
            if (user.getEstado()) {
                if (passwordEncryptor.checkPassword(password, user.getPassword())) {
                    logueado = true;
                    session.setAttribute("username", user);
                    session.setAttribute("estaLogueado", logueado);
                    return user;
                } else {
                    session.setAttribute("estaLogueado", logueado);
                    throw new Exception("Las credenciales son invalidas");
                }
            } else {
                session.setAttribute("estaLogueado", logueado);
                throw new Exception("El usuario esta inhabilitado");
            }
        } else {
            session.setAttribute("estaLogueado", logueado);
            throw new Exception("Las credenciales son invalidas");
        }
    }
    
    @Override
    public void enviarCorreoCambioPassword(String email) throws Exception {
        List<Usuario> usuarios = (List<Usuario>) em.createQuery("Select u from Usuario u where u.actor.email=:email").setParameter("email", email).getResultList();
        if (usuarios != null && !usuarios.isEmpty()) {

            Usuario user = usuarios.get(0);
            List<Actor> actores = new ArrayList<Actor>();
            actores.add(user.getActor());

            String NUMEROS = "0123456789";

            String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";

            String cadena = NUMEROS + MAYUSCULAS + MINUSCULAS;

            String pswd = "";

            for (int i = 0; i < 8; i++) {
                pswd += (cadena.charAt((int) (Math.random() * cadena.length())));
            }

            StringBuilder message = new StringBuilder();
            message.append("Por solicitud del usuario se restablecio la contraseña,");
            message.append(" las credenciales son las siguientes:");
            message.append("<br>");
            message.append("Nombre de usuario: " + user.getUserName());
            message.append("<br>");
            message.append("Contraseña: " + pswd);
            message.append("<br>");
            message.append("<br>");

            cambiarPasswordUsuario(user, pswd);
            notificationBean.guardarCorreo(null, actores, "Reestablecimiento de contraseña", message.toString(), null, "CONTRASEÑA", false);
        } else {
            throw new Exception("El nombre de usuario ingresado no existe");
        }
    }

    @Override
    public Usuario buscarUsuario(Long id) throws Exception {
        Usuario usuario = em.find(Usuario.class, id);
        if (usuario != null) {
            return usuario;
        } else {
            throw new Exception("No existe ningun usuario con es id");
        }
    }

    @Override
    public void logout() {
        if (FacesContext.getCurrentInstance() != null) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                    .getExternalContext().getSession(false);
            logueado = (Boolean) session.getAttribute("estaLogueado");
            session.setAttribute("estaLogueado", logueado = false);
            session.invalidate();
        }
    }

    @Override
    public List<Usuario> buscarUsuarios(String nombre, String username, Perfil perfil) throws Exception {

        Query q = null;
        StringBuilder query = new StringBuilder("select u from Usuario u");
        Map<String, Object> parameters = new HashMap<String, Object>();
        boolean iswhere = false;

        if (StringUtils.isNotBlank(nombre)) {
            query.append(" where u.actor.nombreYApellido like :nombre");
            parameters.put("nombre", "%" + nombre + "%");
            iswhere = true;
        }

        if (StringUtils.isNotBlank(username)) {
            if (iswhere) {
                query.append(" and u.userName=:user");
                parameters.put("user", username);
            } else {
                query.append(" where u.userName=:user");
                parameters.put("user", username);
                iswhere = true;
            }
        }

        if (perfil != null) {
            if (iswhere) {
                query.append(" and u.perfil.id=:perfil");
                parameters.put("perfil", perfil.getId());
            } else {
                query.append(" where u.perfil.id=:perfil");
                parameters.put("perfil", perfil.getId());
                iswhere = true;
            }
        }

        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        List<Usuario> resultados = q.getResultList();
        if (resultados != null && !resultados.isEmpty()) {
            return resultados;
        } else {
            throw new Exception("Los parametros de busqueda no arrojan resultados");
        }
    }

    @Override
    public void editarUsuario(Usuario usuario, Perfil perfil) {
        if (usuario != null) {
            usuario.setPerfil(perfil);
            em.merge(usuario);
        }
    }

    @Override
    public void cambiarEstado(Long usuario_id, Boolean estado) {
        Usuario usuario = em.find(Usuario.class, usuario_id);
        if (usuario != null) {
            usuario.setEstado(estado);
            em.merge(usuario);
        }
    }

    public Boolean isLogueado() {
        return logueado;
    }

    public void setLogueado(Boolean logueado) {
        this.logueado = logueado;
    }

}
