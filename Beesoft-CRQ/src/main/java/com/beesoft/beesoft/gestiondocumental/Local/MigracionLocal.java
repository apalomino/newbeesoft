/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import java.io.File;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cfernandez@sumset.com
 */
@Local
public interface MigracionLocal {

    public void migrarActores();

    public void migrarTablasRetencion();

    public void migrarDocumento(List<String> ids);

    public List<String> consultarIds();

    public void migrarProcedencia(List<String> ids);

    public void migrarDestinos(List<String> ids);

    public void migrarRevisores(List<String> ids);

    public void migrarDOcTrd(List<String> ids);

    public List<String> consultarIdsPorAño(String año);

    public void migrarImagenes(List<String> archivos, String ruta);

    public void migrarImagenes(String nombre, ArchivoDocumento archivo);

    public List<String> consultarDocumentosFaltantes(String id);

    public void migrarDestinosFaltantes(List<String> ids, String id);

    public void migrarFaltantesIncompletos(List<Long> lista);

    public List<Long> consultarDocumentosIncompletos();

    public void migrarRevisiones(List<String> ids);

    public List<String> consultarIdsRevisiones();

    public List<String> consultarCorreosUsuarios();

    public void migrarUsuarios(List<String> correos);

    public void completarTRD();

    public List<Documento> consultarReferenciasCruzadas(String año);

    public void organizarReferenciasCruzadas(List<Documento> lista);
    
    public void migrarClases();
}
