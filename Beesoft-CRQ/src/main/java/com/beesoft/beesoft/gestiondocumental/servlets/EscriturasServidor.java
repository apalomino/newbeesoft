/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristhian
 */
public class EscriturasServidor extends HttpServlet {

    @PersistenceContext(unitName = "BeesoftPU")
    EntityManager em;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String escritura = request.getParameter("escritura");
        String root = System.getenv("JBOSS_HOME") + File.separator + "rootEdeq";
        // Manejar cuando no se encuentra
        ServletOutputStream out = response.getOutputStream();
        
        String directory = root + File.separator + "escritura" + File.separator + escritura + File.separator + escritura + ".tif";
        File archivo = new File(directory);
        try {
            System.out.print("revisando extensiones");

            int dotPosition = (archivo.getName()).lastIndexOf('.');
            String extension[] = {"tif", "tiff", "txt", "htm", "jpg", "gif", "pdf", "doc"};
            String mimeType[] = {"image/tiff", "image/tiff", "text/plain", "text/html", "image/jpg", "image/gif", "application/pdf", "application/msword"};
            String fileExtension = (archivo.getName()).substring(dotPosition + 1);
            System.out.print("extension" + fileExtension);

            boolean encontro = false;
            for (int index = 0; index < mimeType.length; index++) {

                if (fileExtension.equalsIgnoreCase(extension[index])) {
                    response.setContentType(mimeType[index]);
                    index = mimeType.length;
                    encontro = true;
                }
            }
            if (encontro == false) {
                response.setContentType("text/plain");
            }
            System.out.print(response.toString());

            FileInputStream in = new FileInputStream(archivo);

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
