/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Local;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoPersona;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

import org.apache.axis2.classloader.ResourceHandle;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Local
public interface ActorLocal {

    /**
     * Metodo encargado de crear un actor.
     *
     *
     * @param cargo , cargo de la persona.
     * @param cedula , numero de cedula.
     * @param direccion , direccion de la persona.
     * @param email , correo de la persona.
     * @param fechaNacimiento , fecha de nacimiento de la persona.
     * @param nombreYApellido , nombres de la persona.
     * @param ciudad , ciudad de la persona.
     * @param tipopersona ,tipo de persona(empleado, jefe, administrador).
     * @param telefonos , lista de telefonos.
     * @param Jefe , jefe inmediato.
     * @param oficina , oficina donde labora.
     */
    public Actor crearActor(String nombreYApellido, String cedula, String direccion, Ciudad ciudad,
            String email, TipoPersona tipopersona, String telefono, String telefonoExt,
            Actor Jefe, Actor empresa, String cargo, Oficina oficina, String firma) throws Exception;

    /**
     * Metodo encargado de editar un actor.
     *
     * @param cedula , numero de cedula.
     * @param direccion , direccion de la persona.
     * @param email , correo de la persona.
     * @param fechaNacimiento , fecha de nacimiento de la persona.
     * @param nombreYApellido , nombres de la persona.
     * @param ciudad , ciudad de la persona.
     * @param tipopersona ,tipo de persona(empleado, jefe, administrador).
     * @param telefonos , lista de telefonos.
     * @param Jefe , jefe inmediato.
     * @param oficina , oficina donde labora.
     */
    public void editarActor(Long id, String nombreYApellido, String cedula, String direccion1,
            Ciudad ciudad, String email, TipoPersona tipopersona, String telefono, String telefonoExt,
            Actor Jefe, Actor empresa, String cargo, Oficina oficina, String firma, Boolean estado);

    /**
     * Metodo para buscar actor.
     *
     * @param id, identificador del actor.
     * @return actor.
     * @throws Exception si hay algun error.
     */
    public Actor buscarActor(Long id) throws Exception;

    /**
     * Metodo para cargar los actores.
     *
     * @return lista de actores.
     * @throws Exception
     */
    public Map<Long, String> cargarListaActores() throws Exception;

    /**
     * Metodo para cargar lista de procedencia de un documento.
     *
     * @param nombreApellido, nombre del actor.
     * @param cedula, cedula de actor.
     * @param tipo
     * @return lista de actores asociados como procedencias.
     * @throws Exception
     */
    public List<Actor> listaProcedencia(String nombreApellido, String cedula, TipoPersona tipo, Boolean busqueda) throws Exception;

    /**
     * Metodo para listar todos los actores en la base de datos.
     *
     * @return lista de actores.
     * @throws Exception si ocurre algun error.
     */
    public List<Actor> listaProcedencia() throws Exception;

    /**
     * Metodo encargado de cargar los actores que son empleados.
     *
     * @return lista de empleados.
     * @throws Exception si ocurre una excepcion.
     */
    public List<Actor> listaEmpleados() throws Exception;

    /**
     * Metodo para cargar actores que son jefes.
     *
     * @return lista de jefes.
     * @throws Exception si ocurre un error.
     */
    public List<Oficina> listaJefes() throws Exception;

    /**
     * Metodo para cargar actores que son administradores y radicadores.
     *
     * @return lista de admins y radicadores.
     * @throws Exception si hay algun error.
     */
    public Map<Long, String> cargarListaAdminRad() throws Exception;

    /**
     * Metodo para cargar la lista de empleados con usuario.
     *
     * @return lista de usuarios del sistema.
     * @throws Exception si hay algun error.
     */
    public List<Actor> listaUsuarios() throws Exception;

    /**
     * cambia el estado del actor seleccionado.
     *
     * @param id
     * @param estado
     */
    public void cambiarEstadoActor(Long id, Boolean estado);

    /**
     * Metodo para cambiar direccion a actor.
     *
     * @param id
     * @param direccion
     */
    public void cambiarDireccionActor(Long id, String direccion, String nombre, Ciudad ciudad);

    public void editarActorDatosBasicos(Actor personaEditar);

    public List<Actor> cargarEmpresas();

    public List<Actor> listaEmpleadosSinUsuario() throws Exception;

    public List<Actor> listarEmpleados(Long jefeId) throws Exception;

    public Map<Long, String> listarActores() throws Exception;

    public void cambiarJefeOficina(Long idOficina, String cargo, Long idJefe, Long idJefeEncargado,
            Boolean jefeAcargo);

    public String mostrarJefeOficinaTexto(Actor actor);

    public Actor jefeDeOficina(Long idOficina);

    
}
