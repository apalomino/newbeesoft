/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.NotificationMailDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivosAdjuntos;
import com.beesoft.beesoft.gestiondocumental.entidades.DatosConfiguracion;
import com.beesoft.beesoft.gestiondocumental.entidades.NotificationMail;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.RadicacionCorreo;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.rmi.PortableRemoteObject;
import javax.transaction.Transactional;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Stateless
public class NotificationMailBean implements NotificationMailLocal, Serializable {

	private static final Log LOG = LogFactory.getLog(NotificationMailBean.class);

	@PersistenceContext(unitName = "BeesoftPU")
	private EntityManager em;

	@EJB
	private UtilidadesLocal utilidadBean;

	private Context ctx;

	@Override
	public void enviarCorreo(NotificationMailDTO mail) {
		int number = mail.getDestinatarios().size();
		List<String> correos = new ArrayList<>(mail.getDestinatarios());

		if (number <= 50) {
			cuerpoMensaje(mail.getEmailEmisor(), correos, mail.getAsunto(), mail.getAdjuntos(), mail.getMensaje(),
					mail.getCertificado());
		} else if (number > 50) {
			List<String> correosTmp = new ArrayList<>();
			for (int i = 0; i < number; i++) {
				correosTmp.add(correos.get(i));

				int cont = correosTmp.size();
				if (cont == 50) {
					cuerpoMensaje(mail.getEmailEmisor(), correosTmp, mail.getAsunto(), mail.getAdjuntos(),
							mail.getMensaje(), mail.getCertificado());
					correosTmp = new ArrayList<>();
				}
			}

			if (!correosTmp.isEmpty()) {
				cuerpoMensaje(mail.getEmailEmisor(), correosTmp, mail.getAsunto(), mail.getAdjuntos(),
						mail.getMensaje(), mail.getCertificado());
				correosTmp = new ArrayList<>();
			}
		}

	}

	@SuppressWarnings("unchecked")
	public void cuerpoMensaje(String emisor, List<String> correos, String asunto, List<ArchivosAdjuntos> adjuntos,
			String mensaje, Boolean certificado) {
		DatosConfiguracion configuracion = new DatosConfiguracion();
		try {
			javax.mail.Session session = null;
			Properties props = new Properties();
			configuracion = utilidadBean.mostrarConfiguracionCorreo();
			if (configuracion != null) {
				props.put(ConstantsMail.AUTH, configuracion.getAuthetication());
				props.put(ConstantsMail.STARTTLS, configuracion.getStarttls());
//				props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
				props.put(ConstantsMail.HOST, configuracion.getHost());
				props.put(ConstantsMail.PORT, configuracion.getPuerto());
				props.put(ConstantsMail.USER, configuracion.getEmailEmisor());
				props.put(ConstantsMail.PASSWORD, configuracion.getPassword());
				session = javax.mail.Session.getInstance(props);

			} else {
				session = (Session) PortableRemoteObject.narrow((new InitialContext()).lookup("java:/Mail"),
						javax.mail.Session.class);
				String user = session.getProperty("mail.smtp.user");
				String password = session.getProperty("mail.smtp.password");
				String smtpServer = session.getProperty("mail.smtp.host");
				String protocol = session.getProperty("mail.transport.protocol");
			}
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(emisor));
				if (certificado) {
					message.addRecipient(Message.RecipientType.CC,
							new InternetAddress("correo@certificado.4-72.com.co"));
				}

				message.addRecipients(Message.RecipientType.TO, correosDestino(correos));
				message.setSubject(asunto);

				if (adjuntos != null) {
					message.setContent(procesarAdjuntosCompletos(adjuntos, mensaje), "multipart/form-data");
				} else {
					message.setContent(procesarAdjuntosCompletos(null, mensaje), "multipart/form-data");
				}
				if (configuracion != null) {
					Transport t = session.getTransport("smtp");
					t.connect((String) props.get(ConstantsMail.HOST), (String) props.get(ConstantsMail.USER),
							(String) props.get(ConstantsMail.PASSWORD));
					t.sendMessage(message, message.getAllRecipients());
					t.close();
				} else {
					Transport.send(message);
				}

			} catch (MessagingException e) {
				LOG.info(e);
			}

		} catch (NamingException ex) {
			LOG.info(ex);
		}

	}

	@Override
	public void guardarCorreo(Actor emisor, List<Actor> destinations, String asunto, String mensaje,
			List<ArchivosAdjuntos> adjuntos, String tipoComunicado, Boolean certificacion) {
		QueueConnectionFactory factory = null;
		QueueConnection connection = null;
		QueueSession session = null;
		QueueSender sender = null;
		try {
			this.ctx = new InitialContext();
			javax.jms.Queue queue = (javax.jms.Queue) ctx.lookup(ConstantsMail.NOTIFICATION_QUEUE);
			factory = (QueueConnectionFactory) ctx.lookup(ConstantsMail.CONNECTION_FACTORY);
			connection = factory.createQueueConnection();
			session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
			sender = session.createSender(queue);
			Long id = crearEntity(emisor, destinations, asunto, mensaje, tipoComunicado);
			if (id != null) {
				for (Actor actor : destinations) {
					if (ValidacionesUtil.validarPatronCorreo(actor.getEmail())) {
						NotificationMailDTO mailDTO = crearDTO(emisor, actor, asunto, mensaje, adjuntos, certificacion);
						ObjectMessage objectMessage = session.createObjectMessage(mailDTO);
						sender.send(objectMessage);
					}
				}
			}
		} catch (NamingException ex) {
			LOG.info(ex);
		} catch (JMSException ex) {
			LOG.info(ex);
		} finally {
			try {
				if (sender != null) {
					sender.close();
				}
				if (session != null) {
					session.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (JMSException ex) {
				LOG.info(ex);
			}
		}
	}

	@Override
	public List<Oficina> cargarListaDestinatariosOficina() {
		List<Oficina> lista = (List<Oficina>) em.createQuery("Select o from Oficina o").getResultList();
		if (lista != null && !lista.isEmpty()) {
			return lista;
		}
		return null;
	}

	@Override
	public List<Actor> cargarListaDestinatarios() {
		List<Actor> lista = em.createQuery(
				"Select u.actor from Usuario u where u.estado=true and u.actor.tipopersona.descripcion!=:perfil ")
				.setParameter("perfil", ConstantsMail.TIPO_JEFE).getResultList();
		if (lista != null && !lista.isEmpty()) {
			return lista;
		}
		return null;
	}

	@Override
	public List<Actor> cargarTodosDestinatarios() {
		List<Actor> lista = (List<Actor>) em.createQuery("Select a from Actor a where a.estado=true").getResultList();
		if (null != lista) {
			return lista;
		}
		return null;
	}

	public String obtenerHost(String from) {
		String[] array = from.split("@");
		String host = array[1];

		if (host.indexOf(ConstantsMail.HOTMAIL) == 0) {
			return ConstantsMail.STMP_HOTMAIL;
		} else if (host.indexOf(ConstantsMail.GMAIL) == 0) {
			return ConstantsMail.STMP_GMAIL;
		} else {
			return ConstantsMail.STMP_GMAIL;
		}
	}

	public static Address[] correosDestino(List<String> destinos) throws AddressException {
		Address[] add = new Address[destinos.size()];
		for (int i = 0; i < destinos.size(); i++) {
			add[i] = new InternetAddress(destinos.get(i));
		}
		return add;
	}

	public Long crearEntity(Actor from, List<Actor> destinations, String asunto, String mensaje, String moduloEnvio) {
		NotificationMail mail = new NotificationMail();
		List<Actor> destinationsList = new ArrayList<>();

		mail.setFromAdress(from);
		for (Actor a : destinations) {
			if (a.getId() != null) {
				Actor actor = em.find(Actor.class, a.getId());
				destinationsList.add(actor);
			}
		}

		mail.setDestinatarios(destinationsList);
		mail.setAsunto(asunto);
		mail.setMensaje(mensaje);
		mail.setFechaEnvio(new Date());
		mail.setModuloEnvio(moduloEnvio);
		em.persist(mail);

		return mail.getId();
	}

	public NotificationMailDTO crearDTO(Actor from, Actor destino, String asunto, String mensaje,
			List<ArchivosAdjuntos> adjuntos, Boolean certificado) {

		List<DatosConfiguracion> lista = em.createQuery("select d FROM DatosConfiguracion d").getResultList();
		DatosConfiguracion datos = null;
		if (lista != null && !lista.isEmpty()) {
			datos = lista.get(0);
		}
		List<String> correos = new ArrayList<>();
		NotificationMailDTO mail = new NotificationMailDTO();
		mail.setEmailEmisor(datos.getEmailEmisor());
		correos.add(destino.getEmail());
		mail.setMensaje(mensaje);
		mail.setDestinatarios(correos);
		mail.setAsunto(asunto);
		mail.setAdjuntos(adjuntos);
		mail.setCertificado(certificado);
		return mail;
	}

	/**
	 * Notifica por mail que llegó un comunicado interno
	 * @param actor
	 * @param actorReenvio
	 * @param idCI
	 * @param radicacion
	 * @return
	 */
//	public String MensajeComunicadoInterno(Actor actor, Actor actorReenvio, Long idCI, String radicacion) {
//		String url = ValidacionesUtil.urlServer();
//		StringBuilder message = new StringBuilder("Sr(a). " + actor.getNombreYApellido() + " tiene asignado el");
//		message.append(" comunicado interno <a href=\"http://" + url + ":8080/Beesoft/Comunicado?radicacion=")
//				.append(idCI).append("\">").append(radicacion).append("</a>,");
//		message.append("de click en el numero para visualizarlo.");
//		message.append("<br>");
//		if (actorReenvio != null) {
//			message.append("Reenviado por: ").append(actorReenvio.getNombreYApellido());
//		}
//		return message.toString();
//	}

	public  static Multipart procesarAdjuntosCompletos(List<ArchivosAdjuntos> adjuntos, String mensaje) {
		try {
			return procesarAdjuntos(adjuntos, mensaje);
		} catch (MessagingException ex) {
			Logger.getLogger(NotificationMailBean.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(NotificationMailBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public static Multipart procesarAdjuntos(List<ArchivosAdjuntos> adjuntos, String mensaje)
			throws MessagingException, IOException {

		Multipart multipart = new MimeMultipart();
		MimeBodyPart message = new MimeBodyPart();
		if (adjuntos != null && !adjuntos.isEmpty()) {

			for (ArchivosAdjuntos f : adjuntos) {
				try {
					String type = "";
					if (StringUtils.isBlank(f.getTipo())) {
						String ext = FilenameUtils.getExtension(f.getNombreArchivo());

						String extension[] = { "tif", "tiff", "txt", "htm", "jpg", "gif", "pdf", "doc", "ppt", "xls",
								"docx", "pptx", "xlsx" };
						String mimeType[] = { "image/tiff", "image/tiff", "text/plain", "text/html", "image/jpg",
								"image/gif", "application/pdf", "application/msword", "application/vnd.ms-powerpoint",
								"application/vnd.ms-excel",
								"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
								"application/vnd.openxmlformats-officedocument.presentationml.presentation",
								"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" };
						for (int index = 0; index < mimeType.length; index++) {
							if (ext.equalsIgnoreCase(extension[index])) {
								type = mimeType[index];
							}
						}
					} else {
						type = f.getTipo();
					}
					DataSource source = new ByteArrayDataSource(f.getArchivo(), type);
					MimeBodyPart adjunto = new MimeBodyPart();
					adjunto.setDataHandler(new DataHandler(source));
					adjunto.setFileName(f.getNombreArchivo());
					multipart.addBodyPart(adjunto);

				} catch (MessagingException ex) {
					LOG.info(ex);
				}
			}
		}
		message.setContent(mensaje, "text/html");
		multipart.addBodyPart(message);
		return multipart;
	}

	@Override
	public List<NotificationMail> buscarCorreo(String radicacion, List<Actor> actores, String asunto, Date fechaIni,
			Date fechaFin) {

		Query q = null;
		StringBuilder query = new StringBuilder("select mail from NotificationMail mail");
		Map<String, Object> parameters = new HashMap<>();
		boolean iswhere = false;
		boolean isdestinations = false;

		if (actores != null && !actores.isEmpty()) {
			query.append(" left join mail.destinatarios dest");
			isdestinations = true;
		}

		List<NotificationMail> correos = new ArrayList<>();
		List<NotificationMail> mails;

		if (StringUtils.isNotBlank(radicacion)) {
			query.append(" where mail.radicacion=:radicacion");
			parameters.put("radicacion", radicacion);
			iswhere = true;
		}

		if (fechaIni != null && fechaFin != null) {
			if (iswhere) {
				query.append(" and mail.fechaEnvio BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
			} else {
				query.append(" where mail.fechaEnvio BETWEEN :fechaIni AND :fechaFin");
				parameters.put("fechaIni", fechaIni);
				parameters.put("fechaFin", fechaFin);
				iswhere = true;
			}
		}

		if (StringUtils.isNotBlank(asunto)) {
			if (iswhere) {
				query.append(" and mail.asunto like :asunto");
				parameters.put("asunto", "%" + asunto + "%");
			} else {
				query.append(" where mail.asunto like :asunto");
				parameters.put("asunto", "%" + asunto + "%");
			}
		}

		if (isdestinations) {
			if (iswhere) {
				query.append(" and dest in :desti");
				parameters.put("desti", actores);
			} else {
				query.append(" where dest in :desti");
				parameters.put("desti", actores);
				iswhere = true;
			}
		}

		q = em.createQuery(query.toString());

		if (!parameters.isEmpty()) {
			for (Map.Entry<String, Object> p : parameters.entrySet()) {
				q.setParameter(p.getKey(), p.getValue());
			}
		}

		mails = q.getResultList();

		if (mails != null) {
			HashSet<NotificationMail> hash = new HashSet<NotificationMail>(mails);
			mails.clear();
			mails.addAll(hash);
		}
		return mails;
	}

	@Override
	public List<NotificationMail> buscarComunicadosInOut(Actor actor, String mensaje, Boolean inOut) throws Exception {
		Query q = null;
		StringBuilder query = new StringBuilder("select mail from NotificationMail mail");
		Map<String, Object> parameters = new HashMap<>();
		boolean iswhere = false;

		if (actor != null) {
			if (inOut != null) {
				if (inOut) {
					query.append(" where mail.fromAdress.id=:from");
					parameters.put("from", actor.getId());
					iswhere = true;
				} else {
					query.append(" left join mail.destinatarios destination  where destination.id=:dest");
					parameters.put("dest", actor.getId());
					iswhere = true;
				}
			}
		}

		if (StringUtils.isNotBlank(mensaje)) {
			if (iswhere) {
				query.append(" and mail.mensaje like :mensaje");
				parameters.put("mensaje", "%" + mensaje + "%");
			} else {
				query.append(" where mail.mensaje like :mensaje");
				parameters.put("mensaje", "%" + mensaje + "%");
			}
		}

		q = em.createQuery(query.toString());

		if (!parameters.isEmpty()) {
			for (Map.Entry<String, Object> p : parameters.entrySet()) {
				q.setParameter(p.getKey(), p.getValue());
			}
		}

		List<NotificationMail> mails = q.getResultList();

		if (mails != null && !mails.isEmpty()) {
			return mails;
		} else {
			throw new Exception("Los parametros de busqueda no arrojan resltados");
		}
	}

	@Override
	public NotificationMail buscarCorreoId(Long id) {
		NotificationMail mail = em.find(NotificationMail.class, id);
		return mail;
	}

	@Override
	public List<NotificationMail> buscarRenviados(String radicacion) throws Exception {
		List<NotificationMail> lista = em.createQuery("select n from NotificationMail n where n.radicacionReenvio=:rad")
				.setParameter("rad", radicacion).getResultList();
		if (lista != null && !lista.isEmpty()) {
			return lista;
		} else {
			throw new Exception("Este comunicado no ha sido reenviado");
		}
	}

	@Override
	public List<String> consecutivoRadicacionCorreo() throws Exception {
		List<RadicacionCorreo> concecutivos = em.createQuery("select r from RadicacionCorreo r").getResultList();
		List<String> letras = new ArrayList<>();
		if (concecutivos != null && !concecutivos.isEmpty()) {
			for (RadicacionCorreo radicacion : concecutivos) {
				letras.add(radicacion.getLetra() + "00" + radicacion.getNumeroSecuencia());
			}
			return letras;
		} else {
			throw new Exception("Los consecutivos de para radicar correo no se han precargado");
		}
	}

	@Override
	public String consecutivoRadicacionCorreoXLetra(String letra) throws Exception {
		List<RadicacionCorreo> concecutivos = em.createQuery("select r from RadicacionCorreo r where r.letra=:letra")
				.setParameter("letra", letra).getResultList();
		String radicado = "";
		if (concecutivos != null && !concecutivos.isEmpty()) {
			for (RadicacionCorreo letraradicacion : concecutivos) {
				radicado = letraradicacion.getLetra() + "-00" + letraradicacion.getNumeroSecuencia();
			}
			return radicado;
		} else {
			throw new Exception("Ingrese una letra valida");
		}
	}

	@Override
	public void generarRadicado(String cadena) {
		Map<String, Long> mapaCadena = ValidacionesUtil.ValidarRadicado(cadena);
		if (!mapaCadena.isEmpty()) {
			String letras = "";
			Long numeroFormateado = null;
			for (Map.Entry<String, Long> entry : mapaCadena.entrySet()) {
				letras = entry.getKey();
				numeroFormateado = entry.getValue();
			}

			List<RadicacionCorreo> listaLetras = em.createQuery("select l from RadicacionCorreo l where l.letra=:letra")
					.setParameter("letra", letras).getResultList();
			if (listaLetras != null && !listaLetras.isEmpty()) {
				for (RadicacionCorreo letraradicacion : listaLetras) {
					letraradicacion.setNumeroSecuencia(numeroFormateado + 1);
					em.merge(letraradicacion);
				}
			}
		}
	}

	@Override
	public String buscarConsecutivoCorreo(String radicacion) {
		Map<String, Long> mapaCadena = new HashMap<String, Long>();
		mapaCadena = ValidacionesUtil.ValidarRadicado(radicacion);
		String letra = "";
		for (Map.Entry<String, Long> entry : mapaCadena.entrySet()) {
			List<RadicacionCorreo> letras = em.createQuery("Select r from RadicacionCorreo r where r.letra=:letra")
					.setParameter("letra", entry.getKey()).getResultList();
			letra = letras.get(0).getLetra() + "00" + letras.get(0).getNumeroSecuencia();
		}
		return letra;
	}

	public void stringTONumber(String cadena) {
		String letras = "";
		String numeros = "";
		Long numeroFormateado = null;
		for (short indice = 0; indice < cadena.length(); indice++) {
			char caracter = cadena.charAt(indice);
			if (ValidacionesUtil.isNumeric(caracter)) {
				numeros += caracter;
			} else {
				letras += caracter;
			}
		}

		numeroFormateado = new Long(numeros);

		List<RadicacionCorreo> listaLetras = em.createQuery("select r from RadicacionCorreo r where r.letra=:letra")
				.setParameter("letra", letras).getResultList();
		if (listaLetras != null && !listaLetras.isEmpty()) {
			for (RadicacionCorreo letraradicacion : listaLetras) {
				letraradicacion.setNumeroSecuencia(numeroFormateado + 1);
				em.merge(letraradicacion);
			}
		}

	}

}
