package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * The persistent class for the trd database table.
 *
 */
@Entity
public class Trd implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private TipoDocumento tipodocumento;

    private Documento documento;

    private Oficina oficina;

    private Serie serie;

    private SubSerie subserie;
    
    private Boolean medioElectronico;

    public Trd() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public TipoDocumento getTipodocumento() {
        return this.tipodocumento;
    }

    public void setTipodocumento(TipoDocumento tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    
    @ManyToOne
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @ManyToOne
    public Serie getSerie() {
        return this.serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }

    @ManyToOne
    public SubSerie getSubserie() {
        return this.subserie;
    }

    public void setSubserie(SubSerie subserie) {
        this.subserie = subserie;
    }

    @ManyToOne
    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public Boolean getMedioElectronico() {
        return medioElectronico;
    }

    public void setMedioElectronico(Boolean medioElectronico) {
        this.medioElectronico = medioElectronico;
    }
    
}
