/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ClaseLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ComunicadosInternosLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.PerfilLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DestinoDTO;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.model.DualListModel;

/**
 *
 * @author ISSLUNO
 */
@Named
@ApplicationScoped
@Startup
public class ApplicationAction implements Serializable {

    private static final Log LOG = LogFactory.getLog(ApplicationAction.class);

    @EJB
    private UtilidadesLocal utilidadesBean;

    @EJB
    private OficinaLocal oficinaBean;

    @EJB
    private DocumentoLocal docBean;

    @EJB
    private ActorLocal actorBean;

    @EJB
    private PerfilLocal perfilBean;

    @EJB
    private NotificationMailLocal notificationMailBean;

    @EJB
    private ComunicadosInternosLocal comunicadosInternosBean;

    @EJB
    private ClaseLocal claseBean;

    private String oficinaValue;
    private Map<Long, String> oficinas;
    private Oficina oficina;
    private List<Clase> clasesDocumento;
    private List<Actor> responsableIn;
    private List<String> empresaCorreo;
    private Map<Long, String> listaResponsables;
    private String claseDocumento;
    private List<Actor> revisoresIn;
    private List<Actor> revisoresTarget;
    private DualListModel<Actor> revisoresList;
    private Map<Long, String> mapaRevisores;
    private Map<Long, String> revisorMap;
    private Map<Long, String> listaTipoPersona;
    private Map<Long, String> listaTipoPersonaDocumento;
    private Map<Long, String> listaTipoPersonaPqr;
    private Map<Long, String> mapaPerfil = new HashMap<>();
    private Map<Long, String> mapaActor = new HashMap<>();
    private Map<Long, String> mapaActorSinUsuario = new HashMap<>();
    private List<String> listaPerfil;
    private List<String> listaActores;
    private List<Oficina> listaJefes;
    private List<String> tiposCodigoNiu;
    private List<String> subTiposCodigoNiu;
    private Map<String, String> tiposContratos;
    private Map<String, String> subTiposContratos;
    //-----------------------------------------
    private List<Actor> destinations;
    private List<Actor> listaDestinatariosAll;
    private List<Oficina> listaOficinasAll;
    private List<Actor> listaDestinatariosExternosInternos;
    private List<Actor> listaDestinatariosTerceros;
    private List<Actor> listaDestinatariosAllExternos;
    private List<Actor> listaEmpresas;
    private Map<Long, String> mapaDestinatarios;
    private Map<Long, String> mapaDestinatariosExternosInternos;
    private List<String> destinationsAgregados;
    //---------------------------------------------
    private List<Actor> destinationsBusqueda;
    private Map<Long, String> mapaDestinatariosBusqueda;
    private List<String> destinationsAgregadosBusqueda;
    //----------------------------------------------------
    private List<Actor> destinationsReenvio;
    private Map<Long, String> mapaDestinatariosReenvio;
    private List<String> destinationsAgregadosReenvio;
    //-----------------------------------------------------
    private Map<Long, String> mapaDestinatariosExternos;
    private List<String> listaDestinatariosExternosInternosNombres;
    private List<String> listaDestinatariosExternosNombres;

    private List<DestinoDTO> listaComunicadosInternos;

    List<Documento> documentosInbox;

    @PostConstruct
    public void inicializar() {
        clasesDocumento = new ArrayList<>();
        cargarClases();
        cargarOficinas();
        buscarResponsable();
        cargarEmpresaCorreo();
        cargarRevisores();
        cargarPerfiles();
        cargarListaActores();
        cargarListaActoresSinUsuario();
        cargarTipos();
        cargarTiposPersonaDocumento();
        cargarJefes();
        buscarDestinatarios();
        buscarActoresEmpresa();
        listaTipoContratos();
        listaSubTipoConttratos();
        llenarInformacionDestinoDto();
        claseDocumento = ConstantsMail.CLASE_GESTION;
    }
    
    public void cargarOficinas() {
        try {
            oficinas = oficinaBean.cargarOficinas();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void llenarInformacionDestinoDto() {
        listaComunicadosInternos = new ArrayList<>();
        if (listaJefes != null) {
            for (Oficina oficina : listaJefes) {
                if (oficina.getJefe() != null) {
                    if (oficina.getJefeACargo()) {
                        DestinoDTO destino = new DestinoDTO();
                        destino.setId(oficina.getId());
                        destino.setNombreDestino(oficina.getJefe().getNombreYApellido() + "-" + oficina.getDescripcion());
                        destino.setTipoDestino("JEFE");
                        listaComunicadosInternos.add(destino);
                    } else {
                        DestinoDTO destino = new DestinoDTO();
                        destino.setId(oficina.getId());
                        destino.setNombreDestino(oficina.getJefeEncargado().getNombreYApellido() + "-" + oficina.getDescripcion());
                        destino.setTipoDestino("JEFE");
                        listaComunicadosInternos.add(destino);
                    }
                }
            }
        }

        for (Actor actor : listaDestinatariosAll) {
            DestinoDTO destino = new DestinoDTO();
            destino.setId(actor.getId());
            destino.setNombreDestino(actor.getNombreYApellido());
            destino.setTipoDestino("EMPLEADO");
            listaComunicadosInternos.add(destino);
        }

    }

    public void buscarDestinatarios() {
        listaDestinatariosAll = new ArrayList<>();
        listaDestinatariosAll = notificationMailBean.cargarListaDestinatarios();
    }

    public void buscarOficinasDestinatarios() {
        listaOficinasAll = new ArrayList<>();
        listaOficinasAll = notificationMailBean.cargarListaDestinatariosOficina();
    }

    public void destinatariosExternosInternos() {

        mapaDestinatariosExternosInternos = new HashMap<>();
        listaDestinatariosExternosInternosNombres = new ArrayList<>();

        for (Actor actor : listaDestinatariosAllExternos) {
            if (actor.getEmpresa() != null) {
                mapaDestinatariosExternosInternos.put(actor.getId(), actor.getNombreYApellido() + " (" + actor.getEmpresa().getNombreYApellido() + ")");
                listaDestinatariosExternosInternosNombres.add(actor.getNombreYApellido() + " (" + actor.getEmpresa().getNombreYApellido() + ")");
            } else {
                mapaDestinatariosExternosInternos.put(actor.getId(), actor.getNombreYApellido());
                listaDestinatariosExternosInternosNombres.add(actor.getNombreYApellido());
            }
        }

//        for (Actor act : listaDestinatariosAll) {
//            if (act.getEmpresa() != null) {
//                mapaDestinatariosExternosInternos.put(act.getId(), act.getNombreYApellido() + " (" + act.getEmpresa().getNombreYApellido() + ")");
//                listaDestinatariosExternosInternosNombres.add(act.getNombreYApellido() + " (" + act.getEmpresa().getNombreYApellido() + ")");
//            } else {
//                mapaDestinatariosExternosInternos.put(act.getId(), act.getNombreYApellido());
//                listaDestinatariosExternosInternosNombres.add(act.getNombreYApellido());
//            }
//        }
    }

    public void buscarDocumentosPendientes(Actor actorRevisor) {
        try {
            documentosInbox = new ArrayList<>();
            documentosInbox = docBean.buscarDocumentosPendientes(actorRevisor);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void buscarActoresEmpresa() {
        listaEmpresas = actorBean.cargarEmpresas();
    }

    public void cargarClases() {
        try {
            clasesDocumento = claseBean.listarClases();
        } catch (Exception e) {
            LOG.info(e);
        }
    }

    public void buscarResponsable() {
        try {
            responsableIn = new ArrayList<>();
            responsableIn = actorBean.listaUsuarios();
            listaResponsables = new HashMap<>();
            for (Actor actor : responsableIn) {
                listaResponsables.put(actor.getId(), actor.getNombreYApellido());
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarEmpresaCorreo() {
        empresaCorreo = new ArrayList<>();
        empresaCorreo.add(ConstantsMail.EMP_472);
    }

    public void cargarRevisores() {
        try {
            revisoresIn = new ArrayList<>();
            revisoresIn = actorBean.listaUsuarios();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarListaActores() {
        List<Actor> lista = new ArrayList<>();
        try {
            lista = actorBean.listaEmpleados();
            for (Actor a : lista) {
                mapaActor.put(a.getId(), a.getNombreYApellido());
            }
        } catch (Exception e) {
            LOG.info(e);
        }
    }

    public void cargarListaActoresSinUsuario() {
        List<Actor> lista = new ArrayList<>();
        try {
            lista = actorBean.listaEmpleadosSinUsuario();
            for (Actor a : lista) {
                mapaActorSinUsuario.put(a.getId(), a.getNombreYApellido());
            }
        } catch (Exception e) {
            LOG.info(e);
        }
    }

    public void cargarPerfiles() {
        listaPerfil = new ArrayList<>();
        try {
            mapaPerfil = perfilBean.cargarPerfiles();
            for (Map.Entry<Long, String> p : mapaPerfil.entrySet()) {
                listaPerfil.add(p.getValue());
            }
            Collections.sort(listaPerfil);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarTipos() {
        try {
            listaTipoPersona = new HashMap<>();
            listaTipoPersona = utilidadesBean.cargarTipos();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarTiposPersonaDocumento() {
        try {
            Map<Long, String> lista = utilidadesBean.cargarTipos();
            listaTipoPersonaDocumento = new HashMap<>();
            for (Map.Entry<Long, String> tipoP : lista.entrySet()) {
                if (tipoP.getValue().equals(ConstantsMail.TIPO_EMPRESA)) {
                    listaTipoPersonaDocumento.put(tipoP.getKey(), tipoP.getValue());
                } else if (tipoP.getValue().equals(ConstantsMail.TIPO_PERSONA)) {
                    listaTipoPersonaDocumento.put(tipoP.getKey(), tipoP.getValue());
                }
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarTiposPersonaPQR() {
        try {
            Map<Long, String> lista = utilidadesBean.cargarTipos();
            listaTipoPersonaPqr = new HashMap<>();
            for (Map.Entry<Long, String> tipoP : lista.entrySet()) {
                if (tipoP.getValue().equals(ConstantsMail.TIPO_PERSONA)) {
                    listaTipoPersonaPqr.put(tipoP.getKey(), tipoP.getValue());
                }
            }
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void cargarJefes() {
        try {
            listaJefes = actorBean.listaJefes();
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void listaTipoContratos() {
        try {
            tiposContratos = utilidadesBean.cargarTiposContratosTexto(true);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    public void listaSubTipoConttratos() {
        try {
            subTiposContratos = utilidadesBean.cargarTiposContratosTexto(false);
        } catch (Exception ex) {
            LOG.info(ex);
        }
    }

    //----------------------------gettes.---------------------------------------------------
    public String getOficinaValue() {
        return oficinaValue;
    }

    public void setOficinaValue(String oficinaValue) {
        this.oficinaValue = oficinaValue;
    }

    public Map<Long, String> getOficinas() {
        return oficinas;
    }

    public void setOficinas(Map<Long, String> oficinas) {
        this.oficinas = oficinas;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public List<Clase> getClasesDocumento() {
        return clasesDocumento;
    }

    public void setClasesDocumento(List<Clase> clasesDocumento) {
        this.clasesDocumento = clasesDocumento;
    }

    public List<Actor> getResponsableIn() {
        return responsableIn;
    }

    public void setResponsableIn(List<Actor> responsableIn) {
        this.responsableIn = responsableIn;
    }

    public List<String> getEmpresaCorreo() {
        return empresaCorreo;
    }

    public void setEmpresaCorreo(List<String> empresaCorreo) {
        this.empresaCorreo = empresaCorreo;
    }

    public Map<Long, String> getListaResponsables() {
        return listaResponsables;
    }

    public void setListaResponsables(Map<Long, String> listaResponsables) {
        this.listaResponsables = listaResponsables;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public List<Actor> getRevisoresIn() {
        return revisoresIn;
    }

    public void setRevisoresIn(List<Actor> revisoresIn) {
        this.revisoresIn = revisoresIn;
    }

    public List<Actor> getRevisoresTarget() {
        return revisoresTarget;
    }

    public void setRevisoresTarget(List<Actor> revisoresTarget) {
        this.revisoresTarget = revisoresTarget;
    }

    public DualListModel<Actor> getRevisoresList() {
        return revisoresList;
    }

    public void setRevisoresList(DualListModel<Actor> revisoresList) {
        this.revisoresList = revisoresList;
    }

    public Map<Long, String> getMapaRevisores() {
        return mapaRevisores;
    }

    public void setMapaRevisores(Map<Long, String> mapaRevisores) {
        this.mapaRevisores = mapaRevisores;
    }

    public Map<Long, String> getRevisorMap() {
        return revisorMap;
    }

    public void setRevisorMap(Map<Long, String> revisorMap) {
        this.revisorMap = revisorMap;
    }

    public Map<Long, String> getMapaPerfil() {
        return mapaPerfil;
    }

    public void setMapaPerfil(Map<Long, String> mapaPerfil) {
        this.mapaPerfil = mapaPerfil;
    }

    public Map<Long, String> getMapaActor() {
        return mapaActor;
    }

    public void setMapaActor(Map<Long, String> mapaActor) {
        this.mapaActor = mapaActor;
    }

    public List<String> getListaActores() {
        return listaActores;
    }

    public List<String> getListaPerfil() {
        return listaPerfil;
    }

    public void setListaPerfil(List<String> listaPerfil) {
        this.listaPerfil = listaPerfil;
    }

    public void setListaActores(List<String> listaActores) {
        this.listaActores = listaActores;
    }

    public Map<Long, String> getListaTipoPersonaDocumento() {
        return listaTipoPersonaDocumento;
    }

    public void setListaTipoPersonaDocumento(Map<Long, String> listaTipoPersonaDocumento) {
        this.listaTipoPersonaDocumento = listaTipoPersonaDocumento;
    }

    public Map<Long, String> getListaTipoPersona() {
        return listaTipoPersona;
    }

    public void setListaTipoPersona(Map<Long, String> listaTipoPersona) {
        this.listaTipoPersona = listaTipoPersona;
    }

    public List<Oficina> getListaJefes() {
        return listaJefes;
    }

    public void setListaJefes(List<Oficina> listaJefes) {
        this.listaJefes = listaJefes;
    }

    public List<Actor> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Actor> destinations) {
        this.destinations = destinations;
    }

    public List<Actor> getListaDestinatariosAll() {
        return listaDestinatariosAll;
    }

    public void setListaDestinatariosAll(List<Actor> listaDestinatariosAll) {
        this.listaDestinatariosAll = listaDestinatariosAll;
    }

    public List<Oficina> getListaOficinasAll() {
        return listaOficinasAll;
    }

    public void setListaOficinasAll(List<Oficina> listaOficinasAll) {
        this.listaOficinasAll = listaOficinasAll;
    }

    public Map<Long, String> getMapaDestinatarios() {
        return mapaDestinatarios;
    }

    public void setMapaDestinatarios(Map<Long, String> mapaDestinatarios) {
        this.mapaDestinatarios = mapaDestinatarios;
    }

    public List<String> getDestinationsAgregados() {
        return destinationsAgregados;
    }

    public void setDestinationsAgregados(List<String> destinationsAgregados) {
        this.destinationsAgregados = destinationsAgregados;
    }

    public List<Actor> getDestinationsBusqueda() {
        return destinationsBusqueda;
    }

    public void setDestinationsBusqueda(List<Actor> destinationsBusqueda) {
        this.destinationsBusqueda = destinationsBusqueda;
    }

    public Map<Long, String> getMapaDestinatariosBusqueda() {
        return mapaDestinatariosBusqueda;
    }

    public void setMapaDestinatariosBusqueda(Map<Long, String> mapaDestinatariosBusqueda) {
        this.mapaDestinatariosBusqueda = mapaDestinatariosBusqueda;
    }

    public List<String> getDestinationsAgregadosBusqueda() {
        return destinationsAgregadosBusqueda;
    }

    public void setDestinationsAgregadosBusqueda(List<String> destinationsAgregadosBusqueda) {
        this.destinationsAgregadosBusqueda = destinationsAgregadosBusqueda;
    }

    public List<Actor> getDestinationsReenvio() {
        return destinationsReenvio;
    }

    public void setDestinationsReenvio(List<Actor> destinationsReenvio) {
        this.destinationsReenvio = destinationsReenvio;
    }

    public Map<Long, String> getMapaDestinatariosReenvio() {
        return mapaDestinatariosReenvio;
    }

    public void setMapaDestinatariosReenvio(Map<Long, String> mapaDestinatariosReenvio) {
        this.mapaDestinatariosReenvio = mapaDestinatariosReenvio;
    }

    public List<String> getDestinationsAgregadosReenvio() {
        return destinationsAgregadosReenvio;
    }

    public void setDestinationsAgregadosReenvio(List<String> destinationsAgregadosReenvio) {
        this.destinationsAgregadosReenvio = destinationsAgregadosReenvio;
    }

    public List<Actor> getListaDestinatariosAllExternos() {
        return listaDestinatariosAllExternos;
    }

    public void setListaDestinatariosAllExternos(List<Actor> listaDestinatariosAllExternos) {
        this.listaDestinatariosAllExternos = listaDestinatariosAllExternos;
    }

    public List<String> getTiposCodigoNiu() {
        return tiposCodigoNiu;
    }

    public void setTiposCodigoNiu(List<String> tiposCodigoNiu) {
        this.tiposCodigoNiu = tiposCodigoNiu;
    }

    public List<String> getSubTiposCodigoNiu() {
        return subTiposCodigoNiu;
    }

    public void setSubTiposCodigoNiu(List<String> subTiposCodigoNiu) {
        this.subTiposCodigoNiu = subTiposCodigoNiu;
    }

    public Map<String, String> getTiposContratos() {
        return tiposContratos;
    }

    public void setTiposContratos(Map<String, String> tiposContratos) {
        this.tiposContratos = tiposContratos;
    }

    public Map<String, String> getSubTiposContratos() {
        return subTiposContratos;
    }

    public void setSubTiposContratos(Map<String, String> subTiposContratos) {
        this.subTiposContratos = subTiposContratos;
    }

    public List<Actor> getListaDestinatariosTerceros() {
        return listaDestinatariosTerceros;
    }

    public void setListaDestinatariosTerceros(List<Actor> listaDestinatariosTerceros) {
        this.listaDestinatariosTerceros = listaDestinatariosTerceros;
    }

    public Map<Long, String> getListaTipoPersonaPqr() {
        return listaTipoPersonaPqr;
    }

    public void setListaTipoPersonaPqr(Map<Long, String> listaTipoPersonaPqr) {
        this.listaTipoPersonaPqr = listaTipoPersonaPqr;
    }

    public List<Actor> getListaEmpresas() {
        return listaEmpresas;
    }

    public void setListaEmpresas(List<Actor> listaEmpresas) {
        this.listaEmpresas = listaEmpresas;
    }

    public List<Actor> getListaDestinatariosExternosInternos() {
        return listaDestinatariosExternosInternos;
    }

    public void setListaDestinatariosExternosInternos(List<Actor> listaDestinatariosExternosInternos) {
        this.listaDestinatariosExternosInternos = listaDestinatariosExternosInternos;
    }

    public Map<Long, String> getMapaDestinatariosExternosInternos() {
        return mapaDestinatariosExternosInternos;
    }

    public void setMapaDestinatariosExternosInternos(Map<Long, String> mapaDestinatariosExternosInternos) {
        this.mapaDestinatariosExternosInternos = mapaDestinatariosExternosInternos;
    }

    public List<String> getListaDestinatariosExternosInternosNombres() {
        return listaDestinatariosExternosInternosNombres;
    }

    public void setListaDestinatariosExternosInternosNombres(List<String> listaDestinatariosExternosInternosNombres) {
        this.listaDestinatariosExternosInternosNombres = listaDestinatariosExternosInternosNombres;
    }

    public Map<Long, String> getMapaDestinatariosExternos() {
        return mapaDestinatariosExternos;
    }

    public void setMapaDestinatariosExternos(Map<Long, String> mapaDestinatariosExternos) {
        this.mapaDestinatariosExternos = mapaDestinatariosExternos;
    }

    public List<String> getListaDestinatariosExternosNombres() {
        return listaDestinatariosExternosNombres;
    }

    public void setListaDestinatariosExternosNombres(List<String> listaDestinatariosExternosNombres) {
        this.listaDestinatariosExternosNombres = listaDestinatariosExternosNombres;
    }

    public Map<Long, String> getMapaActorSinUsuario() {
        return mapaActorSinUsuario;
    }

    public void setMapaActorSinUsuario(Map<Long, String> mapaActorSinUsuario) {
        this.mapaActorSinUsuario = mapaActorSinUsuario;
    }

    public List<Documento> getDocumentosInbox() {
        return documentosInbox;
    }

    public void setDocumentosInbox(List<Documento> documentosInbox) {
        this.documentosInbox = documentosInbox;
    }

    public List<DestinoDTO> getListaComunicadosInternos() {
        return listaComunicadosInternos;
    }

    public void setListaComunicadosInternos(List<DestinoDTO> listaComunicadosInternos) {
        this.listaComunicadosInternos = listaComunicadosInternos;
    }

}
