package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Bean.DocumentosRespuestaBean;
import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ClaseDocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.ClaseLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.DocumentosRespuestaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.NotificationMailLocal;
import com.beesoft.beesoft.gestiondocumental.Local.OficinaLocal;
import com.beesoft.beesoft.gestiondocumental.Local.PermisoLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.DocumentoDTO;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Clase;
import com.beesoft.beesoft.gestiondocumental.entidades.ClasesDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import com.beesoft.beesoft.gestiondocumental.entidades.DocumentosRespuesta;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.RevisionDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Serie;
import com.beesoft.beesoft.gestiondocumental.entidades.SubClase;
import com.beesoft.beesoft.gestiondocumental.entidades.SubSerie;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoCorreo;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Trd;
import com.beesoft.beesoft.gestiondocumental.entidades.TrdOld;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;
import com.csvreader.CsvReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 *
 */
@Named(value = "buscarAction")
@SessionScoped
public class BuscarDocumentoAction implements Serializable {

	private static final Log LOG = LogFactory.getLog(BuscarDocumentoAction.class);

	@EJB
	private DocumentoLocal documentoBean;

	@EJB
	private ActorLocal actorBean;

	@EJB
	private OficinaLocal oficinaBean;

	@EJB
	private NotificationMailLocal notificationMailBean;

	@EJB
	private UtilidadesLocal utilidadesBean;

	@EJB
	private PermisoLocal permisoBean;

	@EJB
	private ClaseLocal claseBean;

	@EJB
	private ClaseDocumentoLocal claseDocumentoBean;

	@EJB
	private DocumentosRespuestaLocal docRespuesta;

	private String radicacion;
	private Boolean activo = null;
	private String anexos = null;
	private Boolean medioElectronico;
	private Boolean aprobado = null;
	private String asunto;
	private String comentario;
	private Date fechaControl;
	private Date fechaEmision;
	private Date fechaRecepcion;
	private Date fechaSistema;
	private String guia;
	private TipoCorreo tipoCorreo;
	private String notificacionEntrega;
	private Boolean pendiente = null;
	private String pruebaEntrega;
	private String[] pruebaEntregaList;
	private Boolean revisado = null;
	private String observacion;
	private String ubicacionFisica;
	private String claseDocumento;
	private String claseOld;
	private Long claseId;
	private List<Actor> procedencia;
	private List<Actor> destinos;
	private Documento documentoReferencia;
	private Documento documento;
	private DocumentoDTO documentoDto;
	private Documento documentoCampos;
	private String empresacorreo;
	private String empresaCorreoEnviado;
	private String mensajero;
	private List<Actor> revisoresNuevos;
	private List<Actor> revisores;
	private List<Actor> revisorEliminar;
	private Actor responsable;
	private Actor responsableNuevo;
	private Actor radicador;
	private String cedula;
	private String cedulaDestino;

	private String nombre;
	private List<Actor> destinoIn;
	private List<Actor> procedenciaIn;
	private List<Actor> revisoresIn;
	private List<Actor> revisoresTarget;
	private List<Actor> listaRevisores;
	private Map<Long, String> mapaRevisores;
	private List<Actor> responsableIn;

	private Map<Long, String> mapaClaseDocumento = new HashMap<>();
	private List<Clase> clasesDocumento;

	private Date fechaEmisionInicial;
	private Date fechaEmisionFinal;
	private Actor actorProcedencia;
	private Actor actorDestino;
	private Actor actorRevisor;
	private String responsableName;
	private Long responsableValue;
	private String destinoName;
	private String procedenciaName;

	private String oficinaValue;
	private Map<Long, String> oficinas;
	private Oficina oficina;
	private String serieValue;
	private Map<Long, String> series;
	private Serie serie;
	private String subserieValue;
	private Map<Long, String> subseries;
	private SubSerie subserie;
	private String tipoDocValue;
	private Map<Long, String> tiposDoc;
	private Map<Long, String> revisorMap;
	private String revisorName;
	private String nombreRevisor;
	private String revisorNameText;
	private TipoDocumento tipoDoc;
	private Usuario usuarioLogin;

	private ArchivoDocumento archivoSelected;
	private List<ArchivoDocumento> listaArchivos = new ArrayList<>();
	private List<ArchivoDocumento> listaArchivosNuevos = new ArrayList<>();
	private List<ArchivoDocumento> listaArchivosEliminar = new ArrayList<>();

	private List<Documento> resultadoDocs;
	private String procedenciaText;
	private String destinosText;

	private String radicacionConsulta;
	private Actor actorProcedenciaConsulta;
	private Actor actorDestinoConsulta;
	private String asuntoConsulta;
	private String claseConsulta;
	private String pruebaEntregaConsulta;

	private Map<Long, String> listaResponsables;

	private List<Trd> trds = new ArrayList<>();
	private List<TrdOld> trdsOld = new ArrayList<>();
	private List<Trd> trdsNuevo = new ArrayList<>();
	private List<Trd> trdsEliminar = new ArrayList<>();
	private List<Documento> listaRef = new ArrayList<>();
	private List<DocumentosRespuesta> listaRespuesta = new ArrayList<>();
	private String documentoRespuesta = "";
	private String radicadoRespuesta = "";
	private Boolean tieneRespuesta;
	private Map<Long, String> listaCiudades;
	private String municipio;

	private File file;
	private List<File> files = new ArrayList<>();

	private Actor actorRevisorConsulta;
	private Actor revisorComentario;
	private Actor revisorComentarioCorreo;

	private Actor outProcedencia;
	private Actor outDestino;
	private Actor outRevisor;
	private Trd outTrd;

	private Boolean aprobadoRevision;
	private Boolean revisadoRevision;
	private Boolean pendienteRevision;

	private Boolean showAprobado;
	private Boolean showRevisado;
	private Boolean showPendiente;
	private Boolean showRadicador;
	private Boolean showComentario;
	private String nombreArchivo;

	private StringBuilder comentarios;
	private int goCreate = 1;
	private int goEdit = 1;

	private List<RevisionDocumento> revisiones;
	private Boolean responsableRevisor;
	private Boolean isEmpresa;
	private String pqr;
	private String contractType;
	private String contractCod;
	private List<DocumentoDTO> ResultadosOld;
	private String radicadorOld;
	private boolean ocultarMensajero;
	private boolean ocultarCorreoCertificado;
	private String tipoCertificado;
	private Long idRespuesta;
	// ---------------Inbox-------------------------
	List<Documento> documentosInbox;
	private Boolean gestionBuscar;
	private Boolean gestionInbox;
	private String inbox;
	private List<String> empresaCorreo;

	private String carpeta;
	private Date fechaExtIni;
	private Date fechaExtFin;
	private String letraExtraccion;
	private SubClase subClase;
	private Long idsubClase;
	private List<SubClase> subClases;
	private List<ClasesDocumento> listaClasesDocumento;
	private Map<String, String> mapClasesDocumentos;

	String fileNameExtraccion = "";

	// -----trds-------------------
	private List<Oficina> listaOficina;
	private List<Serie> listaSerie;
	private List<SubSerie> listaSubSerie;
	private List<TipoDocumento> listaTipoDocumento;
	private List<Oficina> listaOficinaFiltered;
	private List<Serie> listaSerieFiltered;
	private List<SubSerie> listaSubSerieFiltered;
	private List<TipoDocumento> listaTipoDocumentoFiltered;

	// ----------------Etiqueta------------------------
	String radicacionEtiqueta = "";
	String procedenciaEtiqueta = "";
	String destinosEtiqueta = "";
	String asuntoEtiqueta = "";
	String radicadorEtiqueta = "";
	String fechaEtiqueta = "";
	String anexosEtiqueta = "";
	String urlServer = "";
	String sticker = "";

	// ------------cambiar radicado---------
	private String radicadoCambio = "";
	private String comentarioCambio = "";
	private Boolean modoAdmin;

	public BuscarDocumentoAction() {
	}

	public void cargarClasesDocumento() {
		try {
			listaClasesDocumento = claseDocumentoBean.listarClasesDocumento("A");

			mapClasesDocumentos = new TreeMap<>();
			for (ClasesDocumento clase : listaClasesDocumento) {
				mapClasesDocumentos.put(clase.getNombre(), clase.getNombre());
			}
		} catch (Exception e) {
			LOG.info(e);
		}
	}

	public String editarDocumento() {

		try {
			int validacion = 0;
			// validacion += ValidacionesUtil.validarCadena(claseDocumento.getNombre(),
			// "Clase documento");

			if ((listaArchivos != null && !listaArchivos.isEmpty())
					|| (listaArchivosNuevos != null && !listaArchivosNuevos.isEmpty())) {
				validacion += 1;
			} else {
				validacion -= 1;
				ValidacionesUtil.addMessage("Los archivos son obligatorios");
			}

			if (goEdit == validacion) {
				obtenerRevisoresEdicion();
				obtenerTrds();
				if (!trdsNuevo.isEmpty()) {

					Object[] info = ValidacionesUtil.mostrarJefe(trds.get(0).getOficina());
					responsableNuevo = (Actor) info[0];
					if (trdsNuevo.size() > 1) {
						for (Trd trd : trdsNuevo) {
							Object[] info1 = ValidacionesUtil.mostrarJefe(trd.getOficina());
							Actor revisor = (Actor) info1[0];
							if (!revisor.getId().equals(responsableNuevo.getId())) {
								revisoresNuevos.add(revisor);
							}
						}

					}
				}

				documentoBean.gestionarDocumento(documento.getId(), fechaControl, claseDocumento, listaRef,
						revisoresTarget, revisoresNuevos, responsableNuevo, trdsEliminar, trdsNuevo,
						listaArchivosNuevos, listaArchivosEliminar, observacion, empresacorreo, empresaCorreoEnviado,
						mensajero, guia, pruebaEntrega, null, notificacionEntrega, contractType, contractCod,
						usuarioLogin.getActor());
				// Acá se guarda en la tabla DocumentosRespuesta
				List<Documento> lista = new ArrayList<Documento>();
				lista = documentoBean.buscarDocumento(radicacion, null, null, null, null, "", "", "", null, null, null,
						null, "");
				
				if (lista != null) {
					for (Documento doc : listaRef) {
						Long siEsta = docRespuesta.consultaIdRespuesta(doc.getId(), lista.get(0).getId());
						if (siEsta == null) {
							docRespuesta.crearDocumentoRespuesta(lista.get(0).getId(), doc.getId(),
								lista.get(0).getRadicacion());
						}
					}
				}
				limpiarEdicion();
				return "volverEditar";
			}

		} catch (Exception e) {
			LOG.info(e);
		}

		return "failed";
	}

	public void cambiarRadicado() {
		if (StringUtils.isNotBlank(radicadoCambio) && StringUtils.isNotBlank(comentarioCambio)) {
			documentoBean.cambiarRadicacionDocumento(documento.getId(), radicadoCambio, comentarioCambio,
					usuarioLogin.getId());
			radicadoCambio = "";
			comentarioCambio = "";
			ValidacionesUtil.addMessageInfo("Se cambio el numero de radicado");
		}
	}

	public void validarRenderedCampos() {
		if (empresaCorreoEnviado != null && empresaCorreoEnviado.equals("MENSAJERO")) {
			tipoCertificado = "";
			ocultarMensajero = false;
			ocultarCorreoCertificado = true;
		} else if (empresaCorreoEnviado != null && empresaCorreoEnviado.equals("CERTIFICADO")) {
			mensajero = "";
			ocultarMensajero = true;
			ocultarCorreoCertificado = false;
		} else {
			ocultarMensajero = true;
			ocultarCorreoCertificado = true;
			mensajero = "";
			tipoCertificado = "";
		}
	}

	public String validarFechaControl(Documento doc) {
		if (doc != null) {
			int dias = (int) ((doc.getFechaControl().getTime() - new Date().getTime()) / 86400000);
			if (doc.getActivo()) {
				if (dias >= 10) {
					return "background-color: green;padding: 5px 5px 5px 5px;";
				} else if (dias <= 10 && dias >= 3) {
					return "background-color: yellow;padding: 5px 5px 5px 5px;";
				} else if (dias < 3) {
					return "background-color: red;padding: 5px 5px 5px 5px;";
				}
			}
		}
		return "";
	}

	public void handleFileUpload(FileUploadEvent event) throws Exception {
		String fileName = FilenameUtils.getName(event.getFile().getFileName());
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "El documento " + fileName + " se cargo", null);
		FacesContext.getCurrentInstance().addMessage("info", msg);
		listaArchivosNuevos.add(fileToAdjunto(event.getFile()));
	}

	public ArchivoDocumento fileToAdjunto(UploadedFile file) {
		ArchivoDocumento aa = new ArchivoDocumento();
		aa.setNombreArchivo(FilenameUtils.getName(file.getFileName()));
		aa.setArchivo(file.getContents());
		return aa;
	}

	public void eliminarDocumento(Documento doc) {
		Documento documento = doc;
		if (documento != null) {
			documentoBean.cambiarEstado(documento.getId(), usuarioLogin.getActor(), false);
			buscarDocumento();
		}
	}

	public void buscarDocumentoInbox() {
		try {
			gestionBuscar = false;
			gestionInbox = true;
			Actor actorRev = usuarioLogin.getActor();
			documentosInbox = new ArrayList<>();
			limpiar();
			documentosInbox = documentoBean.buscarDocumentosPendientes(actorRev);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarDocumento() {
		try {
			gestionBuscar = true;
			gestionInbox = false;
			obtenerRevisor();
			ResultadosOld = new ArrayList<>();

			if (validaciones()) {

				if (fechaEmisionInicial != null && fechaEmisionFinal != null) {
					ValidacionesUtil.validarFechasIniFin(fechaEmisionInicial, fechaEmisionFinal);
					resultadoDocs = documentoBean.buscarDocumento(radicacionConsulta, fechaEmisionInicial,
							fechaEmisionFinal, actorProcedenciaConsulta, actorDestinoConsulta, asuntoConsulta,
							claseConsulta, pruebaEntregaConsulta, actorRevisor, aprobado, revisado, pendiente,
							obtenerIdCiudad());
				} else if (fechaEmisionInicial != null && fechaEmisionFinal == null) {
					ValidacionesUtil.addMessage("El rango de fechas esta incompleto agregue la Fecha de Emision Final");
				} else if (fechaEmisionInicial == null && fechaEmisionFinal != null) {
					ValidacionesUtil
							.addMessage("El rango de fechas esta incompleto agregue la Fecha de Emision Inicial");
				} else if (fechaEmisionInicial == null && fechaEmisionFinal == null) {
					resultadoDocs = documentoBean.buscarDocumento(radicacionConsulta, fechaEmisionInicial,
							fechaEmisionFinal, actorProcedenciaConsulta, actorDestinoConsulta, asuntoConsulta,
							claseConsulta, pruebaEntregaConsulta, actorRevisor, aprobado, revisado, pendiente,
							obtenerIdCiudad());
				}

			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	private String obtenerIdCiudad() {
		Ciudad ciudad = new Ciudad();
		String idCiudad = "";
		if (StringUtils.isNotBlank(municipio)) {
			ciudad = obtenerCiudad(municipio);
			idCiudad = ciudad.getId().toString();
		} else
			idCiudad = "";

		return idCiudad;
	}

	public Ciudad obtenerCiudad(String city) {
		try {
			Long idCiudad = null;
			for (Entry<Long, String> c : listaCiudades.entrySet()) {
				if (c.getValue().equals(city)) {
					idCiudad = c.getKey();
				}
			}
			return utilidadesBean.buscarCiudad(idCiudad);

		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public Boolean validaciones() {
		if (StringUtils.isBlank(radicacionConsulta) && fechaEmisionInicial == null && fechaEmisionFinal == null
				&& actorProcedenciaConsulta == null && actorDestinoConsulta == null
				&& StringUtils.isBlank(asuntoConsulta) && StringUtils.isBlank(claseConsulta) && actorRevisor == null
				&& StringUtils.isBlank(obtenerIdCiudad())) {
			ValidacionesUtil.addMessage("Debe utilizar por lo menos un filtro de busqueda");
			return false;
		} else {
			return true;
		}
	}

	public List<DocumentoDTO> addListaDTO(List<Documento> lista) {
		List<DocumentoDTO> listaDTO = new ArrayList<>();
		for (Documento doc : lista) {
			DocumentoDTO dto = null;
			dto = new DocumentoDTO(doc, doc.getRadicacion(), doc.getFechaEmision(), doc.getFechaSistema(),
					doc.getAsunto(), doc.getActivo(), false);
			listaDTO.add(dto);
		}
		return listaDTO;
	}

	public void revisarDocumento() {
		if (revisionesDocumento()) {
			if (StringUtils.isBlank(nombreRevisor)) {
				revisorComentario = responsable;
			}

			aprobadoRevision = false;
			pendienteRevision = false;
			revisadoRevision = true;
			documentoBean.enviarComentario(documento, fechaControl, true, false, false, "", revisorComentario, false,
					revisoresTarget, revisoresNuevos, false);
			cargarComentarios(documento);
			// ValidacionesUtil.addMessageInfo("El documento con radicado " +
			// documento.getRadicacion() + " fue revisado");
		}
	}

	public Boolean revisionesDocumento() {
		if (revisiones != null && !revisiones.isEmpty()) {
			for (RevisionDocumento rev : revisiones) {
				if (revisorComentario.getId().equals(rev.getActor().getId())) {
					if (rev.getAprobado() && rev.getRevisado()) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public String crearComentario() {

		Boolean enviarCorreo = obtenerRevisorComentario();
		obtenerValorBoolean();
		if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN)) {
			revisorComentario = usuarioLogin.getActor();
		} else if (StringUtils.isBlank(nombreRevisor)) {
			revisorComentario = responsable;
		}

		if (enviarCorreo) {
			documentoBean.enviarComentario(documento, fechaControl, revisadoRevision, aprobadoRevision,
					pendienteRevision, comentario, revisorComentario, enviarCorreo, revisoresTarget, revisoresNuevos,
					false);
		} else {
			documentoBean.enviarComentario(documento, fechaControl, revisadoRevision, aprobadoRevision,
					pendienteRevision, comentario, revisorComentario, enviarCorreo, revisoresTarget, null,
					responsableRevisor);
		}

		if (gestionBuscar) {
			limpiar();
			limpiarEdicion();
			limpiarGestion();
			return "buscar";
		} else if (gestionInbox) {
			limpiar();
			limpiarEdicion();
			limpiarGestion();
			return "volverInbox";
		}

		return "";
	}

	public void obtenerTrds() {
		if (trds != null && !trds.isEmpty()) {
			for (Trd trd : trds) {
				if (trd.getId() == null) {
					trdsNuevo.add(trd);
				}
			}
		}
	}

	public void obtenerRevisoresEdicion() {
		revisoresNuevos = new ArrayList<>();
		List<Actor> revisor = documentoBean.consultarRevisorDocumento(documento.getId());
		if (revisoresTarget != null && !revisoresTarget.isEmpty()) {
			for (Actor actor : revisoresTarget) {
				if (!ValidacionesUtil.contains(revisor, actor)) {
					revisoresNuevos.add(actor);
				}
			}
		}
	}

	public List<Actor> enviarRevisores(List<String> lista) {
		List<Actor> ListaRev = new ArrayList<>();
		for (Object value : lista) {
			for (Entry<Long, String> entry : mapaRevisores.entrySet()) {
				if (entry.getValue().equals(value)) {
					try {
						Long id = entry.getKey();
						Actor actor = actorBean.buscarActor(id);
						ListaRev.add(actor);

					} catch (Exception ex) {
						LOG.info(ex);
					}
				}
			}
		}
		return ListaRev;
	}

	public void obtenerValorBoolean() {

		if (revisadoRevision && aprobadoRevision) {
			pendienteRevision = false;
		} else if (aprobadoRevision && !revisadoRevision) {
			pendienteRevision = false;
			revisadoRevision = true;
		}

	}

	public String mostrarProcedenciaTabla(Documento dto) {
		Documento documento = dto;
		List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(documento.getId());
		return ValidacionesUtil.listToCadena(procedencia);
	}

	public String mostrarDestinoTabla(Documento dto) {
		Documento documento = dto;
		List<Actor> destino = documentoBean.consultarDestinoDocumento(documento.getId());
		return ValidacionesUtil.listToCadena(destino);
	}

	public Boolean obtenerRevisorComentario() {
		revisoresNuevos = new ArrayList<>();
		List<Actor> revisor = documentoBean.consultarRevisorDocumento(documento.getId());
		if (revisoresTarget != null && !revisoresTarget.isEmpty()) {
			for (Actor actor : revisoresTarget) {
				if (!ValidacionesUtil.contains(revisor, actor)) {
					revisoresNuevos.add(actor);
				}
			}
		}
		if (revisoresNuevos != null && !revisoresNuevos.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public void selectProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		if (actorProcedenciaConsulta != null) {
			this.actorProcedenciaConsulta = actorProcedenciaConsulta;
			procedenciaName = actorProcedenciaConsulta.getNombreYApellido();
			iniciarDialog();
		}
	}

	public void selectDestinoConsulta(Actor actorDestinoConsulta) {
		if (actorDestinoConsulta != null) {
			this.actorDestinoConsulta = actorDestinoConsulta;
			destinoName = actorDestinoConsulta.getNombreYApellido();
			iniciarDialog();
		}
	}

	public String selectDocumentoInbox(Documento documento) {
		limpiarGestion();
		DocumentoDTO doc = new DocumentoDTO();
		doc.setDocumento(documento);
		doc.setOld(false);
		gestionInbox = true;
		gestionBuscar = false;
		return selectDocumento(documento);
	}

	public String selectDocumento(Documento doc) {
		String redirect = "";
		this.documento = doc;

//        if (showButtons(doc) && !doc.getOld()) {
//            if (documento != null) {
		redirect = asignarInformacion(documento);
		return redirect;
//            } else {
//                ValidacionesUtil.addMessage("No ha seleccionado un documento");
//                return "failed";
//            }
//        } else {
//            redirect = asignarInformacionOld(doc);
//            return redirect;
//        }

	}

	public String asignarInformacion(Documento documento) {
		String redirect = "";
		radicacion = documento.getRadicacion();
		radicador = documento.getRadicador();
		pqr = documento.getPqr();
		contractType = documento.getContratacionTipo();
		contractCod = documento.getContratacionNumero();
		fechaEmision = documento.getFechaEmision();
		fechaRecepcion = documento.getFechaRecepcion();
		asunto = documento.getAsunto();
		observacion = documento.getObservacion();
		claseDocumento = documento.getClasedocumento();
		if (documento.getCiudad() != null)
			municipio = documento.getCiudad().getDescripcion();

		RevisionDocumento revisionActual = documentoBean.ultimaRevisionDocumentoPorUsuario(documento.getId(),
				usuarioLogin.getActor().getId());
		if (revisionActual != null) {
			aprobadoRevision = revisionActual.getAprobado();
			revisadoRevision = revisionActual.getRevisado();
			pendienteRevision = revisionActual.getPendiente();
		} else {
			aprobadoRevision = documento.getAprobado();
			revisadoRevision = documento.getRevisado();
			pendienteRevision = documento.getPendiente();
		}
		actorRevisor = obtenerActor(documento);
		if (actorRevisor != null) {
			revisorComentario = actorRevisor;
			nombreRevisor = revisorComentario.getNombreYApellido();
		}
		List<Actor> revisor = documentoBean.consultarRevisorDocumento(documento.getId());
		revisores = revisor;
		buscarRevisores(documento);
		if (documento.getResponsable() != null) {
			responsable = documento.getResponsable();
			responsableValue = responsable.getId();
			responsableName = responsable.getNombreYApellido();
		} else {
			responsableName = "DOCUMENTO SIN RESPONSABLE";
		}
		List<Actor> procedencias = documentoBean.consultarProcedenciaDocumento(documento.getId());
		List<Actor> destino = documentoBean.consultarDestinoDocumento(documento.getId());
		List<ArchivoDocumento> archivo = documentoBean.consultarArchivoDocumento(documento.getId());
		procedencia = procedencias;
		procedenciaText = ValidacionesUtil.listToCadena(procedencias);
		destinosText = ValidacionesUtil.listToCadena(destino);
		listaArchivos = archivo;
		fechaControl = documento.getFechaControl();
		listaRef = new ArrayList<>();
		listaRef = documentoBean.consultarReferenciasDocumento(documento.getId());
		if (documento.getDocumentoRespuesta() != null) {
			// ********Proceso para buscar la lista de respuesta
			try {
				listaRespuesta = documentoBean.consultarRespuestasDocumento(documento.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}

//			documentoRespuesta = documento.getDocumentoRespuesta().getId() + "";
//			radicadoRespuesta = documento.getDocumentoRespuesta().getRadicacion() + "";
			tieneRespuesta = true;
		} else {
			documentoRespuesta = "";
			radicadoRespuesta = "";
			tieneRespuesta = false;
		}
		empresacorreo = documento.getEmpresaCorreoRecibido();
		empresaCorreoEnviado = documento.getEmpresacorreoEnviado();
		// tipoCertificado= documento.getTipoCertificado();
		validarRenderedCampos();
		guia = documento.getGuia();

		pruebaEntrega = documento.getPruebaEntrega();
		if (StringUtils.isNotBlank(pruebaEntrega)) {
			if (pruebaEntrega.contains(",")) {
				pruebaEntregaList = pruebaEntrega.split(",");
			} else {
				pruebaEntregaList = new String[1];
				pruebaEntregaList[0] = pruebaEntrega;
			}
		}
		notificacionEntrega = documento.getNotificacionEntrega();
		mensajero = documento.getMensajero();
		ubicacionFisica = documento.getUbicacionFisica();
		medioElectronico = documento.getMedioElectronico();
		listaRef = new ArrayList<>();
		listaRef = documentoBean.consultarReferenciasDocumento(documento.getId());

		if (documento.getMigrado() != null && documento.getMigrado()) {
			cargarTrdsOld(documento.getId());
		} else {
			cargarTrds(documento.getId());
		}

		cargarComentarios(documento);
		Boolean permission = true;
		if (usuarioLogin != null) {
			permission = usuarioPermitido(documento, usuarioLogin);
		}
		if (permission) {
			if (documento.getMigrado() != null && documento.getMigrado()) {
				mostrarCampos(documento.getClaseOld());
			} else {
				showAprobado = true;
				showRevisado = true;
			}
			showRadicador = true;
			redirect = "success";
		} else {
			showAprobado = false;
			showPendiente = false;
			showRevisado = false;
			showComentario = false;
			showRadicador = false;
			fechaControl = documento.getFechaControl();
			revisorNameText = ValidacionesUtil.listToCadena(revisor);
			redirect = "ver";
		}
		return redirect;
	}

	public String asignarInformacionOld(DocumentoDTO documento) {
		String redirect = "";
		radicacion = documento.getRadicacion();
		fechaEmision = documento.getFechaEmision();
		fechaRecepcion = documento.getFechaRecepcion();
		asunto = documento.getAsunto();
		observacion = documento.getObservacion();
		claseDocumento = documento.getClaseAux();
		aprobadoRevision = documento.getAprobado();
		revisadoRevision = documento.getRevisado();
		pendienteRevision = documento.getPendiente();
		revisoresTarget = new ArrayList<>();
		// buscarRevisores();
		// responsable = documento.getResponsable();
		// responsableName = responsable.getNombreYApellido();
		// procedencia = documento.getProcedencia();
		fechaControl = documento.getFechaControl();

		empresacorreo = documento.getRecibido();
		empresaCorreoEnviado = documento.getEnviado();
		guia = documento.getGuia();
		pruebaEntrega = documento.getPrueba();
		notificacionEntrega = documento.getNotificacion();
		mensajero = documento.getFuncionario();
		try {
			responsableName = documentoBean.buscarResponsableOld(documento.getResponsable());
			radicadorOld = documento.getRadicador();
			procedenciaText = ValidacionesUtil
					.organizarCadenaXComas(documentoBean.buscarProcedenciaOld(documento.getId()));
			destinosText = ValidacionesUtil.organizarCadenaXComas(documentoBean.buscarDestinosOld(documento.getId()));
		} catch (Exception ex) {
			LOG.info(ex);
		}
		if (true) {
			mostrarCampos(claseDocumento);
			showRadicador = true;
			redirect = "verOld";
		} else {
			showAprobado = false;
			showPendiente = false;
			showRevisado = false;
			showComentario = false;
			showRadicador = false;
			fechaControl = documento.getFechaControl();
		}
		return redirect;
	}

	public void informacionParaGestion(Documento documento, Actor actor) {
		String redirect = "";
		this.documento = documento;
		radicacion = documento.getRadicacion();
		radicador = documento.getRadicador();
		pqr = documento.getPqr();
		contractType = documento.getContratacionTipo();
		contractCod = documento.getContratacionNumero();
		fechaEmision = documento.getFechaEmision();
		fechaRecepcion = documento.getFechaRecepcion();
		asunto = documento.getAsunto();
		observacion = documento.getObservacion();
//        claseDocumento = documento.getClasedocumento();
		aprobadoRevision = documento.getAprobado();
		revisadoRevision = documento.getRevisado();
		pendienteRevision = documento.getPendiente();
		municipio = documento.getCiudad().getDescripcion();
		actorRevisor = actor;
		if (actorRevisor != null) {
			revisorComentario = actorRevisor;
			nombreRevisor = revisorComentario.getNombreYApellido();
		}
		List<Actor> revisor = documentoBean.consultarRevisorDocumento(documento.getId());
		revisores = revisor;
		buscarRevisores(documento);
		if (documento.getResponsable() != null) {
			responsable = documento.getResponsable();
			responsableValue = responsable.getId();
			responsableName = responsable.getNombreYApellido();
		} else {
			responsableName = "DOCUMENTO SIN RESPONSABLE";
		}
		List<Actor> procedencias = documentoBean.consultarProcedenciaDocumento(documento.getId());
		List<Actor> destino = documentoBean.consultarDestinoDocumento(documento.getId());
		List<ArchivoDocumento> archivo = documentoBean.consultarArchivoDocumento(documento.getId());
		procedencia = procedencias;
		procedenciaText = ValidacionesUtil.listToCadena(procedencias);
		destinosText = ValidacionesUtil.listToCadena(destino);
		listaArchivos = archivo;
		fechaControl = documento.getFechaControl();
		empresacorreo = documento.getEmpresaCorreoRecibido();
		empresaCorreoEnviado = documento.getEmpresacorreoEnviado();
		guia = documento.getGuia();
		pruebaEntrega = documento.getPruebaEntrega();
		notificacionEntrega = documento.getNotificacionEntrega();
		mensajero = documento.getMensajero();
		ubicacionFisica = documento.getUbicacionFisica();

		listaRef = new ArrayList<>();
		listaRef = documentoBean.consultarReferenciasDocumento(documento.getId());

		cargarTrds(documento.getId());
		cargarComentarios(documento);
		mostrarCampos(claseDocumento);
		showRadicador = true;
	}

	public Boolean usuarioPermitido(Documento documento, Usuario logged) {
		Actor actor = logged.getActor();
		List<String> revisores = new ArrayList<>();
		List<Actor> revisor = documentoBean.consultarRevisorDocumento(documento.getId());
		if (revisor != null && !revisor.isEmpty()) {
			for (Actor a : revisor) {
				revisores.add(a.getNombreYApellido());
			}
			if (revisores.contains(actor.getNombreYApellido())) {
				return true;
			}
		}

		if (documento.getResponsable() != null) {
			if (documento.getResponsable().getId().equals(actor.getId())) {
				return true;
			}
		} else {
			return true;
		}
		switch (logged.getPerfil().getDescripcion()) {
		case ConstantsMail.RADICADOR:
			return true;
		case ConstantsMail.ADMIN:
			return true;
		}
		return false;
	}

	public Actor obtenerActor(Documento doc) {
		List<Actor> revisor = documentoBean.consultarRevisorDocumento(doc.getId());
		for (Actor actor : revisor) {
			if (usuarioLogin.getActor().getId().equals(actor.getId())) {
				return actor;
			}
		}
		if (doc.getResponsable() != null) {
			if (usuarioLogin.getActor().getId().equals(doc.getResponsable().getId())) {
				return doc.getResponsable();
			}
		}
		return null;
	}

	public String selectDocumentoEdicion(Documento doc) {
		this.documento = doc;
		if ((usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN))
				|| (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.RADICADOR))) {

			if (documento != null) {
				radicacion = documento.getRadicacion();
				radicador = documento.getRadicador();
				pqr = documento.getPqr();
				contractType = documento.getContratacionTipo();
				contractCod = documento.getContratacionNumero();
				fechaEmision = documento.getFechaEmision();
				fechaRecepcion = documento.getFechaRecepcion();
				List<Actor> procedencias = documentoBean.consultarProcedenciaDocumento(documento.getId());
				List<Actor> destino = documentoBean.consultarDestinoDocumento(documento.getId());
				procedencia = procedencias;
				destinos = destino;
				procedenciaText = ValidacionesUtil.listToCadena(procedencias);
				destinosText = ValidacionesUtil.listToCadena(destino);
				asunto = documento.getAsunto();
				idRespuesta = documento.getId();
				observacion = documento.getObservacion();
				empresacorreo = documento.getEmpresaCorreoRecibido();
				empresaCorreoEnviado = documento.getEmpresacorreoEnviado();
				guia = documento.getGuia();
				pruebaEntrega = documento.getPruebaEntrega();
				notificacionEntrega = documento.getNotificacionEntrega();
				mensajero = documento.getMensajero();
				ubicacionFisica = documento.getUbicacionFisica();
				fechaControl = documento.getFechaControl();
				listaRef = new ArrayList<>();
				listaRef = documentoBean.consultarReferenciasDocumento(documento.getId());
				medioElectronico = documento.getMedioElectronico();

				if (documento.getMigrado() != null && documento.getMigrado()) {
					cargarTrdsOld(documento.getId());
					claseOld = documento.getClaseOld();
				} else {
					cargarTrds(documento.getId());
					claseDocumento = documento.getClasedocumento();
//                    claseId = claseDocumento.getId();
//                    subClase = documento.getSubClase();
//                    idsubClase = subClase.getId();
//                    cargarClases();
//                    cargarSubClases();
				}

				buscarResponsableEdicion();

				if (documento.getResponsable() != null) {
					responsable = documento.getResponsable();
					responsableValue = responsable.getId();
				} else {
					responsableName = "DOCUMENTO SIN RESPONSABLE";
				}
				List<ArchivoDocumento> archivo = documentoBean.consultarArchivoDocumento(documento.getId());
				listaArchivos = archivo;

				revisoresTarget = new ArrayList<>();
				revisoresTarget = documentoBean.consultarRevisorDocumento(documento.getId());
				buscarRevisoresEdicion();
				cargarOficinas();
				llenarTablaOficina(1L);
				return "success";
			} else {
				ValidacionesUtil.addMessage("No ha seleccionado un documento");
				return "failed";
			}
		} else {
			ValidacionesUtil.addMessage("El usuario no es revisor de este documento");
			limpiar();
			return "failed";
		}
	}

	public String confirmarRevisor(String radicacion) {
		if (usuarioLogin == null) {
			if (FacesContext.getCurrentInstance() != null) {
				HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
						.getSession(false);
				usuarioLogin = (Usuario) session.getAttribute("username");
			}
		}
		Boolean conf = documentoBean.esResponsable(radicacion, usuarioLogin.getActor());
		responsableRevisor = !conf;
		if (conf) {
			return "REVISOR";
		} else {
			return "RESPONSABLE";
		}
	}

	public Boolean showButtons(Documento doc) {
		Actor actor = usuarioLogin.getActor();

		switch (usuarioLogin.getPerfil().getDescripcion()) {
		case ConstantsMail.AUDITOR:
			return true;
		case ConstantsMail.RADICADOR:
			return true;
		case ConstantsMail.ADMIN:
			return true;
		}

		if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.EDITOR)) {
			return true;
		}
		Documento documento = doc;
		List<String> revisores = new ArrayList<>();
		List<Actor> revisor = documentoBean.consultarRevisorDocumento(documento.getId());
		if (revisor != null && !revisor.isEmpty()) {
			for (Actor a : revisor) {
				revisores.add(a.getNombreYApellido());
			}
			if (revisores.contains(actor.getNombreYApellido())) {
				return true;
			}
		}
		if (documento.getResponsable() != null) {
			if (documento.getResponsable().getId().equals(actor.getId())) {
				return true;
			}
		} else {
			return false;
		}
		return false;
	}

	public Boolean showOtherButtons(Documento doc) {
		Documento documento = doc;

		if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN) && documento.getActivo()) {
			return true;
		} else if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.RADICADOR) && documento.getActivo()) {
			return true;
		}

		return false;
	}

	public Boolean disabledButtons(DocumentoDTO doc) {
		if (!doc.getOld()) {
			Documento documento = doc.getDocumento();
			return documento.getActivo();
		} else {
			return doc.getActivo();
		}
	}

	public Boolean showEditorButtons() {
		return usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.EDITOR);
	}

	public void selectDocumentoEditor(Documento documento) {
		this.documentoCampos = documento;
	}

	public void editarCamposDocumento() {
		documentoBean.editarPruebaEntregaDocumento(documentoCampos.getId(), documentoCampos.getPruebaEntrega(),
				documentoCampos.getObservacion());
		documentoCampos = null;
	}

	public void mostrarCampos(String clase) {
		if (clase.equalsIgnoreCase(ConstantsMail.CLASE_INFORMACION)) {
			showRevisado = true;
		} else if (clase.equalsIgnoreCase(ConstantsMail.CLASE_CONTROL)) {
			showRevisado = true;
			showPendiente = true;
		} else if (clase.equalsIgnoreCase(ConstantsMail.CLASE_GESTION)) {
			showRevisado = true;
			showPendiente = true;
			showAprobado = true;
		}
	}

	public String showButtonRadicar() {
		return "radicar";
	}

	public void cargarTrds(Long id) {
		try {
			trds = new ArrayList<>();
			trds = oficinaBean.buscarTrds(id);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarTrdsOld(Long id) {
		try {
			trdsOld = new ArrayList<>();
			trdsOld = oficinaBean.buscarTrdsOld(id);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarProcedencia() {
		try {
			procedenciaIn = actorBean.listaProcedencia(nombre, cedula, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarDestinos() {
		try {
			destinoIn = actorBean.listaProcedencia(nombre, cedulaDestino, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarRevisores(Documento documento) {
		try {
			revisoresIn = actorBean.listaUsuarios();
			revisoresTarget = new ArrayList<>();
			revisoresTarget = documentoBean.consultarRevisorDocumento(documento.getId());
			listaRevisores = revisoresIn;
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarRevisores() {
		try {
			revisoresIn = actorBean.listaUsuarios();
			revisorMap = new HashMap<>();

			for (Actor r : revisoresIn) {
				revisorMap.put(r.getId(), r.getNombreYApellido());
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String formatFecha(Date fecha) {
		if (fecha != null) {
			return ValidacionesUtil.formatearFecha(fecha);
		}
		return "";
	}

	public void obtenerRevisor() {
		try {
			if (StringUtils.isNotBlank(revisorName)) {
				Long id = Long.valueOf(revisorName);
				if (id != null) {
					actorRevisor = actorBean.buscarActor(id);
				}
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void obtenerRevisorReferencia() {
		try {
			Long id = ValidacionesUtil.obtenerIdFromMap(revisorMap, revisorName);
			actorRevisorConsulta = actorBean.buscarActor(id);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarResponsable() {
		try {
			responsableIn = actorBean.listaProcedencia(nombre, null, null, false);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void revisorUser() {
		revisorName = usuarioLogin.getActor().getNombreYApellido();
	}

	public void iniciarDialog() {
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
		nombre = "";
		oficinaValue = "";
		medioElectronico = false;
	}

	public void cargarComentarios(Documento documento) {
		try {
			revisiones = new ArrayList<>();
			List<RevisionDocumento> revisionList = documentoBean.comentariosDocumento(documento.getId());

			if (revisionList != null && !revisionList.isEmpty()) {
				for (RevisionDocumento rev : revisionList) {
					if (StringUtils.isNotBlank(rev.getComentario())) {
						revisiones.add(rev);
					}
				}
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarDocumentoReferencia() {
		try {

			if (fechaEmisionInicial != null && fechaEmisionFinal != null) {
				if (fechaEmisionFinal.after(fechaEmisionInicial)) {
					gestionBuscar = true;
					gestionInbox = false;
					obtenerRevisorReferencia();
					resultadoDocs = documentoBean.buscarDocumento(radicacionConsulta, fechaEmisionInicial,
							fechaEmisionFinal, actorProcedenciaConsulta, actorDestinoConsulta, asuntoConsulta, "",
							pruebaEntregaConsulta, actorRevisorConsulta, aprobado, revisado, pendiente, "");
				} else {
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"La fecha de emisión inicial debe ser menor a la fecha de emición final", null);
					FacesContext.getCurrentInstance().addMessage("validateDoc", message);

				}
			} else {
				obtenerRevisorReferencia();
				resultadoDocs = documentoBean.buscarDocumento(radicacionConsulta, fechaEmisionInicial,
						fechaEmisionFinal, actorProcedenciaConsulta, actorDestinoConsulta, asuntoConsulta, "",
						pruebaEntregaConsulta, actorRevisorConsulta, aprobado, revisado, pendiente, "");

			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String mostrarProcedencia(Documento doc) {
		List<Actor> procedencia = documentoBean.consultarProcedenciaDocumento(doc.getId());
		return ValidacionesUtil.listToCadena(procedencia);
	}

	public void cargarClases() {

		try {
			clasesDocumento = claseBean.listarClases();

		} catch (Exception e) {
			LOG.info(e);
		}
	}

	public void cargarOficinas() {
		try {
			oficinas = oficinaBean.cargarOficinas();

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public String onFlowProcess(FlowEvent event) {
		switch (event.getNewStep()) {
		case "serieTab":
			if (oficina != null) {
				llenarTablaSerie(oficina.getId());
				return event.getNewStep();
			} else {
				ValidacionesUtil.addMessage("Seleccione una oficina");
				return "oficinaTab";
			}
		case "subTab":
			if (serie != null) {
				llenarTablaSubSerie(serie.getId());
				return event.getNewStep();
			} else {
				ValidacionesUtil.addMessage("Seleccione una serie");
				return "serieTab";
			}
		case "tipoDocTab":
			if (subserie != null) {
				llenarTablaTipoDoc(subserie.getId());
				return event.getNewStep();
			} else {
				ValidacionesUtil.addMessage("Seleccione una subserie");
				return "subTab";
			}
		default:
			return event.getNewStep();
		}
	}

	public List<Oficina> llenarTablaOficina(Long id) {
		try {
			listaOficina = new ArrayList<>();
			listaOficina = oficinaBean.listarOficinas(id);
			return listaOficina;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public List<Serie> llenarTablaSerie(Long id) {
		try {
			listaSerie = new ArrayList<>();
			listaSerie = oficinaBean.listarSeries(id);
			return listaSerie;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public List<SubSerie> llenarTablaSubSerie(Long id) {
		try {
			listaSubSerie = new ArrayList<>();
			listaSubSerie = oficinaBean.listarSubSeries(id);
			return listaSubSerie;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public List<TipoDocumento> llenarTablaTipoDoc(Long id) {
		try {
			listaTipoDocumento = new ArrayList<>();
			listaTipoDocumento = oficinaBean.listarTipoDocumentos(id);
			return listaTipoDocumento;
		} catch (Exception ex) {
			LOG.info(ex);
		}
		return null;
	}

	public void crearTrd() {
		try {
//            Long idOficina = ValidacionesUtil.obtenerIdFromMap(oficinas, oficinaValue);
//            oficina = oficinaBean.buscarOficina(idOficina);
			Trd trd = new Trd();
			trd.setOficina(oficina);
			trd.setSerie(serie);
			trd.setSubserie(subserie);
			trd.setTipodocumento(tipoDoc);
//            trd.setMedioElectronico(medioElectronico);
			trds.add(trd);
			RequestContext.getCurrentInstance().execute("PF('wzTRD').loadStep(PF('wzTRD').cfg.steps[0], true);");
		} catch (Exception ex) {
			LOG.info(ex);
		}

	}

	public void cargarSeries() {
		try {
			series = new HashMap<>();
			Long idOficina = ValidacionesUtil.obtenerIdFromMap(oficinas, oficinaValue);
			oficina = oficinaBean.buscarOficina(idOficina);
			series = oficinaBean.cargarSeries(idOficina);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarSubSeries() {
		try {
			subseries = new HashMap<>();
			Long idserie = ValidacionesUtil.obtenerIdFromMap(series, serieValue);
			serie = oficinaBean.buscarSerie(idserie);
			subseries = oficinaBean.cargarSubSeries(idserie);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void cargarTiposDoc() {
		try {
			tiposDoc = new HashMap<>();
			Long id = ValidacionesUtil.obtenerIdFromMap(subseries, subserieValue);
			subserie = oficinaBean.buscarSubserie(id);
			tiposDoc = oficinaBean.cargarTipoDoc(id);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void buscarResponsableEdicion() {
		try {
			responsableIn = actorBean.listaUsuarios();
			listaResponsables = new HashMap<>();
			for (Actor actor : responsableIn) {
				listaResponsables.put(actor.getId(), actor.getNombreYApellido());

			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void obtenerResponsable() {
		try {
			if (responsable != null) {
				if (!responsable.getId().equals(responsableValue)) {
					responsableNuevo = actorBean.buscarActor(responsableValue);
				}
			} else {
				responsableNuevo = actorBean.buscarActor(responsableValue);
			}
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void selectDocumentoReferencia(Documento documentoReferencia) {
		if (documentoReferencia != null) {
			this.documentoReferencia = documentoReferencia;
			listaRef.add(documentoReferencia);
			limpiar();
		}
	}

	public void buscarRevisoresEdicion() {
		try {
			revisoresIn = actorBean.listaUsuarios();
			listaRevisores = revisoresIn;
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void quitarArchivo(ArchivoDocumento archivo) {
		listaArchivos.remove(archivo);
		listaArchivosEliminar.add(archivo);
	}

	public void quitarDocumentoReferencia(Documento documento) {
		listaRef.remove(documento);
		// ****Borrar registro de tabla DocumentosRespuesta
		try {
			Long idR = docRespuesta.consultaIdRespuesta(documento.getId(), idRespuesta);
			docRespuesta.eliminarRegRespuesta(idR);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void quitarTrd(Trd trd) {
		trds.remove(trd);
		trdsEliminar.add(trd);
	}

	public void cargarEmpresaCorreo() {
		try {
			empresaCorreo = new ArrayList<>();
			empresaCorreo.add(ConstantsMail.EMP_ROR);
			empresaCorreo.add(ConstantsMail.EMP_ROR_RECUPERACION);
			empresaCorreo.add(ConstantsMail.EMP_472);
			empresaCorreo.add(ConstantsMail.EMP_SERVIENTREGA);
			empresaCorreo.add(ConstantsMail.EMP_FUNCIONARIO);

		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void generarEtiqueta(Documento documento) {

		fechaEtiqueta = ValidacionesUtil.formatearFechaHora(documento.getFechaSistema());
		anexosEtiqueta = documento.getAnexos();
		radicacionEtiqueta = documento.getRadicacion();
		procedenciaEtiqueta = validarLista(documentoBean.consultarProcedenciaDocumento(documento.getId()));
		destinosEtiqueta = validarLista(documentoBean.consultarDestinoDocumento(documento.getId()));
		asuntoEtiqueta = documento.getAsunto();
		radicadorEtiqueta = documento.getRadicador().getNombreYApellido();

		if (documento.getRadicacion().contains("R")) {
			sticker = "PopupCenter('http://" + urlServer
					+ ":8080/Beesoft/html/documento/EtiquetaReimpresaR.xhtml', 'Etiqueta',450,250);";
		} else {
			sticker = "PopupCenter('http://" + urlServer
					+ ":8080/Beesoft/html/documento/EtiquetaReimpresionE.xhtml', 'Etiqueta',450,250);";
		}

	}

	public String validarLista(List<Actor> lista) {
		return ValidacionesUtil.listToCadena(lista);
	}

	public List<Actor> completeRevisores(String query) {
		List<Actor> filtrados = new ArrayList<>();

		for (Actor actor : listaRevisores) {
			if (StringUtils.startsWithIgnoreCase(actor.getNombreYApellido(), query)) {
				filtrados.add(actor);
			}
		}
		return filtrados;
	}

	public String volver() {
		if (usuarioLogin == null) {
			if (FacesContext.getCurrentInstance() != null) {
				HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
						.getSession(false);
				usuarioLogin = (Usuario) session.getAttribute("username");
			}
		}

		if (gestionBuscar) {
			limpiarEdicion();
			limpiarGestion();
			return "buscar";
		} else if (gestionInbox) {
			limpiarEdicion();
			limpiarGestion();
			buscarDocumentoInbox();
			return "volverInbox";
		}
		return "failed";
	}

	public void cerrarModal() {
		limpiar();
	}

	@PostConstruct
	public void inicializar() {
		listaRef = new ArrayList<>();
		listaRespuesta = new ArrayList<>();
		fechaEmisionInicial = null;
		fechaEmisionFinal = null;
		radicacionConsulta = "";
		asuntoConsulta = "";
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
		destinos = new ArrayList<>();
		procedencia = new ArrayList<>();
		resultadoDocs = new ArrayList<>();
		ResultadosOld = new ArrayList<>();
		aprobado = null;
		pendiente = null;
		revisado = null;
		urlServer = ValidacionesUtil.urlServer();
		cargarCiudadesQuindio();
		if (FacesContext.getCurrentInstance() != null) {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
					.getSession(false);
			usuarioLogin = (Usuario) session.getAttribute("username");
			if (usuarioLogin.getPerfil().getDescripcion().equals(ConstantsMail.ADMIN)) {
				modoAdmin = true;
			} else {
				modoAdmin = false;
			}
		}
	}

	@PreDestroy
	public void limpiar() {
		radicadoCambio = "";
		comentarioCambio = "";
		fechaEmisionInicial = null;
		fechaEmisionFinal = null;
		radicacionConsulta = "";
		asuntoConsulta = "";
		pruebaEntregaConsulta = "";
		destinoIn = new ArrayList<>();
		procedenciaIn = new ArrayList<>();
		destinos = new ArrayList<>();
		procedencia = new ArrayList<>();
		resultadoDocs = new ArrayList<>();
		ResultadosOld = new ArrayList<>();
		revisiones = new ArrayList<>();
		aprobado = null;
		revisado = null;
		pendiente = null;
		procedenciaName = "";
		destinoName = "";
		revisorName = "";
		actorRevisor = null;
		revisorComentario = null;
		actorDestinoConsulta = null;
		actorProcedenciaConsulta = null;
		aprobadoRevision = null;
		revisadoRevision = null;
		pendienteRevision = null;
		comentario = "";
		pruebaEntrega = "";
		pruebaEntregaList = null;
		revisoresTarget = new ArrayList<>();
		claseConsulta = "";
	}

	public void limpiarGestion() {
		radicacion = "";
		destinos = new ArrayList<>();
		procedencia = new ArrayList<>();
		asunto = "";
		observacion = "";
		guia = "";
		comentario = "";
		revisiones = new ArrayList<>();
		revisores = new ArrayList<>();
		responsable = null;
		oficina = null;
		serie = null;
		subserie = null;
		tipoDoc = null;
		oficinaValue = null;
		serieValue = null;
		subserieValue = null;
		tipoDocValue = null;
		trds = new ArrayList<>();
		clasesDocumento = new ArrayList<>();
		fechaEmision = null;
		fechaRecepcion = null;
		fechaControl = null;
		procedenciaName = "";
		destinoName = "";
		claseDocumento = null;
		comentarios = new StringBuilder();
		responsableName = "";
		destinoName = "";
		procedenciaName = "";
		comentario = "";
		revisorName = "";
		nombre = "";
		pruebaEntrega = "";
		pruebaEntregaList = null;
		revisoresTarget = new ArrayList<>();
	}

	public void limpiarEdicion() {
		radicacion = "";
		destinos = new ArrayList<>();
		procedencia = new ArrayList<>();
		asunto = "";
		observacion = "";
		guia = "";
		empresacorreo = "";
		empresaCorreoEnviado = "";
		documentoReferencia = null;
		listaRef = new ArrayList<>();
		listaRespuesta = new ArrayList<>();
		mensajero = "";
		pruebaEntrega = "";
		pruebaEntregaList = null;
		notificacionEntrega = "";
		ubicacionFisica = "";
		revisiones = new ArrayList<>();
		revisores = new ArrayList<>();
		revisoresTarget = new ArrayList<>();
		revisorEliminar = new ArrayList<>();
		revisoresNuevos = new ArrayList<>();
		responsable = null;
		responsableName = "";
		responsableNuevo = null;
		oficina = null;
		serie = null;
		subserie = null;
		tipoDoc = null;
		oficinaValue = null;
		serieValue = null;
		subserieValue = null;
		tipoDocValue = null;
		trds = new ArrayList<>();
		clasesDocumento = new ArrayList<>();
		fechaEmision = null;
		fechaRecepcion = null;
		fechaControl = null;
		procedenciaName = "";
		destinoName = "";
		claseDocumento = null;
		comentarios = new StringBuilder();
		responsableName = "";
		responsableValue = null;
		destinoName = "";
		procedenciaName = "";
		nombre = "";
		listaArchivos = new ArrayList<>();
		listaArchivosEliminar = new ArrayList<>();
		listaArchivosNuevos = new ArrayList<>();
		trdsEliminar = new ArrayList<>();
		trdsNuevo = new ArrayList<>();
		outTrd = null;
		revisoresTarget = new ArrayList<>();
		buscarDocumento();
		idRespuesta = null;
	}

	public String getInbox() {
		buscarDocumentoInbox();
		return inbox;
	}

	public void extraccion() {
		try {
			List<Documento> lista = documentoBean.buscarDocumento(letraExtraccion, fechaExtIni, fechaExtFin, null, null,
					null, null, null, null, null, null, null, "");
			List<Documento> pedazo = new ArrayList<>();

			List<List<Documento>> pedazos = new ArrayList<>();
			for (int i = 0; i < lista.size(); i++) {
				pedazo.add(lista.get(i));

				if ((i + 1) % 100 == 0) {
					pedazos.add(pedazo);
					pedazo = new ArrayList<>();
				} else if ((i + 1) == lista.size()) {
					pedazos.add(pedazo);
					pedazo = new ArrayList<>();
				}
			}

			for (List<Documento> otra : pedazos) {
				documentoBean.extraccionImagenes(otra, carpeta);

			}

		} catch (Exception ex) {
			Logger.getLogger(BuscarDocumentoAction.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void extraccionOld() {
		List<String> lista = new ArrayList<>();
		try {
			CsvReader reader = new CsvReader(
					new InputStreamReader(new FileInputStream(fileNameExtraccion), "ISO-8859-1"));
			reader.setDelimiter(',');
			while (reader.readRecord()) {
				String doc = reader.get(0);
				lista.add(doc);
			}
			System.out.println("estos son los numeros: " + lista.size());

		} catch (FileNotFoundException ex) {
			Logger.getLogger(BuscarDocumentoAction.class.getName()).log(Level.SEVERE, null, ex);

		} catch (IOException ex) {
			Logger.getLogger(BuscarDocumentoAction.class.getName()).log(Level.SEVERE, null, ex);
		}

		List<String> pedazo = new ArrayList<>();

		List<List<String>> pedazos = new ArrayList<>();
		for (int i = 0; i < lista.size(); i++) {
			pedazo.add(lista.get(i));

			if ((i + 1) % 100 == 0) {
				pedazos.add(pedazo);
				pedazo = new ArrayList<>();
			} else if ((i + 1) == lista.size()) {
				pedazos.add(pedazo);
				pedazo = new ArrayList<>();
			}
		}

		for (List<String> otra : pedazos) {
			documentoBean.extraccionImagenesOld(otra, carpeta);
		}

	}

	public void fileUploadExtraccion(FileUploadEvent event) throws Exception {
		String fileName = FilenameUtils.getName(event.getFile().getFileName());
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"El documento " + fileNameExtraccion + " se cargo", null);
		FacesContext.getCurrentInstance().addMessage("info", msg);
		String directory = System.getenv("JBOSS_HOME") + File.separator + "standalone" + File.separator + "tmp";
		fileNameExtraccion = directory + File.separator + fileName;
		File file = new File(directory);
		file.mkdir();
		file = copyFile(fileNameExtraccion, event.getFile().getInputstream());
	}

	public File copyFile(String fileAbsolutePath, InputStream in) throws Exception {

		try {
			OutputStream out = new FileOutputStream(new File(fileAbsolutePath));
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			in.close();
			return new File(fileAbsolutePath);
		} catch (IOException e) {
			LOG.info(e);
		}
		return null;
	}

	public void cargarCiudadesQuindio() {
		try {
			listaCiudades = utilidadesBean.cargarCiudades(24l);
		} catch (Exception e) {
			LOG.info(e);
		}
	}

	/// -------------------Getters--------------------------------------------------
	public String getRadicacion() {
		return radicacion;
	}

	public void setRadicacion(String radicacion) {
		this.radicacion = radicacion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getAnexos() {
		return anexos;
	}

	public void setAnexos(String anexos) {
		this.anexos = anexos;
	}

	public Boolean getAprobado() {
		return aprobado;
	}

	public void setAprobado(Boolean aprobado) {
		this.aprobado = aprobado;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Date getFechaControl() {
		return fechaControl;
	}

	public void setFechaControl(Date fechaControl) {
		this.fechaControl = fechaControl;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public Date getFechaSistema() {
		return fechaSistema;
	}

	public void setFechaSistema(Date fechaSistema) {
		this.fechaSistema = fechaSistema;
	}

	public String getGuia() {
		return guia;
	}

	public void setGuia(String guia) {
		this.guia = guia;
	}

	public TipoCorreo getTipoCorreo() {
		return tipoCorreo;
	}

	public void setTipoCorreo(TipoCorreo tipoCorreo) {
		this.tipoCorreo = tipoCorreo;
	}

	public String getNotificacionEntrega() {
		return notificacionEntrega;
	}

	public void setNotificacionEntrega(String notificacionEntrega) {
		this.notificacionEntrega = notificacionEntrega;
	}

	public Boolean getPendiente() {
		return pendiente;
	}

	public void setPendiente(Boolean pendiente) {
		this.pendiente = pendiente;
	}

	public String getPruebaEntrega() {
		return pruebaEntrega;
	}

	public void setPruebaEntrega(String pruebaEntrega) {
		this.pruebaEntrega = pruebaEntrega;
	}

	public String getUbicacionFisica() {
		return ubicacionFisica;
	}

	public void setUbicacionFisica(String ubicacionFisica) {
		this.ubicacionFisica = ubicacionFisica;
	}

	public String getClaseDocumento() {
		return claseDocumento;
	}

	public void setClaseDocumento(String claseDocumento) {
		this.claseDocumento = claseDocumento;
	}

	public List<Actor> getProcedencia() {
		return procedencia;
	}

	public void setProcedencia(List<Actor> procedencia) {
		this.procedencia = procedencia;
	}

	public Documento getDocumentoReferencia() {
		return documentoReferencia;
	}

	public void setDocumentoReferencia(Documento documentoReferencia) {
		this.documentoReferencia = documentoReferencia;
	}

	public String getEmpresacorreo() {
		return empresacorreo;
	}

	public void setEmpresacorreo(String empresacorreo) {
		this.empresacorreo = empresacorreo;
	}

	public List<Actor> getRevisores() {
		return revisores;
	}

	public void setRevisores(List<Actor> revisores) {
		this.revisores = revisores;
	}

	public Actor getResponsable() {
		return responsable;
	}

	public void setResponsable(Actor responsable) {
		this.responsable = responsable;
	}

	public Actor getRadicador() {
		return radicador;
	}

	public void setRadicador(Actor radicador) {
		this.radicador = radicador;
	}

	public Map<Long, String> getMapaClaseDocumento() {
		return mapaClaseDocumento;
	}

	public void setMapaClaseDocumento(Map<Long, String> mapaClaseDocumento) {
		this.mapaClaseDocumento = mapaClaseDocumento;
	}

	public List<Clase> getClasesDocumento() {
		return clasesDocumento;
	}

	public void setClasesDocumento(List<Clase> clasesDocumento) {
		this.clasesDocumento = clasesDocumento;
	}

	public List<Actor> getDestinoIn() {
		return destinoIn;
	}

	public void setDestinoIn(List<Actor> destinoIn) {
		this.destinoIn = destinoIn;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaEmisionInicial() {
		return fechaEmisionInicial;
	}

	public void setFechaEmisionInicial(Date fechaEmisionInicial) {
		this.fechaEmisionInicial = fechaEmisionInicial;
	}

	public Date getFechaEmisionFinal() {
		return fechaEmisionFinal;
	}

	public void setFechaEmisionFinal(Date fechaEmisionFinal) {
		this.fechaEmisionFinal = fechaEmisionFinal;
	}

	public Actor getActorProcedencia() {
		return actorProcedencia;
	}

	public void setActorProcedencia(Actor actorProcedencia) {
		this.actorProcedencia = actorProcedencia;
	}

	public Actor getActorDestino() {
		return actorDestino;
	}

	public void setActorDestino(Actor actorDestino) {
		this.actorDestino = actorDestino;
	}

	public List<Actor> getProcedenciaIn() {
		return procedenciaIn;
	}

	public void setProcedenciaIn(List<Actor> procedenciaIn) {
		this.procedenciaIn = procedenciaIn;
	}

	public List<Actor> getDestinos() {
		return destinos;
	}

	public void setDestinos(List<Actor> destinos) {
		this.destinos = destinos;
	}

	public List<Actor> getRevisoresIn() {
		return revisoresIn;
	}

	public void setRevisoresIn(List<Actor> revisoresIn) {
		this.revisoresIn = revisoresIn;
	}

	public List<Actor> getResponsableIn() {
		return responsableIn;
	}

	public void setResponsableIn(List<Actor> responsableIn) {
		this.responsableIn = responsableIn;
	}

	public Actor getActorRevisor() {
		return actorRevisor;
	}

	public void setActorRevisor(Actor actorRevisor) {
		this.actorRevisor = actorRevisor;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public String getResponsableName() {
		return responsableName;
	}

	public void setResponsableName(String responsableName) {
		this.responsableName = responsableName;
	}

	public String getOficinaValue() {
		return oficinaValue;
	}

	public void setOficinaValue(String oficina) {
		this.oficinaValue = oficina;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public String getSerieValue() {
		return serieValue;
	}

	public void setSerieValue(String serie) {
		this.serieValue = serie;
	}

	public Map<Long, String> getSeries() {
		return series;
	}

	public void setSeries(Map<Long, String> series) {
		this.series = series;
	}

	public String getSubserieValue() {
		return subserieValue;
	}

	public void setSubserieValue(String subserieValue) {
		this.subserieValue = subserieValue;
	}

	public Map<Long, String> getSubseries() {
		return subseries;
	}

	public void setSubseries(Map<Long, String> subseries) {
		this.subseries = subseries;
	}

	public String getTipoDocValue() {
		return tipoDocValue;
	}

	public void setTipoDocValue(String tipoDoc) {
		this.tipoDocValue = tipoDoc;
	}

	public Map<Long, String> getTiposDoc() {
		return tiposDoc;
	}

	public void setTiposDoc(Map<Long, String> tiposDoc) {
		this.tiposDoc = tiposDoc;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public SubSerie getSubserie() {
		return subserie;
	}

	public void setSubserie(SubSerie subserie) {
		this.subserie = subserie;
	}

	public TipoDocumento getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(TipoDocumento tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public List<Trd> getTrds() {
		return trds;
	}

	public void setTrds(List<Trd> trds) {
		this.trds = trds;
	}

	public String getDestinoName() {
		return destinoName;
	}

	public void setDestinoName(String destinoName) {
		this.destinoName = destinoName;
	}

	public String getProcedenciaName() {
		return procedenciaName;
	}

	public void setProcedenciaName(String procedenciaName) {
		this.procedenciaName = procedenciaName;
	}

	public List<Documento> getResultadoDocs() {
		return resultadoDocs;
	}

	public void setResultadoDocs(List<Documento> resultadoDocs) {
		this.resultadoDocs = resultadoDocs;
	}

	public Boolean getRevisado() {
		return revisado;
	}

	public void setRevisado(Boolean revisado) {
		this.revisado = revisado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public String getProcedenciaText() {
		return procedenciaText;
	}

	public void setProcedenciaText(String procedenciaText) {
		this.procedenciaText = procedenciaText;
	}

	public String getDestinosText() {
		return destinosText;
	}

	public void setDestinosText(String destinosText) {
		this.destinosText = destinosText;
	}

	public Actor getOutProcedencia() {
		return outProcedencia;
	}

	public void setOutProcedencia(Actor outProcedencia) {
		this.outProcedencia = outProcedencia;
	}

	public Actor getOutDestino() {
		return outDestino;
	}

	public void setOutDestino(Actor outDestino) {
		this.outDestino = outDestino;
	}

	public Actor getOutRevisor() {
		return outRevisor;
	}

	public void setOutRevisor(Actor outRevisor) {
		this.outRevisor = outRevisor;
	}

	public Actor getRevisorComentario() {
		return revisorComentario;
	}

	public void setRevisorComentario(Actor revisorComentario) {
		this.revisorComentario = revisorComentario;
	}

	public Actor getActorProcedenciaConsulta() {
		return actorProcedenciaConsulta;
	}

	public void setActorProcedenciaConsulta(Actor actorProcedenciaConsulta) {
		this.actorProcedenciaConsulta = actorProcedenciaConsulta;
	}

	public Actor getActorDestinoConsulta() {
		return actorDestinoConsulta;
	}

	public void setActorDestinoConsulta(Actor actorDestinoConsulta) {
		this.actorDestinoConsulta = actorDestinoConsulta;
	}

	public String getRadicacionConsulta() {
		return radicacionConsulta;
	}

	public void setRadicacionConsulta(String radicacionConsulta) {
		this.radicacionConsulta = radicacionConsulta;
	}

	public String getAsuntoConsulta() {
		return asuntoConsulta;
	}

	public void setAsuntoConsulta(String asuntoConsulta) {
		this.asuntoConsulta = asuntoConsulta;
	}

	public Trd getOutTrd() {
		return outTrd;
	}

	public void setOutTrd(Trd outTrd) {
		this.outTrd = outTrd;
	}

	public Boolean getAprobadoRevision() {
		return aprobadoRevision;
	}

	public void setAprobadoRevision(Boolean aprobadoRevision) {
		this.aprobadoRevision = aprobadoRevision;
	}

	public Boolean getRevisadoRevision() {
		return revisadoRevision;
	}

	public void setRevisadoRevision(Boolean revisadoRevision) {
		this.revisadoRevision = revisadoRevision;
	}

	public Boolean getPendienteRevision() {
		return pendienteRevision;
	}

	public void setPendienteRevision(Boolean pendienteRevision) {
		this.pendienteRevision = pendienteRevision;
	}

	public Boolean getShowAprobado() {
		return showAprobado;
	}

	public void setShowAprobado(Boolean showAprobado) {
		this.showAprobado = showAprobado;
	}

	public Boolean getShowRevisado() {
		return showRevisado;
	}

	public void setShowRevisado(Boolean showRevisado) {
		this.showRevisado = showRevisado;
	}

	public Boolean getShowPendiente() {
		return showPendiente;
	}

	public void setShowPendiente(Boolean showPendiente) {
		this.showPendiente = showPendiente;
	}

	public Map<Long, String> getRevisorMap() {
		return revisorMap;
	}

	public void setRevisorMap(Map<Long, String> revisorMap) {
		this.revisorMap = revisorMap;
	}

	public String getRevisorName() {
		return revisorName;
	}

	public void setRevisorName(String revisorName) {
		this.revisorName = revisorName;
	}

	public StringBuilder getComentarios() {
		return comentarios;
	}

	public void setComentarios(StringBuilder comentarios) {
		this.comentarios = comentarios;
	}

	public Boolean getShowRadicador() {
		return showRadicador;
	}

	public void setShowRadicador(Boolean showRadicador) {
		this.showRadicador = showRadicador;
	}

	public Boolean getShowComentario() {
		return showComentario;
	}

	public void setShowComentario(Boolean showComentario) {
		this.showComentario = showComentario;
	}

	public Usuario getUsuarioLogin() {
		return usuarioLogin;
	}

	public void setUsuarioLogin(Usuario usuarioLogin) {
		this.usuarioLogin = usuarioLogin;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public List<RevisionDocumento> getRevisiones() {
		return revisiones;
	}

	public void setRevisiones(List<RevisionDocumento> revisiones) {
		this.revisiones = revisiones;
	}

	public List<Documento> getDocumentosInbox() {
		return documentosInbox;
	}

	public void setDocumentosInbox(List<Documento> documentosInbox) {
		this.documentosInbox = documentosInbox;
	}

	public Boolean getGestionBuscar() {
		return gestionBuscar;
	}

	public void setGestionBuscar(Boolean gestionBuscar) {
		this.gestionBuscar = gestionBuscar;
	}

	public Boolean getGestionInbox() {
		return gestionInbox;
	}

	public void setGestionInbox(Boolean gestionInbox) {
		this.gestionInbox = gestionInbox;
	}

	public Boolean getResponsableRevisor() {
		return responsableRevisor;
	}

	public void setResponsableRevisor(Boolean responsableRevisor) {
		this.responsableRevisor = responsableRevisor;
	}

	public List<Documento> getListaRef() {
		return listaRef;
	}

	public void setListaRef(List<Documento> listaRef) {
		this.listaRef = listaRef;
	}

	public List<DocumentosRespuesta> getListaRespuesta() {
		return listaRespuesta;
	}

	public void setListaRespuesta(List<DocumentosRespuesta> listaRespuesta) {
		this.listaRespuesta = listaRespuesta;
	}

	public Map<Long, String> getListaResponsables() {
		return listaResponsables;
	}

	public void setListaResponsables(Map<Long, String> listaResponsables) {
		this.listaResponsables = listaResponsables;
	}

	public List<Actor> getRevisoresTarget() {
		return revisoresTarget;
	}

	public void setRevisoresTarget(List<Actor> revisoresTarget) {
		this.revisoresTarget = revisoresTarget;
	}

	public Actor getResponsableNuevo() {
		return responsableNuevo;
	}

	public void setResponsableNuevo(Actor responsableNuevo) {
		this.responsableNuevo = responsableNuevo;
	}

	public List<Actor> getListaRevisores() {
		return listaRevisores;
	}

	public void setListaRevisores(List<Actor> listaRevisores) {
		this.listaRevisores = listaRevisores;
	}

	public List<ArchivoDocumento> getListaArchivosNuevos() {
		return listaArchivosNuevos;
	}

	public void setListaArchivosNuevos(List<ArchivoDocumento> listaArchivosNuevos) {
		this.listaArchivosNuevos = listaArchivosNuevos;
	}

	public List<ArchivoDocumento> getListaArchivosEliminar() {
		return listaArchivosEliminar;
	}

	public void setListaArchivosEliminar(List<ArchivoDocumento> listaArchivosEliminar) {
		this.listaArchivosEliminar = listaArchivosEliminar;
	}

	public List<Trd> getTrdsEliminar() {
		return trdsEliminar;
	}

	public void setTrdsEliminar(List<Trd> trdsEliminar) {
		this.trdsEliminar = trdsEliminar;
	}

	public Actor getRevisorComentarioCorreo() {
		return revisorComentarioCorreo;
	}

	public void setRevisorComentarioCorreo(Actor revisorComentarioCorreo) {
		this.revisorComentarioCorreo = revisorComentarioCorreo;
	}

	public Map<Long, String> getMapaRevisores() {
		return mapaRevisores;
	}

	public void setMapaRevisores(Map<Long, String> mapaRevisores) {
		this.mapaRevisores = mapaRevisores;
	}

	public Actor getActorRevisorConsulta() {
		return actorRevisorConsulta;
	}

	public void setActorRevisorConsulta(Actor actorRevisorConsulta) {
		this.actorRevisorConsulta = actorRevisorConsulta;
	}

	public List<ArchivoDocumento> getListaArchivos() {
		return listaArchivos;
	}

	public void setListaArchivos(List<ArchivoDocumento> listaArchivos) {
		this.listaArchivos = listaArchivos;
	}

	public ArchivoDocumento getArchivoSelected() {
		return archivoSelected;
	}

	public void setArchivoSelected(ArchivoDocumento archivoSelected) {
		this.archivoSelected = archivoSelected;
	}

	public String getRevisorNameText() {
		return revisorNameText;
	}

	public void setRevisorNameText(String revisorNameText) {
		this.revisorNameText = revisorNameText;
	}

	public Boolean getIsEmpresa() {
		return isEmpresa;
	}

	public void setIsEmpresa(Boolean isEmpresa) {
		this.isEmpresa = isEmpresa;
	}

	public String getMensajero() {
		return mensajero;
	}

	public void setMensajero(String mensajero) {
		this.mensajero = mensajero;
	}

	public String getEmpresaCorreoEnviado() {
		return empresaCorreoEnviado;
	}

	public void setEmpresaCorreoEnviado(String empresaCorreoEnviado) {
		this.empresaCorreoEnviado = empresaCorreoEnviado;
	}

	public List<String> getEmpresaCorreo() {
		return empresaCorreo;
	}

	public void setEmpresaCorreo(List<String> empresaCorreo) {
		this.empresaCorreo = empresaCorreo;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getContractCod() {
		return contractCod;
	}

	public void setContractCod(String contractCod) {
		this.contractCod = contractCod;
	}

	public String getPqr() {
		return pqr;
	}

	public void setPqr(String pqr) {
		this.pqr = pqr;
	}

	public Documento getDocumentoCampos() {
		return documentoCampos;
	}

	public void setDocumentoCampos(Documento documentoCampos) {
		this.documentoCampos = documentoCampos;
	}

	public List<DocumentoDTO> getResultadosOld() {
		return ResultadosOld;
	}

	public void setResultadosOld(List<DocumentoDTO> ResultadosOld) {
		this.ResultadosOld = ResultadosOld;
	}

	public DocumentoDTO getDocumentoDto() {
		return documentoDto;
	}

	public void setDocumentoDto(DocumentoDTO documentoDto) {
		this.documentoDto = documentoDto;
	}

	public String getRadicadorOld() {
		return radicadorOld;
	}

	public void setRadicadorOld(String radicadorOld) {
		this.radicadorOld = radicadorOld;
	}

	public String getNombreRevisor() {
		return nombreRevisor;
	}

	public void setNombreRevisor(String nombreRevisor) {
		this.nombreRevisor = nombreRevisor;
	}

	public Long getResponsableValue() {
		return responsableValue;
	}

	public void setResponsableValue(Long responsableValue) {
		this.responsableValue = responsableValue;
	}

	public Boolean getMedioElectronico() {
		return medioElectronico;
	}

	public void setMedioElectronico(Boolean medioElectronico) {
		this.medioElectronico = medioElectronico;
	}

	public String getPruebaEntregaConsulta() {
		return pruebaEntregaConsulta;
	}

	public void setPruebaEntregaConsulta(String pruebaEntregaConsulta) {
		this.pruebaEntregaConsulta = pruebaEntregaConsulta;
	}

	public String[] getPruebaEntregaList() {
		return pruebaEntregaList;
	}

	public void setPruebaEntregaList(String[] pruebaEntregaList) {
		this.pruebaEntregaList = pruebaEntregaList;
	}

	public String getCarpeta() {
		return carpeta;
	}

	public void setCarpeta(String carpeta) {
		this.carpeta = carpeta;
	}

	public Date getFechaExtIni() {
		return fechaExtIni;
	}

	public void setFechaExtIni(Date fechaExtIni) {
		this.fechaExtIni = fechaExtIni;
	}

	public Date getFechaExtFin() {
		return fechaExtFin;
	}

	public void setFechaExtFin(Date fechaExtFin) {
		this.fechaExtFin = fechaExtFin;
	}

	public String getLetraExtraccion() {
		return letraExtraccion;
	}

	public void setLetraExtraccion(String letraExtraccion) {
		this.letraExtraccion = letraExtraccion;
	}

	public Long getClaseId() {
		return claseId;
	}

	public void setClaseId(Long claseId) {
		this.claseId = claseId;
	}

	public SubClase getSubClase() {
		return subClase;
	}

	public void setSubClase(SubClase subClase) {
		this.subClase = subClase;
	}

	public Long getIdsubClase() {
		return idsubClase;
	}

	public void setIdsubClase(Long idsubClase) {
		this.idsubClase = idsubClase;
	}

	public List<SubClase> getSubClases() {
		return subClases;
	}

	public void setSubClases(List<SubClase> subClases) {
		this.subClases = subClases;
	}

	public void cargarSubClases() {
		try {
			subClases = claseBean.buscarSubClases(claseId);
		} catch (Exception ex) {
			LOG.info(ex);
		}
	}

	public void onload() {
		cargarClasesDocumento();
	}

	public void actualizarFecha() {
//        claseDocumento = claseBean.buscarClasePorId(claseId);
//        subClase = claseBean.buscarSubClasePorId(this.idsubClase);
//        int contador = 0;
//        Date date = new Date();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        int limite = claseDocumento.getTiempoDias();
//        do {
//            if (calendar.get(Calendar.DAY_OF_WEEK) != 7 && calendar.get(Calendar.DAY_OF_WEEK) != 1) {
//                calendar.add(Calendar.DATE, 1);
//                contador++;
//            } else {
//                calendar.add(Calendar.DATE, 1);
//            }
//        } while (contador < limite);
//        fechaControl = calendar.getTime();
//        System.out.println(fechaControl);
	}

	public List<TrdOld> getTrdsOld() {
		return trdsOld;
	}

	public void setTrdsOld(List<TrdOld> trdsOld) {
		this.trdsOld = trdsOld;
	}

	public String getClaseOld() {
		return claseOld;
	}

	public void setClaseOld(String claseOld) {
		this.claseOld = claseOld;
	}

	public List<Oficina> getListaOficina() {
		return listaOficina;
	}

	public void setListaOficina(List<Oficina> listaOficina) {
		this.listaOficina = listaOficina;
	}

	public List<Oficina> getListaOficinaFiltered() {
		return listaOficinaFiltered;
	}

	public void setListaOficinaFiltered(List<Oficina> listaOficinaFiltered) {
		this.listaOficinaFiltered = listaOficinaFiltered;
	}

	public List<Serie> getListaSerie() {
		return listaSerie;
	}

	public void setListaSerie(List<Serie> listaSerie) {
		this.listaSerie = listaSerie;
	}

	public List<SubSerie> getListaSubSerie() {
		return listaSubSerie;
	}

	public void setListaSubSerie(List<SubSerie> listaSubSerie) {
		this.listaSubSerie = listaSubSerie;
	}

	public List<TipoDocumento> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}

	public List<Serie> getListaSerieFiltered() {
		return listaSerieFiltered;
	}

	public void setListaSerieFiltered(List<Serie> listaSerieFiltered) {
		this.listaSerieFiltered = listaSerieFiltered;
	}

	public List<SubSerie> getListaSubSerieFiltered() {
		return listaSubSerieFiltered;
	}

	public void setListaSubSerieFiltered(List<SubSerie> listaSubSerieFiltered) {
		this.listaSubSerieFiltered = listaSubSerieFiltered;
	}

	public List<TipoDocumento> getListaTipoDocumentoFiltered() {
		return listaTipoDocumentoFiltered;
	}

	public void setListaTipoDocumentoFiltered(List<TipoDocumento> listaTipoDocumentoFiltered) {
		this.listaTipoDocumentoFiltered = listaTipoDocumentoFiltered;
	}

	public String getRadicacionEtiqueta() {
		return radicacionEtiqueta;
	}

	public void setRadicacionEtiqueta(String radicacionEtiqueta) {
		this.radicacionEtiqueta = radicacionEtiqueta;
	}

	public String getProcedenciaEtiqueta() {
		return procedenciaEtiqueta;
	}

	public void setProcedenciaEtiqueta(String procedenciaEtiqueta) {
		this.procedenciaEtiqueta = procedenciaEtiqueta;
	}

	public String getDestinosEtiqueta() {
		return destinosEtiqueta;
	}

	public void setDestinosEtiqueta(String destinosEtiqueta) {
		this.destinosEtiqueta = destinosEtiqueta;
	}

	public String getAsuntoEtiqueta() {
		return asuntoEtiqueta;
	}

	public void setAsuntoEtiqueta(String asuntoEtiqueta) {
		this.asuntoEtiqueta = asuntoEtiqueta;
	}

	public String getRadicadorEtiqueta() {
		return radicadorEtiqueta;
	}

	public void setRadicadorEtiqueta(String radicadorEtiqueta) {
		this.radicadorEtiqueta = radicadorEtiqueta;
	}

	public String getFechaEtiqueta() {
		return fechaEtiqueta;
	}

	public void setFechaEtiqueta(String fechaEtiqueta) {
		this.fechaEtiqueta = fechaEtiqueta;
	}

	public String getUrlServer() {
		return urlServer;
	}

	public void setUrlServer(String urlServer) {
		this.urlServer = urlServer;
	}

	public String getDocumentoRespuesta() {
		return documentoRespuesta;
	}

	public void setDocumentoRespuesta(String documentoRespuesta) {
		this.documentoRespuesta = documentoRespuesta;
	}

	public Boolean getTieneRespuesta() {
		return tieneRespuesta;
	}

	public void setTieneRespuesta(Boolean tieneRespuesta) {
		this.tieneRespuesta = tieneRespuesta;
	}

	public String getRadicadoRespuesta() {
		return radicadoRespuesta;
	}

	public void setRadicadoRespuesta(String radicadoRespuesta) {
		this.radicadoRespuesta = radicadoRespuesta;
	}

	public String getClaseConsulta() {
		return claseConsulta;
	}

	public void setClaseConsulta(String claseConsulta) {
		this.claseConsulta = claseConsulta;
	}

	public String getRadicadoCambio() {
		return radicadoCambio;
	}

	public void setRadicadoCambio(String radicadoCambio) {
		this.radicadoCambio = radicadoCambio;
	}

	public String getComentarioCambio() {
		return comentarioCambio;
	}

	public void setComentarioCambio(String comentarioCambio) {
		this.comentarioCambio = comentarioCambio;
	}

	public Boolean getModoAdmin() {
		return modoAdmin;
	}

	public void setModoAdmin(Boolean modoAdmin) {
		this.modoAdmin = modoAdmin;
	}

	public String getSticker() {
		return sticker;
	}

	public void setSticker(String sticker) {
		this.sticker = sticker;
	}

	public String getAnexosEtiqueta() {
		return anexosEtiqueta;
	}

	public void setAnexosEtiqueta(String anexosEtiqueta) {
		this.anexosEtiqueta = anexosEtiqueta;
	}

	public Map<Long, String> getListaCiudades() {
		return listaCiudades;
	}

	public void setListaCiudades(Map<Long, String> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getCedulaDestino() {
		return cedulaDestino;
	}

	public void setCedulaDestino(String cedulaDestino) {
		this.cedulaDestino = cedulaDestino;
	}

	public Long getIdRespuesta() {
		return idRespuesta;
	}

	public void setIdRespuesta(Long idRespuesta) {
		this.idRespuesta = idRespuesta;
	}

}
