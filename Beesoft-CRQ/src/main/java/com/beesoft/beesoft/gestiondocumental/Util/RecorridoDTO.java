/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

import java.util.List;

/**
 *
 * @author Cristhian
 */
public class RecorridoDTO {

	private String numero;
	private String fecha;
	private String radicacion;
	private String procedencia;
	private String destino;
	private String clase;
	private String asunto;
	private String dependencia;
	private String anexos;
	private String mercurio;
	private String medio;
	private String referenciaCruzada;
	private String responsable;
	private String fechaenvio;
	private String fechalimite;
	private String fecharecepcion;
	private int diasVencimiento;
	private List<String> mails;

	// Trd attributes
	private String oficina;
	private String serie;
	private String subSerie;
	private String tipo;
	private String municipio;

	public RecorridoDTO() {
	}

	public RecorridoDTO(String numero, String fecha, String radicacion, String procedencia, String destino,
			String asunto, String dependencia, String anexos, String medio) {
		this.numero = numero;
		this.fecha = fecha;
		this.radicacion = radicacion;
		this.procedencia = procedencia;
		this.destino = destino;
		this.asunto = asunto;
		this.dependencia = dependencia;
		this.anexos = anexos;
		this.medio = medio;
	}

	public List<String> getMails() {
		return mails;
	}

	public void setMails(List<String> mails) {
		this.mails = mails;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getRadicacion() {
		return radicacion;
	}

	public void setRadicacion(String radicacion) {
		this.radicacion = radicacion;
	}

	public String getProcedencia() {
		return procedencia;
	}

	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getDependencia() {
		return dependencia;
	}

	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}

	public String getAnexos() {
		return anexos;
	}

	public void setAnexos(String anexos) {
		this.anexos = anexos;
	}

	public String getMercurio() {
		return mercurio;
	}

	public void setMercurio(String mercurio) {
		this.mercurio = mercurio;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getMedio() {
		return medio;
	}

	public void setMedio(String medio) {
		this.medio = medio;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getReferenciaCruzada() {
		return referenciaCruzada;
	}

	public void setReferenciaCruzada(String referenciaCruzada) {
		this.referenciaCruzada = referenciaCruzada;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getFechaenvio() {
		return fechaenvio;
	}

	public void setFechaenvio(String fechaenvio) {
		this.fechaenvio = fechaenvio;
	}

	public String getFechalimite() {
		return fechalimite;
	}

	public void setFechalimite(String fechalimite) {
		this.fechalimite = fechalimite;
	}

	public String getOficina() {
		return oficina;
	}

	public String getSerie() {
		return serie;
	}

	public String getSubSerie() {
		return subSerie;
	}

	public String getTipo() {
		return tipo;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setSubSerie(String subSerie) {
		this.subSerie = subSerie;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public int getDiasVencimiento() {
		return diasVencimiento;
	}

	public void setDiasVencimiento(int diasVencimiento) {
		this.diasVencimiento = diasVencimiento;
	}

	public String getFecharecepcion() {
		return fecharecepcion;
	}

	public void setFecharecepcion(String fecharecepcion) {
		this.fecharecepcion = fecharecepcion;
	}
	
}
