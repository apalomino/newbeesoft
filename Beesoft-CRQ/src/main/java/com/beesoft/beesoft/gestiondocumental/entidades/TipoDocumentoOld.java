package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * The persistent class for the tipodocumento database table.
 *
 */
@Entity
public class TipoDocumentoOld implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Boolean activo;

    private String descripcion;

    private SubSerieOld subserie;

    public TipoDocumentoOld() {
    }

    public TipoDocumentoOld(Long id, Boolean activo, String descripcion, SubSerieOld subserie) {
        this.id = id;
        this.activo = activo;
        this.descripcion = descripcion;
        this.subserie = subserie;
    }

    @Id
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ManyToOne
    public SubSerieOld getSubSerieOld() {
        return this.subserie;
    }

    public void setSubSerieOld(SubSerieOld subserie) {
        this.subserie = subserie;
    }

}
