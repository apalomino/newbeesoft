package com.beesoft.beesoft.gestiondocumental.entidades;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Type;

/**
 * The persistent class for the archivodocumento database table.
 *
 */
@Entity
public class ArchivoDocumento implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private byte[] archivo;

    private String nombreArchivo;

    public ArchivoDocumento() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(length = 100000)
    public byte[] getArchivo() {
        return this.archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getNombreArchivo() {
        return this.nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    @Override
    public boolean equals(Object obj) {
        return id.equals(((ArchivoDocumento) obj).getId());
    }

}
