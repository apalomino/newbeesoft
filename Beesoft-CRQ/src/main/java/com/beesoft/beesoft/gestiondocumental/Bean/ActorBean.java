/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Bean;

import com.beesoft.beesoft.gestiondocumental.Local.ActorLocal;
import com.beesoft.beesoft.gestiondocumental.Local.UtilidadesLocal;
import com.beesoft.beesoft.gestiondocumental.Util.ConstantsMail;
import com.beesoft.beesoft.gestiondocumental.Util.ValidacionesUtil;
import com.beesoft.beesoft.gestiondocumental.entidades.Actor;
import com.beesoft.beesoft.gestiondocumental.entidades.Ciudad;
import com.beesoft.beesoft.gestiondocumental.entidades.Oficina;
import com.beesoft.beesoft.gestiondocumental.entidades.TipoPersona;
import com.beesoft.beesoft.gestiondocumental.entidades.Usuario;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletContext;

import org.apache.axis2.classloader.ResourceHandle;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Cristhian Camilo Fernandez V. cfernandez@sumset.com
 * @param <MultipartFile>
 *
 */
@Stateless
public class ActorBean<MultipartFile> implements ActorLocal {

    private static final Log LOG = LogFactory.getLog(ActorBean.class);

    @PersistenceContext(unitName = "BeesoftPU")
    private EntityManager em;

    @EJB
    private UtilidadesLocal utilidades;
    
    @Override
    public Actor crearActor(String nombreYApellido, String cedula, String direccion, Ciudad ciudad,
            String email, TipoPersona tipopersona, String telefono, String telefonoExt,
            Actor Jefe, Actor empresa, String cargo, Oficina oficina, String firma) throws Exception {
        try {
            Actor actor = new Actor();
            actor.setCedula(cedula);
            actor.setNombreYApellido(nombreYApellido);
            actor.setDireccion1(direccion);
            actor.setCiudad(ciudad);
            actor.setTelefono(telefono);
            actor.setTelefonoExt(telefonoExt);
            actor.setTipopersona(tipopersona);
            actor.setEstado(true);
            actor.setEmail(email);
            actor.setFirma(firma);
            switch (tipopersona.getDescripcion()) {
                case ConstantsMail.TIPO_JEFE:
                    actor.setOficina(oficina);
                    oficina = em.find(Oficina.class, oficina.getId());
                    if (oficina.getJefe() != null) {
                        oficina.setJefeACargo(false);
                        oficina.setJefeEncargado(actor);
                    } else {
                        oficina.setJefe(actor);
                        oficina.setJefeACargo(true);
                    }
                    actor.setCargo(cargo);
                    break;
                case ConstantsMail.TIPO_EMPLEADO:
                    actor.setJefe(Jefe);
                    actor.setOficina(oficina);
                    actor.setCargo(cargo);
                    break;
            }
            em.persist(actor);
            return actor;
        } catch (Exception ex) {
            LOG.info(ex);
            throw new Exception(
                    "Esta persona esta registrada en la base de datos");
        }
    }

    @Override
    public void editarActor(Long id, String nombreYApellido, String cedula, String direccion1,
            Ciudad ciudad, String email, TipoPersona tipopersona, String telefono, String telefonoExt,
            Actor Jefe, Actor empresa, String cargo, Oficina oficina, String firma, Boolean estado) {

        Actor actor = em.find(Actor.class, id);
        if (actor != null) {
            actor.setCedula(cedula);
            actor.setNombreYApellido(nombreYApellido);
            actor.setDireccion1(direccion1);
            actor.setEmail(email);
            actor.setCiudad(ciudad);
            actor.setTelefono(telefono);
            actor.setTelefonoExt(telefonoExt);
            actor.setTipopersona(tipopersona);
            actor.setEstado(estado);
            actor.setEmpresa(empresa);
            actor.setFirma(firma);
            switch (tipopersona.getDescripcion()) {
                case ConstantsMail.TIPO_JEFE:
                    actor.setOficina(oficina);
                    oficina = em.find(Oficina.class, oficina.getId());
                    if (oficina.getJefe() != null) {
                        if (!oficina.getJefe().getId().equals(actor.getId())) {
                            oficina.setJefeEncargado(actor);
                            oficina.setJefeACargo(false);
                        }
                    } else {
                        oficina.setJefeACargo(true);
                        oficina.setJefe(actor);
                    }
                    actor.setCargo(cargo);
                    break;
                case ConstantsMail.TIPO_EMPLEADO:
                    actor.setJefe(Jefe);
                    actor.setOficina(oficina);
                    actor.setCargo(cargo);
                    break;
            }

            em.merge(actor);
        } else {
            try {
                throw new Exception(
                        "Esta persona no esta registrada en la base de datos");
            } catch (Exception ex) {
                Logger.getLogger(ActorBean.class.getName()).log(Level.SEVERE,
                        null, ex);
            }
        }
    }

    @Override
    public void cambiarEstadoActor(Long id, Boolean estado
    ) {
        Actor actor = em.find(Actor.class, id);
        if (actor != null) {
            actor.setEstado(estado);
            em.merge(actor);
            if (!estado) {
                List<Usuario> lista = em.createQuery("select u from Usuario u where u.actor.id=:id").setParameter("id", id).getResultList();
                if (lista != null && !lista.isEmpty()) {
                    for (Usuario u : lista) {
                        u.setEstado(estado);
                        em.merge(u);
                    }
                }
            }
        }
    }

    @Override
    public Map<Long, String> cargarListaActores() throws Exception {
        Map<Long, String> lista = new HashMap<Long, String>();
        List<Actor> listaActores = em.createQuery("select a from Actor a")
                .getResultList();

        if (listaActores != null && !listaActores.isEmpty()) {
            for (Actor a : listaActores) {
                lista.put(a.getId(), a.getNombreYApellido());
            }
            return lista;
        } else {
            throw new Exception("No se encuentran actores");
        }
    }

    @Override
    public Map<Long, String> cargarListaAdminRad() throws Exception {
        Map<Long, String> lista = new HashMap<Long, String>();
        List<Usuario> listaActores = em.createQuery("select u from Usuario u where u.perfil.descripcion=:admin OR u.perfil.descripcion=:rad").setParameter("admin", ConstantsMail.ADMIN).setParameter("rad", ConstantsMail.RADICADOR)
                .getResultList();

        if (listaActores != null && !listaActores.isEmpty()) {
            for (Usuario a : listaActores) {
                lista.put(a.getId(), a.getActor().getNombreYApellido());
            }
            return lista;
        } else {
            throw new Exception("No se encuentran actores");
        }
    }

    @Override
    public Actor buscarActor(Long id) throws Exception {
        if (id != null) {
            Actor actor = em.find(Actor.class, id);
            if (actor != null) {
                return actor;
            }
        } else {
            throw new Exception("No existe un actor con ese Id");
        }
        return null;
    }

    @Override
    public List<Actor> listaProcedencia(String nombreApellido, String cedula, TipoPersona tipo, Boolean busqueda) throws Exception {
        Query q = null;
        StringBuilder query = new StringBuilder("select a from Actor a");
        Map<String, Object> parameters = new HashMap<>();
        boolean iswhere = false;

        if (StringUtils.isNotBlank(cedula)) {
            String query0 = "a.cedula Like :cedula";
            iswhere = ValidacionesUtil.dinamicQueryString("cedula", "%" + cedula + "%", query0, iswhere, query, parameters);
        }

        if (StringUtils.isNotBlank(nombreApellido)) {
            String query1 = "UPPER(translate(a.nombreYApellido, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) Like UPPER(translate(:nombreApellido, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU'))";
            iswhere = ValidacionesUtil.dinamicQueryString("nombreApellido", "%" + nombreApellido + "%", query1, iswhere, query, parameters);
        }

        if (tipo != null) {
            String query2 = "a.tipopersona.id=:tipo";
            iswhere = ValidacionesUtil.dinamicQueryObject("tipo", tipo.getId(), query2, iswhere, query, parameters);
        }

        if (!busqueda) {
            if (iswhere) {
                query.append(" and a.estado=true");
            } else {
                query.append(" where a.estado=true");
            }
        }
        q = em.createQuery(query.toString());

        if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                q.setParameter(p.getKey(), p.getValue());
            }
        }

        List<Actor> procedencia = q.getResultList();

        if (procedencia != null && !procedencia.isEmpty()) {
            return procedencia;
        } else {
            throw new Exception("No se ha precargado la lista de Personal");
        }
    }

    @Override
    public List<Actor> listaProcedencia() throws Exception {
        List<Actor> procedencia = em.createQuery("select a from Actor a")
                .getResultList();
        if (procedencia != null && !procedencia.isEmpty()) {
            return procedencia;
        } else {
            throw new Exception("No se han precargado la lista de procedencias");
        }
    }

    @Override
    public List<Actor> listaEmpleados() throws Exception {

        List<Actor> lista = em.createQuery("select a from Actor a where (a.tipopersona.descripcion=:empleados OR a.tipopersona.descripcion=:jefe) and a.estado=true").setParameter("empleados", ConstantsMail.TIPO_EMPLEADO).setParameter("jefe", ConstantsMail.TIPO_JEFE).getResultList();
        List<Actor> usuarios = new ArrayList<>();
        if (lista != null && !lista.isEmpty()) {
            for (Actor actor : lista) {
                if (actor.getEstado()) {
                    usuarios.add(actor);
                }
            }
            return usuarios;
        } else {
            throw new Exception("No se han precargado la lista de empleados");
        }
    }

    @Override
    public List<Actor> listaEmpleadosSinUsuario() throws Exception {

        List<Actor> lista = em.createQuery("select a from Actor a where a.id not in (select u.actor.id from Usuario u) and (a.tipopersona.descripcion=:empleados OR a.tipopersona.descripcion=:jefe) and a.estado=true").setParameter("empleados", ConstantsMail.TIPO_EMPLEADO).setParameter("jefe", ConstantsMail.TIPO_JEFE).getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        } else {
            throw new Exception("No se han precargado la lista de empleados");
        }
    }

    @Override
    public List<Actor> listaUsuarios() throws Exception {

        List<Usuario> usuarios = em.createQuery("select u from Usuario u where u.estado=true").getResultList();

        if (usuarios != null && !usuarios.isEmpty()) {
            List<Actor> empleados = new ArrayList<Actor>();
            for (Usuario usuario : usuarios) {
                empleados.add(usuario.getActor());
            }
            return empleados;
        } else {
            throw new Exception("No se han precargado la lista de usuarios");
        }
    }

    @Override
    public List<Oficina> listaJefes() throws Exception {
        List<Oficina> oficinas = em.createQuery("select o from Oficina o where o.esretencion = true").getResultList();

        if (oficinas != null && !oficinas.isEmpty()) {
            return oficinas;
        } else {
            throw new Exception("No se han precargado la lista de jefes");
        }
    }

    @Override
    public void cambiarDireccionActor(Long id, String direccion, String nombre, Ciudad ciudad) {
        Actor actor = em.find(Actor.class, id);
        if (actor != null) {
            actor.setDireccion1(direccion);
            actor.setNombreYApellido(nombre);
            actor.setCiudad(ciudad);
            em.merge(actor);
        }
    }

    @Override
    public void editarActorDatosBasicos(Actor personaEditar) {
        Actor persona = em.find(Actor.class, personaEditar.getId());
        persona.setCedula(personaEditar.getCedula());
        persona.setNombreYApellido(personaEditar.getNombreYApellido());
        persona.setEmail(personaEditar.getEmail());
        persona.setTelefono(personaEditar.getTelefono());
        em.merge(persona);
    }

    @Override
    public List<Actor> cargarEmpresas() {
        List<Actor> lista = em.createQuery("select a from Actor a where a.tipopersona.descripcion=:tipo order by a.nombreYApellido").setParameter("tipo", ConstantsMail.TIPO_EMPRESA).getResultList();
        if (lista != null && !lista.isEmpty()) {
            return lista;
        }
        return null;
    }

    @Override

    public List<Actor> listarEmpleados(Long oficinaId) throws Exception {
        List<Actor> empleados = em.createQuery("select a from Actor a where a.oficina.id=:oficinaId and a.estado=true").setParameter("oficinaId", oficinaId).getResultList();

        if (empleados != null && !empleados.isEmpty()) {
            return empleados;
        } else {
            throw new Exception("No se han precargado la lista de empleados");
        }
    }

    @Override
    public Map<Long, String> listarActores() throws Exception {
        Map<Long, String> empleadosMap = new HashMap<>();
        List<Actor> empleados = em.createQuery("select a.actor from Usuario a where a.estado=true").getResultList();

        if (empleados != null && !empleados.isEmpty()) {
            for (Actor o : empleados) {
                empleadosMap.put(o.getId(), o.getNombreYApellido());
            }
            return empleadosMap;
        } else {
            throw new Exception("No se han cargado oficinas");
        }
    }

    @Override
    public void cambiarJefeOficina(Long idOficina, String cargo, Long idJefe, Long idJefeEncargado,
            Boolean jefeAcargo) {
        try {
            Oficina oficina = em.find(Oficina.class, idOficina);
            Actor jefe = em.find(Actor.class, idJefe);
            oficina.setJefe(jefe);
            if (idJefeEncargado != null) {
                Actor jefeEncargado = em.find(Actor.class, idJefeEncargado);
                oficina.setJefeEncargado(jefeEncargado);
            }
            oficina.setJefeACargo(jefeAcargo);
            oficina.setCargoJefe(cargo);
            em.merge(oficina);
        } catch (Exception ex) {
            Logger.getLogger(ActorBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String mostrarJefeOficinaTexto(Actor actor
    ) {
        StringBuffer jefe = new StringBuffer();
        if (actor != null) {
            jefe.append(actor.getNombreYApellido() + "\n " + actor.getOficina().getDescripcion());
            return jefe.toString();
        }
        return jefe.toString();
    }

    @Override
    public Actor jefeDeOficina(Long idOficina
    ) {
        List<Actor> jefes = (List<Actor>) em.createQuery("select a from Actor a where a.oficina.id=:oficina").setParameter("oficina", idOficina).getResultList();
        if (jefes != null & !jefes.isEmpty()) {
            Actor jefe = jefes.get(0);
            return jefe;
        }
        return null;
    }

    public Actor buscarJefeOficina(Long idOficina) throws Exception {
        List<Actor> empleados = em.createQuery("select a from Actor a where a.oficina.id = :idOficina and a.tipopersona.descripcion=:jefe and a.estado=true").setParameter("idOficina", idOficina).setParameter("jefe", ConstantsMail.TIPO_JEFE).getResultList();

        if (empleados != null && !empleados.isEmpty()) {
            return empleados.get(0);
        } else {
            throw new Exception("No se han precargado la lista de empleados");
        }
    }
	
}

