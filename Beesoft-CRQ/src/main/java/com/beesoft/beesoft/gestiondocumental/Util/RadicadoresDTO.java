/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.Util;

import com.beesoft.beesoft.gestiondocumental.entidades.Actor;

/**
 *
 * @author Cristhian
 */
public class RadicadoresDTO {

    private Actor radicador;
    private Long radicadosE;
    private Long radicadosR;
    private Long radicadosCJ;
    private Long radicadosDP;
    private Long radicadosFA;
    private Long radicadosI;
    private Long radicadosV;
    private Long radicadosM;
    private Long radicadosMA;
    private Long radicadosC;

    public RadicadoresDTO() {
    }

    public RadicadoresDTO(Actor radicador, Long radicadosE, Long radicadosR, Long radicadosCJ, Long radicadosDP, Long radicadosFA, Long radicadosI, Long radicadosV, Long radicadosM, Long radicadosMA, Long radicadosC) {
        this.radicador = radicador;
        this.radicadosE = radicadosE;
        this.radicadosR = radicadosR;
        this.radicadosCJ = radicadosCJ;
        this.radicadosDP = radicadosDP;
        this.radicadosFA = radicadosFA;
        this.radicadosI = radicadosI;
        this.radicadosV = radicadosV;
        this.radicadosM = radicadosM;
        this.radicadosMA = radicadosMA;
        this.radicadosC = radicadosC;
    }

    public Actor getRadicador() {
        return radicador;
    }

    public void setRadicador(Actor radicador) {
        this.radicador = radicador;
    }

    public Long getRadicadosE() {
        return radicadosE;
    }

    public void setRadicadosE(Long radicadosE) {
        this.radicadosE = radicadosE;
    }

    public Long getRadicadosR() {
        return radicadosR;
    }

    public void setRadicadosR(Long radicadosR) {
        this.radicadosR = radicadosR;
    }

    public Long getRadicadosCJ() {
        return radicadosCJ;
    }

    public void setRadicadosCJ(Long radicadosCJ) {
        this.radicadosCJ = radicadosCJ;
    }

    public Long getRadicadosDP() {
        return radicadosDP;
    }

    public void setRadicadosDP(Long radicadosDP) {
        this.radicadosDP = radicadosDP;
    }

    public Long getRadicadosFA() {
        return radicadosFA;
    }

    public void setRadicadosFA(Long radicadosFA) {
        this.radicadosFA = radicadosFA;
    }

    public Long getRadicadosI() {
        return radicadosI;
    }

    public void setRadicadosI(Long radicadosI) {
        this.radicadosI = radicadosI;
    }

    public Long getRadicadosV() {
        return radicadosV;
    }

    public void setRadicadosV(Long radicadosV) {
        this.radicadosV = radicadosV;
    }

    public Long getRadicadosM() {
        return radicadosM;
    }

    public void setRadicadosM(Long radicadosM) {
        this.radicadosM = radicadosM;
    }

    public Long getRadicadosMA() {
        return radicadosMA;
    }

    public void setRadicadosMA(Long radicadosMA) {
        this.radicadosMA = radicadosMA;
    }

    public Long getRadicadosC() {
        return radicadosC;
    }

    public void setRadicadosC(Long radicadosC) {
        this.radicadosC = radicadosC;
    }

    
}
