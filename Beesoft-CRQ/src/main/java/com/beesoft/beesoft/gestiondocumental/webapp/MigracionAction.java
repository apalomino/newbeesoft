/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beesoft.beesoft.gestiondocumental.webapp;

import com.beesoft.beesoft.gestiondocumental.Local.MigracionLocal;
import com.beesoft.beesoft.gestiondocumental.entidades.ArchivoDocumento;
import com.beesoft.beesoft.gestiondocumental.entidades.Documento;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author cfernandez@sumset.com
 */
@Named(value = "migracionAction")
@SessionScoped
public class MigracionAction implements Serializable {

    @EJB
    private MigracionLocal migracionBean;

    private String value;
    private String año;

    public void migrarActores() {
        migracionBean.migrarActores();
    }

    public void migrarTrd() {
        migracionBean.migrarTablasRetencion();
    }

    public void migrarDocumento() {

        List<String> lista = migracionBean.consultarIds();
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarDocumento(otra);
        }
    }

    public void migrarProcedencia() {

        List<String> lista = migracionBean.consultarIds();
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarProcedencia(otra);
        }
    }

    public void migrarDestinos() {

        List<String> lista = migracionBean.consultarIds();
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarDestinos(otra);
        }
    }

    public void migrarDestinosFaltantes() {

        List<String> lista = migracionBean.consultarDocumentosFaltantes(value);
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarDestinosFaltantes(otra, value);
        }
    }

    public void migrarRevisores() {

        List<String> lista = migracionBean.consultarIds();
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarRevisores(otra);
        }
    }

    public void migrarDocTrd() {

        List<String> lista = migracionBean.consultarIds();
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarDOcTrd(otra);
        }
    }

    public void migrarImagenes() {

        String path = "E:\\CRQ\\" + año;
        File directorio = new File(path);
        String[] ficheros = directorio.list();

        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < ficheros.length; i++) {
            pedazo.add(ficheros[i]);

            if ((i + 1) % 10 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == ficheros.length) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarImagenes(otra, path);
        }
    }

    public void cargarImagenes(FileUploadEvent event) {
        ArchivoDocumento archivo = new ArchivoDocumento();
        archivo.setArchivo(event.getFile().getContents());
        archivo.setNombreArchivo(event.getFile().getFileName());
        String nombre = event.getFile().getFileName();
        migracionBean.migrarImagenes(nombre, archivo);
    }

    public void migrarIncompletos() {
        List<Long> lista = migracionBean.consultarDocumentosIncompletos();
        List<Long> pedazo = new ArrayList<>();

        List<List<Long>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<Long> otra : pedazos) {
            migracionBean.migrarFaltantesIncompletos(otra);
        }

    }

    public void migrarRevisiones() {
        List<String> lista = migracionBean.consultarIdsRevisiones();
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarRevisiones(otra);
        }

    }

    public void migrarUsuarios() {
        List<String> lista = migracionBean.consultarCorreosUsuarios();
        List<String> pedazo = new ArrayList<>();

        List<List<String>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<String> otra : pedazos) {
            migracionBean.migrarUsuarios(otra);
        }

    }

    public void migrarReferencias() {

        List<Documento> lista = migracionBean.consultarReferenciasCruzadas(año);
        List<Documento> pedazo = new ArrayList<>();

        List<List<Documento>> pedazos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            pedazo.add(lista.get(i));

            if ((i + 1) % 100 == 0) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            } else if ((i + 1) == lista.size()) {
                pedazos.add(pedazo);
                pedazo = new ArrayList<>();
            }
        }

        for (List<Documento> otra : pedazos) {
            migracionBean.organizarReferenciasCruzadas(otra);
        }
    }

    public void migrarClases() {
        migracionBean.migrarClases();
    }

    public void completarTRD() {
        migracionBean.completarTRD();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

}
