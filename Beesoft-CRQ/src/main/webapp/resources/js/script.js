function permitirSoloNumeros(inputText, event) {
    var BACKSP_KEY = 8, TAB_KEY = 9, ENTER_KEY = 13, CTRL_KEY = 17, ALT_KEY = 18, ESC_KEY = 27, INS_KEY = 45, DEL_KEY = 46;
    var keyCode = event.keyCode ? event.keyCode : event.charCode;

    // Permite pegar
    if (event.ctrlKey && (event.keyCode == 86 || event.keyCode == 118)) {
        return true;
    }

    if (isNumber(keyCode) && event.ctrlKey == false && event.shiftKey == false
            && event.altKey == false) {
        return true;
    }

    // Ini, End, Pg Up, Pg Dn
    if (33 <= keyCode && keyCode <= 36) {
        return true;
    }

    // Arrow keys
    if (37 <= keyCode && keyCode <= 40) {
        return true;
    }

    // Numeric keyboard
    if (96 <= keyCode && keyCode <= 105) {
        return true;
    }

    // Special keys
    if (BACKSP_KEY == keyCode || TAB_KEY == keyCode || ENTER_KEY == keyCode
            || CTRL_KEY == keyCode || ALT_KEY == keyCode || ESC_KEY == keyCode
            || INS_KEY == keyCode || DEL_KEY == keyCode) {
        return true;
    }

    return false;
}


function permitirSoloLetras(inputText, event) {
    var BACKSP_KEY = 8, TAB_KEY = 9, ENTER_KEY = 13, CTRL_KEY = 17, ALT_KEY = 18, ESC_KEY = 27, INS_KEY = 45, DEL_KEY = 46;
    var keyCode = event.keyCode ? event.keyCode : event.charCode;

    //CombinaciÃ³n tecla shift + letra
    if (65 <= keyCode && keyCode <= 90) {
        return true;
    }

    // Numeric keyboard
    if (96 <= keyCode && keyCode <= 105) {
        return false;
    }

    if (!isNumber(keyCode) && event.ctrlKey == false && event.shiftKey == false
            && event.altKey == false) {
        return true;
    }


    // Ini, End, Pg Up, Pg Dn
    if (33 <= keyCode && keyCode <= 36) {
        return true;
    }

    // Arrow keys
    if (37 <= keyCode && keyCode <= 40) {
        return true;
    }

    // Special keys
    if (BACKSP_KEY == keyCode || TAB_KEY == keyCode || ENTER_KEY == keyCode
            || CTRL_KEY == keyCode || ALT_KEY == keyCode || ESC_KEY == keyCode
            || INS_KEY == keyCode || DEL_KEY == keyCode) {
        return true;
    }

    return false;
}

function isNumber(keyCode) {
    if ('0' <= String.fromCharCode(keyCode)
            && String.fromCharCode(keyCode) <= '9') {
        return true;
    }
    return false;
}


PrimeFaces.locales['es'] = {
    closeText: 'Cerrar',
    prevText: 'Anterior',
    nextText: 'Siguiente',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    weekHeader: 'Semana',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Sólo hora',
    timeText: 'Tiempo',
    hourText: 'Hora',
    minuteText: 'Minuto',
    secondText: 'Segundo',
    currentText: 'Fecha actual',
    ampm: true,
    month: 'Mes',
    week: 'Semana',
    day: 'Día',
    allDayText: 'Todo el día'
};
