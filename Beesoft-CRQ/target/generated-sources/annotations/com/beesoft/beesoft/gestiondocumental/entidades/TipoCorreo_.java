package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoCorreo.class)
public abstract class TipoCorreo_ {

	public static volatile SingularAttribute<TipoCorreo, String> descripcion;
	public static volatile SingularAttribute<TipoCorreo, Long> id;

}

