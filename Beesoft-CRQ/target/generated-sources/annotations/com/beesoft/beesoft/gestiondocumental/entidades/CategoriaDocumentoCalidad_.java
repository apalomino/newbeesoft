package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(CategoriaDocumentoCalidad.class)
public abstract class CategoriaDocumentoCalidad_ {

	public static volatile SingularAttribute<CategoriaDocumentoCalidad, String> descripcion;
	public static volatile SingularAttribute<CategoriaDocumentoCalidad, Long> id;

}

