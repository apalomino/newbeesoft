package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Ciudad.class)
public abstract class Ciudad_ {

	public static volatile SingularAttribute<Ciudad, String> descripcion;
	public static volatile SingularAttribute<Ciudad, Departamento> departamento;
	public static volatile SingularAttribute<Ciudad, Long> id;

}

