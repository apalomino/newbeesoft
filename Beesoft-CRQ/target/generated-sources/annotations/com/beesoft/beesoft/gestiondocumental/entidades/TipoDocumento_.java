package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoDocumento.class)
public abstract class TipoDocumento_ {

	public static volatile SingularAttribute<TipoDocumento, String> descripcion;
	public static volatile SingularAttribute<TipoDocumento, SubSerie> subserie;
	public static volatile SingularAttribute<TipoDocumento, Long> id;
	public static volatile SingularAttribute<TipoDocumento, Boolean> activo;

}

