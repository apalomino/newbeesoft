package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ArchivosAdjuntos.class)
public abstract class ArchivosAdjuntos_ {

	public static volatile SingularAttribute<ArchivosAdjuntos, String> nombreArchivo;
	public static volatile SingularAttribute<ArchivosAdjuntos, String> tipo;
	public static volatile SingularAttribute<ArchivosAdjuntos, byte[]> archivo;
	public static volatile SingularAttribute<ArchivosAdjuntos, Long> id;

}

