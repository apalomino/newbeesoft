package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Oficina.class)
public abstract class Oficina_ {

	public static volatile SingularAttribute<Oficina, String> descripcion;
	public static volatile SingularAttribute<Oficina, Boolean> esretencion;
	public static volatile SingularAttribute<Oficina, TipoRecorrido> tiporecorrido;
	public static volatile SingularAttribute<Oficina, String> codigo;
	public static volatile SingularAttribute<Oficina, Boolean> esinforme;
	public static volatile SingularAttribute<Oficina, String> cargoJefe;
	public static volatile SingularAttribute<Oficina, Long> id;
	public static volatile SingularAttribute<Oficina, Actor> jefe;
	public static volatile SingularAttribute<Oficina, Boolean> jefeACargo;
	public static volatile SingularAttribute<Oficina, Long> oficina_padre;
	public static volatile SingularAttribute<Oficina, Actor> jefeEncargado;
	public static volatile SingularAttribute<Oficina, Boolean> activo;

}

