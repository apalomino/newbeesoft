package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoDocumentoOld.class)
public abstract class TipoDocumentoOld_ {

	public static volatile SingularAttribute<TipoDocumentoOld, String> descripcion;
	public static volatile SingularAttribute<TipoDocumentoOld, Long> id;
	public static volatile SingularAttribute<TipoDocumentoOld, SubSerieOld> subSerieOld;
	public static volatile SingularAttribute<TipoDocumentoOld, Boolean> activo;

}

