package com.beesoft.beesoft.gestiondocumental.entidades;

import java.util.Date;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ComunicadosInternos.class)
public abstract class ComunicadosInternos_ {

	public static volatile SingularAttribute<ComunicadosInternos, Actor> redactor;
	public static volatile SingularAttribute<ComunicadosInternos, String> radicacionReenvio;
	public static volatile SingularAttribute<ComunicadosInternos, String> estado;
	public static volatile ListAttribute<ComunicadosInternos, ArchivosAdjuntos> adjuntosComunicados;
	public static volatile SingularAttribute<ComunicadosInternos, Actor> origen;
	public static volatile SingularAttribute<ComunicadosInternos, String> cargoActualEmisor;
	public static volatile SingularAttribute<ComunicadosInternos, String> radicacion;
	public static volatile SingularAttribute<ComunicadosInternos, String> comentario;
	public static volatile SingularAttribute<ComunicadosInternos, Date> fechaEnvio;
	public static volatile ListAttribute<ComunicadosInternos, DestinosComunicados> destinatarios;
	public static volatile SingularAttribute<ComunicadosInternos, Oficina> origenOficina;
	public static volatile SingularAttribute<ComunicadosInternos, String> asunto;
	public static volatile SingularAttribute<ComunicadosInternos, Date> fechaCreacion;
	public static volatile SingularAttribute<ComunicadosInternos, Boolean> leido;
	public static volatile SingularAttribute<ComunicadosInternos, Long> id;
	public static volatile SingularAttribute<ComunicadosInternos, String> mensaje;
	public static volatile SingularAttribute<ComunicadosInternos, Actor> emisor;

}

