package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(LetraRadicacion.class)
public abstract class LetraRadicacion_ {

	public static volatile SingularAttribute<LetraRadicacion, Long> id;
	public static volatile SingularAttribute<LetraRadicacion, String> letra;
	public static volatile SingularAttribute<LetraRadicacion, Long> numeroSecuencia;

}

