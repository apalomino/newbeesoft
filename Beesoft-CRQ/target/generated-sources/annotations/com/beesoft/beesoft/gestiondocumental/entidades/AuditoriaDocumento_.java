package com.beesoft.beesoft.gestiondocumental.entidades;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(AuditoriaDocumento.class)
public abstract class AuditoriaDocumento_ {

	public static volatile SingularAttribute<AuditoriaDocumento, String> accion;
	public static volatile SingularAttribute<AuditoriaDocumento, Date> fechaHora;
	public static volatile SingularAttribute<AuditoriaDocumento, Documento> documento;
	public static volatile SingularAttribute<AuditoriaDocumento, Actor> usuario;
	public static volatile SingularAttribute<AuditoriaDocumento, Long> id;

}

