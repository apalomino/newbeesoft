package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Trd.class)
public abstract class Trd_ {

	public static volatile SingularAttribute<Trd, SubSerie> subserie;
	public static volatile SingularAttribute<Trd, Oficina> oficina;
	public static volatile SingularAttribute<Trd, Boolean> medioElectronico;
	public static volatile SingularAttribute<Trd, Serie> serie;
	public static volatile SingularAttribute<Trd, Documento> documento;
	public static volatile SingularAttribute<Trd, Long> id;
	public static volatile SingularAttribute<Trd, TipoDocumento> tipodocumento;

}

