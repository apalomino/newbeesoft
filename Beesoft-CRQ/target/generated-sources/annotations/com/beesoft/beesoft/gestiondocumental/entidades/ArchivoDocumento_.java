package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ArchivoDocumento.class)
public abstract class ArchivoDocumento_ {

	public static volatile SingularAttribute<ArchivoDocumento, String> nombreArchivo;
	public static volatile SingularAttribute<ArchivoDocumento, byte[]> archivo;
	public static volatile SingularAttribute<ArchivoDocumento, Long> id;

}

