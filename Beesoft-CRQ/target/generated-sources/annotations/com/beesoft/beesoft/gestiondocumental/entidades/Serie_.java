package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Serie.class)
public abstract class Serie_ {

	public static volatile SingularAttribute<Serie, String> descripcion;
	public static volatile SingularAttribute<Serie, Boolean> esretencion;
	public static volatile SingularAttribute<Serie, String> codigo;
	public static volatile SingularAttribute<Serie, Oficina> oficina;
	public static volatile SingularAttribute<Serie, Long> id;
	public static volatile SingularAttribute<Serie, Boolean> activo;

}

