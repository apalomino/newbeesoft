package com.beesoft.beesoft.gestiondocumental.entidades;

import java.util.Date;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Documento.class)
public abstract class Documento_ {

	public static volatile SingularAttribute<Documento, Date> fechaSistema;
	public static volatile SingularAttribute<Documento, String> clasedocumento;
	public static volatile SingularAttribute<Documento, Boolean> prestado;
	public static volatile SingularAttribute<Documento, String> anexos;
	public static volatile SingularAttribute<Documento, String> notificacionEntrega;
	public static volatile SingularAttribute<Documento, Boolean> revisado;
	public static volatile SingularAttribute<Documento, String> empresaCorreoRecibido;
	public static volatile SingularAttribute<Documento, String> radicacionOriginal;
	public static volatile SingularAttribute<Documento, String> contratacionNumero;
	public static volatile ListAttribute<Documento, Documento> documentosReferencia;
	public static volatile SingularAttribute<Documento, String> pqr;
	public static volatile SingularAttribute<Documento, Boolean> migrado;
	public static volatile SingularAttribute<Documento, Boolean> medioElectronico;
	public static volatile ListAttribute<Documento, ArchivoDocumento> archivos;
	public static volatile SingularAttribute<Documento, String> asunto;
	public static volatile SingularAttribute<Documento, Date> fechaControl;
	public static volatile SingularAttribute<Documento, Long> id;
	public static volatile SingularAttribute<Documento, Documento> documentoRespuesta;
	public static volatile SingularAttribute<Documento, String> observacion;
	public static volatile ListAttribute<Documento, Actor> procedencia;
	public static volatile SingularAttribute<Documento, String> claseOld;
	public static volatile SingularAttribute<Documento, Actor> responsable;
	public static volatile SingularAttribute<Documento, String> empresacorreoEnviado;
	public static volatile SingularAttribute<Documento, String> comentarioCambio;
	public static volatile SingularAttribute<Documento, Boolean> aprobado;
	public static volatile SingularAttribute<Documento, Boolean> pendiente;
	public static volatile SingularAttribute<Documento, Date> fechaEmision;
	public static volatile SingularAttribute<Documento, String> pruebaEntrega;
	public static volatile SingularAttribute<Documento, String> radicacion;
	public static volatile SingularAttribute<Documento, Documento> documentoReferencia;
	public static volatile SingularAttribute<Documento, String> mensajero;
	public static volatile SingularAttribute<Documento, String> contratacionTipo;
	public static volatile SingularAttribute<Documento, String> guia;
	public static volatile SingularAttribute<Documento, String> ubicacionFisica;
	public static volatile SingularAttribute<Documento, Ciudad> ciudad;
	public static volatile ListAttribute<Documento, Actor> destinos;
	public static volatile SingularAttribute<Documento, SubClase> subClase;
	public static volatile ListAttribute<Documento, Actor> revisores;
	public static volatile SingularAttribute<Documento, Boolean> anulado;
	public static volatile SingularAttribute<Documento, Actor> radicador;
	public static volatile SingularAttribute<Documento, Boolean> activo;
	public static volatile SingularAttribute<Documento, Date> fechaRecepcion;

}

