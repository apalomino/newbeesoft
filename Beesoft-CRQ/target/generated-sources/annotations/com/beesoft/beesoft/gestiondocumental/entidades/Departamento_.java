package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Departamento.class)
public abstract class Departamento_ {

	public static volatile SingularAttribute<Departamento, String> descripcion;
	public static volatile SingularAttribute<Departamento, Long> id;
	public static volatile SingularAttribute<Departamento, Pais> pais;

}

