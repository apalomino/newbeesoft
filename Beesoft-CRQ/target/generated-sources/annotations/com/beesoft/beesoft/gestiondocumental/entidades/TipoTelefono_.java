package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoTelefono.class)
public abstract class TipoTelefono_ {

	public static volatile SingularAttribute<TipoTelefono, String> descripcion;
	public static volatile SingularAttribute<TipoTelefono, Long> id;

}

