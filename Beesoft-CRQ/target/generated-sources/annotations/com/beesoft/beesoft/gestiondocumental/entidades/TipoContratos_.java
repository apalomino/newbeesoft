package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoContratos.class)
public abstract class TipoContratos_ {

	public static volatile SingularAttribute<TipoContratos, String> descripcion;
	public static volatile SingularAttribute<TipoContratos, String> tipoDocumental;
	public static volatile SingularAttribute<TipoContratos, String> bloque;

}

