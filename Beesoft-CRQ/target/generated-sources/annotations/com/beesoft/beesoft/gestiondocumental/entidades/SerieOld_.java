package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SerieOld.class)
public abstract class SerieOld_ {

	public static volatile SingularAttribute<SerieOld, String> descripcion;
	public static volatile SingularAttribute<SerieOld, String> codigo;
	public static volatile SingularAttribute<SerieOld, OficinaOld> oficinaOld;
	public static volatile SingularAttribute<SerieOld, Long> id;
	public static volatile SingularAttribute<SerieOld, Boolean> activo;

}

