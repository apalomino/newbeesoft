package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(RadicacionCorreo.class)
public abstract class RadicacionCorreo_ {

	public static volatile SingularAttribute<RadicacionCorreo, Long> id;
	public static volatile SingularAttribute<RadicacionCorreo, String> letra;
	public static volatile SingularAttribute<RadicacionCorreo, Long> numeroSecuencia;

}

