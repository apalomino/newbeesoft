package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Actor.class)
public abstract class Actor_ {

	public static volatile SingularAttribute<Actor, Boolean> estado;
	public static volatile SingularAttribute<Actor, String> cedula;
	public static volatile SingularAttribute<Actor, Actor> jefe;
	public static volatile SingularAttribute<Actor, String> firma;
	public static volatile SingularAttribute<Actor, String> direccion1;
	public static volatile SingularAttribute<Actor, String> nombreYApellido;
	public static volatile SingularAttribute<Actor, Oficina> oficina;
	public static volatile SingularAttribute<Actor, Ciudad> ciudad;
	public static volatile SingularAttribute<Actor, Integer> hashCode;
	public static volatile SingularAttribute<Actor, String> telefonoExt;
	public static volatile SingularAttribute<Actor, Long> id;
	public static volatile SingularAttribute<Actor, TipoPersona> tipopersona;
	public static volatile SingularAttribute<Actor, String> telefono;
	public static volatile SingularAttribute<Actor, String> cargo;
	public static volatile SingularAttribute<Actor, Actor> empresa;
	public static volatile SingularAttribute<Actor, String> email;

}

