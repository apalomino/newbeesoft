package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SubSerie.class)
public abstract class SubSerie_ {

	public static volatile SingularAttribute<SubSerie, String> descripcion;
	public static volatile SingularAttribute<SubSerie, Boolean> esretencion;
	public static volatile SingularAttribute<SubSerie, String> codigo;
	public static volatile SingularAttribute<SubSerie, Serie> serie;
	public static volatile SingularAttribute<SubSerie, Long> id;
	public static volatile SingularAttribute<SubSerie, Boolean> activo;

}

