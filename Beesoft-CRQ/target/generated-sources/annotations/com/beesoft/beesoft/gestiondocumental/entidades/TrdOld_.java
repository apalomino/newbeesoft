package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TrdOld.class)
public abstract class TrdOld_ {

	public static volatile SingularAttribute<TrdOld, SerieOld> serieOld;
	public static volatile SingularAttribute<TrdOld, Boolean> medioElectronico;
	public static volatile SingularAttribute<TrdOld, OficinaOld> oficinaOld;
	public static volatile SingularAttribute<TrdOld, Documento> documento;
	public static volatile SingularAttribute<TrdOld, Long> id;
	public static volatile SingularAttribute<TrdOld, TipoDocumentoOld> tipodocumentoOld;
	public static volatile SingularAttribute<TrdOld, SubSerieOld> subSerieOld;

}

