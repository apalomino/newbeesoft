package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ClasesDocumento.class)
public abstract class ClasesDocumento_ {

	public static volatile SingularAttribute<ClasesDocumento, String> estado;
	public static volatile SingularAttribute<ClasesDocumento, Long> id;
	public static volatile SingularAttribute<ClasesDocumento, String> nombre;

}

