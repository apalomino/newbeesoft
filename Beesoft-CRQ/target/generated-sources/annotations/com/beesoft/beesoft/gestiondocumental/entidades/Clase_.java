package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Clase.class)
public abstract class Clase_ {

	public static volatile SingularAttribute<Clase, Integer> tiempoDias;
	public static volatile SingularAttribute<Clase, Long> id;
	public static volatile SingularAttribute<Clase, String> nombre;

}

