package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DocumentosRespuesta.class)
public abstract class DocumentosRespuesta_ {

	public static volatile SingularAttribute<DocumentosRespuesta, String> radicado;
	public static volatile SingularAttribute<DocumentosRespuesta, Long> id;
	public static volatile SingularAttribute<DocumentosRespuesta, Long> documentoRespuesta;
	public static volatile SingularAttribute<DocumentosRespuesta, Long> documentoOrigen;

}

