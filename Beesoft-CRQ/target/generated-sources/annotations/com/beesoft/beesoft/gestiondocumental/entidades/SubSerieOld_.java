package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SubSerieOld.class)
public abstract class SubSerieOld_ {

	public static volatile SingularAttribute<SubSerieOld, String> descripcion;
	public static volatile SingularAttribute<SubSerieOld, SerieOld> serieOld;
	public static volatile SingularAttribute<SubSerieOld, String> codigo;
	public static volatile SingularAttribute<SubSerieOld, Long> id;
	public static volatile SingularAttribute<SubSerieOld, Boolean> activo;

}

