package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Perfil.class)
public abstract class Perfil_ {

	public static volatile SingularAttribute<Perfil, String> descripcion;
	public static volatile SingularAttribute<Perfil, Boolean> estado;
	public static volatile SingularAttribute<Perfil, Long> id;

}

