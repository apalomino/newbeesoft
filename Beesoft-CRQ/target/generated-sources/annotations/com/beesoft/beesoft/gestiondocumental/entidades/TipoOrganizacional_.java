package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoOrganizacional.class)
public abstract class TipoOrganizacional_ {

	public static volatile SingularAttribute<TipoOrganizacional, String> descripcion;
	public static volatile SingularAttribute<TipoOrganizacional, Long> id;

}

