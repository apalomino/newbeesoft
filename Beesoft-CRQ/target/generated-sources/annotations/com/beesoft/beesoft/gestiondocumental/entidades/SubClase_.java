package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SubClase.class)
public abstract class SubClase_ {

	public static volatile SingularAttribute<SubClase, Integer> tiempoDias;
	public static volatile SingularAttribute<SubClase, Long> id;
	public static volatile SingularAttribute<SubClase, String> nombre;
	public static volatile SingularAttribute<SubClase, Clase> clase;

}

