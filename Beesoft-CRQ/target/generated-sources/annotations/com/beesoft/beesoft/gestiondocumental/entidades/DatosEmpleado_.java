package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DatosEmpleado.class)
public abstract class DatosEmpleado_ {

	public static volatile SingularAttribute<DatosEmpleado, String> area;
	public static volatile SingularAttribute<DatosEmpleado, String> apellido2;
	public static volatile SingularAttribute<DatosEmpleado, String> cedula;
	public static volatile SingularAttribute<DatosEmpleado, String> apellido1;
	public static volatile SingularAttribute<DatosEmpleado, String> ciudad;
	public static volatile SingularAttribute<DatosEmpleado, String> direcion;
	public static volatile SingularAttribute<DatosEmpleado, String> nombre2;
	public static volatile SingularAttribute<DatosEmpleado, String> telefono;
	public static volatile SingularAttribute<DatosEmpleado, String> nombre1;
	public static volatile SingularAttribute<DatosEmpleado, String> nivel;
	public static volatile SingularAttribute<DatosEmpleado, String> descod;

}

