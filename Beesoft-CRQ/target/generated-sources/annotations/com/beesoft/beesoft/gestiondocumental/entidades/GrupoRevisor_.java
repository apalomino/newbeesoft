package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(GrupoRevisor.class)
public abstract class GrupoRevisor_ {

	public static volatile SingularAttribute<GrupoRevisor, String> nombreGrupo;
	public static volatile SingularAttribute<GrupoRevisor, Long> id;
	public static volatile ListAttribute<GrupoRevisor, Actor> revisores;

}

