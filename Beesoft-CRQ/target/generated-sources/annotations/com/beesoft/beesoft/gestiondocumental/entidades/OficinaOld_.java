package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(OficinaOld.class)
public abstract class OficinaOld_ {

	public static volatile SingularAttribute<OficinaOld, String> descripcion;
	public static volatile SingularAttribute<OficinaOld, String> codigo;
	public static volatile SingularAttribute<OficinaOld, Long> id;
	public static volatile SingularAttribute<OficinaOld, Boolean> activo;

}

