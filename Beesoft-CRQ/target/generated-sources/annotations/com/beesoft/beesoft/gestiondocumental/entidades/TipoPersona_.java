package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoPersona.class)
public abstract class TipoPersona_ {

	public static volatile SingularAttribute<TipoPersona, String> descripcion;
	public static volatile SingularAttribute<TipoPersona, Long> id;

}

