package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Firma.class)
public abstract class Firma_ {

	public static volatile SingularAttribute<Firma, Long> id;
	public static volatile SingularAttribute<Firma, String> url;

}

