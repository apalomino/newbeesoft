package com.beesoft.beesoft.gestiondocumental.entidades;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DatosConfiguracion.class)
public abstract class DatosConfiguracion_ {

	public static volatile SingularAttribute<DatosConfiguracion, Boolean> estado;
	public static volatile SingularAttribute<DatosConfiguracion, String> authetication;
	public static volatile SingularAttribute<DatosConfiguracion, String> starttls;
	public static volatile SingularAttribute<DatosConfiguracion, String> emailEmisor;
	public static volatile SingularAttribute<DatosConfiguracion, String> dominioEmail;
	public static volatile SingularAttribute<DatosConfiguracion, String> password;
	public static volatile SingularAttribute<DatosConfiguracion, String> puerto;
	public static volatile SingularAttribute<DatosConfiguracion, String> nit;
	public static volatile SingularAttribute<DatosConfiguracion, String> host;
	public static volatile SingularAttribute<DatosConfiguracion, Date> fechaCron;
	public static volatile SingularAttribute<DatosConfiguracion, String> proveedor;
	public static volatile SingularAttribute<DatosConfiguracion, String> usuario;
	public static volatile SingularAttribute<DatosConfiguracion, Long> id;

}

