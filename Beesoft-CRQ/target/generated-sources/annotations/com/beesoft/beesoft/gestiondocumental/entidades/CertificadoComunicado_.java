package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(CertificadoComunicado.class)
public abstract class CertificadoComunicado_ {

	public static volatile SingularAttribute<CertificadoComunicado, String> certificadoNombre;
	public static volatile SingularAttribute<CertificadoComunicado, String> asunto;
	public static volatile SingularAttribute<CertificadoComunicado, Long> id;
	public static volatile SingularAttribute<CertificadoComunicado, String> fechaHoraRecibido;
	public static volatile SingularAttribute<CertificadoComunicado, String> fechaHoraEnvio;
	public static volatile SingularAttribute<CertificadoComunicado, String> correoDestino;
	public static volatile SingularAttribute<CertificadoComunicado, byte[]> certificado;

}

