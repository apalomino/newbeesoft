package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Permiso.class)
public abstract class Permiso_ {

	public static volatile SingularAttribute<Permiso, Menu> opcion;
	public static volatile SingularAttribute<Permiso, Boolean> permiso;
	public static volatile SingularAttribute<Permiso, Long> id;
	public static volatile SingularAttribute<Permiso, Perfil> perfil;

}

