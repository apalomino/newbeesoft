package com.beesoft.beesoft.gestiondocumental.entidades;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(RevisionDocumento.class)
public abstract class RevisionDocumento_ {

	public static volatile SingularAttribute<RevisionDocumento, Actor> actor;
	public static volatile SingularAttribute<RevisionDocumento, Boolean> envioCorreo;
	public static volatile SingularAttribute<RevisionDocumento, Boolean> estado;
	public static volatile SingularAttribute<RevisionDocumento, Date> fechaComentario;
	public static volatile SingularAttribute<RevisionDocumento, Boolean> aprobado;
	public static volatile SingularAttribute<RevisionDocumento, Boolean> pendiente;
	public static volatile SingularAttribute<RevisionDocumento, Documento> documento;
	public static volatile SingularAttribute<RevisionDocumento, Long> id;
	public static volatile SingularAttribute<RevisionDocumento, Boolean> revisado;
	public static volatile SingularAttribute<RevisionDocumento, String> comentario;

}

