package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TipoRecorrido.class)
public abstract class TipoRecorrido_ {

	public static volatile SingularAttribute<TipoRecorrido, String> descripcion;
	public static volatile SingularAttribute<TipoRecorrido, Long> id;

}

