package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DestinosComunicados.class)
public abstract class DestinosComunicados_ {

	public static volatile SingularAttribute<DestinosComunicados, Oficina> oficina;
	public static volatile SingularAttribute<DestinosComunicados, Long> id;
	public static volatile SingularAttribute<DestinosComunicados, Actor> destino;
	public static volatile SingularAttribute<DestinosComunicados, Boolean> copia;
	public static volatile SingularAttribute<DestinosComunicados, String> tipoDestino;
	public static volatile SingularAttribute<DestinosComunicados, String> cargoActualDestino;

}

