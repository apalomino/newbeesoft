package com.beesoft.beesoft.gestiondocumental.entidades;

import java.util.Date;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(NotificationMail.class)
public abstract class NotificationMail_ {

	public static volatile SingularAttribute<NotificationMail, Date> fechaEnvio;
	public static volatile ListAttribute<NotificationMail, Actor> destinatarios;
	public static volatile SingularAttribute<NotificationMail, String> moduloEnvio;
	public static volatile SingularAttribute<NotificationMail, Actor> fromAdress;
	public static volatile SingularAttribute<NotificationMail, String> asunto;
	public static volatile SingularAttribute<NotificationMail, Long> id;
	public static volatile SingularAttribute<NotificationMail, String> mensaje;

}

