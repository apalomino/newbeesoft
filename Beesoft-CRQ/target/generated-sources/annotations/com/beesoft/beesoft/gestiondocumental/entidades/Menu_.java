package com.beesoft.beesoft.gestiondocumental.entidades;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Menu.class)
public abstract class Menu_ {

	public static volatile SingularAttribute<Menu, String> descripcion;
	public static volatile SingularAttribute<Menu, String> etiqueta;
	public static volatile SingularAttribute<Menu, Menu> parent;
	public static volatile SingularAttribute<Menu, Boolean> subMenu;
	public static volatile SingularAttribute<Menu, Boolean> raiz;
	public static volatile SingularAttribute<Menu, Long> id;
	public static volatile SingularAttribute<Menu, Integer> orden;
	public static volatile SingularAttribute<Menu, Boolean> url;

}

