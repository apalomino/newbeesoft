package com.beesoft.beesoft.gestiondocumental.entidades;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DocumentoCalidad.class)
public abstract class DocumentoCalidad_ {

	public static volatile SingularAttribute<DocumentoCalidad, String> descripcion;
	public static volatile SingularAttribute<DocumentoCalidad, TipoOrganizacional> tipo;
	public static volatile SingularAttribute<DocumentoCalidad, Boolean> estado;
	public static volatile SingularAttribute<DocumentoCalidad, String> nombreDocumento;
	public static volatile SingularAttribute<DocumentoCalidad, Actor> usuarioCarga;
	public static volatile SingularAttribute<DocumentoCalidad, CategoriaDocumentoCalidad> categoriaDocumento;
	public static volatile SingularAttribute<DocumentoCalidad, String> titulo;
	public static volatile SingularAttribute<DocumentoCalidad, byte[]> documento;
	public static volatile SingularAttribute<DocumentoCalidad, byte[]> imagen;
	public static volatile SingularAttribute<DocumentoCalidad, Date> fechaCreacion;
	public static volatile SingularAttribute<DocumentoCalidad, Long> id;
	public static volatile SingularAttribute<DocumentoCalidad, String> nombreImagen;

}

